package com.example.ibraheem.mirva.util;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.ibraheem.mirva.Model.Advertisment;
import com.example.ibraheem.mirva.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Eng.Mhmod on 8/18/2017.
 */

public class ViewPagerAdapter extends PagerAdapter {

    private Context mContext;
    private ArrayList<Advertisment> mResources;

    public ViewPagerAdapter(Context mContext, ArrayList<Advertisment> mResources) {
        this.mContext = mContext;
        this.mResources = mResources;
    }

    @Override
    public int getCount() {
        return mResources.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.pager_item, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.img_pager_item);
        Picasso.with (mContext)
                .load (helperFun.getLanguage(mContext)=="ar"?"http://apps.mirva.qa/images/advertisement/Ar/"+mResources.get(position).getImage_ar():"http://apps.mirva.qa/images/advertisement/En/"+mResources.get(position).getImage_en())
                .placeholder (R.drawable.avater_image)
                .into (imageView);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent i = new Intent(Intent.ACTION_VIEW);
//                i.setData(Uri.parse(mResources.get(position).getUrl()));
//                mContext.startActivity(i);
            }
        });

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}