package com.example.ibraheem.mirva;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;


/**
 * Created by ibraheem on 3/3/2017.
 */

public class NetworkChangeReceiver extends BroadcastReceiver {
    private NetworkLostCallback mNetworkLostCallback;

    public void setNetworkLostCallback(NetworkLostCallback callback) {
        mNetworkLostCallback = callback;
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {

        if (mNetworkLostCallback != null) {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService (Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo ();

            if (UitilsFun.isNetworkAvailable (context)) {
                mNetworkLostCallback.onNetworkRegained ();
            } else {
                mNetworkLostCallback.onNetworkLost ();
            }
        }
    }

    public interface NetworkLostCallback {
        void onNetworkLost();

        void onNetworkRegained();
    }


}
