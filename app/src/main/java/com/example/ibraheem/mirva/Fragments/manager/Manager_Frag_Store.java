package com.example.ibraheem.mirva.Fragments.manager;


import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.example.ibraheem.mirva.Adapter.StoreItemAdapter;
import com.example.ibraheem.mirva.Interfaces.Communicator;
import com.example.ibraheem.mirva.Model.Response.ManagerResonse.Store;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.util.D;
import com.example.ibraheem.mirva.util.L;
import com.example.ibraheem.mirva.util.SessionManager;
import com.example.ibraheem.mirva.util.helperFun;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Manager_Frag_Store extends Fragment {


    final String TAG = "Store";
    ArrayList<com.example.ibraheem.mirva.Model.Response.ManagerResonse.StoreItem> mStoreItems;
    RecyclerView recyclerView;
    StoreItemAdapter mStoreItemAdapter;

    User user;
    D d;
    private ProgressBar mProgressBar;
    private SessionManager mSessionManager;
    private Context mContext;
    Communicator comm;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user = helperFun.getUser(getActivity());
        d = new D(getActivity());
        // d.ShowProgress (getString (R.string.loading));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mContext = getActivity();
        mSessionManager = new SessionManager(mContext);


        View view = inflater.inflate(R.layout.fragment_fragment_store, container, false);
        mStoreItems = new ArrayList<>();

        recyclerView = (RecyclerView) view.findViewById(R.id.store_item_recyler_view);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        mStoreItemAdapter = new StoreItemAdapter(getActivity(), mStoreItems);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayout.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);

        mStoreItemAdapter = new StoreItemAdapter(getActivity(), mStoreItems);
        recyclerView.setAdapter(mStoreItemAdapter);

        comm = (Communicator) getActivity();
        comm.selectVisibleToolbar(R.id.manager_toolbar_fragments);
        comm.setTitleToolbar(3);

        new FillStoreItemsAsyncTask().execute();

        return view;
    }

    /**
     * get sotre item
     */
    public class FillStoreItemsAsyncTask extends AsyncTask<Void, Void, Void> {

        String lang;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            lang = helperFun.getLanguage(getActivity().getApplicationContext());
        }

        @Override
        protected Void doInBackground(Void... params) {

            ApiInterface apiInterface = ApiUtils.getAPIService(getActivity());
            apiInterface.getStoreItems(user.getApiToken()).enqueue(new Callback<Store>() {
                @Override
                public void onResponse(Call<Store> call, Response<Store> response) {
                    //  d.hide ();
                    mProgressBar.setVisibility(View.INVISIBLE);
                    if (response.isSuccessful()) {

                        Store body = response.body();
                        mStoreItemAdapter.loadData(body);
                        mSessionManager.setStoreOrders(response.body(), lang);
                    } else {
                        d.ShowError("Error");
                        L.d(TAG, "errorBody : " + response.errorBody());

                    }

                }

                @Override
                public void onFailure(Call<Store> call, Throwable t) {
                    mProgressBar.setVisibility(View.INVISIBLE);
                    d.ShowErrorInternetConnection("");
                    // d.hide ();
                    if (mSessionManager.getStoreOrders(lang) != null) {

                        mStoreItemAdapter.loadData(mSessionManager.getStoreOrders(lang));

                    }

                }
            });


            return null;
        }
    }

}
