package com.example.ibraheem.mirva.CustomViews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

/**
 * Created by ibraheem on 5/10/2017.
 */

public class JFButton extends android.support.v7.widget.AppCompatButton {
    public JFButton(Context context) {
        super (context);
        init ();
    }

    public JFButton(Context context, AttributeSet attrs) {
        super (context, attrs);
        init ();
    }

    public JFButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super (context, attrs, defStyleAttr);
        init ();
    }

    private void init() {

        setTypeface (Typeface.createFromAsset (getContext ().getAssets (), "fonts/jf_flat_regular.ttf"));

     /*   setOnTouchListener (new OnTouchListener () {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction ()) {
                    case MotionEvent.ACTION_DOWN: {

                        getBackground ().setColorFilter (0x77000000, PorterDuff.Mode.SRC_ATOP);
                        invalidate ();
                        break;
                    }
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL: {

                        getBackground ().clearColorFilter ();

                        invalidate ();
                        break;
                    }
                }

                return false;
            }
        });*/
    }


}

