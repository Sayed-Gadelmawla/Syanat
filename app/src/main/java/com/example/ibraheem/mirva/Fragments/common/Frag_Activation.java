package com.example.ibraheem.mirva.Fragments.common;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.example.ibraheem.mirva.Activities.ActivateAccountActivity;
import com.example.ibraheem.mirva.Const.FragConst;
import com.example.ibraheem.mirva.CustomViews.JFButton;
import com.example.ibraheem.mirva.CustomViews.JFEditText;
import com.example.ibraheem.mirva.Model.Response.ErrorResponse.ErrorUtils;
import com.example.ibraheem.mirva.Model.Response.helperRes;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.UitilsFun;
import com.example.ibraheem.mirva.util.D;
import com.example.ibraheem.mirva.util.SessionManager;
import com.example.ibraheem.mirva.util.SharedPrefs;
import com.example.ibraheem.mirva.util.V;
import com.example.ibraheem.mirva.util.helperFun;
import com.google.gson.JsonObject;

import java.io.IOException;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Frag_Activation.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Frag_Activation#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Frag_Activation extends BottomSheetDialogFragment implements View.OnClickListener {

    private static final String User_email = "User_email";
    private static final String Service_ID = "Service_ID";

    private String U_email;
    private int ID_Service;
    String language;
    private JFEditText mPolicyEditText, user_phone_number;
    private ApiInterface mAPIService;

    private CheckBox mPolicyCheckBox;
    private SessionManager mSessionManager;
    JFButton btn_activate;
    V vd;
    D dilog;
    private OnFragmentInteractionListener mListener;

    public Frag_Activation() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param email Parameter 1.
     * @return A new instance of fragment Frag_Activation.
     */
    // TODO: Rename and change types and number of parameters
    public static Frag_Activation newInstance(String email , int ServiceID) {
        Frag_Activation fragment = new Frag_Activation();
        Bundle args = new Bundle();
        args.putString(User_email, email);
        args.putInt(Service_ID, ServiceID);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            U_email = getArguments().getString(User_email);
            ID_Service = getArguments().getInt(Service_ID);

        }
        mSessionManager = new SessionManager(getActivity());
        mAPIService = ApiUtils.getAPIService(getContext());

        language = helperFun.getLanguage(getActivity());
        ApiInterface apiInterface = ApiUtils.getAPIService(getActivity());
        apiInterface.getPolicy(language).enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    JsonObject object = response.body();
                    //mPolicyEditText.setText(object.get("policy").toString());
                    mSessionManager.setPolicy(object.get("policy").toString());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
            }
        });
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        //getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();
        if (dialog != null) {
            //dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            this.getDialog().getWindow().setBackgroundDrawableResource(R.drawable.radius_bottom_frag);

            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(
                    Color.TRANSPARENT));
            dialog.getWindow().setGravity(Gravity.BOTTOM);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activation_account, container);

        WindowManager.LayoutParams wmlp = getDialog().getWindow().getAttributes();
        //wmlp.gravity = Gravity.FILL_HORIZONTAL;
        //getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        user_phone_number = (JFEditText) view.findViewById(R.id.user_phone_number);
        mPolicyEditText = (JFEditText) view.findViewById(R.id.policy_et);
        mPolicyCheckBox = (CheckBox) view.findViewById(R.id.policy_check_box);
        btn_activate = (JFButton) view.findViewById(R.id.btn_activate);

        mPolicyEditText.setText(mSessionManager.getPolicy());
        mPolicyEditText.setEnabled(false);
        btn_activate.setOnClickListener(this);
        vd = new V(getActivity());

        return view;

       // return inflater.inflate(R.layout.activation_account, container, false);
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.btn_activate:
                if (!mPolicyCheckBox.isChecked()) {
                    Toast toast  = Toast.makeText(getContext(), getResources().getString(R.string.accept_terms), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER , 0 , 0);
                    toast.show();

                    return ;
                }else if(user_phone_number.getText().toString().isEmpty()){
                    user_phone_number.setError(getResources().getString(R.string.phone_number_error));
                    user_phone_number.requestFocus();
                }else if (!Patterns.PHONE.matcher(user_phone_number.getText().toString()).matches()){
                    user_phone_number.setError(getResources().getString(R.string.phone_format_error));
                }else if(!UitilsFun.isNetworkAvailable(getContext())){
                    Toast toast  = Toast.makeText(getContext(), getResources().getString(R.string.err_quality_internet), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER , 0 , 0);
                    toast.show();
                }else {
                    activateAccount();
                }
                break;
        }
    }

    private void activateAccount() {
        dilog = new D(getActivity());
        dilog.ShowProgress(getString(R.string.processing_act));
        mAPIService.sendActiviation (language, U_email , user_phone_number.getText().toString()).enqueue (new Callback<JsonObject> () {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                dilog.hide ();

                if (response.isSuccessful ()) {

                    Intent intent = new Intent (getContext(), ActivateAccountActivity.class);
                    intent.putExtra ("mobile", user_phone_number.getText().toString());
                    intent.putExtra(FragConst.SERVICE_ID,ID_Service );
                    intent.setFlags (Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    getActivity().overridePendingTransition (R.anim.fadein, R.anim.fadeout);
                    startActivity (intent);
                    getDialog().dismiss();

                } else {

                    ErrorUtils errorMessage = helperRes.getErrorMessage(response.errorBody());
                    String error = null ;
                    if (!String.valueOf(errorMessage.getError().getMobile()).isEmpty())
                        error = String.valueOf(errorMessage.getError().getMobile());
                    else
                        error = String.valueOf(errorMessage.getError().getMessage());

                    new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText (getString (R.string.error))
                            .setContentText (error)
                            .show ();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                dilog.ShowErrorInternetConnection ("");

            }
        });


    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
