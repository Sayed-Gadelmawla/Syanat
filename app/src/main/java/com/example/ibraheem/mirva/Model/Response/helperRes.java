package com.example.ibraheem.mirva.Model.Response;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.ibraheem.mirva.Model.Response.ErrorResponse.Error;
import com.example.ibraheem.mirva.Model.Response.ErrorResponse.ErrorUtils;
import com.example.ibraheem.mirva.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import java.io.IOException;

import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.ResponseBody;

/**
 * Created by ibraheem on 18/06/2017.
 */

public class helperRes {
    public static final String TAG = helperRes.class.getSimpleName ();

    public static ErrorUtils getErrorMessage(ResponseBody responseBody) {
        Gson gson = new GsonBuilder ().create ();
        try {
            return gson.fromJson (responseBody.string (), ErrorUtils.class);
        } catch (IOException e) {
            e.printStackTrace ();
        }
        return null;
    }

    public static Success getSuccessMessage(JsonObject responseBody) {
        Gson gson = new GsonBuilder ().create ();
        return gson.fromJson (responseBody, Success.class);

    }

    public static void showMessageError(Context context, ResponseBody responseBody) {
        Gson gson = new Gson ();
        Error error;
        Log.d (TAG, "showMessageError: " + responseBody.toString ());
        try {
            error = gson.fromJson (responseBody.string (), Error.class);
            SweetAlertDialog dialog = new SweetAlertDialog (context, SweetAlertDialog.ERROR_TYPE);
            dialog.setTitleText (context.getString (R.string.error));
            //  dialog.setContentText (error.getErrorMessage ());
            dialog.show ();

        } catch (IOException e) {
            e.printStackTrace ();
        }
    }

    public static void showMessageSuccess(final Context context, JsonObject responseBody, final Class aClass) {
        Gson gson = new GsonBuilder ().create ();
        Success success;

        success = gson.fromJson (responseBody, Success.class);
        SweetAlertDialog dialog = new SweetAlertDialog (context, SweetAlertDialog.SUCCESS_TYPE);
        dialog.setTitleText ("");
        dialog.setContentText (success.getMessage ());
        dialog.setCancelable(true);
        dialog.show ();
        Button confirm_button = (Button) dialog.findViewById (R.id.confirm_button);
        if (aClass != null) {
            confirm_button.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    context.startActivity (new Intent (context, aClass));
                }
            });

        } else {
            confirm_button.setVisibility (View.GONE);
        }


    }

    public static void showMessageSuccess(final Context context, String responseBody, final Class aClass) {

        SweetAlertDialog dialog = new SweetAlertDialog (context, SweetAlertDialog.SUCCESS_TYPE);
        dialog.setTitleText (context.getString (R.string.sucess));
        dialog.setContentText (responseBody);
        dialog.show ();
        Button confirm_button = (Button) dialog.findViewById (R.id.confirm_button);
        if (aClass != null) {
            confirm_button.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    context.startActivity (new Intent (context, aClass));
                }
            });

        } else {
            confirm_button.setVisibility (View.GONE);
        }


    }


}
