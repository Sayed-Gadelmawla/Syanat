package com.example.ibraheem.mirva.Adapter;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ibraheem.mirva.CustomViews.JFEditText;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.Model.Response.Order.Technical.Requirement;
import com.example.ibraheem.mirva.R;

import java.util.ArrayList;

/**
 * Created by ibraheem on 03/07/2017.
 */

public class RequirmentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context mContext;
    ArrayList<Requirement> requirements, selectedRequirments;
    int[] available;

    public RequirmentAdapter(Context mContext, ArrayList<Requirement> requirements) {
        this.mContext = mContext;
        selectedRequirments = new ArrayList<> ();
        this.requirements = requirements;
    }

    public ArrayList<Requirement> getSelectedRequirments() {
        return selectedRequirments;

    }

    @Override
    public int getItemViewType(int position) {
//        Log.d ("RequirmentAdapter", "getItemViewType: " + requirements.get (position));

        //  return (requirements.get (position).getQuantity () == null && requirements.get (position).getRemaining () != null) ? 1 : 0;
        return 1;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams (ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        View inflate;
        switch (viewType) {

            case 0:
                inflate = LayoutInflater.from (parent.getContext ()).inflate (R.layout.item_requirment_selected, null, false);
                inflate.setLayoutParams (lp);

                return new MyViewHolderRequired (inflate);

            case 1:
                inflate = LayoutInflater.from (parent.getContext ()).inflate (R.layout.item_requirment, null, false);
                inflate.setLayoutParams (lp);
                return new MyViewHolder (inflate);

        }
        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final Requirement requirement = requirements.get (position);
        Log.d ("RequirmentAdapter", "" + requirement);


        switch (holder.getItemViewType ()) {

            case 0:
                MyViewHolderRequired myViewHolderRequired = (MyViewHolderRequired) holder;
                myViewHolderRequired.requirment_required.setText (requirement.getQuantity ());
                myViewHolderRequired.requirmnet_name.setText (requirement.getRequirement ());
                break;


            case 1:
                if (requirement.getRemaining () != null) {
                    available[position] = Integer.parseInt (requirement.getRemaining ());

                }
                final MyViewHolder viewHolderNewOrder = (MyViewHolder) holder;

                viewHolderNewOrder.requirmnet_name.setText (requirement.getRequirement ());
                viewHolderNewOrder.requirment_avilable.setText (requirement.getRemaining ());
                viewHolderNewOrder.requirment_required.addTextChangedListener (new TextWatcher () {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        if (viewHolderNewOrder.requirment_required.getText ().toString ().isEmpty ()) {
                            //requirement.setQuantity ("0");

                            return;
                        }
                        int required = Integer.parseInt (viewHolderNewOrder.requirment_required.getText ().toString ());
                        Log.d ("available", "" + available[position]);
                        Log.d ("required", "" + required);

                        if (required < 0 || required > Integer.parseInt (requirement.getRemaining())) {

                            Log.d ("RequirmentAdapter ", "afterTextChanged: available  " + available[position]);
                            Log.d ("RequirmentAdapter ", "afterTextChanged: required" + required);

                            viewHolderNewOrder.requirment_required.setError (mContext.getString (R.string.err_insert_number));


                        } else {

                            Requirement requirement1 = new Requirement ();
                            requirement1.setId (requirement.getId ());
                            requirement1.setRequirement (requirement.getRequirement ());
                            requirement1.setQuantity (viewHolderNewOrder.requirment_required.getText ().toString ());

                            Log.d ("RequirmentAdapter", "selectedRequirments.contains (requirement1): " + requirement1);


                            if (!selectedRequirments.contains (requirement1)) {

                                selectedRequirments.add (requirement1);


                            } else {

                                selectedRequirments.remove (requirement1);
                                selectedRequirments.add (requirement1);
                            }


                        }
                    }
                });

                break;


        }


    }

    @Override
    public int getItemCount() {
        this.available = new int[this.requirements.size()];
        return requirements != null ? requirements.size () : 0;
    }

    public void loadData(ArrayList<Requirement> dataFiltered) {

        requirements = dataFiltered;
        notifyDataSetChanged ();

    }

    public void clearData() {

        selectedRequirments.clear ();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        JFEditText requirment_required;
        JFTextView requirment_avilable, requirmnet_name;

        public MyViewHolder(View itemView) {
            super (itemView);
            requirment_required = (JFEditText) itemView.findViewById (R.id.requirment_required);
            requirment_avilable = (JFTextView) itemView.findViewById (R.id.requirment_avilable);
            requirmnet_name = (JFTextView) itemView.findViewById (R.id.requirmnet_name);

        }
    }

    public class MyViewHolderRequired extends RecyclerView.ViewHolder {

        JFEditText requirment_required;
        JFTextView requirmnet_name;

        public MyViewHolderRequired(View itemView) {
            super (itemView);
            requirment_required = (JFEditText) itemView.findViewById (R.id.requirment_required);
            requirmnet_name = (JFTextView) itemView.findViewById (R.id.requirmnet_name);

        }
    }


}
