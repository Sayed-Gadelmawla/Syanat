package com.example.ibraheem.mirva.Fragments.common;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Spinner;

import com.example.ibraheem.mirva.Activities.MyNotificationActivity;
import com.example.ibraheem.mirva.Const.FragConst;
import com.example.ibraheem.mirva.CustomViews.FWTextView;
import com.example.ibraheem.mirva.CustomViews.JFButton;
import com.example.ibraheem.mirva.CustomViews.JFEditText;
import com.example.ibraheem.mirva.CustomViews.JFSpinnerAdapter;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.Interfaces.Communicator;
import com.example.ibraheem.mirva.Model.Response.Tickets.TicketType;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.Model.Response.helperRes;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.UitilsFun;
import com.example.ibraheem.mirva.util.D;
import com.example.ibraheem.mirva.util.V;
import com.example.ibraheem.mirva.util.helperFun;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Activity_SelectMessages extends AppCompatActivity {

    public final String TAG = Activity_SelectMessages.class.getSimpleName ();
    JFButton btn_send_message;
    Communicator comm;
    JFSpinnerAdapter customAdapter;
    Spinner spinner_type_of_ticket;
    String lang;
    User user;
    JFEditText ticket_content;
    V vd;
    ApiInterface mAPIService;
    ArrayList<TicketType> ticketTypes;
    D d;

    Toolbar toolbar_view_order;
    JFTextView c_title_toolbar, number_of_notifications;
    FWTextView frag_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.fragment_select_message);
        lang = helperFun.getLanguage (this);
        user = helperFun.getUser (this);
        vd = new V (this);
        findViewById (R.id.frag_notification).setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                startActivity (new Intent (getBaseContext (), MyNotificationActivity.class));

            }
        });
        toolbar_view_order = (Toolbar) findViewById (R.id.toolbar_view_order);
        c_title_toolbar = (JFTextView) toolbar_view_order.findViewById (R.id.c_title_toolbar);
        number_of_notifications = (JFTextView) toolbar_view_order.findViewById (R.id.number_of_notifications);

        c_title_toolbar.setText (getString (R.string.select_message_type));


        frag_back = (FWTextView) toolbar_view_order.findViewById (R.id.frag_back);
        frag_back.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                finish ();
            }
        });


        btn_send_message = (JFButton) findViewById (R.id.btn_send_message);
        ticket_content = (JFEditText) findViewById (R.id.ticket_content);

        spinner_type_of_ticket = (Spinner) findViewById (R.id.spinner_type_of_ticket);
        customAdapter = new JFSpinnerAdapter (Activity_SelectMessages.this, R.layout.item_spinner);
        spinner_type_of_ticket.setAdapter (customAdapter);


        vd.validateTextArea (R.id.ticket_content);


        btn_send_message.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {


                if (spinner_type_of_ticket.getSelectedItemPosition () == 0) {

                    JFTextView selectedView = (JFTextView) spinner_type_of_ticket.getSelectedView ();
                    selectedView.setError (getString (R.string.err_selection));
                    return;
                }

                if (ticket_content.getText ().length () < 1) {
                    ticket_content.setError (getString (R.string.err_empty));
                    ticket_content.requestFocus ();
                    return;
                }

                if (UitilsFun.isNetworkAvailable (Activity_SelectMessages.this)) {
                    int tiket_type_id = ticketTypes.get (spinner_type_of_ticket.getSelectedItemPosition () - 1).getId ();
                    String content = ticket_content.getText ().toString ();
                    createNewTicket (tiket_type_id, content);
                }


            }
        });
        ;

        getTicketTypes ();

    }

    /**
     * get ticket type from server
     */
    void getTicketTypes() {

        customAdapter.add (getString (R.string.select_message_type));
        mAPIService = ApiUtils.getAPIService (this);
        mAPIService.getTicketTypes (lang, user.getApiToken ()).enqueue (new Callback<ArrayList<TicketType>> () {
            @Override
            public void onResponse(Call<ArrayList<TicketType>> call, Response<ArrayList<TicketType>> response) {

                if (response.isSuccessful ()) {
                    ticketTypes = response.body ();
                    for (int i = 0; i < ticketTypes.size (); i++) {
                        customAdapter.add (ticketTypes.get (i).getName ());
                    }
                    customAdapter.notifyDataSetChanged ();
                } else {
                    helperRes.showMessageError (Activity_SelectMessages.this, response.errorBody ());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<TicketType>> call, Throwable t) {

            }
        });
    }

    /**
     *
     * create new ticket  with type and cotent ticket
     * @param type_id
     * @param content
     */
    void createNewTicket(int type_id, final String content) {
        d = new D (this);
        d.ShowProgress (getString (R.string.sending));
        mAPIService = ApiUtils.getAPIService (this);
        mAPIService.createTicket (lang, user.getApiToken (), type_id, content).enqueue (new Callback<JsonObject> () {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                d.hide ();
                if (response.isSuccessful ()) {

                   // Frag_Chat frag_chat = new Frag_Chat ();

                    int ticket_id = response.body ().get ("ticket_id").getAsInt ();
                    String message = response.body ().get ("message").getAsString ();
                  //  d.showMessageSuccess (message, frag_chat);

                    Intent intent = new Intent (Activity_SelectMessages.this, Activity_Chat.class);
                    intent.putExtra (FragConst.TICKET_ID, ticket_id);
                    intent.putExtra (FragConst.CONTENT, message);
                    intent.setFlags (Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity (intent);
                    finish ();
                   /* L.d (TAG, "");
                    Bundle arguments = new Bundle ();
                    arguments.putInt (FragConst.TICKET_ID, ticket_id);
                    arguments.putString (FragConst.CONTENT, content);
                    frag_chat.setArguments (arguments);

                    comm.addFragment (frag_chat);*/
                    // helperRes.showMessageSuccess (getActivity (), message, null);

                  /*  if (getActivity () instanceof MainPageManager) {
                        ((MainPageManager) getActivity ()).addFragment (frag_chat);

                    } else if ((getActivity () instanceof MainPageClient)) {
                        ((MainPageClient) getActivity ()).addFragment (frag_chat);

                    } else if (getActivity () instanceof MainPageTechnical) {
                        ((MainPageTechnical) getActivity ()).addFragment (frag_chat);
                    }*/

                } else {
                    //  helperRes.showMessageError (Activity_SelectMessages.this, response.errorBody ());


                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }
}
