package com.example.ibraheem.mirva.Activities;
/**
 * Created by ibraheem on 16/06/2017.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.example.ibraheem.mirva.Const.FragConst;
import com.example.ibraheem.mirva.CustomViews.Drawer.DataModel;
import com.example.ibraheem.mirva.CustomViews.Drawer.DrawerItemCustomAdapter;
import com.example.ibraheem.mirva.CustomViews.FWTextView;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.CustomViews.ResideMenu.ResideMenu;
import com.example.ibraheem.mirva.CustomViews.ResideMenu.ResideMenuItem;
import com.example.ibraheem.mirva.Fragments.Client.Frag_LostInternet;
import com.example.ibraheem.mirva.Fragments.Client.Frag_MyCompleteOrders;
import com.example.ibraheem.mirva.Fragments.Client.Frag_MyEvluation;
import com.example.ibraheem.mirva.Fragments.Client.Frag_MyOrders;
import com.example.ibraheem.mirva.Fragments.Client.Frag_Profile;
import com.example.ibraheem.mirva.Fragments.Client.Frag_Services;
import com.example.ibraheem.mirva.Fragments.Client.Frag_Subscription;
import com.example.ibraheem.mirva.Fragments.Client.Frag_ViewOrderDetail;
import com.example.ibraheem.mirva.Fragments.common.Frag_Chat;
import com.example.ibraheem.mirva.Fragments.common.Frag_Tickets;
import com.example.ibraheem.mirva.Interfaces.Actions;
import com.example.ibraheem.mirva.Interfaces.Communicator;
import com.example.ibraheem.mirva.Interfaces.sendServiceInformation;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.NetworkChangeReceiver;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.UitilsFun;
import com.example.ibraheem.mirva.util.D;
import com.example.ibraheem.mirva.util.SessionManager;
import com.example.ibraheem.mirva.util.SharedPrefs;
import com.example.ibraheem.mirva.util.helperFun;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainPageClient extends AppCompatActivity
        implements View.OnClickListener, Communicator, sendServiceInformation, NetworkChangeReceiver.NetworkLostCallback, Actions {
    public static int mSelectedItem;
    public String TAG = "Main";
    Toolbar toolbar, toolbar_clints, toolbar_profile;
    android.support.v7.app.ActionBarDrawerToggle mDrawerToggle;
    FWTextView control_navigation_drawer, c_control_navigation_drawer, p_control_navigation_drawer;
    JFTextView user_profile, c_title_toolbar, user_name;
    FragmentManager fragmentManager;
    FWTextView tv_back, p_tv_back;
    RelativeLayout h_show_notification, p_notification_count, c_notification_count;
    de.hdodenhof.circleimageview.CircleImageView user_image;
    Handler mHandler;
    InputMethodManager inputMethodManager;
    User user;
    ImageView edit_profile;
    boolean locked = false;
    NetworkChangeReceiver mNetworkChangeReceiver;
    Runnable mPendingRunnable;
    D d;
    JFTextView pro_number_of_notifications, client_number_of_notifications, main_number_of_notifications;
    private Context mContext;
    private SessionManager mSessionManager;
    public ResideMenu resideMenu;
    String titles[] = new String[10];
    int Direction = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_main_page_client);
        titles[0] = getString(R.string.main_page);
        titles[1] = getString(R.string.my_request);
        titles[2] = getString(R.string.my_completed_request);
        titles[3] = getString(R.string.my_evaluations);
        titles[4] = getString(R.string.messages_compliances);
        titles[5] = getString(R.string.my_subscription);
        titles[6] = getString(R.string.about_app);
        titles[7] = getString(R.string.share_with_others);
        titles[8] = getString(R.string.change_language);
        titles[9] = getString(R.string.exit);

        Direction = getResources().getBoolean(R.bool.is_right_to_left) ? ResideMenu.DIRECTION_RIGHT : ResideMenu.DIRECTION_LEFT;
        initView();
        d = new D(this);
        getUserData();
        inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        mContext = this;
        mSessionManager = new SessionManager(mContext);

        if (!mSessionManager.isLocationChecked()) {
            startActivity(ChooseLocationActivity.newIntent(mContext));
        }
    }

    /**
     * getuser data json opject save in share
     */
    private void getUserData() {

        user = helperFun.getUser(this);
        Log.d(TAG, "onCreate: " + user.getImage());

        Picasso.with(this)
                .load(user.getImage())
                .placeholder(R.drawable.avatar)
                .error(R.drawable.avatar)
                .into(user_image);
        user_name.setText(user.getName());

    }

    private void initView() {
        mHandler = new Handler();
        init_Toolbar();

        fragmentManager = getSupportFragmentManager();

        user_profile = (JFTextView) findViewById(R.id.user_profile);
        user_profile.setOnClickListener(this);

        user_image = (de.hdodenhof.circleimageview.CircleImageView) findViewById(R.id.user_image);
        user_name = (JFTextView) findViewById(R.id.user_name);
        c_title_toolbar = (JFTextView) findViewById(R.id.c_title_toolbar);

        Intent intent = getIntent();
        if (!intent.hasExtra(FragConst.REQUEST_ID) && !intent.hasExtra(FragConst.TICKET_ID)) {
            addFragment(new Frag_Services());


        } else if (intent.hasExtra(FragConst.REQUEST_ID)) {

            Log.d(TAG, "init_Toolbar: " + intent.getIntExtra(FragConst.REQUEST_ID, 0));
            Bundle bundle = new Bundle();
            bundle.putInt(FragConst.REQUEST_ID, intent.getIntExtra(FragConst.REQUEST_ID, 0));
            Fragment fragment = new Frag_ViewOrderDetail();
            fragment.setArguments(bundle);
            addFragment(fragment);

        } else if (intent.hasExtra(FragConst.TICKET_ID)) {
            Log.d(TAG, "init_Toolbar: " + intent.getIntExtra(FragConst.TICKET_ID, 0));
            Bundle bundle = new Bundle();
            bundle.putInt(FragConst.TICKET_ID, intent.getIntExtra(FragConst.TICKET_ID, 0));
            Fragment fragment = new Frag_Chat();
            fragment.setArguments(bundle);
            addFragment(fragment);
        }
    }

    /**
     * prepare toolbar
     */
    private void init_Toolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar_main);
        toolbar_clints = (Toolbar) findViewById(R.id.toolbar_clints);
        toolbar_profile = (Toolbar) findViewById(R.id.toolbar_profile);

        resideMenu = new ResideMenu(this);
        resideMenu.setUse3D(true);
        resideMenu.setScaleValue(0.9f);
        resideMenu.setBackground(R.drawable.menu_background);
        resideMenu.attachToActivity(this);
        resideMenu.setMenuListener(menuListener);
        // create menu items;
        for (int i = 0; i < titles.length; i++){
            ResideMenuItem item = new ResideMenuItem(this, titles[i]);
            item.setOnClickListener(this);
            resideMenu.addMenuItem(item,  Direction); // or  ResideMenu.DIRECTION_RIGHT
        }
        toolbar_clints.setVisibility(View.GONE);
        toolbar_profile.setVisibility(View.GONE);
        toolbar.setVisibility(View.GONE);
        toolbar.findViewById(R.id.main_nav).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                if (resideMenu.isOpened())
                {
                    resideMenu.closeMenu();
                }else
                {
                    resideMenu.openMenu(Direction);
                }
            }
        });


        toolbar.findViewById(R.id.main_notification).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), MyNotificationActivity.class));

            }
        });

        toolbar.findViewById(R.id.notification_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), MyNotificationActivity.class));

            }
        });
        toolbar_clints.findViewById(R.id.frag_nav).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                resideMenu.openMenu(Direction);
            }
        });

        toolbar_clints.findViewById(R.id.frag_notification).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), MyNotificationActivity.class));
            }
        });

        toolbar_clints.findViewById(R.id.client_notification_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), MyNotificationActivity.class));
            }
        });

        toolbar_clints.findViewById(R.id.frag_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (resideMenu.isOpened()) {
                    resideMenu.closeMenu();
                }
                if (fragmentManager.getBackStackEntryCount() == 1) {
                    finish();
                } else {
                    fragmentManager.popBackStackImmediate();
                }
            }
        });


        toolbar_profile.findViewById(R.id.profile_nav).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                resideMenu.openMenu(Direction);
            }
        });

        toolbar_profile.findViewById(R.id.profile_notification).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), MyNotificationActivity.class));

            }
        });

        toolbar_profile.findViewById(R.id.profile_notification_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), MyNotificationActivity.class));

            }
        });

        toolbar_profile.findViewById(R.id.profile_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (resideMenu.isOpened()) {
                    resideMenu.closeMenu();
                }

                addFragment(new Frag_Services());
            }
        });

        toolbar_profile.findViewById(R.id.edit_profile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), EditProfileActivity.class));
            }
        });


        pro_number_of_notifications = (JFTextView) toolbar_profile.findViewById(R.id.number_of_notifications);
        client_number_of_notifications = (JFTextView) toolbar_clints.findViewById(R.id.number_of_notifications);
        main_number_of_notifications = (JFTextView) toolbar.findViewById(R.id.number_of_notifications);
    }

    private ResideMenu.OnMenuListener menuListener = new ResideMenu.OnMenuListener() {
        @Override
        public void openMenu() {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }

        @Override
        public void closeMenu() {
            if (mPendingRunnable != null) {
                mHandler.post(mPendingRunnable);
                mPendingRunnable = null;
            }
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        mNetworkChangeReceiver = new NetworkChangeReceiver();
        mNetworkChangeReceiver.setNetworkLostCallback(new NetworkChangeReceiver.NetworkLostCallback() {
            @Override
            public void onNetworkLost() {
                UitilsFun.ShowAlertDialog(MainPageClient.this);
            }

            @Override
            public void onNetworkRegained() {

            }
        });

        IntentFilter networkFilter = new IntentFilter();
        networkFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);

        registerReceiver(mNetworkChangeReceiver, networkFilter);

        if (UitilsFun.isNetworkAvailable(MainPageClient.this)) {

            if (mPendingRunnable != null) {
                mHandler.post(mPendingRunnable);
                mPendingRunnable = null;
            }

        } else {
            UitilsFun.ShowAlertDialog(MainPageClient.this);
        }
        getNotificationNumber();

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mNetworkChangeReceiver != null) {
            unregisterReceiver(mNetworkChangeReceiver);
            mNetworkChangeReceiver = null;
        }
    }

    /**
     * set title to toolbar
     *
     * @param postion
     */
    @Override
    public void setTitleToolbar(int postion) {
        c_title_toolbar.setText(titles[postion]);

    }

    @Override
    public void setTitleToolbar(String title) {
        c_title_toolbar.setText(title);
    }

    @Override
    public void addFragment(Fragment fragment) {
        ReplaceFragement(fragment);


        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
            mPendingRunnable = null;
        }
    }

    /**
     * add selectd fragment in drawr
     *
     * @param position
     */
    private void selectItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new Frag_Services();
                break;
            case 1:
                fragment = new Frag_MyOrders();
                break;
            case 2:
                fragment = new Frag_MyCompleteOrders();
                break;
            case 3:
                fragment = new Frag_MyEvluation();
                break;
            case 4:
                fragment = new Frag_Tickets();
                break;
            case 5:
                fragment = new Frag_Subscription();
                break;
            case 6:
                startActivity(new Intent(getBaseContext(), About.class));
                resideMenu.closeMenu();
                break;
            case 7:
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
                String shareText = getString(R.string.download_syanat) + " https://play.google.com/store/apps/details?id=" + getPackageName();
                i.putExtra(Intent.EXTRA_TEXT, shareText);
                startActivity(Intent.createChooser(i, getString(R.string.share_via)));
                resideMenu.closeMenu();
                break;
            case 8:
                UitilsFun.changeLanguage(this);
                resideMenu.closeMenu();
                finish();
                break;
            case 9:
                sendLogout();
            default:
                break;
        }


        if (fragment != null) {
            ReplaceFragement(fragment);
            resideMenu.closeMenu();
        } else {
            Log.e("MainActivity", "Error in creating fragment");
        }
    }

    @Override
    public void setToolbatLock(boolean locke) {
        this.locked = locke;
    }

    /***
     *
     *
     * set username  and iamge in header
     *
     */
    @Override
    public void updateUserData() {
        getUserData();
    }

    @Override
    public void TopMessage(Fragment fragment) {
        fragmentManager.beginTransaction()
                .replace(R.id.container, fragment)
                .setCustomAnimations(R.anim.enter, R.anim.exit)
                .commit();
    }

    /**
     * select which toolbar with be available
     *
     * @param t
     */
    @Override
    public void selectVisibleToolbar(int t) {

        switch (t) {
            case R.id.toolbar_main:
                if (!locked) {
                    toolbar.setVisibility(View.VISIBLE);
                    toolbar_clints.setVisibility(View.GONE);
                    toolbar_profile.setVisibility(View.GONE);
                }

                break;
            case R.id.toolbar_clints:
                if (!locked) {
                    toolbar.setVisibility(View.GONE);
                    toolbar_clints.setVisibility(View.VISIBLE);
                    toolbar_profile.setVisibility(View.GONE);
                }
                break;
            case R.id.toolbar_profile:
                toolbar_profile.setVisibility(View.VISIBLE);
                toolbar.setVisibility(View.GONE);
                toolbar_clints.setVisibility(View.GONE);
                locked = true;
                break;


        }
    }

    @Override
    public void actionToselectedPostion(int postion) {

    }

    /***
     *
     * set action to toolbar navigation
     * @param v
     */
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.user_profile:
                ReplaceFragement(new Frag_Profile());
                //mDrawerLayout.closeDrawer(Gravity.START);
                resideMenu.closeMenu();
                break;

            case R.id.control_navigation_drawer:
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                resideMenu.openMenu(Direction);
                Log.d(TAG, "onClick: ");
                break;

            case R.id.edit_profile:
                startActivity(new Intent(getBaseContext(), EditProfileActivity.class));
                finish();
                break;
            default:
                selectItem(Arrays.asList(titles).indexOf(((ResideMenuItem)v).getTitle()));
                break;
        }
    }

    /**
     * @param fragment to repalce fragment when add fragmetn on top of stack
     */
    private void ReplaceFragement(final Fragment fragment) {

        if (resideMenu.isOpened()) {
            resideMenu.closeMenu();
        }
        mPendingRunnable = new Runnable() {
            @Override
            public void run() {

                fragmentManager.beginTransaction()
                        .replace(R.id.container, fragment)
                        .addToBackStack("more")
                        .setCustomAnimations(R.anim.enter, R.anim.exit)
                        .commit();
            }
        };
    }

    @Override
    public String sendSerivceId(String SerivceId) {
        return null;
    }

    @Override
    public void onNetworkLost() {
        ReplaceFragement(new Frag_LostInternet());

    }

    @Override
    public void onNetworkRegained() {

    }

    void sendLogout() {
        //   startActivity (new Intent (getBaseContext (), LoginActivity.class));
        Intent intent = new Intent(getBaseContext(), LoginActivity.class);
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(new Intent(getBaseContext(), LoginActivity.class));
        finish();

        ApiInterface apiInterface = ApiUtils.getAPIService(this);
        apiInterface.sendLogout(user.getApiToken()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    SharedPrefs.revokeShared(getApplicationContext());

                } else {

                    // helperRes.showMessageError (getBaseContext (), response.errorBody ());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                //    d.ShowErrorInternetConnection ("");
            }
        });


    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed ();
        if (resideMenu.isOpened()) {
            resideMenu.closeMenu();
        }


        if (fragmentManager.getBackStackEntryCount() == 1) {
            // addFragment (new Frag_Services ());
            finish();


            //  super.onBackPressed ();


        } else {
            fragmentManager.popBackStackImmediate();


        }
    }

    /**
     * get nnumber of notfication to client
     */
    private void getNotificationNumber() {

        ApiInterface apiInterface = ApiUtils.getAPIService(this);
        apiInterface.getNotifications(user.getApiToken()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                if (response.isSuccessful()) {
                    String number;

                    if (response.body().get("number") == null) {
                        number = "0";
                    } else {

                        number = response.body().get("number").getAsString();

                    }
                    SharedPrefs.save(getApplicationContext(), "number_of_notifications", number);

                    pro_number_of_notifications.setText(number);
                    client_number_of_notifications.setText(number);
                    main_number_of_notifications.setText(number);

                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });


    }

    @Override
    public void beckFragment() {
        getSupportFragmentManager().popBackStack();
    }

}


