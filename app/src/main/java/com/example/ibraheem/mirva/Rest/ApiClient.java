package com.example.ibraheem.mirva.Rest;

/**
 * Created by ibraheem on 10/06/2017.
 */

import android.content.Context;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ApiClient {

    public static final String TAG = ApiClient.class.getSimpleName ();
    private static Retrofit retrofit = null;
    /**
     * get http client for short time
     * @param context
     * @param baseUrl
     * @return
     */
    public static Retrofit getClient(final Context context, String baseUrl) {
        if (retrofit == null) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor ();
            // set your desired log level
            logging.setLevel (HttpLoggingInterceptor.Level.BODY);
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder ();
            httpClient.connectTimeout (100, TimeUnit.SECONDS)
                    .readTimeout (100, TimeUnit.SECONDS)
                    .writeTimeout (100, TimeUnit.SECONDS);
            //     SmartCallFactory smartFactory = new SmartCallFactory(BasicCaching.fromCtx(context));

            httpClient.addInterceptor (logging);  // <-- this is the important line!
            retrofit = new Retrofit.Builder ()
                    .baseUrl (baseUrl)

                    .addConverterFactory (GsonConverterFactory.create ())

                    .client (httpClient.build ())

                    .build ();
        }
        return retrofit;

    }

    /**
     * get http client for long time
     * @param context
     * @param baseUrl
     * @return
     */
    public static Retrofit getLongTimeClient(final Context context, String baseUrl) {
        if (retrofit == null) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor ();
            // set your desired log level
            logging.setLevel (HttpLoggingInterceptor.Level.BODY);
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder ();
            httpClient.connectTimeout (600, TimeUnit.SECONDS)
                    .readTimeout (600, TimeUnit.SECONDS)
                    .writeTimeout (600, TimeUnit.SECONDS);

            httpClient.addInterceptor (logging);  // <-- this is the important line!
            retrofit = new Retrofit.Builder ()
                    .baseUrl (baseUrl)
                    .addConverterFactory (GsonConverterFactory.create ())
                    .client (httpClient.build ())

                    .build ();
        }
        return retrofit;

    }
}