package com.example.ibraheem.mirva.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;

import com.example.ibraheem.mirva.CustomViews.FWTextView;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.util.SessionManager;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class About extends AppCompatActivity {


    JFTextView c_title_toolbar, number_of_notifications;
    FWTextView frag_back;
    Toolbar toolbar_view_order;
    JFTextView about_text;
    private SessionManager mSessionManager;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_about);
        mSessionManager=new SessionManager(getApplicationContext());
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        toolbar_view_order = (Toolbar) findViewById (R.id.toolbar_view_order);

        c_title_toolbar = (JFTextView) toolbar_view_order.findViewById (R.id.c_title_toolbar);
        number_of_notifications = (JFTextView) toolbar_view_order.findViewById (R.id.number_of_notifications);

        frag_back = (FWTextView) toolbar_view_order.findViewById (R.id.frag_back);
        frag_back.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                finish ();
            }
        });
        c_title_toolbar.setText (getString (R.string.about));

        about_text = (JFTextView) findViewById (R.id.about_text);
        about_text.setText (mSessionManager.getAbout());

        ApiInterface apiInterface = ApiUtils.getAPIService (getBaseContext ());
        apiInterface.getAbout ().enqueue (new Callback<JsonObject> () {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful ()) {
                    JsonObject object = response.body ();
                    about_text.setText (object.get ("about").toString ());
                    mSessionManager.setAbout(object.get ("about").toString ());
                    mProgressBar.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                mProgressBar.setVisibility(View.INVISIBLE);
            }
        });
    }


}
