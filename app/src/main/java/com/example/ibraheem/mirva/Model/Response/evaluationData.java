package com.example.ibraheem.mirva.Model.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Eng.Mhmod on 9/6/2017.
 */

public class evaluationData {
    @SerializedName("evaluation_id")
    @Expose
    private Integer evaluation_id;
    @SerializedName("value")
    @Expose
    private Integer value;

    public Integer getEvaluation_id() {
        return evaluation_id;
    }

    public void setEvaluation_id(Integer evaluation_id) {
        this.evaluation_id = evaluation_id;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
