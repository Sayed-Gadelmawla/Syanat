package com.example.ibraheem.mirva.Fragments.Client;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.example.ibraheem.mirva.Adapter.MyEvalutionsAdapter;
import com.example.ibraheem.mirva.Interfaces.Communicator;
import com.example.ibraheem.mirva.Model.Response.EevaluteOrderData.AllEvalutions;
import com.example.ibraheem.mirva.Model.Response.EevaluteOrderData.MyEveluation;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.util.D;
import com.example.ibraheem.mirva.util.L;
import com.example.ibraheem.mirva.util.SessionManager;
import com.example.ibraheem.mirva.util.helperFun;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Frag_MyEvluation extends Fragment {

    public final String TAG = "Evaluations";
    ArrayList<MyEveluation> myEveluations;
    MyEvalutionsAdapter myEvalutionsAdapter;
    RecyclerView recyclerView;
    Communicator comm;
    D d;
    private ProgressBar mProgressBar;
    private SessionManager mSessionManager;
    private Context mContext;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate (R.layout.frag_my_evaluations, container, false);
        mContext = getActivity();
        mSessionManager = new SessionManager(mContext);
        d = new D (getActivity ());
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        recyclerView = (RecyclerView) view.findViewById (R.id.rv_my_evalution);
        myEveluations = new ArrayList<> ();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager (getContext ());
        linearLayoutManager.setOrientation (LinearLayout.VERTICAL);
        recyclerView.setLayoutManager (linearLayoutManager);

        myEvalutionsAdapter = new MyEvalutionsAdapter (getContext (), myEveluations);
        recyclerView.setAdapter (myEvalutionsAdapter);

        comm = (Communicator) getActivity ();
        comm.setTitleToolbar (getString (R.string.title_my_evelautions));
        comm.selectVisibleToolbar (R.id.toolbar_clints);

        new FillAllMyEvaluationsAsyncTask().execute();
        return view;
    }


    public class FillAllMyEvaluationsAsyncTask extends AsyncTask<Void, Void, Void> {
        ApiInterface apiInterface;
        User user;
        @Override
        protected Void doInBackground(Void... voids) {

            user = helperFun.getUser (getActivity ());
            apiInterface = ApiUtils.getAPIService (getActivity ());

            apiInterface.getAllEvaluations (user.getApiToken ()).enqueue (new Callback<AllEvalutions> () {
                @Override
                public void onResponse(Call<AllEvalutions> call, Response<AllEvalutions> response) {
                    if (response.isSuccessful ()) {
                        L.d (TAG, "FillAllMyEvaluationsAsyncTask body " + response.body ());
                        AllEvalutions allEvalutions = response.body ();
                        mSessionManager.setMyEvaluations(allEvalutions);
                        if (!allEvalutions.getData().isEmpty()) {
                            myEvalutionsAdapter.loadData (allEvalutions.getData ());
                        }

                        //   myEvalutionsAdapter.loadData(response.body ());
                    } else {
                        L.d (TAG, "errorBody" + response);

                    }
                    mProgressBar.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onFailure(Call<AllEvalutions> call, Throwable t) {
                    d.ShowErrorInternetConnection ("");
                    AllEvalutions allEvalutions = mSessionManager.getMyEvaluations();
                    if (allEvalutions!=null) {
                        myEvalutionsAdapter.loadData (allEvalutions.getData ());
                    }
                    mProgressBar.setVisibility(View.INVISIBLE);

                }
            });
            return null;
        }
    }


}
