package com.example.ibraheem.mirva.Activities;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.os.AsyncTask;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bruce.pickerview.popwindow.DatePickerPopWin;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.Model.Response.WorkingHour;
import com.example.ibraheem.mirva.Model.Response.helperRes;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.util.SessionManager;
import com.example.ibraheem.mirva.util.SharedPrefs;
import com.example.ibraheem.mirva.util.helperFun;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ClientNewOrder extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, Animation.AnimationListener {
    private static final String TAG = "ClientNewOrder";
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION_TWO = 2;
    private static int serviceId;
    private LatLng tempLng;
    private MapView mMapView;
    private GoogleMap googleMap;
    private Marker mk;
    private GoogleApiClient mGoogleApiClient;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 99;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS_Media = 100;
    private TextView mLocationTextView;
    private int i = 0;
    private Geocoder geocoder;
    private String lang;
    private User user;
    private Animation animMoveToTop;
    private LinearLayout mScheduledLinearLayout, mChooseDateLinearLayout;
    private JFTextView mDateJfTextView;
    private Calendar myCalendar;
    private ApiInterface mAPIService;
    private ArrayList<WorkingHour> workingHours;
    private WorkingHoursAdapter mAdapter;
    private RecyclerView mWorkingHoursRecyclerView;
    private AVLoadingIndicatorView mIndicatorView;
    private LinearLayout mScheduledWorkingLinearLayout;
    private SessionManager mSessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_new_order);
        lang = helperFun.getLanguage(this.getApplicationContext());
        user = helperFun.getUser(this.getApplicationContext());
        mSessionManager = new SessionManager(getApplicationContext());
        mScheduledLinearLayout = (LinearLayout) findViewById(R.id.scheduled_linear_layout);
        mScheduledWorkingLinearLayout = (LinearLayout) findViewById(R.id.scheduled_workings_linear);
        mChooseDateLinearLayout = (LinearLayout) findViewById(R.id.choose_date_linear_layout);
        mDateJfTextView = (JFTextView) findViewById(R.id.date_tv);
        mWorkingHoursRecyclerView = (RecyclerView) findViewById(R.id.working_time_recycler);
        mIndicatorView = (AVLoadingIndicatorView) findViewById(R.id.avi);
        GridLayoutManager mGridLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
        mWorkingHoursRecyclerView.setLayoutManager(mGridLayoutManager);
        mScheduledWorkingLinearLayout.setVisibility(View.GONE);
        if (!SharedPrefs.getString(ClientNewOrder.this, SharedPrefs.LATITUDE, "").isEmpty()) {

            double dragLat = Double.parseDouble(SharedPrefs.getString(ClientNewOrder.this, SharedPrefs.LATITUDE));
            double dragLong = Double.parseDouble(SharedPrefs.getString(ClientNewOrder.this, SharedPrefs.LONGITUDE));
            tempLng = new LatLng(dragLat, dragLong);
        } else {

            tempLng = new LatLng(31.5017672, 34.4665412);
        }
        mMapView = (MapView) findViewById(R.id.mapView_new_order);
        mLocationTextView = (TextView) findViewById(R.id.location_name_tv);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;


                buildGoogleApiClient();

                mMapView.setEnabled(true);
                mk = googleMap.addMarker(new MarkerOptions()
                        .position(tempLng)
                        .draggable(true)
                        .title("Selected Postion")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_placeholder_point)));

                googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                        .target(tempLng)
                        .zoom(15.6F)
                        .build()));

                googleMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
                    @Override
                    public void onCameraMove() {
                        LatLng centerOfMap = googleMap.getCameraPosition().target;
                        SharedPrefs.save(ClientNewOrder.this, SharedPrefs.LATITUDE, String.valueOf(centerOfMap.latitude));
                        SharedPrefs.getString(ClientNewOrder.this, SharedPrefs.LONGITUDE, String.valueOf(centerOfMap.longitude));

                        mk.setPosition(centerOfMap);
                        i++;
                        if (i % 15 == 0)

                        {
                            new getAddressAsyncTask().execute(centerOfMap);
                        }
                    }
                });

            }
        });

        // load the animation
        animMoveToTop = AnimationUtils.loadAnimation(

                getApplicationContext(), R.anim.move);

        // set animation listener
        animMoveToTop.setAnimationListener(this);
        mScheduledLinearLayout.setVisibility(View.VISIBLE);
        mScheduledLinearLayout.startAnimation(animMoveToTop);

        myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                String myFormat = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                //In which you need put here
                /*SimpleDateFormat sdf = new SimpleDateFormat (myFormat, Locale.US);
                Log.d (TAG, "updateLabel: myCalendar : " + myCalendar.getTime ());*/
                mDateJfTextView.setText(getFormatedDateTime(myFormat, "yyyy-MM-dd", "E',' ' 'dd','MMMM','yyyy"));
                getWorkingHours(myFormat);
                //  updateLabel ();
            }

        };

        mChooseDateLinearLayout.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View view) {
                DatePickerDialog _date = new DatePickerDialog(ClientNewOrder.this, R.style.datepicker, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH));
                Calendar c = Calendar.getInstance();
                c.setTimeInMillis(System.currentTimeMillis() - 1000);
                _date.getDatePicker().setMinDate(c.getTimeInMillis());
                _date.show();

            }
        });
    }

    private void getWorkingHours(String date) {
        mScheduledWorkingLinearLayout.setVisibility(View.VISIBLE);
        mWorkingHoursRecyclerView.setVisibility(View.GONE);
        mIndicatorView.show();
        mAPIService = ApiUtils.getAPIService(ClientNewOrder.this);
        mAPIService.getWorkingHour(lang, user.getApiToken(), serviceId, date).enqueue(new Callback<ArrayList<WorkingHour>>() {
            @Override
            public void onResponse(Call<ArrayList<WorkingHour>> call, Response<ArrayList<WorkingHour>> response) {

                if (response.isSuccessful()) {
                    workingHours = new ArrayList<WorkingHour>();
                    workingHours.clear();
                    workingHours = response.body();
                    mAdapter = new WorkingHoursAdapter(workingHours, R.layout.working_hour_item);
                    mWorkingHoursRecyclerView.setAdapter(mAdapter);
                    mWorkingHoursRecyclerView.setVisibility(View.VISIBLE);
                    ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) mMapView.getLayoutParams();
                    params.bottomMargin = mScheduledLinearLayout.getHeight();
                    mMapView.setLayoutParams(params);
                    mAdapter.notifyDataSetChanged();
                    mIndicatorView.hide();
                } else {
                    ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) mMapView.getLayoutParams();
                    params.bottomMargin = mScheduledLinearLayout.getHeight();
                    mMapView.setLayoutParams(params);
                    helperRes.getErrorMessage(response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<WorkingHour>> call, Throwable t) {
                call.cancel();
            }

        });
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();


        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION_TWO);
            }
        }

        Location mLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLocation == null) {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
        } else {
            tempLng = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(ClientNewOrder.this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mk = googleMap.addMarker(new MarkerOptions()
                                .position(tempLng)
                                .title("Selected Postion")
                                .draggable(true)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_placeholder_point)));
                    }

                } else {

                    Toast.makeText(ClientNewOrder.this, "permission denied", Toast.LENGTH_LONG).show();
                }
            }
            break;

            case REQUEST_ID_MULTIPLE_PERMISSIONS_Media: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(ClientNewOrder.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(ClientNewOrder.this, "PERMISSION_GRANTED ", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(ClientNewOrder.this, "permission denied", Toast.LENGTH_LONG).show();
                    }
                }
                break;
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        tempLng = new LatLng(location.getLatitude(), location.getLongitude());
        mk.setPosition(tempLng);
        Log.d(TAG, "(" + tempLng.latitude + "," + tempLng.longitude + ")");
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public static Intent newIntent(Context mContext, int mServiceId) {
        serviceId = mServiceId;
        return new Intent(mContext, ClientNewOrder.class);
    }

    private class getAddressAsyncTask extends AsyncTask<LatLng, Void, Void> {
        String locationText;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(LatLng... params) {

            List<Address> addresses;
            geocoder = new Geocoder(ClientNewOrder.this, Locale.getDefault());
            try {
                tempLng = new LatLng(params[0].latitude, params[0].longitude);

                addresses = geocoder.getFromLocation(tempLng.latitude, tempLng.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                if (addresses != null && !addresses.isEmpty()) {
                    String address = addresses.get(0).getAddressLine(0) == null ? "" : addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    String city = addresses.get(0).getLocality() == null ? "" : addresses.get(0).getLocality();
                    String state = addresses.get(0).getAdminArea() == null ? "" : addresses.get(0).getAdminArea();
                    String country = addresses.get(0).getCountryName() == null ? "" : addresses.get(0).getCountryName();
                    String postalCode = addresses.get(0).getPostalCode() == null ? "" : addresses.get(0).getPostalCode();
                    String knownName = addresses.get(0).getFeatureName() == null ? "" : addresses.get(0).getFeatureName(); // Only if available else return NULL
                    locationText = address + "," + city + "," + state + "," + country + "," + postalCode + "," + knownName;
                    return null;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (locationText != null) {
                mLocationTextView.setText(locationText);
                i = 0;
            }
        }
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        // Take any action after completing the animation
        // check for move animation
        /*if (animation == animMoveToTop) {
            Toast.makeText(getApplicationContext(), "Animation Stopped", Toast.LENGTH_SHORT).show();
        }*/
    }

    @Override
    public void onAnimationRepeat(Animation animation) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onAnimationStart(Animation animation) {
        // TODO Auto-generated method stub
    }

    private class WorkingHoursAdapter extends RecyclerView.Adapter<WorkingHoursViewHolder> {
        private int resource; // the layout we will fill it in recycler (list_item_row)
        private List<WorkingHour> mItemsList; // this is the copy from MainList

        public WorkingHoursAdapter(List<WorkingHour> itemList, int res) {
            mItemsList = itemList;
            this.resource = res;
        }

        @Override
        public WorkingHoursViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getApplicationContext()); // move the layout controller from this activity layout (mContext)
            View view = layoutInflater.inflate(this.resource, parent, false); // move int resource (list_item_row)
            return new WorkingHoursViewHolder(view); // take parameter of the view (layout) we will fill in it in the recycler
        }

        @Override
        public void onBindViewHolder(WorkingHoursViewHolder holder, int position) {
            WorkingHour mWorkingHour = mItemsList.get(position);
            holder.bindItem(mWorkingHour); // this is the item of (kids) we will fill it in the view on the recycler
        }

        @Override
        public int getItemCount() {
            return mItemsList.size();
        }
    }


    private class WorkingHoursViewHolder extends RecyclerView.ViewHolder {
        private JFTextView mWorkingTime;
        private LinearLayout mLinearLayout;
        private View currentItemView;

        public WorkingHoursViewHolder(View itemView) {
            super(itemView);
            currentItemView = itemView;
            mWorkingTime = (JFTextView) itemView.findViewById(R.id.time_tv);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mSessionManager.setCurrentOrderWorkingId(workingHours.get(getAdapterPosition()).getId());
                    finish();
                }
            });

        }

        public void bindItem(final WorkingHour mWorkingHour) {
            mWorkingTime.setText(getFormatedDateTime(String.valueOf(mWorkingHour.getFrom()), "HH:mm:ss", "hh:mm a") + " - " + getFormatedDateTime(String.valueOf(mWorkingHour.getTo()), "HH:mm:ss", "hh:mm a"));
            mLinearLayout = (LinearLayout) itemView.findViewById(R.id.working_hour_list_linear);
            DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
            mLinearLayout.setMinimumWidth(displayMetrics.widthPixels / 2);
            if (mWorkingHour.getStatus() == 5) {
                currentItemView.setEnabled(true);
            } else {
                currentItemView.setEnabled(false);
                mWorkingTime.setBackground(getResources().getDrawable(R.drawable.oval_working_disable));
            }

        }

    }

    public static String getFormatedDateTime(String dateStr, String strReadFormat, String strWriteFormat) {

        String formattedDate = dateStr;

        DateFormat readFormat = new SimpleDateFormat(strReadFormat, Locale.getDefault());
        DateFormat writeFormat = new SimpleDateFormat(strWriteFormat, Locale.getDefault());

        Date date = null;

        try {
            date = readFormat.parse(dateStr);
        } catch (ParseException e) {
        }

        if (date != null) {
            formattedDate = writeFormat.format(date);
        }

        return formattedDate;
    }

}
