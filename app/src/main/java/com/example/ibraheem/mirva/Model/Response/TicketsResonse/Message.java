package com.example.ibraheem.mirva.Model.Response.TicketsResonse;

import android.graphics.Bitmap;

import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ibraheem on 22/06/2017.
 */

public class Message {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("is_user_send")
    @Expose
    private String isUserSend;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("image")
    @Expose
    private String image;

    @SerializedName("date")
    @Expose
    private String date;
    private Bitmap tempImage;

    public Bitmap getTempImage() {
        return tempImage;
    }

    public void setTempImage(Bitmap tempImage) {
        this.tempImage = tempImage;
    }

    public String getUrl() {
        return ApiUtils.BASE_URL + url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIsUserSend() {
        return isUserSend;
    }

    public void setIsUserSend(String isUserSend) {
        this.isUserSend = isUserSend;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImage() {
        return ApiUtils.BASE_URL + image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Message{" +
                "name='" + name + '\'' +
                ", isUserSend='" + isUserSend + '\'' +
                ", content='" + content + '\'' +
                ", url='" + url + '\'' +
                ", image='" + image + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}
