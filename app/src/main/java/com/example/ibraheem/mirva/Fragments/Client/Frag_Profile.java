package com.example.ibraheem.mirva.Fragments.Client;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.ibraheem.mirva.CustomViews.JFButton;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.Fragments.common.Activity_SelectMessages;
import com.example.ibraheem.mirva.Fragments.common.Frag_Profile_MyData;
import com.example.ibraheem.mirva.Fragments.common.Frag_SelectMessageType;
import com.example.ibraheem.mirva.Fragments.common.Frag_Tickets;
import com.example.ibraheem.mirva.Interfaces.Communicator;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.util.helperFun;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Frag_Profile extends Fragment {
    FragmentManager supportFragmentManager;
    String TAG = "fragment_my_profile";
    JFButton btn_send_message;
    de.hdodenhof.circleimageview.CircleImageView profile_userImage;
    JFTextView profile_username, maintenance_count;
    Communicator comm;
    User user;
    LinearLayout layout_icon_account_type;
    ImageView icon_account_type;
    String lang;
    JFTextView number_of_messages;
    MyPagerAdapter adapterViewPager;
    ViewPager vpPager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        number_of_messages = (JFTextView) view.findViewById(R.id.number_of_messages);
        user = helperFun.getUser(getContext());
        lang = helperFun.getLanguage(getContext());
        comm = (Communicator) getActivity();
        comm.selectVisibleToolbar(R.id.toolbar_profile);
        profile_userImage = (de.hdodenhof.circleimageview.CircleImageView) view.findViewById(R.id.profile_userImage);
        profile_username = (JFTextView) view.findViewById(R.id.profile_username);
        layout_icon_account_type = (LinearLayout) view.findViewById(R.id.layout_icon_account_type);
        icon_account_type = (ImageView) view.findViewById(R.id.icon_account_type);
        maintenance_count = (JFTextView) view.findViewById(R.id.maintenance_count);

        vpPager = (ViewPager) view.findViewById(R.id.vpPager);
        adapterViewPager = new MyPagerAdapter(getActivity().getSupportFragmentManager());
        vpPager.setAdapter(adapterViewPager);
        vpPager.setCurrentItem(0);


        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        getUserData();

    }

    private void getUserData() {


        Log.d(TAG, "onCreate: " + user.getImage());

        Picasso.with(getActivity())
                .load(user.getImage())
                .placeholder(R.drawable.avatar)
                .error(R.drawable.avatar)
                .into(profile_userImage);

        profile_username.setText(user.getName());

        Log.d(TAG, "Login: " + user);

        if (user.getSubscription() != null && user.getSubscription() == 1) {
            layout_icon_account_type.setVisibility(View.VISIBLE);
            icon_account_type.setVisibility(View.GONE);

            switch (user.getSubscription_type()) {
                case 1:
                    layout_icon_account_type.setBackground(getResources().getDrawable(R.drawable.icon_pronz));
                    //   icon_account_type.setImageBitmap ( BitmapFactory.decodeResource(getResources(), R.drawable.icon_pronz));


                    break;
                case 2:
                    layout_icon_account_type.setBackground(getResources().getDrawable(R.drawable.icon_selver));

                    //    icon_account_type.setImageBitmap (BitmapFactory.decodeResource(getResources(), R.drawable.icon_selver));

                    break;
                case 3:
                    layout_icon_account_type.setBackground(getResources().getDrawable(R.drawable.icon_golden));

                    //  icon_account_type.setImageBitmap (BitmapFactory.decodeResource(getResources(), R.drawable.icon_golden));

                    break;
                case 4:
                    layout_icon_account_type.setBackground(getResources().getDrawable(R.drawable.icon_masy));

                    //  icon_account_type.setImageBitmap (BitmapFactory.decodeResource(getResources(), R.drawable.icon_masy));

                    break;
            }

            maintenance_count.setVisibility(View.VISIBLE);
            maintenance_count.setText("0");


        } else {


            layout_icon_account_type.setVisibility(View.GONE);

        }

        comm.updateUserData();
        getUserDataFromServer();
        getMeessageNumber();


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        supportFragmentManager = getActivity().getSupportFragmentManager();
        btn_send_message = (JFButton) view.findViewById(R.id.btn_send_message);
        final LinearLayout menu_my_request = (LinearLayout) view.findViewById(R.id.menu_my_request);
        final LinearLayout menu_my_messages = (LinearLayout) view.findViewById(R.id.menu_my_messages);
        final LinearLayout menu_my_data = (LinearLayout) view.findViewById(R.id.menu_my_data);
        ImageView imageView = (ImageView) view.findViewById(R.id.icon_account_type);
        LinearLayout layout_icon_account_type = (LinearLayout) view.findViewById(R.id.layout_icon_account_type);

        imageView.setColorFilter(getActivity().getResources().getColor(R.color.colorWhite));
        layout_icon_account_type.setBackground(getActivity().getResources().getDrawable(R.drawable.shap_dahap));

        // supportFragmentManager.beginTransaction ().add (R.id.container_menu_selected, new Frag_Tickets ()).commit ();
        btn_send_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getActivity(), Activity_SelectMessages.class));
                /*comm.setToolbatLock (false);
                comm.addFragment (new Frag_SelectMessageType ());*/


            }
        });

        menu_my_request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menu_my_request.setBackgroundColor(getResources().getColor(R.color.selected_menu));
                menu_my_messages.setBackgroundColor(getResources().getColor(R.color.unselected_menu));
                menu_my_data.setBackgroundColor(getResources().getColor(R.color.unselected_menu));

                //   supportFragmentManager.beginTransaction ().replace (R.id.container_menu_selected, new Frag_MyOrders ()).commit ();

                vpPager.setCurrentItem(0);
            }
        });
        menu_my_messages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //supportFragmentManager.beginTransaction ().replace (R.id.container_menu_selected, new Frag_Tickets ()).commit ();

                menu_my_request.setBackgroundColor(getResources().getColor(R.color.unselected_menu));
                menu_my_messages.setBackgroundColor(getResources().getColor(R.color.selected_menu));
                menu_my_data.setBackgroundColor(getResources().getColor(R.color.unselected_menu));


                vpPager.setCurrentItem(1);

            }
        });
        menu_my_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // supportFragmentManager.beginTransaction ().replace (R.id.container_menu_selected, new Frag_Profile_MyData ()).commit ();
                menu_my_request.setBackgroundColor(getResources().getColor(R.color.unselected_menu));
                menu_my_messages.setBackgroundColor(getResources().getColor(R.color.unselected_menu));
                menu_my_data.setBackgroundColor(getResources().getColor(R.color.selected_menu));

                vpPager.setCurrentItem(2);

            }
        });


    }

    @Override
    public void onStart() {
        super.onStart();
        comm.setToolbatLock(true);

    }

    @Override
    public void onStop() {
        super.onStop();
        comm.setToolbatLock(false);
    }

    private void getMeessageNumber() {

        ApiInterface apiInterface = ApiUtils.getAPIService(getActivity());
        apiInterface.getMessagesnumber(user.getApiToken()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                String number = "0";

                if (response.isSuccessful()) {

                    if (response.body().get("number") != null) {
                        number = response.body().get("number").getAsString();
                        number_of_messages.setText(number);

                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    private void getUserDataFromServer() {

        ApiInterface apiInterface = ApiUtils.getAPIService(getActivity());
        apiInterface.getUserPorfileData(lang, user.getApiToken()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {


                if (response.isSuccessful()) {
                    JsonObject userJson = response.body();


                    if (userJson.get("subscription").getAsInt() == 1) {

                        layout_icon_account_type.setVisibility(View.VISIBLE);
                        switch (userJson.get("subscription_type").getAsInt()) {

                            case 1:
                                icon_account_type.setImageDrawable(getResources().getDrawable(R.drawable.icon_pronz));

                                break;
                            case 2:
                                icon_account_type.setImageDrawable(getResources().getDrawable(R.drawable.icon_selver));

                                break;
                            case 3:
                                icon_account_type.setImageDrawable(getResources().getDrawable(R.drawable.icon_golden));

                                break;
                            case 4:
                                icon_account_type.setImageDrawable(getResources().getDrawable(R.drawable.icon_masy));

                                break;
                        }

                        maintenance_count.setVisibility(View.VISIBLE);
                        maintenance_count.setText(userJson.get("maintenance_count").getAsString() + " " + getString(R.string.visits));


                    } else {

                        layout_icon_account_type.setVisibility(View.GONE);

                    }


                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });


    }

    public static class MyPagerAdapter extends FragmentPagerAdapter {
        private static int NUM_ITEMS = 3;

        public MyPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        // Returns total number of pages
        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        // Returns the fragment to display for that page
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0: // Fragment # 0 - This will show FirstFragment
                    return (new Frag_MyOrders());
                case 1: // Fragment # 0 - This will show FirstFragment different title
                    return (new Frag_Tickets());
                case 2: // Fragment # 1 - This will show SecondFragment
                    return (new Frag_Profile_MyData());
                default:
                    return null;
            }
        }

        // Returns the page title for the top indicator
        @Override
        public CharSequence getPageTitle(int position) {
            return "Page " + position;
        }

    }
}
