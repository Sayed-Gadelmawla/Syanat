package com.example.ibraheem.mirva.Model.Response.Notification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by ibraheem on 04/07/2017.
 */

public class Notfications {
    @SerializedName("status")
    @Expose
    private NStatus status;
    @SerializedName("data")
    @Expose
    private ArrayList<Notification> data = null;

    public NStatus getStatus() {
        return status;
    }

    public void setStatus(NStatus status) {
        this.status = status;
    }

    public ArrayList<Notification> getData() {
        return data;
    }

    public void setData(ArrayList<Notification> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Notfications{" +
                "status=" + status +
                ", data=" + data +
                '}';
    }
}
