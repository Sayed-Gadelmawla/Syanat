package com.example.ibraheem.mirva.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.example.ibraheem.mirva.CustomViews.JFButton;
import com.example.ibraheem.mirva.CustomViews.JFEditText;
import com.example.ibraheem.mirva.CustomViews.JFTextInputLayout;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.UitilsFun;
import com.example.ibraheem.mirva.util.D;
import com.example.ibraheem.mirva.util.SharedPrefs;
import com.example.ibraheem.mirva.util.V;
import com.example.ibraheem.mirva.util.helperFun;
import com.google.gson.Gson;
import com.onesignal.OSSubscriptionObserver;
import com.onesignal.OSSubscriptionStateChanges;
import com.onesignal.OneSignal;

import org.json.JSONException;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, OSSubscriptionObserver {
    public final String TAG = LoginActivity.class.getSimpleName ();
    public Intent intent;

    JFEditText user_email, user_password;
    JFButton btn_login;
    JFTextView tv_register, forget_password;
    V vd;
    D dialog;
    String lang;
    User user;
    String token;
    private ApiInterface mAPIService;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_login);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mProgressBar.setVisibility(View.INVISIBLE);
        initView ();
        OneSignal.setSubscription (true);
        lang = helperFun.getLanguage (this);

        vd = new V (LoginActivity.this);
        dialog = new D (LoginActivity.this);
        SharedPrefs.revokeShared (getApplicationContext ());

        vd.validateEmail (R.id.user_email);
        vd.validatePassword (R.id.user_password);
    }

    void initView() {
        user_email = (JFEditText) findViewById (R.id.user_email);
        user_password = (JFEditText) findViewById (R.id.user_password);

        btn_login = (JFButton) findViewById (R.id.btn_login);

        tv_register = (JFTextView) findViewById (R.id.tv_register);
        forget_password = (JFTextView) findViewById (R.id.forget_password);

        btn_login.setOnClickListener (this);
        tv_register.setOnClickListener (this);
        forget_password.setOnClickListener (this);

        UitilsFun.setLocale (getApplicationContext (), SharedPrefs.getString (getApplicationContext (), SharedPrefs.LANGUAGE));
    }

    @Override
    public void onClick(View v) {
        mAPIService = ApiUtils.getAPIService (getBaseContext ());
        switch (v.getId ()) {
            case R.id.tv_register:
                startActivity (new Intent (LoginActivity.this, RegisterActivity.class));
                break;

            case R.id.forget_password:
                startActivity (new Intent (LoginActivity.this, ForgetPassword.class));
                break;

            case R.id.btn_login:
                if (vd.isValid ()) {
                    if ((user_password.getText ().toString ().length () < 5) ) {
                        user_password.setError (getString (R.string.err_password));

                    } else if(!UitilsFun.isNetworkAvailable (this)){
                        dialog.ShowErrorInternetConnection("");
                    }else{
                        attempLogin ();
                    }
                } else {
                    Log.d (TAG, "onClick: " + "validation Error : ");
                }
                break;
        }
    }

    private void attempLogin() {
        OneSignal.idsAvailable (new OneSignal.IdsAvailableHandler () {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                if (token == null) {
                    token = userId;
                }
            }
        });

        mProgressBar.setVisibility(View.VISIBLE);

        Call<User> accessToken = mAPIService.getAccessToken (lang, token, user_email.getText ().toString (), user_password.getText ().toString ());
        accessToken.enqueue (new Callback<User> () {
            private static final String TAG = "LoginActivity";

            @Override
            public void onResponse(Call<User> call, final Response<User> response) {
                mProgressBar.setVisibility(View.INVISIBLE);

                if (response.isSuccessful ()) {
                    user = response.body ();
                    Gson gson = new Gson ();
                    SharedPrefs.save (getApplicationContext (), SharedPrefs.USER, gson.toJson (user));

                    switch (user.getType ()) {
                        case "Supervisor":
                            intent = new Intent (getApplicationContext (), MainPageManager.class);
                            break;
                        case "Technician":
                            intent = new Intent (getApplicationContext (), MainPageTechnical.class);
                            break;
                        case "Client":
                            intent = new Intent (getApplicationContext (), MainPageClient.class);
                            break;
                    }
                    intent.setFlags (Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    overridePendingTransition (R.anim.fadein, R.anim.fadeout);
                    startActivity (intent);
                    finish ();
                } else {
                    mProgressBar.setVisibility(View.INVISIBLE);
                    dialog.ShowError (getString (R.string.err_login));
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.d (TAG, "onFailure: ");
                Log.d (TAG, "onFailure: Throwable " + t.toString ());

            }
        });
    }


    @Override
    public void onOSSubscriptionChanged(OSSubscriptionStateChanges stateChanges) {
        if (!stateChanges.getFrom ().getSubscribed () &&
                stateChanges.getTo ().getSubscribed ()) {
        }

        try {
            token = stateChanges.toJSONObject ().getString ("userId");
        } catch (JSONException e) {
            e.printStackTrace ();
        }
    }
}
