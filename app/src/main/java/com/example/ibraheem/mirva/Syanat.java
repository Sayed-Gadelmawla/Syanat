package com.example.ibraheem.mirva;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.example.ibraheem.mirva.Activities.MainPageClient;
import com.example.ibraheem.mirva.Activities.MyNotificationActivity;
import com.example.ibraheem.mirva.Const.FragConst;
import com.example.ibraheem.mirva.Fragments.Client.Activity_ViewOrderClient;
import com.example.ibraheem.mirva.Fragments.Technical.Activity_ViewOrderTechnical;
import com.example.ibraheem.mirva.Fragments.common.Activity_Chat;
import com.example.ibraheem.mirva.Fragments.manager.Activity_ViewOrderManager;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.util.L;
import com.example.ibraheem.mirva.util.SharedPrefs;
import com.example.ibraheem.mirva.util.helperFun;
import com.onesignal.OSNotification;
import com.onesignal.OSNotificationAction;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

/*import com.onesignal.OSNotification;
import com.onesignal.OSNotificationAction;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;
*/

/**
 * Created by ibraheem on 13/06/2017.
 */

public class Syanat extends Application {

    public final String TAG = "Notfications";

    @Override
    public void onCreate() {
        super.onCreate ();
        OneSignal.startInit (this)
                .autoPromptLocation (false) // default call promptLocation later
                .setNotificationReceivedHandler (new ExampleNotificationReceivedHandler ())
                .setNotificationOpenedHandler (new ExampleNotificationOpenedHandler ())
                .inFocusDisplaying (OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled (true)
                .init ();

        if (helperFun.getLanguage(this).equals("")) {
            UitilsFun.setLocale(getApplicationContext(), "ar");
        }else {
            UitilsFun.setLocale(getApplicationContext(), helperFun.getLanguage(this));
        }
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }
    private class ExampleNotificationReceivedHandler implements OneSignal.NotificationReceivedHandler {

        @Override
        public void notificationReceived(OSNotification notification) {
            JSONObject data = notification.payload.additionalData;
            String notificationID = notification.payload.notificationID;
            String title = notification.payload.title;
            String body = notification.payload.body;
            String smallIcon = notification.payload.smallIcon;
            String largeIcon = notification.payload.largeIcon;
            String bigPicture = notification.payload.bigPicture;
            String smallIconAccentColor = notification.payload.smallIconAccentColor;
            String sound = notification.payload.sound;
            String ledColor = notification.payload.ledColor;
            int lockScreenVisibility = notification.payload.lockScreenVisibility;
            String groupKey = notification.payload.groupKey;
            String groupMessage = notification.payload.groupMessage;
            String fromProjectNumber = notification.payload.fromProjectNumber;
            //BackgroundImageLayout backgroundImageLayout = notification.payload.backgroundImageLayout;
            String rawPayload = notification.payload.rawPayload;

            String customKey;

            Log.i ("OneSignalExample", "NotificationID received: " + notificationID);

            if (data != null) {
                customKey = data.optString ("customkey", null);
                if (customKey != null)
                    Log.i ("OneSignalExample", "customkey set with value: " + customKey);
            }
        }
    }

    private class ExampleNotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {
        // This fires when a notification is opened by tapping on it.
        @Override
        public void notificationOpened(OSNotificationOpenResult result) {
            OSNotificationAction.ActionType actionType = result.action.type;
            JSONObject data = result.notification.payload.additionalData;
            String launchUrl = result.notification.payload.launchURL; // update docs launchUrl
            L.d (TAG, "" + data);

            String customKey;
            String openURL = null;
            Object activityToLaunch = MyNotificationActivity.class;

            if (data != null) {
                customKey = data.optString ("customkey", null);
                openURL = data.optString ("openURL", null);

                if (customKey != null)
                    Log.i ("OneSignalExample", "customkey set with value: " + customKey);

                if (openURL != null)
                    Log.i ("OneSignalExample", "openURL to webview with URL value: " + openURL);
            }

            if (actionType == OSNotificationAction.ActionType.ActionTaken) {
                Log.i ("OneSignalExample", "Button pressed with id: " + result.action.actionID);
                if (result.action.actionID.equals ("id1")) {
                    Log.i ("OneSignalExample", "button id called: " + result.action.actionID);
                    activityToLaunch = MainPageClient.class;
                } else {
                    Log.i ("OneSignalExample", "button id called: " + result.action.actionID);
                }

            }
            User user = helperFun.getUser (getApplicationContext ());
            Intent intent = null;

            try {


              /*  int REQUEST_ID = data.getInt (FragConst.REQUEST_ID, 0);
                int TICKET_ID = data.getInt (FragConst.TICKET_ID, 0);*/





                if (data.has (FragConst.REQUEST_ID)) {

                    switch (user.getType ()) {
                        case "Supervisor":
                            // intent = new Intent (getBaseContext (), MainPageManager.class);
                            intent = new Intent (getBaseContext (), Activity_ViewOrderManager.class);

                            break;
                        case "Technician":
                            //  intent = new Intent (getBaseContext (), MainPageTechnical.class);
                            intent = new Intent (getBaseContext (), Activity_ViewOrderTechnical.class);

                            break;
                        case "Client":
                            intent = new Intent (getBaseContext (), Activity_ViewOrderClient.class);

                            break;
                    }

                    intent.putExtra (FragConst.REQUEST_ID, data.getInt (FragConst.REQUEST_ID));
                    intent.setFlags (Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity (intent);
                } else if (data.has (FragConst.TICKET_ID)) {
                    intent = new Intent (getBaseContext (), Activity_Chat.class);
                    intent.putExtra (FragConst.TICKET_ID, data.getInt (FragConst.TICKET_ID));
                    intent.setFlags (Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity (intent);

                }




                // The following can be used to open an Activity of your choice.
                // Replace - getApplicationContext() - with any Android Context.
                // Intent intent = new Intent(getApplicationContext(), YourActivity.class);
                //  Intent intent = new Intent (getApplicationContext (), (Class<?>) activityToLaunch);
                // intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
              /*  intent.setFlags (Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra ("openURL", openURL);
                Log.i ("OneSignalExample", "openURL = " + openURL);
                // startActivity(intent);
                startActivity (intent);
*/
            } catch (JSONException e) {
                e.printStackTrace ();
            }


            // Add the following to your AndroidManifest.xml to prevent the launching of your main Activity
            //   if you are calling startActivity above.
     /*
        <application ...>
          <meta-data android:name="com.onesignal.NotificationOpened.DEFAULT" android:value="DISABLE" />
        </application>
     */


        }

    }


}


