package com.example.ibraheem.mirva.CustomViews;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Switch;

/**
 * Created by ibraheem on 23/05/2017.
 */

public class JFSwitch extends Switch {
    public JFSwitch(Context context) {
        super (context);
        init ();
    }

    public JFSwitch(Context context, AttributeSet attrs) {
        super (context, attrs);
        init ();
    }

    public JFSwitch(Context context, AttributeSet attrs, int defStyleAttr) {
        super (context, attrs, defStyleAttr);
        init ();
    }

    void init() {

        setTypeface (Typeface.createFromAsset (getContext ().getAssets (), "fonts/jf_flat_regular.ttf"));


    }
}
