package com.example.ibraheem.mirva.Model.Response.ManagerResonse;

/**
 * Created by ibraheem on 03/07/2017.
 */

import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Thechnican {


    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("statusNote")
    @Expose
    private Integer statusNote;
    @SerializedName("techType")
    @Expose
    private Integer techType;
    @SerializedName("status_id")
    @Expose
    private Integer status_id;
    @SerializedName("d_longitude")
    @Expose
    private String d_longitude;
    @SerializedName("d_latitude")
    @Expose
    private String d_latitude;

    public Integer getStatus_id() {
        return status_id;
    }

    public void setStatus_id(Integer status_id) {
        this.status_id = status_id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return ApiUtils.BASE_URL + image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getStatusNote() {
        return statusNote;
    }

    public void setStatusNote(Integer statusNote) {
        this.statusNote = statusNote;
    }

    public String getD_longitude() {
        return d_longitude;
    }

    public void setD_longitude(String d_longitude) {
        this.d_longitude = d_longitude;
    }

    public String getD_latitude() {
        return d_latitude;
    }

    public void setD_latitude(String d_latitude) {
        this.d_latitude = d_latitude;
    }

    public Integer getTechType() {
        return techType;
    }

    public void setTechType(Integer techType) {
        this.techType = techType;
    }
}