package com.example.ibraheem.mirva.Model.Response.Order;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by ibraheem on 01/07/2017.
 */

public class AllOrders {
    @SerializedName("status")
    @Expose
    private OrderStatus status;
    @SerializedName("data")
    @Expose
    private ArrayList<Order> data = null;

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public ArrayList<Order> getData() {
        return data;
    }

    public void setData(ArrayList<Order> data) {
        this.data = data;
    }
}
