package com.example.ibraheem.mirva.Model.Response.Order.Technical;

/**
 * Created by ibraheem on 03/07/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Requirement {


    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("requirement")
    @Expose
    private String requirement;
    @SerializedName("remaining")
    @Expose
    private String remaining;
    @SerializedName("quantity")
    @Expose
    private String quantity;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRequirement() {
        return requirement;
    }

    public void setRequirement(String requirement) {
        this.requirement = requirement;
    }

    public String getRemaining() {
        return remaining;
    }

    public void setRemaining(String remaining) {
        this.remaining = remaining;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Requirement{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", requirement='" + requirement + '\'' +
                ", remaining='" + remaining + '\'' +
                ", quantity='" + quantity + '\'' +
                '}';
    }

    public boolean equals(Object c) {
        if (!(c instanceof Requirement)) {
            return false;
        }

        Requirement that = (Requirement) c;
        return this.id.equals (that.getId ())

                ;
    }
}