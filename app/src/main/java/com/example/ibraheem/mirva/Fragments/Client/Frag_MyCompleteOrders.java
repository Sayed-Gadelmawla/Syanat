package com.example.ibraheem.mirva.Fragments.Client;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.example.ibraheem.mirva.Adapter.MyCompleteOrdersAdapter;
import com.example.ibraheem.mirva.Adapter.MyOrdersAdapter;
import com.example.ibraheem.mirva.Interfaces.Communicator;
import com.example.ibraheem.mirva.Model.Notification;
import com.example.ibraheem.mirva.Model.Response.Order.AllOrders;
import com.example.ibraheem.mirva.Model.Response.Order.Order;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.util.D;
import com.example.ibraheem.mirva.util.L;
import com.example.ibraheem.mirva.util.SessionManager;
import com.example.ibraheem.mirva.util.helperFun;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Frag_MyCompleteOrders extends Fragment {

    public final String TAG = ActivityNewOrder.class.getSimpleName();
    RecyclerView recyclerView;
    MyCompleteOrdersAdapter myOrdersAdapter;
    ArrayList<Notification> list;
    Communicator comm;
    ArrayList<Order> arrayList;
    ApiInterface apiInterface;
    User user;
    private ProgressBar mProgressBar;
    private Context mContext;
    private SessionManager mSessionManager;
    //D d;
    D d;
    private ArrayList<Order> mCompletedList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiInterface = ApiUtils.getAPIService(getActivity());
        d = new D(getActivity());
        user = helperFun.getUser(getActivity());

    }

    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_my_orders, container, false);
        mContext = getActivity();
        mSessionManager = new SessionManager(mContext);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        list = new ArrayList<>();
        arrayList = new ArrayList<>();
        mCompletedList = new ArrayList<>();

        myOrdersAdapter = new MyCompleteOrdersAdapter(view.getContext(), arrayList);
        recyclerView = (RecyclerView) view.findViewById(R.id.rv_my_orders);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(view.getContext());
        linearLayoutManager.setOrientation(LinearLayout.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(myOrdersAdapter);

        comm = (Communicator) getActivity();
        comm.setTitleToolbar(2);
        comm.selectVisibleToolbar(R.id.toolbar_clints);

        new FillAllMyOrdersAsyncTask().execute();
        return view;
    }


    public class FillAllMyOrdersAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            apiInterface.getAllUserOrder(user.getApiToken()).enqueue(new Callback<AllOrders>() {
                @Override
                public void onResponse(Call<AllOrders> call, Response<AllOrders> response) {
                    if (response.isSuccessful()) {
                        AllOrders orders = response.body();
                        mSessionManager.setMyOrders(orders);
                        Log.d(TAG, "onResponse: orders  " + orders.toString());

                        ArrayList<Order> requestData = orders.getData();
                        Log.d(TAG, "onResponse: requestData " + requestData.toString());

                        if (requestData.size() != 0) {
                            myOrdersAdapter.loadData(orders);
                        }
                        mProgressBar.setVisibility(View.INVISIBLE);
                    } else {
                        L.d(TAG, "Frag_NewOrder" + response.errorBody());

                    }
                    //     d.hide ();
                }

                @Override
                public void onFailure(Call<AllOrders> call, Throwable t) {
                    //   d.hide ();
                    d.ShowErrorInternetConnection("");
                    mProgressBar.setVisibility(View.INVISIBLE);
                }
            });
            return null;
        }


    }


}
