package com.example.ibraheem.mirva.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Toast;

import com.example.ibraheem.mirva.Const.FragConst;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.Fragments.Client.EvalutionOrder;
import com.example.ibraheem.mirva.Model.Response.EevaluteOrderData.MyEveluation;
import com.example.ibraheem.mirva.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by ibraheem on 14/05/2017.
 */

public class MyEvalutionsAdapter extends RecyclerView.Adapter<MyEvalutionsAdapter.MyHolder> {

    ArrayList<MyEveluation> myEveluationArrayList;

    Context mContext;

    public MyEvalutionsAdapter(Context mContext, ArrayList<MyEveluation> myEveluationArrayList) {
        this.myEveluationArrayList = myEveluationArrayList;
        this.mContext = mContext;
    }

    public void loadData(ArrayList<MyEveluation> list) {
        myEveluationArrayList = list;
        notifyDataSetChanged();
    }

    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_evaluation, parent, false);
        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(MyHolder holder, int position) {
        MyEveluation eveluation = myEveluationArrayList.get(position);
        Picasso.with(mContext)
                .load(eveluation.getImage())
                .placeholder(R.drawable.avatar)
                .error(R.drawable.avatar)
                .into(holder.my_evaluation_item_icon);


        holder.my_evaluation_item_since_date.setText(eveluation.getDate());
        holder.my_evaluation_item_name.setText(eveluation.getName());
        holder.my_evaluation_item_rate.setProgress((int) Float.parseFloat(eveluation.getEvaluation()));
    }

    @Override
    public int getItemCount() {
        return myEveluationArrayList.size();
    }


    class MyHolder extends RecyclerView.ViewHolder {
        public ImageView my_evaluation_item_icon;
        public JFTextView my_evaluation_item_since_date, my_evaluation_item_name;
        public RatingBar my_evaluation_item_rate;

        public MyHolder(View itemView) {
            super(itemView);

            my_evaluation_item_icon = (ImageView) itemView.findViewById(R.id.my_evaluation_item_icon);
            my_evaluation_item_since_date = (JFTextView) itemView.findViewById(R.id.my_evaluation_item_since_date);
            my_evaluation_item_rate = (RatingBar) itemView.findViewById(R.id.my_evaluation_item_rate);
            my_evaluation_item_name = (JFTextView) itemView.findViewById(R.id.my_evaluation_item_name);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mContext, EvalutionOrder.class);
                    intent.putExtra("isJustShow", true);
                    intent.putExtra(FragConst.REQUEST_ID, Integer.parseInt(myEveluationArrayList.get(getAdapterPosition()).getRequest_id()));
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    mContext.startActivity(intent);
                }
            });


        }
    }

}
