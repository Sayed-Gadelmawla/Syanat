package com.example.ibraheem.mirva.Model.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ibraheem on 21/06/2017.
 */

public class WorkingHour {

    @SerializedName("pk_i_id")
    @Expose
    private Integer id;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("fk_working_hour_id")
    @Expose
    private String day;
    @SerializedName("dt_from")
    @Expose
    private Object from;
    @SerializedName("dt_to")
    @Expose
    private Object to;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public Object getFrom() {
        return from;
    }

    public void setFrom(Object from) {
        this.from = from;
    }

    public Object getTo() {
        return to;
    }

    public void setTo(Object to) {
        this.to = to;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "WorkingHour{" +
                "id=" + id +
                ", day='" + day + '\'' +
                ", from=" + from +
                ", to=" + to +
                '}';
    }
}

