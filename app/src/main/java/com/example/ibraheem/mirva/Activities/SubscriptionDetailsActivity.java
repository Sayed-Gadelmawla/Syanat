package com.example.ibraheem.mirva.Activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.ibraheem.mirva.Const.FragConst;
import com.example.ibraheem.mirva.CustomViews.FWTextView;
import com.example.ibraheem.mirva.CustomViews.JFButton;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.Model.Subscription;
import com.example.ibraheem.mirva.Model.SubscriptionDetailsSescription;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class SubscriptionDetailsActivity extends AppCompatActivity {

    private static final String TAG = "SubscriptionDetailsActivity";
    private static final String SubscriptionTAG = "Subscription";


    private Context mContext;
    private Toolbar toolbar_view_subscription;
    private JFTextView number_of_notifications;
    private FWTextView frag_back;
    private JFButton buy_subscription ;
    private LinearLayout layout ;
    private ImageView image ;
    private RecyclerView mSubscriptionDetailsRecyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_subscription);

        mContext = this;
        findViewById(R.id.frag_notification).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), MyNotificationActivity.class));

            }
        });

        findViewById(R.id.view_order_notification_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), MyNotificationActivity.class));

            }
        });

        final Subscription subscription = (Subscription) getIntent().getSerializableExtra(SubscriptionTAG);


        toolbar_view_subscription = (Toolbar) findViewById(R.id.toolbar_view_subscription);
        number_of_notifications = (JFTextView) toolbar_view_subscription.findViewById(R.id.number_of_notifications);


        frag_back = (FWTextView) toolbar_view_subscription.findViewById(R.id.frag_back);
        frag_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        layout = (LinearLayout) findViewById(R.id.layout);
        image = (ImageView) findViewById(R.id.image);

        bindData(subscription);

        mSubscriptionDetailsRecyclerView = (RecyclerView) findViewById(R.id.subscription_detail_recycle);
        SubscriptionsAdapter adapter = new SubscriptionsAdapter(subscription.getDetails_description() );
        mSubscriptionDetailsRecyclerView.setAdapter(adapter);

        buy_subscription = (JFButton) findViewById(R.id.buy_subscription);
        buy_subscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext() , BankTransferActivity.class);
                intent.putExtra(FragConst.SUBSCRIPTION_NAME , subscription.getName());
                intent.putExtra(FragConst.SUBSCRIPTION_ID , subscription.getId());
                intent.putExtra(FragConst.SUBSCRIPTION_PRICE , subscription.getPrice());
                startActivity(intent);
            }
        });
    }

    void bindData(Subscription subscription){
        switch (subscription.getType()){
            case "bronze" :
                layout.setBackground(getResources().getDrawable(R.drawable.rounded_bronze));
                image.setBackground(getResources().getDrawable(R.drawable.rounded_bronze));
                image.setImageResource(R.drawable.bronze);
                ((TextView) findViewById(R.id.name)).setText(subscription.getName());
                ((TextView) findViewById(R.id.duration)).setText(String.format(getResources().getString(R.string.subscription_duration), subscription.getDuration()));
                ((TextView) findViewById(R.id.price)).setText(String.format(getResources().getString(R.string.subscription_price), subscription.getPrice()));
                ((TextView) findViewById(R.id.name)).setTextColor(getResources().getColor(R.color.white));
                ((TextView) findViewById(R.id.duration)).setTextColor(getResources().getColor(R.color.white));
                ((TextView) findViewById(R.id.price)).setTextColor(getResources().getColor(R.color.white));
                break;
            case "silver" :
                layout.setBackground(getResources().getDrawable(R.drawable.rounded_silver));
                image.setBackground(getResources().getDrawable(R.drawable.rounded_silver));
                image.setImageResource(R.drawable.silver);
                ((TextView) findViewById(R.id.name)).setText(subscription.getName());
                ((TextView) findViewById(R.id.duration)).setText(String.format(getResources().getString(R.string.subscription_duration), subscription.getDuration()));
                ((TextView) findViewById(R.id.price)).setText(String.format(getResources().getString(R.string.subscription_price), subscription.getPrice()));
                ((TextView) findViewById(R.id.name)).setTextColor(getResources().getColor(R.color.package_silver));
                ((TextView) findViewById(R.id.duration)).setTextColor(getResources().getColor(R.color.package_silver));
                ((TextView) findViewById(R.id.price)).setTextColor(getResources().getColor(R.color.package_silver));
                break;
            case "golden" :
                layout.setBackground(getResources().getDrawable(R.drawable.rounded_golden));
                image.setBackground(getResources().getDrawable(R.drawable.rounded_golden));
                image.setImageResource(R.drawable.gold);
                ((TextView) findViewById(R.id.name)).setText(subscription.getName());
                ((TextView) findViewById(R.id.duration)).setText(String.format(getResources().getString(R.string.subscription_duration), subscription.getDuration()));
                ((TextView) findViewById(R.id.price)).setText(String.format(getResources().getString(R.string.subscription_price), subscription.getPrice()));
                ((TextView) findViewById(R.id.name)).setTextColor(getResources().getColor(R.color.package_golden));
                ((TextView) findViewById(R.id.duration)).setTextColor(getResources().getColor(R.color.package_golden));
                ((TextView) findViewById(R.id.price)).setTextColor(getResources().getColor(R.color.package_golden));
                break;
            case "diamond" :
                layout.setBackground(getResources().getDrawable(R.drawable.rounded_diamond));
                image.setBackground(getResources().getDrawable(R.drawable.rounded_diamond));
                image.setImageResource(R.drawable.diamond);
                ((TextView) findViewById(R.id.name)).setText(subscription.getName());
                ((TextView) findViewById(R.id.duration)).setText(String.format(getResources().getString(R.string.subscription_duration), subscription.getDuration()));
                ((TextView) findViewById(R.id.price)).setText(String.format(getResources().getString(R.string.subscription_price), subscription.getPrice()));
                ((TextView) findViewById(R.id.name)).setTextColor(getResources().getColor(R.color.package_diamond));
                ((TextView) findViewById(R.id.duration)).setTextColor(getResources().getColor(R.color.package_diamond));
                ((TextView) findViewById(R.id.price)).setTextColor(getResources().getColor(R.color.package_diamond));
                break;
        }
        if(subscription.getDescription() == null){

            ((TextView) findViewById(R.id.sub_description)).setText(getResources().getString(R.string.no_description));
            ((TextView) findViewById(R.id.sub_description)).setTextSize(20);


        }else {

            ((TextView) findViewById(R.id.sub_description)).setText(subscription.getDescription());
        }
    }

    private class SubscriptionsAdapter extends RecyclerView.Adapter<SubscriptionDetailsActivity.SubscriptionsViewHolder> {
//        private int resource; // the layout we will fill it in recycler (list_item_row)
        private ArrayList<SubscriptionDetailsSescription> mItemsList; // this is the copy from MainList

        public SubscriptionsAdapter(ArrayList<SubscriptionDetailsSescription> itemList) {
            mItemsList = itemList;
//            this.resource = res;
        }

        public SubscriptionsAdapter() {
            super();
        }

        @Override
        public void onBindViewHolder(SubscriptionsViewHolder holder, int position) {
            holder.bindItem(mItemsList.get(position)); // this is the item of (kids) we will fill it in the view on the recycler
        }

        @Override
        public SubscriptionsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext()); // move the layout controller from this activity layout (mContext)
            View view = layoutInflater.inflate(viewType, parent, false); // move int resource (list_item_row)
            view.getLayoutParams().width = parent.getWidth() / 4;

            return new SubscriptionsViewHolder(view); // take parameter of the view (layout) we will fill in it in the recycler
        }

        @Override
        public int getItemViewType(final int position) {
            return R.layout.subscription_item_list_row;
        }


        @Override
        public int getItemCount() {
            return mItemsList.size();
        }
    }

    private class SubscriptionsViewHolder extends RecyclerView.ViewHolder {
        private JFTextView mNameJfTextView, mCountJfTextView;

        public SubscriptionsViewHolder(View itemView) {
            super(itemView);
            mNameJfTextView = (JFTextView) itemView.findViewById(R.id.name);
            mCountJfTextView = (JFTextView) itemView.findViewById(R.id.count);
        }

        public void bindItem(final SubscriptionDetailsSescription mSubscription) {
            mNameJfTextView.setText(mSubscription.getSub_service_name());
            mCountJfTextView.setText(String.valueOf(mSubscription.getMaintenance_count()));
        }

    }

}
