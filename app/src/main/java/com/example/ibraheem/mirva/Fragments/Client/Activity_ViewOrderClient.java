package com.example.ibraheem.mirva.Fragments.Client;

import android.app.Dialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.VideoView;

import com.example.ibraheem.mirva.Activities.MainPageClient;
import com.example.ibraheem.mirva.Activities.MyNotificationActivity;
import com.example.ibraheem.mirva.Adapter.ViewImageAdapter;
import com.example.ibraheem.mirva.Const.FragConst;
import com.example.ibraheem.mirva.CustomViews.FWTextView;
import com.example.ibraheem.mirva.CustomViews.JFButton;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.FragmentMessages.SendAcceptPayment;
import com.example.ibraheem.mirva.FragmentMessages.SendRejectPayment;
import com.example.ibraheem.mirva.Fragments.common.Activity_SelectMessages;
import com.example.ibraheem.mirva.Fragments.manager.Activity_ViewOrderManager;
import com.example.ibraheem.mirva.Interfaces.Communicator;
import com.example.ibraheem.mirva.Model.Advertisment;
import com.example.ibraheem.mirva.Model.AllAdvertisment;
import com.example.ibraheem.mirva.Model.Response.Order.ViewOrder.Request;
import com.example.ibraheem.mirva.Model.Response.Order.ViewOrder.ViewOrder;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.UitilsFun;
import com.example.ibraheem.mirva.util.D;
import com.example.ibraheem.mirva.util.V;
import com.example.ibraheem.mirva.util.helperFun;
import com.github.rtoshiro.view.video.FullscreenVideoLayout;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Activity_ViewOrderClient extends AppCompatActivity {

    Communicator comm;
    View type_order_layout;
    User user;
    JFTextView order_id, deivery_time, order_cat, order_service, order_status, sended_order_status, order_details, order_price,
            payment_done;
    FullscreenVideoLayout order_layout_video;
    LinearLayout layout_case, show_price_layout;
    MapView mMapView;
    GoogleMap googleMap;
    RecyclerView order_layout_images;
    ArrayList<String> imagePaths;
    ViewImageAdapter viewImageAdapter;
    JFTextView order_no_attachment, evaluteOrder, evelaution_done, order_completion_status;
    JFButton price_reject, price_accept, btn_order_not_done, btn_order_done;
    View send_payment_done, send_payment_reject;
    LatLng latLng;

    JFTextView c_title_toolbar, number_of_notifications;
    FWTextView frag_back;
    Toolbar toolbar_view_order;
    int selectedCase = 18;
    V vd;
    //D dialog;

    int REQUEST_ID;
    private ProgressBar mProgressBar;
    private LinearLayout mOrderDetailsLinearLayout;
    private Dialog mPlayVideoDialog;
    private JFTextView order_date;
    private String TAG = "Activity_ViewOrderClient";
    private LinearLayout completion_layout;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__view_order_client);

        mOrderDetailsLinearLayout = (LinearLayout) findViewById(R.id.order_details_linear_layout);
        mOrderDetailsLinearLayout.setVisibility(View.INVISIBLE);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        vd = new V(this);
        /*dialog = new D (getApplicationContext ());
        dialog.ShowProgress (getString (R.string.loading));*/
        user = helperFun.getUser(this);
        findViewById(R.id.frag_notification).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), MyNotificationActivity.class));

            }
        });

        findViewById(R.id.view_order_notification_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), MyNotificationActivity.class));

            }
        });
        toolbar_view_order = (Toolbar) findViewById(R.id.toolbar_view_order);
        c_title_toolbar = (JFTextView) toolbar_view_order.findViewById(R.id.c_title_toolbar);
        number_of_notifications = (JFTextView) toolbar_view_order.findViewById(R.id.number_of_notifications);

        c_title_toolbar.setText(getString(R.string.order_details));

        frag_back = (FWTextView) toolbar_view_order.findViewById(R.id.frag_back);
        frag_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Intent intent = getIntent();
        if (intent != null) {
            REQUEST_ID = intent.getIntExtra(FragConst.REQUEST_ID, -1);
        } else {
            finish();
        }


        evaluteOrder = (JFTextView) findViewById(R.id.evaluteOrder);
        evelaution_done = (JFTextView) findViewById(R.id.evelaution_done);
        order_completion_status = (JFTextView) findViewById(R.id.order_completion_status);


        type_order_layout = findViewById(R.id.type_order_layout);
        send_payment_done = findViewById(R.id.send_payment_done);
        send_payment_reject = findViewById(R.id.send_payment_reject);
        deivery_time = (JFTextView) findViewById(R.id.order_date);
        order_date = (JFTextView) findViewById(R.id.label_order_date);
        order_id = (JFTextView) findViewById(R.id.sended_order_id);
        order_status = (JFTextView) findViewById(R.id.order_status);
        order_layout_video = (FullscreenVideoLayout) findViewById(R.id.order_layout_video);
        order_layout_video.setFullscreen(false);
        order_layout_video.setVisibility(View.INVISIBLE);
        layout_case = (LinearLayout) findViewById(R.id.layout_case);
        order_cat = (JFTextView) findViewById(R.id.order_cat);
        order_service = (JFTextView) findViewById(R.id.order_service);
        mMapView = (MapView) findViewById(R.id.mapView_new_order);
        sended_order_status = (JFTextView) findViewById(R.id.sended_order_status);
        mMapView.onCreate(savedInstanceState);
        order_no_attachment = (JFTextView) findViewById(R.id.order_no_attachment);
        order_layout_images = (RecyclerView) findViewById(R.id.order_layout_images);
        imagePaths = new ArrayList<>();

        //** price **///

        show_price_layout = (LinearLayout) findViewById(R.id.show_price_layout);
        order_price = (JFTextView) findViewById(R.id.order_price);
        price_reject = (JFButton) findViewById(R.id.price_reject);
        price_accept = (JFButton) findViewById(R.id.price_accept);
        btn_order_not_done = (JFButton) findViewById(R.id.btn_order_not_done);
        completion_layout = (LinearLayout) findViewById(R.id.completion_layout);
        btn_order_done = (JFButton) findViewById(R.id.btn_order_done);
        btn_order_not_done.setVisibility(View.INVISIBLE);
        btn_order_done.setVisibility(View.INVISIBLE);
        ///
        payment_done = (JFTextView) findViewById(R.id.payment_done);
        //** price * ///


        order_details = (JFTextView) findViewById(R.id.order_details);

        viewImageAdapter = new ViewImageAdapter(this, imagePaths);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        order_layout_images.setLayoutManager(layoutManager);
        order_layout_images.setAdapter(viewImageAdapter);


        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                mMapView.onResume();           //The methods
                mMapView.onEnterAmbient(null); // that made my map work

            }
        });

        getOrderData(REQUEST_ID);

        getNotificationNumber();


    }


    void getOrderData(final int requiest_id) {

        //dialog.ShowProgress (getString (R.string.loading));

        ApiInterface apiInterface = ApiUtils.getAPIService(this);
        apiInterface.getSODClient(user.getApiToken(), requiest_id).enqueue(new Callback<ViewOrder>() {
            @Override
            public void onResponse(Call<ViewOrder> call, final Response<ViewOrder> response) {

                // dialog.hide ();
                if (response.isSuccessful()) {

                    ViewOrder viewOrder = response.body();
                    final Request request = viewOrder.getData();


                    if (request.getStatusType().equals("19")) {
                        type_order_layout.setVisibility(View.VISIBLE);
                        //deivery_time.setText(request.getEndTime());
                        deivery_time.setText(getFormatedDateTime(String.valueOf(request.getStartTime()), "HH:mm:ss", "hh:mm a") + " - " + getFormatedDateTime(String.valueOf(request.getEndTime()), "HH:mm:ss", "hh:mm a"));
                        order_status.setText(getString(R.string.type_case_schedule));

                    } else {
                        order_status.setText(getString(R.string.type_case_emergency));

                    }
                    order_details.setText(request.getDetails());

                    DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date startDate;
                    Date currentDate = new Date();
                    String startDateString;


                    switch (request.getOrder_status_type()) {


                        case 34:
                            sended_order_status.setBackground(getResources().getDrawable(R.drawable.background_under_revision));
                            sended_order_status.setText(getResources().getString(R.string.underRevision));
                            order_id.setBackground(getResources().getDrawable(R.drawable.background_under_revision));
                            order_id.setText(requiest_id + "");
                            startDateString = request.getCreated_date();
                            try {
                                startDate = df.parse(startDateString);
                                long diff = currentDate.getTime() - startDate.getTime();
                                int days = (int) (diff / (1000 * 60 * 60 * 24));
                                DateFormat outputFormat;
                                DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                if (days == 0) {
                                    outputFormat = new SimpleDateFormat("KK:mm a");
                                } else if (days > 360) {
                                    outputFormat = new SimpleDateFormat("E','MMMM dd yyyy KK:mm a");
                                } else {
                                    outputFormat = new SimpleDateFormat("E','dd MMMM  KK:mm a");
                                }
                                order_date.setText(outputFormat.format(inputFormat.parse(startDateString)));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            break;
                        case 35:
                            sended_order_status.setBackground(getResources().getDrawable(R.drawable.background_ended));
                            order_id.setBackground(getResources().getDrawable(R.drawable.background_ended));
                            sended_order_status.setText(getResources().getString(R.string.ended));
                            order_id.setText(requiest_id + "");
                            startDateString = request.getCreated_date();
                            try {
                                startDate = df.parse(startDateString);
                                long diff = currentDate.getTime() - startDate.getTime();
                                int days = (int) (diff / (1000 * 60 * 60 * 24));
                                DateFormat outputFormat;
                                DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                if (days == 0) {
                                    outputFormat = new SimpleDateFormat("KK:mm a");
                                } else if (days > 360) {
                                    outputFormat = new SimpleDateFormat("E','MMMM dd yyyy KK:mm a");
                                } else {
                                    outputFormat = new SimpleDateFormat("E','dd MMMM  KK:mm a");
                                }
                                order_date.setText(outputFormat.format(inputFormat.parse(startDateString)));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            break;
                        case 40:
                            sended_order_status.setBackground(getResources().getDrawable(R.drawable.background_under_way));
                            order_id.setBackground(getResources().getDrawable(R.drawable.background_under_way));
                            sended_order_status.setText(getResources().getString(R.string.underWay));
                            order_id.setText(requiest_id + "");
                            startDateString = request.getCreated_date();
                            try {
                                startDate = df.parse(startDateString);
                                long diff = currentDate.getTime() - startDate.getTime();
                                int days = (int) (diff / (1000 * 60 * 60 * 24));
                                DateFormat outputFormat;
                                DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                if (days == 0) {
                                    outputFormat = new SimpleDateFormat("KK:mm a");
                                } else if (days > 360) {
                                    outputFormat = new SimpleDateFormat("E','MMMM dd yyyy KK:mm a");
                                } else {
                                    outputFormat = new SimpleDateFormat("E','dd MMMM  KK:mm a");
                                }
                                order_date.setText(outputFormat.format(inputFormat.parse(startDateString)));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            break;
                        case 41:
                            sended_order_status.setBackground(getResources().getDrawable(R.drawable.background_recived));
                            order_id.setBackground(getResources().getDrawable(R.drawable.background_recived));
                            sended_order_status.setText(getResources().getString(R.string.completed));
                            order_id.setText(requiest_id + "");
                            startDateString = request.getCreated_date();
                            try {
                                startDate = df.parse(startDateString);
                                long diff = currentDate.getTime() - startDate.getTime();
                                int days = (int) (diff / (1000 * 60 * 60 * 24));
                                DateFormat outputFormat;
                                DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                if (days == 0) {
                                    outputFormat = new SimpleDateFormat("KK:mm a");
                                } else if (days > 360) {
                                    outputFormat = new SimpleDateFormat("E','MMMM dd yyyy KK:mm a");
                                } else {
                                    outputFormat = new SimpleDateFormat("E','dd MMMM  KK:mm a");
                                }
                                order_date.setText(outputFormat.format(inputFormat.parse(startDateString)));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            break;
                        case 43:
                            sended_order_status.setBackground(getResources().getDrawable(R.drawable.background_rejected));
                            order_id.setBackground(getResources().getDrawable(R.drawable.background_rejected));
                            sended_order_status.setText(getResources().getString(R.string.rejected));
                            order_id.setText(requiest_id + "");
                            startDateString = request.getCreated_date();
                            try {
                                startDate = df.parse(startDateString);
                                long diff = currentDate.getTime() - startDate.getTime();
                                int days = (int) (diff / (1000 * 60 * 60 * 24));
                                DateFormat outputFormat;
                                DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                if (days == 0) {
                                    outputFormat = new SimpleDateFormat("KK:mm a");
                                } else if (days > 360) {
                                    outputFormat = new SimpleDateFormat("E','MMMM dd yyyy KK:mm a");
                                } else {
                                    outputFormat = new SimpleDateFormat("E','dd MMMM  KK:mm a");
                                }
                                order_date.setText(outputFormat.format(inputFormat.parse(startDateString)));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            LinearLayout mRejectLayout = (LinearLayout) findViewById(R.id.reject_layout);
                            mRejectLayout.setVisibility(View.VISIBLE);
                            JFTextView mRejectJfTextView = (JFTextView) findViewById(R.id.order_reject_note);
                            mRejectJfTextView.setText(request.getRejectNote() + "");

                            break;
                        case 44:
                            sended_order_status.setBackground(getResources().getDrawable(R.drawable.background_closed));
                            order_id.setBackground(getResources().getDrawable(R.drawable.background_closed));
                            sended_order_status.setText(getResources().getString(R.string.closed));
                            order_id.setText(requiest_id + "");
                            startDateString = request.getCreated_date();
                            try {
                                startDate = df.parse(startDateString);
                                long diff = currentDate.getTime() - startDate.getTime();
                                int days = (int) (diff / (1000 * 60 * 60 * 24));
                                DateFormat outputFormat;
                                DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                if (days == 0) {
                                    outputFormat = new SimpleDateFormat("KK:mm a");
                                } else if (days > 360) {
                                    outputFormat = new SimpleDateFormat("E','MMMM dd yyyy KK:mm a");
                                } else {
                                    outputFormat = new SimpleDateFormat("E','dd MMMM  KK:mm a");
                                }
                                order_date.setText(outputFormat.format(inputFormat.parse(startDateString)));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            break;
                    }
                    // sended_order_status.setBackground (getActivity ().getResources ().getDrawable (R.drawable.background_under_revision));


                    // sended_order_status.setText (request.getStatus ());
                    order_cat.setText(request.getSubcategory());
                    order_service.setText(request.getService());

                    Log.d("requiest_id = ", requiest_id + "");

                    latLng = new LatLng(Double.parseDouble(request.getLatitude()), Double.parseDouble(request.getLongitude()));

                    if (latLng != null) {


                        googleMap.addMarker(new MarkerOptions()
                                .position(latLng)
                                .title("Location")
                                .snippet("First Marker")
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_placeholder_point)));

                        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                                .target(latLng)
                                .zoom(15.5F)
                                .build()));


                    }
                    if (request.getImages() != null && request.getImages().size() != 0) {

                        order_layout_images.setVisibility(View.VISIBLE);
                        viewImageAdapter.loadData(request.getImages());
                        //   Log.dialog (TAG, "onResponse: " + request.getImages ());

                    } else if (request.getVideos() != null && +request.getVideos().size() != 0) {
                        if (request.getVideos().get(0) != null) {
                            order_layout_video.setVisibility(View.VISIBLE);
                            ///  L.dialog (TAG, "getVideos ()" + request.getVideos ().get (0));
//                            order_layout_video.setVideoURI (Uri.parse (ApiUtils.BASE_URL + request.getVideos ().get (0)));
//                            order_layout_video.start();
//                            order_layout_video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//                                @Override
//                                public void onPrepared(MediaPlayer mediaPlayer) {
//                                    Toast.makeText(Activity_ViewOrderClient.this, "play", Toast.LENGTH_SHORT).show();
//                                    order_layout_video.start();
//                                }
//                            });
                            order_layout_video.setActivity(Activity_ViewOrderClient.this);
                            try {
                                order_layout_video.setVideoURI(Uri.parse(ApiUtils.BASE_URL + request.getVideos().get(0)));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
//                            order_layout_video.setOnTouchListener(new View.OnTouchListener() {
//                                @Override
//                                public boolean onTouch(View view, MotionEvent motionEvent) {
//                                    if(order_layout_video.isPlaying()){
//                                        order_layout_video.pause();
//                                    }else{
//                                        order_layout_video.start();
//                                    }
//
//                                    return false;
//                                }
//                            });

                        }

                    } else {
                        order_no_attachment.setVisibility(View.VISIBLE);
                    }

                   /* "34": "قيد المراجعة",
                            "35": "منتهي",
                            "40": "قيد التن
                            */
                    if (request.getStatusId() == 2) {
                        if (request.getIsSubscription() == 0) {
                            if (request.getPrice() != null && request.getIsConfirmCompletion() == -1 && request.getPrice_status() == -1) {
                                completion_layout.setVisibility(View.GONE);
                                if (request.getPrice() != null && request.getPrice_status() != 5) {
                                    show_price_layout.setVisibility(View.VISIBLE);
                                    order_price.setText(getString(R.string.SAR_p, request.getPrice()));
                                    price_accept.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            AcceptOrRecjectPrice(requiest_id, 1);
                                        }
                                    });

                                    price_reject.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            AcceptOrRecjectPrice(requiest_id, 0);

                                        }
                                    });
                                }
                            }
                        }

                        if (request.getPrice() != null && request.getIsConfirmCompletion() == -1 && request.getPrice_status() == 5) {
                            completion_layout.setVisibility(View.VISIBLE);
                            btn_order_not_done.setVisibility(View.VISIBLE);
                            btn_order_done.setVisibility(View.VISIBLE);
                            btn_order_not_done.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    ConfirmOrder(requiest_id, 0);
                                }
                            });
                            btn_order_done.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    ConfirmOrder(requiest_id, 1);
                                }
                            });
                        }
                    }
                    if (request.getStatusId() == 3) {
                        if (request.getIsSubscription() == 0) {// just show price to user not suscription
                            if (request.getPrice() == null && request.getIsConfirmCompletion() == -1 && request.getPrice_status() == null) {
                                completion_layout.setVisibility(View.GONE);

                            } else if (request.getPrice() != null && request.getIsConfirmCompletion() == -1 && request.getPrice_status() == -1) {
                                completion_layout.setVisibility(View.GONE);
                                if (request.getPrice() != null && request.getPrice_status() != 5) {
                                    show_price_layout.setVisibility(View.VISIBLE);
                                    order_price.setText(getString(R.string.SAR_p, request.getPrice()));
                                    price_accept.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            AcceptOrRecjectPrice(requiest_id, 1);
                                        }
                                    });

                                    price_reject.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            AcceptOrRecjectPrice(requiest_id, 0);

                                        }
                                    });
                                }

                            } else if (request.getPrice() == null && request.getIsConfirmCompletion() == -1 && request.getPrice_status() == 6) {
                                completion_layout.setVisibility(View.VISIBLE);
                                btn_order_not_done.setVisibility(View.VISIBLE);
                                btn_order_done.setVisibility(View.VISIBLE);
                                btn_order_not_done.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        ConfirmOrder(requiest_id, 0);
                                    }
                                });
                                btn_order_done.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        ConfirmOrder(requiest_id, 1);
                                    }
                                });
                            } else if (request.getPrice() != null && request.getIsConfirmCompletion() == -1 && request.getPrice_status() == 5) {
                                completion_layout.setVisibility(View.VISIBLE);
                                btn_order_not_done.setVisibility(View.VISIBLE);
                                btn_order_done.setVisibility(View.VISIBLE);
                                btn_order_not_done.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        ConfirmOrder(requiest_id, 0);
                                    }
                                });
                                btn_order_done.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        ConfirmOrder(requiest_id, 1);
                                    }
                                });
                            } else if (request.getPrice() == null && request.getIsConfirmCompletion() != -1 && request.getPrice_status() == 6) {
                                completion_layout.setVisibility(View.GONE);
                                show_price_layout.setVisibility(View.GONE);
                            } else if (request.getPrice() != null && request.getIsConfirmCompletion() != -1 && request.getPrice_status() == 5) {
                                completion_layout.setVisibility(View.GONE);
                                show_price_layout.setVisibility(View.GONE);
                            }
                        }



                        /*if(request.getPrice()!=null && request.getIsConfirmCompletion()==-1 && request.getPrice_status()!=null && request.getPrice_status()!=6){
                            if (request.getIsConfirmCompletion() == -1) {
                                btn_order_not_done.setVisibility(View.VISIBLE);
                                btn_order_done.setVisibility(View.VISIBLE);
                                btn_order_not_done.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        ConfirmOrder(requiest_id,0);
                                    }
                                });
                                btn_order_done.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        ConfirmOrder(requiest_id,1);
                                    }
                                });
                            } else if (request.getIsConfirmCompletion() == 1) {
                                order_completion_status.setVisibility(View.VISIBLE);
                                order_completion_status.setText(getString(R.string.completed));
                            } else if (request.getIsConfirmCompletion() == 0) {
                                order_completion_status.setVisibility(View.VISIBLE);
                                order_completion_status.setText(getString(R.string.order_not_complete));
                            }
                        }*/

                    } else if (request.getStatusId() == 4) {
                        payment_done.setVisibility(View.VISIBLE);


                        if (request.getRated() == 0) {

                            evaluteOrder.setVisibility(View.VISIBLE);

                            evaluteOrder.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {


                                    Intent intent = new Intent(getBaseContext(), EvalutionOrder.class);
                                    intent.putExtra(FragConst.REQUEST_ID, requiest_id);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                    overridePendingTransition(R.anim.fadein, R.anim.fadeout);
                                    startActivity(intent);
                                    finish();


                                }
                            });
                        } else {

                            evelaution_done.setVisibility(View.VISIBLE);
                        }
                    }
                    mProgressBar.setVisibility(View.INVISIBLE);
                    mOrderDetailsLinearLayout.setVisibility(View.VISIBLE);
                } else {


                }

            }

            @Override
            public void onFailure(Call<ViewOrder> call, Throwable t) {
                mProgressBar.setVisibility(View.INVISIBLE);
                //dialog.hide ();
                //dialog.ShowErrorInternetConnection ("");
            }
        });


    }

    void AcceptOrRecjectPrice(final int request_id, final int status) {
        //dialog.ShowProgress (getString (R.string.confirme_price));


        ApiInterface apiInterface = ApiUtils.getAPIService(this);
        apiInterface.acceptOrRejectPrice(user.getApiToken(), request_id, status).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                //   dialog.hide ();
                Log.d(TAG, response.body().toString());
                if (response.isSuccessful()) {
                    if (status == 1) {

                        //   comm.TopMessage (new SendAcceptPayment ());
                        Intent intent = new Intent(getBaseContext(), SendAcceptPayment.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
                        startActivity(intent);
                        finish();
                        //     send_payment_done.setVisibility (View.VISIBLE);

                    } else {
                        //     send_payment_reject.setVisibility (View.VISIBLE);
                        // comm.TopMessage (new SendRejectPayment ());
                        Intent intent = new Intent(getBaseContext(), SendRejectPayment.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
                        startActivity(intent);
                        finish();

                    }

                    //  helperRes.showMessageSuccess (getActivity (), response.body (), null);
                } else {

                    //dialog.ShowError (getString (R.string.error));

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                //   dialog.hide ();
                //  dialog.ShowErrorInternetConnection ("");
            }
        });


    }

    void ConfirmOrder(final int request_id, final int status) {
        //dialog.ShowProgress (getString (R.string.confirme_price));


        ApiInterface apiInterface = ApiUtils.getAPIService(this);
        apiInterface.ConfirmOrder(user.getApiToken(), request_id, status).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                //   dialog.hide ();
                if (response.isSuccessful()) {
                    if (status == 1) {
                        btn_order_not_done.setVisibility(View.GONE);
                        btn_order_done.setVisibility(View.GONE);
                        order_completion_status.setVisibility(View.VISIBLE);
                        order_completion_status.setText(getString(R.string.completed));
                    } else {
                        btn_order_not_done.setVisibility(View.GONE);
                        btn_order_done.setVisibility(View.GONE);
                        order_completion_status.setVisibility(View.VISIBLE);
                        order_completion_status.setText(getString(R.string.order_not_complete));
                        finish();
                        startActivity(new Intent(getApplicationContext(), Activity_SelectMessages.class));
                    }
                    //  helperRes.showMessageSuccess (getActivity (), response.body (), null);
                } else {

                    //dialog.ShowError (getString (R.string.error));

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                //   dialog.hide ();
                //  dialog.ShowErrorInternetConnection ("");
            }
        });


    }

    @Override
    public void onResume() {


        super.onResume();

        mMapView.onResume();
    }

    @Override
    public void onPause() {

        super.onPause();

        mMapView.onPause();
    }

    @Override
    public void onStop() {

        super.onStop();
        mMapView.onStop();
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
        mMapView.onDestroy();


    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();

        mMapView.onLowMemory();

    }


    private void getNotificationNumber() {

        ApiInterface apiInterface = ApiUtils.getAPIService(this);
        apiInterface.getNotifications(user.getApiToken()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                if (response.isSuccessful()) {
                    String number;
                    if (response.body().get("number") == null) {
                        number = "0";
                    } else {

                        number = response.body().get("number").getAsString();

                    }

                    number_of_notifications.setText(number);


                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });


    }

    public static String getFormatedDateTime(String dateStr, String strReadFormat, String strWriteFormat) {

        String formattedDate = dateStr;

        DateFormat readFormat = new SimpleDateFormat(strReadFormat, Locale.getDefault());
        DateFormat writeFormat = new SimpleDateFormat(strWriteFormat, Locale.getDefault());

        Date date = null;

        try {
            date = readFormat.parse(dateStr);
        } catch (ParseException e) {
        }

        if (date != null) {
            formattedDate = writeFormat.format(date);
        }

        return formattedDate;
    }
}
