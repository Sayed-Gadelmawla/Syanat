package com.example.ibraheem.mirva.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.ibraheem.mirva.CustomViews.JFButton;
import com.example.ibraheem.mirva.CustomViews.JFEditText;
import com.example.ibraheem.mirva.Enum.DialogAction;
import com.example.ibraheem.mirva.Model.Response.ErrorResponse.ErrorUtils;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.Model.Response.helperRes;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.UitilsFun;
import com.example.ibraheem.mirva.util.D;
import com.example.ibraheem.mirva.util.V;
import com.example.ibraheem.mirva.util.helperFun;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdatePassword extends AppCompatActivity {

    JFEditText user_password, user_password_again, user_old_password;
    D dialog;
    V vd;
    JFButton btn_bank_pay;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        dialog = new D (this);
        vd = new V (this);
        user = helperFun.getUser (this);
        setContentView (R.layout.activity_update_password);
        user_old_password = (JFEditText) findViewById (R.id.user_old_password);

        user_password = (JFEditText) findViewById (R.id.user_password);
        user_password_again = (JFEditText) findViewById (R.id.user_password_again);

        findViewById (R.id.tv_back).setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                finish ();
            }
        });

        vd.validatePassword (R.id.user_old_password);
        vd.validatePassword (R.id.user_password);
        vd.validateConfirmPassword (R.id.user_password, R.id.user_password_again);

        btn_bank_pay = (JFButton) findViewById (R.id.btn_bank_pay);

        btn_bank_pay.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                if (vd.isValid () && UitilsFun.isNetworkAvailable (getBaseContext ())) {

                    updatePassword ();
                }
            }
        });


    }

    /**
     *
     * update password send old  password  and new passord  with use token
     */
    void updatePassword() {

        dialog.ShowProgress (getString (R.string.check));

        ApiInterface apiInterface = ApiUtils.getAPIService (this);
        apiInterface.upldatePassword (user.getApiToken (), user_old_password.getText ().toString (), user_password.getText ().toString (), user_password_again.getText ().toString ()).enqueue (new Callback<JsonObject> () {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                dialog.hide ();
                if (response.isSuccessful ()) {
                    dialog.showMessageSuccess (response.body ().get ("message").getAsString (), DialogAction.CLOSE);
                } else {

                    ErrorUtils errorMessage = helperRes.getErrorMessage (response.errorBody ());
                    //  user_name.setError (errorMessage.getError ().getEmail ());
                    user_old_password.setError (getErrorMessage (errorMessage.getError ().getPassword ()));

                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                dialog.hide ();
                dialog.ShowErrorInternetConnection ("");


            }
        });

    }

    private String getErrorMessage(ArrayList<String> type) {
        return type == null ? null : type.get (0);
    }

}
