package com.example.ibraheem.mirva.Model.Response.ErrorResponse;

import android.util.Log;

import com.google.gson.Gson;

import java.io.IOException;

import okhttp3.ResponseBody;

/**
 * Created by ibraheem on 09/07/2017.
 */

public class ErrorFactory {


    public static Error getErrors(ResponseBody responseBody) {
        Gson gson = new Gson ();
        Error error = null;
        try {
            error = gson.fromJson (responseBody.string (), Error.class);


        } catch (IOException e) {
            Log.d ("ErrorFactory", "getErrors: ");
            e.printStackTrace ();
        }

        return error;
    }

}
