package com.example.ibraheem.mirva.Fragments.Client;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.ibraheem.mirva.Activities.AllSubscriptionsActivity;
import com.example.ibraheem.mirva.Activities.MainPageClient;
import com.example.ibraheem.mirva.Adapter.ServiceAdapter;
import com.example.ibraheem.mirva.CustomViews.JFButton;
import com.example.ibraheem.mirva.Fragments.common.Frag_Activation;
import com.example.ibraheem.mirva.Interfaces.Communicator;
import com.example.ibraheem.mirva.Model.Response.Service;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.util.SessionManager;
import com.example.ibraheem.mirva.util.SharedPrefs;
import com.example.ibraheem.mirva.util.helperFun;


import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static cn.pedant.SweetAlert.SweetAlertDialog.PROGRESS_TYPE;

/**
 * Created by ibraheem on 21/05/2017.
 */

public class Frag_Services extends Fragment {


    public final String TAG = Frag_Services.class.getSimpleName();
    ArrayList<Service> mServiceList;
    ServiceAdapter serviceAdapter;
    JFButton btn_premium;
    RecyclerView recyclerView;
    Toolbar toolbar;
    View view = null;
    AppCompatActivity activity;
    ApiInterface mAPIService;
    User user;
    ArrayList<Service> services;
    Service service;
    private ProgressBar mProgressBar;
    private SessionManager mSessionManager;
    private Context mContext;

    @Override
    public void onStart() {
        super.onStart();
    }


    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        setHasOptionsMenu(true);
        view = inflater.inflate(R.layout.frag_services, container, false);
        mContext = getActivity();
        mSessionManager = new SessionManager(mContext);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        if (view == null) return view;
        activity = (AppCompatActivity) getActivity();

        //   activeToolbar ();
        mServiceList = new ArrayList<>();
        recyclerView = (RecyclerView) view.findViewById(R.id.request_recyler_view);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);

        btn_premium = view.findViewById(R.id.btn_premium);

        Communicator comm = (Communicator) getActivity();
        comm.selectVisibleToolbar(R.id.toolbar_main);

        //= ProgressDialog.show (view.getContext (), "Loading", "");
        new FillServicesAsyncTask().execute();

        //  getActivity ().getSupportFragmentManager ().popBackStack (null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btn_premium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User user = helperFun.getUser ( mContext);
                if (user.getStatus () == 0) {

                    FragmentTransaction ft = ((MainPageClient) mContext).getSupportFragmentManager().beginTransaction();
                    Frag_Activation newFragment = Frag_Activation.newInstance(user.getEmail(),0);
                    newFragment.show(ft , "Dialog");

                } else {
                    Intent intent = new Intent(mContext, AllSubscriptionsActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    mContext.startActivity(intent);
                }
            }
        });
    }

    private class FillServicesAsyncTask extends AsyncTask<Void, Void, Void> {
        String lang;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            user = helperFun.getUser(getActivity().getApplicationContext());
            lang = helperFun.getLanguage(getActivity().getApplicationContext());
            Log.d(TAG, "onStart: user name" + user.getApiToken());
            mAPIService = ApiUtils.getAPIService(getActivity());
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mAPIService.getServices(lang, user.getApiToken()).enqueue(new Callback<ArrayList<Service>>() {
                @Override
                public void onResponse(Call<ArrayList<Service>> call, Response<ArrayList<Service>> response) {


                    Log.d(TAG, "onResponse: ");
                    if (response.isSuccessful()) {
                        Log.d(TAG, "onResponse: " + response.body().toString());
                        services = response.body();

                        Log.d(TAG, "onResponse:size  " + services.size());
                        mSessionManager.setServices(services, lang);

                        serviceAdapter = new ServiceAdapter(getActivity(), mServiceList);
                        recyclerView.setAdapter(serviceAdapter);
                        serviceAdapter.loadData(services);
                        mProgressBar.setVisibility(View.INVISIBLE);
                    }

                }


                @Override
                public void onFailure(Call<ArrayList<Service>> call, Throwable t) {
                    services = mSessionManager.getServices(lang);
                    if(!services.isEmpty()) {
                        serviceAdapter = new ServiceAdapter(getActivity(), mServiceList);
                        recyclerView.setAdapter(serviceAdapter);
                        serviceAdapter.loadData(services);
                    }
                    mProgressBar.setVisibility(View.INVISIBLE);
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            new FillInvertServicesAsyncTask().execute();
        }
    }

    private class FillInvertServicesAsyncTask extends AsyncTask<Void, Void, Void> {
        String lang;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            user = helperFun.getUser(getActivity().getApplicationContext());
            lang = SharedPrefs.getString(mContext, SharedPrefs.LANGUAGE);
            lang = lang.equals("en") ? "ar" : "en";
            Log.d(TAG, "onStart: user name" + user.getApiToken());
            mAPIService = ApiUtils.getAPIService(getActivity());
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mAPIService.getServices(lang, user.getApiToken()).enqueue(new Callback<ArrayList<Service>>() {
                @Override
                public void onResponse(Call<ArrayList<Service>> call, Response<ArrayList<Service>> response) {


                    Log.d(TAG, "onResponse: ");
                    if (response.isSuccessful()) {
                        Log.d(TAG, "onResponse: " + response.body().toString());
                        services = response.body();

                        Log.d(TAG, "onResponse:size  " + services.size());
                        mSessionManager.setServices(services, lang);
                    }

                }


                @Override
                public void onFailure(Call<ArrayList<Service>> call, Throwable t) {
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }


}