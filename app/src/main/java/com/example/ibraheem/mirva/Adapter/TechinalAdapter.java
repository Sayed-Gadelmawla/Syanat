package com.example.ibraheem.mirva.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.ibraheem.mirva.Activities.ActivateAccountActivity;
import com.example.ibraheem.mirva.Activities.ShowTechnicianLocation;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.Model.Response.ManagerResonse.Thechnican;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.Model.Response.helperRes;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.util.helperFun;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;
import com.nex3z.notificationbadge.NotificationBadge;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ibraheem on 28/05/2017.
 */

public class TechinalAdapter extends RecyclerView.Adapter<TechinalAdapter.MyViewHolder> {


    ArrayList<Thechnican> mList;

    Context mContext;
    int selected;
    String TAG = "TAG";
    Dialog dialog;
    String[] statuses;
    private ApiInterface mAPIService;
    User user;

    public TechinalAdapter(Context context, ArrayList<Thechnican> technicals) {
        mList = technicals;
        mContext = context;
        statuses = mContext.getResources().getStringArray(R.array.spinner_status);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        mAPIService = ApiUtils.getAPIService (mContext );
        user = helperFun.getUser(mContext);

        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_manager_search_technical, parent, false));
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder: ");
        final Thechnican techical = mList.get(position);

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });

        holder.layout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                selected = position;
                //    notifyDataSetChanged ();

                return false;
            }
        });

        holder.layout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if(techical.getD_latitude()!=null && techical.getD_longitude()!=null) {
                    mContext.startActivity(ShowTechnicianLocation.newIntent(mContext, techical.getD_latitude(), techical.getD_longitude()));
                }else{
                    Toast.makeText(mContext, mContext.getString(R.string.sorry_technician_does_not_has_location_yet)+"", Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });

        if (position == selected) {
            holder.layout.setBackground(mContext.getResources().getDrawable(R.drawable.shape_border_selected));

        } else {
            holder.layout.setBackgroundColor((mContext.getResources().getColor(R.color.colorWhite)));


        }


        Picasso.with(mContext)
                .load(techical.getImage())
                .placeholder(R.drawable.avatar)
                .error(R.drawable.avatar)
                .into(holder.tech_image);

        holder.tech_name.setText(techical.getName());

        mAPIService.getTechnicalRequests (user.getApiToken(), techical.getId()).enqueue (new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful ()) {
                    JsonObject res = response.body() ;
                    holder.mBadge.setNumber(res.get("nb_req").getAsInt());

                    Log.d("technical", String.valueOf(techical.getId()));
                    Log.d("technical", String.valueOf(response.body()));
                } else {

                }
            }
            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
            }
        });
        /*switch (techical.getStatus_id ()) {

            case 1:
                holder.tech_status.setBackground (mContext.getResources ().getDrawable (R.drawable.background_ended));
                holder.tech_status.setText (mContext.getResources ().getString (R.string.available));
                break;
            case 0:
                holder.tech_status.setBackground (mContext.getResources ().getDrawable (R.drawable.shape_voction));
                holder.tech_status.setText (mContext.getResources ().getString (R.string.not_available));
                break;
        }*/
        //holder.tech_status.setBackground (mContext.getResources ().getDrawable (R.drawable.shape_voction));

        if(techical.getStatusNote()!=null && techical.getStatusNote()!=0){
            if(techical.getStatusNote()==1){
                holder.tech_status.setText(statuses[techical.getStatusNote()]);
                holder.tech_status.setBackground(mContext.getResources ().getDrawable (R.drawable.shape_voction));
            }else{
                holder.tech_status.setText(statuses[techical.getStatusNote()]);
                holder.tech_status.setBackground(mContext.getResources ().getDrawable (R.drawable.background_under_revision));
            }
        }else {
            holder.tech_status.setText(statuses[techical.getStatusNote()]);
            holder.tech_status.setBackground(mContext.getResources().getDrawable(R.drawable.shape_voction));

        }
             holder.tech_type.setText(techical.getTechType()==1 ?mContext.getString(R.string.internal):mContext.getString(R.string.external));
   /*     holder.itemView.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                dialog.dismiss ();

            }
        });*/


    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void setDialog(android.app.Dialog dialog) {
        this.dialog = dialog;

    }

    public void loadData(ArrayList<Thechnican> thechnicen) {

        mList.addAll(thechnicen);
        notifyDataSetChanged();

    }

    public Thechnican getSelectedTechnical() {

        return mList.isEmpty() ? null : mList.get(selected);

    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        JFTextView tech_name, tech_status,tech_type;
        ImageView tech_image;
        LinearLayout layout;
        NotificationBadge mBadge;

        public MyViewHolder(View itemView) {
            super(itemView);
            tech_image = (ImageView) itemView.findViewById(R.id.tech_image);
            tech_name = (JFTextView) itemView.findViewById(R.id.tech_name);
            tech_status = (JFTextView) itemView.findViewById(R.id.tech_status);
            tech_type = (JFTextView) itemView.findViewById(R.id.tech_type);
            layout = (LinearLayout) itemView.findViewById(R.id.layout);
            mBadge = (NotificationBadge) itemView.findViewById(R.id.badge);

            Log.d(TAG, "MyViewHolder: ");

        }
    }

    /**
     * filter  technical when user select query
     *
     * @param query
     * @param techicals
     */
    public void filter(String query, ArrayList<Thechnican> techicals) {

        mList.clear();

        if (!query.isEmpty() && !query.equals("")) {
            for (int i = 0; i < techicals.size(); i++) {
                if (techicals.get(i).getName().toLowerCase().contains(query)) {
                    mList.add(techicals.get(i));
                }
            }


        } else {

            mList.addAll(techicals);
        }


        notifyDataSetChanged();

    }
}
