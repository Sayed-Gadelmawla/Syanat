package com.example.ibraheem.mirva.Enum;

/**
 * Created by ibraheem on 12/06/2017.
 */

public enum USER_TYPE {
    Supervisor,
    Technician,
    Client
}
