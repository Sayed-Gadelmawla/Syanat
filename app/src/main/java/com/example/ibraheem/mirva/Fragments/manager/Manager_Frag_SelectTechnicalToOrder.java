package com.example.ibraheem.mirva.Fragments.manager;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ViewFlipper;

import com.example.ibraheem.mirva.Adapter.RequirmentAdapter;
import com.example.ibraheem.mirva.Adapter.TechinalAdapter;
import com.example.ibraheem.mirva.Const.FragConst;
import com.example.ibraheem.mirva.CustomViews.JFButton;
import com.example.ibraheem.mirva.CustomViews.JFEditText;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.Interfaces.Communicator;
import com.example.ibraheem.mirva.Model.ManagerSendOrder;
import com.example.ibraheem.mirva.Model.Response.ManagerResonse.AllThechnican;
import com.example.ibraheem.mirva.Model.Response.ManagerResonse.Thechnican;
import com.example.ibraheem.mirva.Model.Response.Order.Technical.Requirement;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.util.D;
import com.example.ibraheem.mirva.util.V;
import com.example.ibraheem.mirva.util.helperFun;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Manager_Frag_SelectTechnicalToOrder extends Fragment {

    ///
    User user;
    D d;
    V vd;


    int REQUEST_ID;
    JFTextView general, under_supervise;
    ViewFlipper viewFlipper;
    ArrayList<Thechnican> list, list_all;
    TechinalAdapter mTechinalAdapter, mTechinalAdapter_all;
    RecyclerView recyclerView, recyclerView_all;
    String TAG = "dialog";
    android.app.Dialog dialog = null;
    JFTextView select_technical, btn_select_requirments, btn_select_technical;
    JFEditText addtional_notes;
    JFButton btn_snd_order;
    RecyclerView rv_selected_requirments;
    View not_selected_tools, transfer2technical_done;
    Communicator comm;
    Thechnican selectedTechnical;
    android.app.Dialog reqDialog;
    RecyclerView rv_requirment;
    RequirmentAdapter requirmentAdapter, selectedRequirmentAdapter;
    ArrayList<Requirement> requirements;
    JFButton select_requirments;
    ArrayList<Requirement> dataFiltered;
    View no_tools_message;
    private String lang;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        user = helperFun.getUser (getActivity ());
        lang = helperFun.getLanguage(getActivity().getApplicationContext());
        d = new D (getActivity ());
        vd = new V (getActivity ());
        d.ShowProgress (getString (R.string.loading));
        Bundle arguments = getArguments ();
        REQUEST_ID = arguments.getInt (FragConst.REQUEST_ID, 0);

        Log.d (TAG, "onCreate: REQUEST_ID : " + REQUEST_ID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        requirements = new ArrayList<> ();
        dataFiltered = new ArrayList<> ();

        View view = inflater.inflate (R.layout.fragment_manager_select_tech_to_order, container, false);
      //  transfer2technical_done = view.findViewById (R.id.transfer2technical_done);
        not_selected_tools = view.findViewById (R.id.not_selected_tools);

        rv_selected_requirments = (RecyclerView) view.findViewById (R.id.rv_selected_requirments);
        rv_selected_requirments.setLayoutManager (new LinearLayoutManager (getContext (), LinearLayout.VERTICAL, false));
        selectedRequirmentAdapter = new RequirmentAdapter (getActivity (), dataFiltered);
        rv_selected_requirments.setAdapter (selectedRequirmentAdapter);


        btn_select_requirments = (JFTextView) view.findViewById (R.id.btn_select_requirments);
        btn_select_technical = (JFTextView) view.findViewById (R.id.btn_select_technical);

        addtional_notes = (JFEditText) view.findViewById (R.id.addtional_notes);
        btn_snd_order = (JFButton) view.findViewById (R.id.btn_snd_order);

        vd.validateTextArea (R.id.addtional_notes);

        btn_snd_order.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {

                if (selectedTechnical != null) {

                    if (vd.isValid ()) {
                        sendViewedbyManagerOrder ();
                    }

                } else {
                    btn_select_technical.setError (getString (R.string.error));

                }


            }
        });




      /*  general ();
        under_supervise ();*/


        getAllThechnican ();

        getRequipments ();

        createDaialog (getContext ());

        createRequirmentDaialog (getActivity ());


        btn_select_technical.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {

                dialog.show ();

            }
        });

        btn_select_requirments.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {

                reqDialog.show ();

            }
        });

        comm = (Communicator) getActivity ();

        return view;
    }

    /**
     * create dialog for select technical to both type all and  under supervise
     * @param context
     */
    private void createDaialog(Context context) {
        dialog = new android.app.Dialog (context);
        dialog.requestWindowFeature (Window.FEATURE_NO_TITLE); //before
        dialog.getWindow ().setBackgroundDrawable (new ColorDrawable (android.graphics.Color.TRANSPARENT));


        dialog.setContentView (R.layout.fragment_dialog);
        general = (JFTextView) dialog.findViewById (R.id.general);
        under_supervise = (JFTextView) dialog.findViewById (R.id.under_supervise);
        viewFlipper = (ViewFlipper) dialog.findViewById (R.id.choose_type);
        ImageView imageView = (ImageView) dialog.findViewById (R.id.exist);
        general ();
        under_supervise ();

        mTechinalAdapter_all.setDialog (dialog);
        mTechinalAdapter.setDialog (dialog);


        imageView.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {

                if (viewFlipper.getDisplayedChild () == 0) {

                    selectedTechnical = mTechinalAdapter_all.getSelectedTechnical ();

                } else {
                    selectedTechnical = mTechinalAdapter.getSelectedTechnical ();


                }

                btn_select_technical.setText (selectedTechnical.getName ());


                dialog.dismiss ();
            }
        });


        general.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {

                general.setBackground (getContext ().getResources ().getDrawable (R.drawable.shape_dialog_selected));
                under_supervise.setBackground (getContext ().getResources ().getDrawable (R.drawable.shape_dialog));
                viewFlipper.setDisplayedChild (0);
            }
        });

        under_supervise.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                general.setBackground (getContext ().getResources ().getDrawable (R.drawable.shape_dialog));

                under_supervise.setBackground (getContext ().getResources ().getDrawable (R.drawable.shape_dialog_selected));

                viewFlipper.setDisplayedChild (1);
            }
        });

    }

    private void under_supervise() {
        list = new ArrayList<> ();

        recyclerView = (RecyclerView) dialog.findViewById (R.id.select_tech_recyler_view);
        mTechinalAdapter = new TechinalAdapter (dialog.getContext (), list);
        GridLayoutManager gridLayoutManager = new GridLayoutManager (dialog.getContext (), 3);
        gridLayoutManager.setOrientation (GridLayout.VERTICAL);
        recyclerView.setLayoutManager (gridLayoutManager);
        recyclerView.setAdapter (mTechinalAdapter);
    }

    private void general() {
        list_all = new ArrayList<> ();

        recyclerView_all = (RecyclerView) dialog.findViewById (R.id.select_tech_recyler_view_all);
        mTechinalAdapter_all = new TechinalAdapter (dialog.getContext (), list_all);
        GridLayoutManager gridLayoutManager = new GridLayoutManager (dialog.getContext (), 3);
        gridLayoutManager.setOrientation (GridLayout.VERTICAL);

        recyclerView_all.setLayoutManager (gridLayoutManager);
        recyclerView_all.setAdapter (mTechinalAdapter_all);
    }

    private void getAllThechnican() {

        ApiInterface apiInterface = ApiUtils.getAPIService (getActivity ());
        apiInterface.getAllThechnicans (user.getApiToken (),lang).enqueue (new Callback<AllThechnican> () {
            @Override
            public void onResponse(Call<AllThechnican> call, Response<AllThechnican> response) {
                if (response.isSuccessful ()) {
                    AllThechnican body = response.body ();
                    mTechinalAdapter_all.loadData (body.getAll ());
                    mTechinalAdapter.loadData (body.getSpecific ());
                }
                d.hide ();
            }

            @Override
            public void onFailure(Call<AllThechnican> call, Throwable t) {
                d.hide ();

                d.ShowErrorInternetConnection ("");

            }
        });
    }
    /**
     * create dialog for  requirement
     * @param context
     */
    private void createRequirmentDaialog(Context context) {
        reqDialog = new android.app.Dialog (context);
        reqDialog.requestWindowFeature (Window.FEATURE_NO_TITLE); //before
        reqDialog.getWindow ().setBackgroundDrawable (new ColorDrawable (android.graphics.Color.TRANSPARENT));

        reqDialog.setContentView (R.layout.dialog_requirment);
        no_tools_message = reqDialog.findViewById (R.id.no_tools_message);

        rv_requirment = (RecyclerView) reqDialog.findViewById (R.id.rv_requirment);


        rv_requirment.setLayoutManager (new LinearLayoutManager (context, LinearLayout.VERTICAL, false));
        requirmentAdapter = new RequirmentAdapter (context, requirements);
        rv_requirment.setAdapter (requirmentAdapter);

        select_requirments = (JFButton) reqDialog.findViewById (R.id.select_requirments);
        select_requirments.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {


                dataFiltered = requirmentAdapter.getSelectedRequirments ();

                for (Requirement req : requirements) {
                    Log.d (TAG, "onClick: requirements : " + req.toString ());

                }
                Log.d (TAG, "onClick: dataFiltered" + dataFiltered.toString ());

                if (!dataFiltered.isEmpty ()) {

                    not_selected_tools.setVisibility (View.GONE);

                    selectedRequirmentAdapter.loadData (dataFiltered);


                }

                reqDialog.dismiss ();
            }
        });

    }

/**
 * get super vise requirement
 */
    private void getRequipments() {

        ApiInterface apiInterface = ApiUtils.getAPIService (getActivity ());
        apiInterface.geRequirments (user.getApiToken ()).enqueue (new Callback<ArrayList<Requirement>> () {
            @Override
            public void onResponse(Call<ArrayList<Requirement>> call, Response<ArrayList<Requirement>> response) {
                if (response.isSuccessful ()) {
                    ArrayList<Requirement> requirements = response.body ();

                    if (requirements == null || requirements.isEmpty ()) {
                        no_tools_message.setVisibility (View.GONE);
                    } else {

                        Manager_Frag_SelectTechnicalToOrder.this.requirements.addAll (requirements);
                        requirmentAdapter.notifyDataSetChanged ();
                    }

                }

            }

            @Override
            public void onFailure(Call<ArrayList<Requirement>> call, Throwable t) {

            }
        });
    }

    /**
     * submit order to selected technical
     */
    public void sendViewedbyManagerOrder() {

        d.ShowProgress (getString (R.string.sending));

        ApiInterface apiInterface = ApiUtils.getAPIService (getContext ());
        ManagerSendOrder sendOrder = new ManagerSendOrder ();
        sendOrder.setNotes (addtional_notes.getText ().toString ());
        sendOrder.setRequestId ("" + REQUEST_ID);
        if (dataFiltered != null && !dataFiltered.isEmpty ()) {
            sendOrder.setRequirements (dataFiltered);

        }
        sendOrder.setTechnicianId ("" + selectedTechnical.getId ());

        apiInterface.sendViewedbyManagerOrder ("application/json", user.getApiToken (), sendOrder).enqueue (new Callback<JsonObject> () {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful ()) {

                    //  helperRes.showMessageSuccess (getActivity (), response.body (), null);
                    Log.d (TAG, "onResponse: " + response.body ());
                    // comm.setTitleToolbar (getString (R.string.transfer_done));
                    //  transfer2technical_done.setVisibility (View.VISIBLE);

                   // comm.TopMessage (new TrasferToTechnicalDone ());
                } else {

                    Log.d (TAG, "onResponse: " + response.errorBody ());
                }

                d.hide ();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {


                d.ShowErrorInternetConnection ("");

            }
        });


    }


}
