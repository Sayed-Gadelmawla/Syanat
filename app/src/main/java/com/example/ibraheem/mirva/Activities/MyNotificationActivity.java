package com.example.ibraheem.mirva.Activities;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.example.ibraheem.mirva.Adapter.MyNotificationsAdapter;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.Model.Response.Notification.Notfications;
import com.example.ibraheem.mirva.Model.Response.Notification.Notification;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.util.D;
import com.example.ibraheem.mirva.util.helperFun;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyNotificationActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "NotificationActivity : ";
    JFTextView all, ended, delayed, underWay, recived;
    ArrayList<Integer> buttons;
    RecyclerView recyclerView;
    MyNotificationsAdapter myNotificationsAdapter;
    Toolbar toolbar_notifications;
    View btn_finish;

    View filter;
    LinearLayout linearLayout;
    D d;

    User user;
    JFTextView number_of_notifications;
    int clicked = 0;
    ArrayList<Notification> notifications;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_notifications);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        user = helperFun.getUser(getApplicationContext());
        d = new D(this);

        initView();
        //d.ShowProgress (getString (R.string.loading));


    }

    private void initView() {
        toolbar_notifications = (Toolbar) findViewById(R.id.toolbar_notifications);
        filter = findViewById(R.id.filter);
        linearLayout = (LinearLayout) findViewById(R.id.filter_layout);
        btn_finish = findViewById(R.id.btn_finish);
        btn_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        buttons = new ArrayList<>();
        all = (JFTextView) findViewById(R.id.all);
        ended = (JFTextView) findViewById(R.id.ended);
        delayed = (JFTextView) findViewById(R.id.delayed);
        recived = (JFTextView) findViewById(R.id.recived);
        underWay = (JFTextView) findViewById(R.id.underWay);

        notifications = new ArrayList<>();

        number_of_notifications = (JFTextView) toolbar_notifications.findViewById(R.id.number_of_notifications);

        myNotificationsAdapter = new MyNotificationsAdapter(this, notifications);
        recyclerView = (RecyclerView) findViewById(R.id.rv_my_notifications);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayout.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);

        recyclerView.setAdapter(myNotificationsAdapter);

        all.setOnClickListener(this);
        ended.setOnClickListener(this);
        delayed.setOnClickListener(this);
        underWay.setOnClickListener(this);
        recived.setOnClickListener(this);

        buttons.add(R.id.all);
        buttons.add(R.id.ended);
        buttons.add(R.id.underWay);
        buttons.add(R.id.delayed);
        buttons.add(R.id.recived);


        new FillAllNotificationsAsyncTask().execute();


        all.callOnClick();
        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clicked == 0) {
                    linearLayout.setVisibility(View.VISIBLE);
                    clicked = 1;

                } else {
                    number_of_notifications.setText(String.valueOf(notifications.size()));
                    all.callOnClick();
                    clicked = 0;
                    linearLayout.setVisibility(View.GONE);

                }
            }
        });
    }

    ArrayList<Notification> filterList(ArrayList<Notification> notifications, int notification_status) {

        ArrayList<Notification> notify = new ArrayList<>();

        for (Notification n : notifications) {

            if (n.getStatusId() == notification_status) {
                notify.add(n);

            }

        }

        return notify;

    }

    @Override
    public void onClick(View v) {

        reversColor(v.getId());

        ArrayList<Notification> list;
        switch (v.getTag().toString()) {

            case "all":
                number_of_notifications.setText(String.valueOf(notifications.size()));
                myNotificationsAdapter.loadData(this.notifications);
                break;
            case "underRevision":

                list = filterList(this.notifications, 1);

                // number_of_notifications.setText (String.valueOf (list.size ()));
                myNotificationsAdapter.loadData(list);

                break;
            case "underWay":
                list = filterList(this.notifications, 3);

                // number_of_notifications.setText (String.valueOf (list.size ()));
                myNotificationsAdapter.loadData(list);

                break;
            case "ended":
                list = filterList(this.notifications, 4);

                // number_of_notifications.setText (String.valueOf (list.size ()));
                myNotificationsAdapter.loadData(list);

                break;

            case "recived":
                list = filterList(this.notifications, 2);

                //  number_of_notifications.setText (String.valueOf (list.size ()));
                myNotificationsAdapter.loadData(list);
                break;


        }

    }

    /**
     * revere color for select menu item
     */
    void reversColor(int id) {

        Log.d(TAG, "reversColor: id: 0" + id);
        for (int j = 0; j < buttons.size(); j++) {
            int btn_id = buttons.get(j);
            Log.d(TAG, "reversColor: btn_id: 0" + btn_id);
            JFTextView button = (JFTextView) findViewById(btn_id);
            if (btn_id == id) {

                button.setBackground(getResources().getDrawable(R.drawable.background_notifiction_button_checked));
                button.setTextColor(getResources().getColor(R.color.colorWhite));

            } else {
                button.setBackground(getResources().getDrawable(R.drawable.background_notifiction_button_uncheck));
                button.setTextColor(getResources().getColor(R.color.colorNotificationButton));

            }


        }

    }

    /***
     *
     * get notification data from server according api token for current user
     */
    private void getNotification() {

        ApiInterface apiInterface = ApiUtils.getAPIService(this);
        apiInterface.getAllUserNotfications(user.getApiToken()).enqueue(new Callback<Notfications>() {
            @Override
            public void onResponse(Call<Notfications> call, Response<Notfications> response) {
                //d.hide ();
                mProgressBar.setVisibility(View.INVISIBLE);
                if (response.isSuccessful()) {
                    notifications = response.body().getData();

                    if (notifications != null) {
                        // number_of_notifications.setText (String.valueOf (notifications.size ()));
                    }

                    //     number_of_notifications.setText (notifications.size ());
                    myNotificationsAdapter.loadData(notifications);

                    sendNotificationSeen();

                } else {
                    Log.d(TAG, "onResponse: " + response.errorBody().toString());
                }

            }

            @Override
            public void onFailure(Call<Notfications> call, Throwable t) {
                d.ShowErrorInternetConnection("");
                mProgressBar.setVisibility(View.INVISIBLE);

            }
        });


    }

    /**
     * send to server notification  is seen
     */

    private void sendNotificationSeen() {


        ApiInterface apiInterface = ApiUtils.getAPIService(this);
        apiInterface.updateSeenNotification(user
                .getApiToken()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {


                    Log.d(TAG, "onResponse: " + response.body().toString());
                } else {
                    Log.d(TAG, "onResponse: " + response.errorBody().toString());

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {


            }
        });
    }


    public class FillAllNotificationsAsyncTask extends AsyncTask<Void,Void,Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            getNotification();
            return null;
        }
    }

}
