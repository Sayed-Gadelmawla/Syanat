package com.example.ibraheem.mirva.FragmentMessages;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ibraheem.mirva.Activities.MyNotificationActivity;
import com.example.ibraheem.mirva.CustomViews.FWTextView;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.Interfaces.Communicator;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.util.helperFun;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ibraheem on 16/07/2017.
 */

public class SendAcceptPayment extends AppCompatActivity {


    JFTextView c_title_toolbar, number_of_notifications;
    FWTextView frag_back;
    Toolbar toolbar_order;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.fragment_payment_done);
        findViewById (R.id.frag_notification).setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                startActivity (new Intent (getBaseContext (), MyNotificationActivity.class));

            }
        });
        toolbar_order = (Toolbar) findViewById (R.id.toolbar_view_order);
        c_title_toolbar = (JFTextView) toolbar_order.findViewById (R.id.c_title_toolbar);

        c_title_toolbar.setText (getString (R.string.accept));
        number_of_notifications = (JFTextView) toolbar_order.findViewById (R.id.number_of_notifications);

        frag_back = (FWTextView) toolbar_order.findViewById (R.id.frag_back);

        frag_back.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                finish ();
            }
        });
        getNotificationNumber ();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        }, 2000) ;
    }

    private void getNotificationNumber() {
        User user = helperFun.getUser (this);

        ApiInterface apiInterface = ApiUtils.getAPIService (this);
        apiInterface.getNotifications (user.getApiToken ()).enqueue (new Callback<JsonObject> () {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                if (response.isSuccessful ()) {
                    String number;
                    if (response.body ().get ("number") == null) {
                        number = "0";
                    } else {

                        number = response.body ().get ("number").getAsString ();

                    }

                    number_of_notifications.setText (number);


                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });


    }
}
