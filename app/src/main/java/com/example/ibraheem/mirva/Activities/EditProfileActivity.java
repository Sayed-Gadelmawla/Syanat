package com.example.ibraheem.mirva.Activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.example.ibraheem.mirva.CustomViews.FWTextView;
import com.example.ibraheem.mirva.CustomViews.JFButton;
import com.example.ibraheem.mirva.CustomViews.JFEditText;
import com.example.ibraheem.mirva.CustomViews.JFSpinnerAdapter;
import com.example.ibraheem.mirva.Model.Response.ErrorResponse.ErrorUtils;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.Model.Response.helperRes;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.UitilsFun;
import com.example.ibraheem.mirva.util.D;
import com.example.ibraheem.mirva.util.L;
import com.example.ibraheem.mirva.util.SessionManager;
import com.example.ibraheem.mirva.util.SharedPrefs;
import com.example.ibraheem.mirva.util.V;
import com.example.ibraheem.mirva.util.helperFun;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import com.squareup.picasso.Picasso;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ibraheem on 02/06/2017.
 */

public class EditProfileActivity extends AppCompatActivity {
    final static int RESULT_LOAD_IMAGE = 1;
    final static int REQUEST_CODE_PICKER = 101;
    public final String TAG = EditProfileActivity.class.getSimpleName();
    JFButton btn_save_edit;
    FWTextView btn_add_image;
    de.hdodenhof.circleimageview.CircleImageView user_image;
    JFEditText user_name, user_email, user_phone_number;
    FWTextView user_birthday;
    Spinner spinner_user_gender,user_status;
    int selected_gender=0,selected_status = 0;
    SweetAlertDialog sweetAlertDialog;
    String picturePath;
    MultipartBody.Part body;
    JFSpinnerAdapter genderAdapter;
    D dialog;
    V vd;
    String imageUrl;
    User user;
    private ApiInterface mAPIService;
    private String lang, apiToken;
    JFButton btn_change_password;
    private ProgressBar mProgressBar;
    private SessionManager mSessionManager;
    private RelativeLayout mStatusLayout, edit_user_birthday;
    private JFSpinnerAdapter statusAdapter;

    void initView() {
        dialog = new D(this);
        vd = new V(this);
        mSessionManager = new SessionManager(getApplicationContext());

        lang = helperFun.getLanguage(getBaseContext());
        user = helperFun.getUser(getBaseContext());

        btn_change_password = (JFButton) findViewById(R.id.btn_change_password);

        btn_change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), UpdatePassword.class);

                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                overridePendingTransition(R.anim.fadein, R.anim.fadeout);
                startActivity(intent);
                finish();

            }
        });


        Log.d(TAG, "initView: " + user.toString());
        spinner_user_gender = (Spinner) findViewById(R.id.user_gender);
        user_name = (JFEditText) findViewById(R.id.user_name);

        user_email = (JFEditText) findViewById(R.id.user_email);
        user_phone_number = (JFEditText) findViewById(R.id.user_phone_number);
        user_status = (Spinner) findViewById(R.id.user_status);
        user_birthday = (FWTextView) findViewById(R.id.user_birthday);
        mStatusLayout = (RelativeLayout) findViewById(R.id.status_layout);
        edit_user_birthday = (RelativeLayout) findViewById(R.id.user_birthday_container);
        /*if (user.getType ().equals ("Supervisor")) {
            user_email.setEnabled (false);
            user_phone_number.setEnabled (false);

        }*/
        if (user.getType().equals("Technician")) {
            mStatusLayout.setVisibility(View.VISIBLE);
        }
        user_email.setEnabled(false);
        user_phone_number.setEnabled(false);


        user_image = (de.hdodenhof.circleimageview.CircleImageView) findViewById(R.id.user_image);

        spinner_user_gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 1) {
                    selected_gender = 12;
                } else if (position == 2){
                    selected_gender = 13;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        user_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selected_status = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinner_user_gender.setSelection(0);
        user_status.setSelection(0);
        genderAdapter = new JFSpinnerAdapter(this, R.layout.item_spinner, Arrays.asList(getResources().getStringArray(R.array.spinner_gender)));
        statusAdapter = new JFSpinnerAdapter(this, R.layout.item_spinner, Arrays.asList(getResources().getStringArray(R.array.spinner_status)));
        spinner_user_gender.setAdapter(genderAdapter);
        user_status.setAdapter(statusAdapter);


        findViewById(R.id.tv_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user.getApiToken() != null) {
                    Intent intent = null;
                    switch (user.getType()) {
                        case "Supervisor":
                            intent = new Intent(EditProfileActivity.this, MainPageManager.class);

                            break;
                        case "Technician":
                            intent = new Intent(EditProfileActivity.this, MainPageTechnical.class);

                            break;
                        case "Client":
                            intent = new Intent(EditProfileActivity.this, MainPageClient.class);

                            break;

                    }

                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    overridePendingTransition(R.anim.fadein, R.anim.fadeout);
                    startActivity(intent);
                    finish();
                } else {

                    finish();
                }


            }
        });


        btn_add_image = (FWTextView) findViewById(R.id.btn_add_image);

        btn_save_edit = (JFButton) findViewById(R.id.btn_save_edit);
        btn_save_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (vd.isValid() && UitilsFun.isNetworkAvailable(EditProfileActivity.this)) {

                    updateProfileData();
                }

            }
        });
        btn_add_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent i = new Intent (Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult (i, RESULT_LOAD_IMAGE);*/
                ImagePicker.create(EditProfileActivity.this)
                        .returnAfterFirst(true) // set whether pick or camera action should return immediate result or not. For pick image only work on single mode
                        .imageTitle(getString(R.string.choose_image)) // image selection title
                        .single() // single mode
                        .showCamera(true) // show camera or not (true by default)
                        .theme(R.style.AppTheme) // must inherit ef_BaseTheme. please refer to sample
                        .start(REQUEST_CODE_PICKER); // start image picker activity with request code
            }
        });

        vd.ValidateUserName(R.id.user_name);
        vd.validateEmail(R.id.user_email);
        vd.validatePhone(R.id.user_phone_number);
        //vd.validateDate(R.id.user_birthday);

        getProfileData();

        myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                String myFormat = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                //In which you need put here
                /*SimpleDateFormat sdf = new SimpleDateFormat (myFormat, Locale.US);
                Log.d (TAG, "updateLabel: myCalendar : " + myCalendar.getTime ());*/
                user_birthday.setText(myFormat);

                //  updateLabel ();
            }

        };


        user_birthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

                new DatePickerDialog(EditProfileActivity.this, R.style.datepicker, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


    }

    Calendar myCalendar;

    private void updateLabel() {

        String myFormat = "yyyy-mm-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        Log.d(TAG, "updateLabel: myCalendar : " + myCalendar.getTime());
        user_birthday.setText(sdf.format(myCalendar.getTime()));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        initView();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        /*if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData ();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver ().query (selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst ();
            int columnIndex = cursor.getColumnIndex (filePathColumn[0]);
            picturePath = cursor.getString (columnIndex);
            user_image.setImageBitmap (BitmapFactory.decodeFile (picturePath));
        } */
        if (requestCode == REQUEST_CODE_PICKER && resultCode == RESULT_OK && data != null) {
            ArrayList<Image> images = (ArrayList<Image>) ImagePicker.getImages(data);
            picturePath = images.get(0).getPath();
            user_image.setImageBitmap(BitmapFactory.decodeFile(picturePath));
        }


    }

    void getProfileData() {


        if (user == null) {
            L.d(TAG, "user is null");
        }

        //dialog.ShowProgress (getString (R.string.loading));


        ApiInterface mAPIService = ApiUtils.getAPIService(this);


        mAPIService.getUserPorfileData(lang, user.getApiToken()).enqueue(new Callback<JsonObject>() {
            private static final String TAG = "EditProfileActivity";

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                if (response.isSuccessful()) {

                    JsonObject object = response.body();
                    mSessionManager.setMyProfile(object);
                    user_name.setText(getData(object, "name"));
                    //user_status.setText(String.valueOf(getData(object, "statusNote") == null ? "" : getData(object, "statusNote")));
                    user_email.setText(getData(object, "email"));
                    user_phone_number.setText(getData(object, "mobile"));

                    if (!object.get("gender_id").isJsonNull() && object.get("gender_id").getAsInt() != 0) {
                        spinner_user_gender.setSelection(object.get("gender_id").getAsInt() == 12 ? 1 : 2);

                    }else{
                        spinner_user_gender.setSelection(0);
                    }

                    if (!object.get("statusNote").isJsonNull()) {
                        user_status.setSelection(object.get("statusNote").getAsInt());

                    }else{
                        spinner_user_gender.setSelection(0);
                    }


                    user_birthday.setText(getData(object, "birth_date"));
                    imageUrl = getData(object, "image");
                    Picasso.with(getBaseContext())
                            .load(ApiUtils.BASE_URL + (getData(object, "image")))
                            .placeholder(R.drawable.avatar)
                            .error(R.drawable.avatar)
                            .into(user_image);
                } else {

                    ErrorUtils errorMessage = helperRes.getErrorMessage(response.errorBody());
                    //  user_name.setError (errorMessage.getError ().getEmail ());
                    user_email.setError(getErrorMessage(errorMessage.getError().getEmail()));
                    //  user_password.setError (getErrorMessage (errorMessage.getError ().getPassword ()));
                    user_phone_number.setError(getErrorMessage(errorMessage.getError().getMobile()));
                }
                //dialog.hide ();
                mProgressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d(TAG, "onResponse:JsonObject onFailure" + t.getMessage());
                mProgressBar.setVisibility(View.INVISIBLE);
                dialog.ShowErrorInternetConnection("");
                JsonObject object = mSessionManager.getMyProfile();
                user_name.setText(getData(object, "name"));
                user_email.setText(getData(object, "email"));
                user_phone_number.setText(getData(object, "mobile"));

                if (!object.get("gender_id").isJsonNull() && object.get("gender_id").getAsInt() != 0) {
                    spinner_user_gender.setSelection(object.get("gender_id").getAsInt() == 12 ? 1 : 2);

                }else{
                    spinner_user_gender.setSelection(0);
                }

                if (!object.get("statusNote").isJsonNull()) {
                    user_status.setSelection(object.get("statusNote").getAsInt());

                }else{
                    spinner_user_gender.setSelection(0);
                }
                user_birthday.setText(getData(object, "birth_date"));
                imageUrl = getData(object, "image");
                Picasso.with(getBaseContext())
                        .load(ApiUtils.BASE_URL + (getData(object, "image")))
                        .placeholder(R.drawable.avatar)
                        .error(R.drawable.avatar)
                        .into(user_image);

            }
        });

    }

    private String getErrorMessage(ArrayList<String> type) {
        return type == null ? null : type.get(0);
    }


    void updateProfileData() {


        if (picturePath != null) {
            File file = new File(picturePath);
            RequestBody requestFile =
                    RequestBody.create(MediaType.parse("multipart/form-data"), file);

            body =
                    MultipartBody.Part.createFormData("image", file.getName(), requestFile);
        }

        RequestBody userName = helperFun.getRequestBody(user_name);
        RequestBody useEmail = helperFun.getRequestBody(user_email);
        String mobile_number = user_phone_number.getText().toString();

        if (mobile_number.contains("+") ||
                mobile_number.subSequence(0, 3).equals("00")) {

        } else {

            mobile_number = "+".concat(mobile_number);
        }

        RequestBody userPhoneNumber = RequestBody.create(MediaType.parse("multipart/form-data"), mobile_number);

        // RequestBody userPhoneNumber = helperFun.getRequestBody (user_phone_number);
        if(selected_gender==0){
            Toast.makeText(this, getString(R.string.select_gender)+"", Toast.LENGTH_SHORT).show();
        }else if(selected_status == 0 && user.getType().equals("Technician")){
            Toast.makeText(this, getString(R.string.select_status)+"", Toast.LENGTH_SHORT).show();
        } else{
            dialog.ShowProgress(getString(R.string.update_data));
            ApiInterface mAPIService = ApiUtils.getAPIService(this);
            RequestBody userBirthdate = helperFun.getRequestBody(user_birthday);

            Log.d(TAG, "updateProfileData: " + user.toString());
            mAPIService.updateProfileDate(lang, user.getApiToken(), userName, useEmail, userPhoneNumber,
                    selected_gender, userBirthdate
                    , body, selected_status).enqueue(new Callback<User>() {

                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    dialog.hide();

                    if (response.isSuccessful()) {

                        User user = response.body();

                        Gson gson = new Gson();


                        SharedPrefs.replace(getApplicationContext(), SharedPrefs.USER, gson.toJson(user));

                        if (user.getApiToken() != null) {
                            Intent intent = null;
                            switch (user.getType()) {
                                case "Supervisor":
                                    intent = new Intent(EditProfileActivity.this, MainPageManager.class);

                                    break;
                                case "Technician":
                                    intent = new Intent(EditProfileActivity.this, MainPageTechnical.class);

                                    break;
                                case "Client":
                                    intent = new Intent(EditProfileActivity.this, MainPageClient.class);

                                    break;

                            }

                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            overridePendingTransition(R.anim.fadein, R.anim.fadeout);
                            dialog.showMessageSuccess(user.getMessage(), intent
                            );


                            intent = null;
                            switch (user.getType()) {
                                case "Supervisor":
                                    intent = new Intent(EditProfileActivity.this, MainPageManager.class);

                                    break;
                                case "Technician":
                                    intent = new Intent(EditProfileActivity.this, MainPageTechnical.class);

                                    break;
                                case "Client":
                                    intent = new Intent(EditProfileActivity.this, MainPageClient.class);

                                    break;

                            }
                            finish();
                            startActivity(intent);

                        } else {

                            ErrorUtils errorMessage = helperRes.getErrorMessage(response.errorBody());
                            if (errorMessage.getError() == null
                                    ) {

                            }
                        }


                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    dialog.ShowErrorInternetConnection("");

                }
            });
        }
    }

    private String getData(JsonObject object, String param) {

        if (object.get(param).isJsonNull()) {
            return "";
        }
        return object.get(param).getAsString();
    }


}




