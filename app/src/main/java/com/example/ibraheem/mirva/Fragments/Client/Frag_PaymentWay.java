package com.example.ibraheem.mirva.Fragments.Client;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.ViewFlipper;

import com.example.ibraheem.mirva.Const.FragConst;
import com.example.ibraheem.mirva.CustomViews.FWTextView;
import com.example.ibraheem.mirva.CustomViews.JFButton;
import com.example.ibraheem.mirva.CustomViews.JFEditText;
import com.example.ibraheem.mirva.CustomViews.JFSpinnerAdapter;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.Interfaces.Communicator;
import com.example.ibraheem.mirva.Model.Response.BankAccount;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.Model.Response.helperRes;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.util.helperFun;
import com.google.gson.JsonObject;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;

//import paytabs.project.PayTabActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//import paytabs.project.PayTabActivity;

public class Frag_PaymentWay extends Fragment {

    public static final int PAYPAL_REQUEST_CODE = 123;
    private static final String PAYPAL_CLIENT_ID = "AYAq3lwOQxdHwmtHSaMrlF9m4BHdbbwQCaEszHuPHfb_xscGvEmVN_fO4xI2iEUjO5uRbTgsaah9wJkh";
    //Paypal Configuration Object
    private static final PayPalConfiguration config = new PayPalConfiguration()
            // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
            // or live (ENVIRONMENT_PRODUCTION)
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
            .clientId(PAYPAL_CLIENT_ID);
    public final String TAG = Frag_PaymentWay.class.getSimpleName();
    User user;
    String lang;
    LinearLayout bankLayout, visaLayout, paypalLayout, parentPaymentLayout;
    FWTextView pay_bank_type_icon, pay_visa_type_icon, pay_paypal_icon;
    JFTextView pay_bank_type_name, pay_visa_type_name, pay_paypal_type_name;
    ViewFlipper choose_payment_way;
    Communicator comm;
    InputMethodManager inputMethodManager;
    int SUBSCRIPTION_ID;
    String SUBSCRIPTION_PRICE, SUBSCRIPTION_NAME;
    JFButton btn_pay_paypal, pay_visa;
    JFSpinnerAdapter adapterBankAccounts;
    ImageView bank_icon;
    JFTextView bank_name, bank_number, bank_ipn, bank_owner_name;
    JFEditText bank_transfer_name, bank_transfer_number;
    Spinner spinner_bankacounts;
    JFButton btn_bank_pay;
    int VISA_REQUEST_CODE = 101;
    ArrayList<BankAccount> bankAccounts;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();

        if (arguments != null) {
            SUBSCRIPTION_NAME = arguments.getString(FragConst.SUBSCRIPTION_NAME);
            SUBSCRIPTION_ID = arguments.getInt(FragConst.SUBSCRIPTION_ID);
            SUBSCRIPTION_PRICE = arguments.getString(FragConst.SUBSCRIPTION_PRICE);
        }


        Intent intent = new Intent(getActivity(), PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        getActivity().startService(intent);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View inflate = inflater.inflate(R.layout.fragment_payment_way, container, false);
        comm = (Communicator) getActivity();
        comm.selectVisibleToolbar(R.id.toolbar_clints);
        comm.setTitleToolbar(getString(R.string.select_payment_way));
        lang = helperFun.getLanguage(getActivity());
        user = helperFun.getUser(getActivity());
        init_view(inflate);
        init_action();

        inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        adapterBankAccounts = new JFSpinnerAdapter(getContext(), R.layout.item_spinner);
        spinner_bankacounts.setAdapter(adapterBankAccounts);

        return inflate;
    }

    private void init_action() {
        bankLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);

                bankLayout.setBackgroundColor(getActivity().getResources().getColor(R.color.colorPrimarylight));
                pay_bank_type_name.setTextColor(getActivity().getResources().getColor(R.color.colorWhite));
                pay_bank_type_icon.setTextColor(getActivity().getResources().getColor(R.color.colorWhite));

                visaLayout.setBackground(ContextCompat.getDrawable(getActivity(),R.drawable.border_edit_text));
                pay_visa_type_icon.setTextColor(getActivity().getResources().getColor(R.color.colorAccent));
                pay_visa_type_name.setTextColor(getActivity().getResources().getColor(R.color.colorAccent));

                paypalLayout.setBackground(ContextCompat.getDrawable(getActivity(),R.drawable.border_edit_text));
                pay_paypal_type_name.setTextColor(getActivity().getResources().getColor(R.color.colorAccent));
                pay_paypal_icon.setTextColor(getActivity().getResources().getColor(R.color.colorAccent));
                choose_payment_way.setDisplayedChild(0);

            }
        });

        visaLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);

                visaLayout.setBackgroundColor(getActivity().getResources().getColor(R.color.colorPrimarylight));
                pay_visa_type_icon.setTextColor(getActivity().getResources().getColor(R.color.colorWhite));
                pay_visa_type_name.setTextColor(getActivity().getResources().getColor(R.color.colorWhite));

                bankLayout.setBackground(ContextCompat.getDrawable(getActivity(),R.drawable.border_edit_text));
                pay_bank_type_icon.setTextColor(getActivity().getResources().getColor(R.color.colorAccent));
                pay_bank_type_name.setTextColor(getActivity().getResources().getColor(R.color.colorAccent));

                paypalLayout.setBackground(ContextCompat.getDrawable(getActivity(),R.drawable.border_edit_text));
                pay_paypal_type_name.setTextColor(getActivity().getResources().getColor(R.color.colorAccent));
                pay_paypal_icon.setTextColor(getActivity().getResources().getColor(R.color.colorAccent));
                choose_payment_way.setDisplayedChild(1);


            }
        });

        paypalLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);

                paypalLayout.setBackgroundColor(getActivity().getResources().getColor(R.color.colorPrimarylight));
                pay_paypal_icon.setTextColor(getActivity().getResources().getColor(R.color.colorWhite));
                pay_paypal_type_name.setTextColor(getActivity().getResources().getColor(R.color.colorWhite));

                bankLayout.setBackground(ContextCompat.getDrawable(getActivity(),R.drawable.border_edit_text));
                pay_bank_type_icon.setTextColor(getActivity().getResources().getColor(R.color.colorAccent));
                pay_bank_type_name.setTextColor(getActivity().getResources().getColor(R.color.colorAccent));

                visaLayout.setBackground(ContextCompat.getDrawable(getActivity(),R.drawable.border_edit_text));
                pay_visa_type_icon.setTextColor(getActivity().getResources().getColor(R.color.colorAccent));
                pay_visa_type_name.setTextColor(getActivity().getResources().getColor(R.color.colorAccent));
                choose_payment_way.setDisplayedChild(2);

            }
        });
    }

    private void init_view(View inflate) {
        choose_payment_way = (ViewFlipper) inflate.findViewById(R.id.choose_payment_way);
        bankLayout = (LinearLayout) inflate.findViewById(R.id.bankLayout);
        visaLayout = (LinearLayout) inflate.findViewById(R.id.visaLayout);
        paypalLayout = (LinearLayout) inflate.findViewById(R.id.paypalLayout);
        pay_bank_type_icon = (FWTextView) inflate.findViewById(R.id.pay_bank_type_icon);
        pay_visa_type_icon = (FWTextView) inflate.findViewById(R.id.pay_visa_type_icon);
        pay_paypal_icon = (FWTextView) inflate.findViewById(R.id.pay_paypal_icon);
        pay_bank_type_name = (JFTextView) inflate.findViewById(R.id.pay_bank_type_name);
        pay_visa_type_name = (JFTextView) inflate.findViewById(R.id.pay_visa_type_name);

        pay_paypal_type_name = (JFTextView) inflate.findViewById(R.id.pay_paypal_type_name);

        initViewBank(inflate);
        btn_pay_paypal = (JFButton) inflate.findViewById(R.id.btn_pay_paypal);
        btn_pay_paypal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PayPayPal();
            }
        });
        pay_visa = (JFButton) inflate.findViewById(R.id.pay_visa);
        pay_visa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //PayByVisa (SUBSCRIPTION_PRICE, SUBSCRIPTION_NAME, SUBSCRIPTION_ID);
            }
        });

    }

    private void initViewBank(View view) {
        bank_owner_name = (JFTextView) view.findViewById(R.id.bank_owner_name);
        bank_ipn = (JFTextView) view.findViewById(R.id.bank_ipn);
        bank_number = (JFTextView) view.findViewById(R.id.bank_number);
        bank_name = (JFTextView) view.findViewById(R.id.bank_name);
        bank_icon = (ImageView) view.findViewById(R.id.bank_icon);
        btn_bank_pay = (JFButton) view.findViewById(R.id.btn_bank_pay);

        bank_transfer_name = (JFEditText) view.findViewById(R.id.bank_transfer_name);
        bank_transfer_number = (JFEditText) view.findViewById(R.id.bank_transfer_number);

        btn_bank_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int bank_Id = bankAccounts.get(spinner_bankacounts.getSelectedItemPosition()).getId();
                String transfer_name = bank_transfer_name.getText().toString();
                String transfer_number = bank_transfer_number.getText().toString();

                if (bank_Id > 0 & !TextUtils.isEmpty(transfer_name) && !TextUtils.isEmpty(transfer_number)) {
                    PayByBank(bank_Id, transfer_name, transfer_number, SUBSCRIPTION_ID);
                }
            }
        });
        spinner_bankacounts = (Spinner) view.findViewById(R.id.spinner_bankacounts);
        spinner_bankacounts.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position != 0) {
                    BankAccount bankAccount = bankAccounts.get(position - 1);
                    bank_owner_name.setText(bankAccount.getOwner());
                    bank_ipn.setText(bankAccount.getIbanNumber());
                    bank_number.setText(bankAccount.getAccountNumber());
                    bank_name.setText(bankAccount.getBank());
                    Picasso.with(getActivity())
                            .load(bankAccount.getImage())
                            .placeholder(R.drawable.avatar)
                            .error(R.drawable.avatar)
                            .into(bank_icon);

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinner_bankacounts.setSelection(0);
        spinner_bankacounts.setAdapter(adapterBankAccounts);


    }

    @Override
    public void onDestroy() {
        //getActivity ().stopService (new Intent (getActivity (), PayPalService.class));
        super.onDestroy();


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK && data != null) {
            //If the result is from paypal
            //If the result is OK i.e. user has not canceled the payment
            if (requestCode == PAYPAL_REQUEST_CODE) {
                //Getting the payment confirmation
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                //if confirmation is not null
                if (confirm != null) {
                    try {
                        //Getting the payment details
                        String paymentDetails = confirm.toJSONObject().toString(4);
                        Log.i("paymentExample", paymentDetails);

                        JSONObject jsonDetails = new JSONObject(paymentDetails);
                        String id = jsonDetails.getJSONObject("response").getString("id");
                        String state = jsonDetails.getJSONObject("response").getString("state");
                        ApiInterface apiInterface = ApiUtils.getAPIService(getActivity());
                        apiInterface.payPayPal(user.getApiToken(), id, SUBSCRIPTION_ID).enqueue(new Callback<JsonObject>() {
                            @Override
                            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                if (response.isSuccessful()) {

                                    helperRes.showMessageSuccess(getActivity(), response.body(), null);
                                } else {
                                    helperRes.showMessageError(getActivity(), response.errorBody());


                                }
                            }

                            @Override
                            public void onFailure(Call<JsonObject> call, Throwable t) {

                            }
                        });
                    } catch (JSONException e) {
                        Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }

            } else if (requestCode == VISA_REQUEST_CODE) {

                SharedPreferences shared_prefs = getActivity().getSharedPreferences("myapp_shared", Context.MODE_PRIVATE);
                String pt_response_code = shared_prefs.getString("pt_response_code", "");
                String pt_transaction_id = shared_prefs.getString("pt_transaction_id", "");
         /*   Toast.makeText (this, "PayTabs Response Code : " + pt_response_code,
                        Toast.LENGTH_LONG).show ();
                Toast.makeText (this, "Paytabs transaction ID after payment : " +
                        pt_transaction_id, Toast.LENGTH_LONG).show ();*/

            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            Log.i("paymentExample", "The user canceled.");
        } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
            Log.i("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
        }


    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getBankAccounts();
        bankLayout.callOnClick();

    }

    void getBankAccounts() {
        adapterBankAccounts.add(getActivity().getString(R.string.select_bank));
        ApiInterface apiInterface = ApiUtils.getAPIService(getActivity());
        apiInterface.getBankAccount(lang, user.getApiToken()).enqueue(new Callback<ArrayList<BankAccount>>() {
            private static final String TAG = "Frag_PaymentWay";

            @Override
            public void onResponse(Call<ArrayList<BankAccount>> call, Response<ArrayList<BankAccount>> response) {

                if (response.isSuccessful()) {
                    bankAccounts = response.body();
                    for (int i = 0; i < bankAccounts.size(); i++) {
                        adapterBankAccounts.add(bankAccounts.get(i).getBank());
                        Log.d(TAG, "onResponse: ");
                    }

                    adapterBankAccounts.notifyDataSetChanged();
                } else {

                    helperRes.showMessageError(getActivity(), response
                            .errorBody());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<BankAccount>> call, Throwable t) {

                Log.d(TAG, "onFailure: getMessage " + t.getMessage());

            }
        });


    }

    void PayByBank(int bank_transfer_id, String transfer_name, String transfer_number, int supscription_id) {
        ApiInterface apiInterface = ApiUtils.getAPIService(getActivity());
        apiInterface.payByBankTransfer(user.getApiToken(), bank_transfer_id, transfer_name, transfer_number, supscription_id).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    helperRes.showMessageSuccess(getActivity(), response.body(), null);
                } else {
                    helperRes.showMessageError(getActivity(), response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });

    }

    void PayPayPal() {


        PayPalPayment payment = new PayPalPayment(new BigDecimal(String.valueOf(SUBSCRIPTION_PRICE)), "USD", "Simplified Coding Fee",
                PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent = new Intent(getActivity(), PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);
        startActivityForResult(intent, PAYPAL_REQUEST_CODE);

    }

    void PayByVisa(String price, String subscription, int id) {

     /* Intent intent = new Intent (getActivity (), PayBYVisa.class);
        intent.putExtra (FragConst.SUBSCRIPTION_PRICE, price);
        intent.putExtra (FragConst.SUBSCRIPTION_ID, price);
        intent.putExtra (FragConst.SUBSCRIPTION_NAME, subscription);
        startActivity (intent);*/

       /* Intent in = new Intent (getActivity (), PayTabActivity.class);
        in.putExtra ("pt_merchant_email", "info@adeksa.com"); //this a demo account for
        in.putExtra ("pt_secret_key", "hL1qwUK2OnQKo75q0oQK6p3Z0hq934alQbMm5woTTeEimKIn9y7gYafZsG8vLiQBtDXz9mt3B8lXT3k2CsiBDyA5eIBZ0rBu5z4e");//Add your Secret Key Here
        in.putExtra ("pt_transaction_title", "Mirva Subscription");
        in.putExtra ("pt_amount", price);
        in.putExtra ("pt_currency_code", "SAR"); //Use Standard 3 character ISO
        // in.putExtra ("pt_shared_prefs_name", "myapp_shared");
        in.putExtra ("pt_customer_email", user.getEmail ());
        in.putExtra ("pt_customer_phone_number", user.getMobile ());
        //in.putExtra ("pt_order_id", "1234567");
        in.putExtra ("pt_product_name", "Mirva Subscription " + subscription + "type account");
        in.putExtra ("pt_timeout_in_seconds", "3000"); //Optional
//Billing Address
      /*  in.putExtra ("pt_address_billing", "Flat 1,Building 123, Road 2345");
        in.putExtra ("pt_city_billing", "Juffair");
        in.putExtra ("pt_state_billing", "Manama");
        in.putExtra ("pt_country_billing", "Bahrain");
        in.putExtra ("pt_postal_code_billing", "00973"); //Put Country Phone code if Postal
        code not available '00973'*/
//Shipping Address
      /*  in.putExtra ("pt_address_shipping", "Flat 1,Building 123, Road 2345");
        in.putExtra ("pt_city_shipping", "Juffair");
        in.putExtra ("pt_state_shipping", "Manama");
        in.putExtra ("pt_country_shipping", "Bahrain");
        in.putExtra ("pt_postal_code_shipping", "00973"); //Put Country Phone code if Postal
*/

        //startActivityForResult (in, VISA_REQUEST_CODE);


    }


}
