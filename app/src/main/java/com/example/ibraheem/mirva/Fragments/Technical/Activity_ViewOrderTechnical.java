package com.example.ibraheem.mirva.Fragments.Technical;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.VideoView;

import com.example.ibraheem.mirva.Activities.MyNotificationActivity;
import com.example.ibraheem.mirva.Adapter.ViewImageAdapter;
import com.example.ibraheem.mirva.Const.FragConst;
import com.example.ibraheem.mirva.CustomViews.FWTextView;
import com.example.ibraheem.mirva.CustomViews.JFButton;
import com.example.ibraheem.mirva.CustomViews.JFEditText;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.FragmentMessages.JobDone;
import com.example.ibraheem.mirva.FragmentMessages.TrasferPriceToClientDone;
import com.example.ibraheem.mirva.Fragments.manager.Activity_ViewOrderManager;
import com.example.ibraheem.mirva.Interfaces.Communicator;
import com.example.ibraheem.mirva.Model.Response.Order.ViewOrder.Request;
import com.example.ibraheem.mirva.Model.Response.Order.ViewOrder.ViewOrder;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.Model.Response.helperRes;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.UitilsFun;
import com.example.ibraheem.mirva.util.D;
import com.example.ibraheem.mirva.util.L;
import com.example.ibraheem.mirva.util.V;
import com.example.ibraheem.mirva.util.helperFun;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;
import me.next.tagview.TagCloudView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Activity_ViewOrderTechnical extends AppCompatActivity {

    JFTextView deivery_time, order_cat, order_service, order_status, sended_order_status, order_details, payment_done, order_supervise_note;
    VideoView order_layout_video;
    LinearLayout layout_case, show_price_layout;
    MapView mMapView;
    GoogleMap googleMap;
    RecyclerView order_layout_images;
    ArrayList<String> imagePaths;
    ViewImageAdapter mageAdapter;
    JFTextView order_no_attachment;
    View type_order_layout;
    TagCloudView order_supplies;
    JFButton price_confirm, technical_send_notes;
    JFEditText order_price, order_technical_note;
    View transfer_price2client;
    JFButton end_job;
    Communicator comm;
    LatLng latLng;
    JFTextView client_name , client_phone ;

    JFTextView c_title_toolbar, number_of_notifications;
    FWTextView frag_back;
    Toolbar toolbar_order;
    int selectedCase = 18;
    V vd;
    D dialog;
    private CircleImageView mClientImageView;

    int REQUEST_ID;
    User user;
    JFTextView job_done;
    private JFTextView order_id, order_date;
    private ProgressBar mProgressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__view_order_technical);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        vd = new V(this);
        dialog = new D(this);
       //dialog.ShowProgress(getString(R.string.loading));

        user = helperFun.getUser(getApplicationContext());

        findViewById(R.id.frag_notification).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), MyNotificationActivity.class));

            }
        });

        findViewById(R.id.view_order_notification_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), MyNotificationActivity.class));

            }
        });

        job_done = (JFTextView) findViewById(R.id.job_done);

        toolbar_order = (Toolbar) findViewById(R.id.toolbar_view_order);
        c_title_toolbar = (JFTextView) toolbar_order.findViewById(R.id.c_title_toolbar);
        number_of_notifications = (JFTextView) toolbar_order.findViewById(R.id.number_of_notifications);

        c_title_toolbar.setText(getString(R.string.order_details));

        frag_back = (FWTextView) toolbar_order.findViewById(R.id.frag_back);

        frag_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Intent intent = getIntent();
        if (intent != null) {
            REQUEST_ID = intent.getIntExtra(FragConst.REQUEST_ID, -1);
        } else {
            finish();
        }


        transfer_price2client = findViewById(R.id.transfer_price2client);
        end_job = (JFButton) findViewById(R.id.end_job);

        type_order_layout = findViewById(R.id.type_order_layout);
        deivery_time = (JFTextView) findViewById(R.id.order_date);
        order_id = (JFTextView) findViewById(R.id.sended_order_id);

        client_name = (JFTextView) findViewById(R.id.client_name);
        client_phone = (JFTextView) findViewById(R.id.client_mobile);
        mClientImageView = (CircleImageView) findViewById(R.id.user_image);

        order_date = (JFTextView) findViewById(R.id.label_order_date);
        order_status = (JFTextView) findViewById(R.id.order_status);
        order_layout_video = (VideoView) findViewById(R.id.order_layout_video);
        layout_case = (LinearLayout) findViewById(R.id.layout_case);
        order_cat = (JFTextView) findViewById(R.id.order_cat);
        order_service = (JFTextView) findViewById(R.id.order_service);
        mMapView = (MapView) findViewById(R.id.mapView_new_order);
        sended_order_status = (JFTextView) findViewById(R.id.sended_order_status);
        mMapView.onCreate(savedInstanceState);
        order_no_attachment = (JFTextView) findViewById(R.id.order_no_attachment);
        order_layout_images = (RecyclerView) findViewById(R.id.order_layout_images);

        order_supervise_note = (JFTextView) findViewById(R.id.order_supervise_note);
        order_technical_note = (JFEditText) findViewById(R.id.order_technical_note);
        order_technical_note.setEnabled(false);
        order_details = (JFTextView) findViewById(R.id.order_details);

        imagePaths = new ArrayList<>();
        mageAdapter = new ViewImageAdapter(this, imagePaths);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        order_layout_images.setLayoutManager(layoutManager);
        order_layout_images.setAdapter(mageAdapter);
        //

        order_supplies = (TagCloudView) findViewById(R.id.order_supplies);


        ///


        //** price **///

        show_price_layout = (LinearLayout) findViewById(R.id.show_price_layout);
        order_price = (JFEditText) findViewById(R.id.order_price);
        price_confirm = (JFButton) findViewById(R.id.price_confirm);
        technical_send_notes = (JFButton) findViewById(R.id.send_technical_notes);
        technical_send_notes.setVisibility(View.INVISIBLE);
        ///
        payment_done = (JFTextView) findViewById(R.id.payment_done);
        //** price * ///


        ///
     /*   tag_cloud_setOnTagClickListener (new TagCloudView.OnTagClickListener () {
            @Override
            public void onTagClick(int position) {

            }
        });
        tag_cloud_setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View  {

            }
        });*/


        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;
                mMapView.onResume();           //The methods
                //    mMapView.onEnterAmbient (null); // that made my map work

            }
        });


        if (REQUEST_ID != 0) {
            getOrderData(REQUEST_ID);
        } else {
            dialog.ShowError("");
        }
        getNotificationNumber();

    }

    /**
     * get order view for technical
     *
     * @param REQUEST_ID
     */
    void getOrderData(final int REQUEST_ID) {

        ApiInterface apiInterface = ApiUtils.getAPIService(this);
        apiInterface.getTechSpecificOrderData(user.getApiToken(), REQUEST_ID).enqueue(new Callback<ViewOrder>() {
            @Override
            public void onResponse(Call<ViewOrder> call, Response<ViewOrder> response) {

                if (response.isSuccessful()) {
                    mProgressBar.setVisibility(View.INVISIBLE);
                    ViewOrder viewOrder = response.body();

                    final Request request = viewOrder.getData();
                    //Log.d (TAG, "onResponse: " + request);

                    Log.d ("getMobile " , viewOrder.getUser().getMobile());
                    Log.d ("getNmae " , viewOrder.getUser().getName());


                    if (request.getStatusType().equals("19")) {
                        type_order_layout.setVisibility(View.VISIBLE);
                        deivery_time.setText(request.getEndTime());
                        order_status.setText(getString(R.string.type_case_schedule));
                    } else {
                        order_status.setText(getString(R.string.type_case_emergency));

                    }
                    order_details.setText(request.getDetails());

                    client_name.setText(viewOrder.getUser().getName());
                    client_phone.setText(viewOrder.getUser().getMobile());

                    Picasso.with (Activity_ViewOrderTechnical.this)
                            .load (viewOrder.getUser().getImage ())
                            .placeholder (R.drawable.avatar)
                            .error (R.drawable.avatar)
                            .into (mClientImageView);
                    DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date startDate;
                    Date currentDate = new Date();
                    String startDateString;
                    switch (request.getOrder_status_type()) {
                        case 34:
                            sended_order_status.setBackground(getResources().getDrawable(R.drawable.background_under_revision));
                            sended_order_status.setText(getResources().getString(R.string.underRevision));
                            order_id.setBackground(getResources().getDrawable(R.drawable.background_under_revision));
                            order_id.setText(String.valueOf(REQUEST_ID));
                            //order_date.setBackground(getResources().getDrawable(R.drawable.shap_profile_word));
                            startDateString = request.getCreated_date();
                            try {
                                startDate = df.parse(startDateString);
                                long diff = currentDate.getTime() - startDate.getTime();
                                int days = (int) (diff / (1000 * 60 * 60 * 24));
                                DateFormat outputFormat;
                                DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                if (days == 0) {
                                    outputFormat = new SimpleDateFormat("KK:mm a");
                                } else if (days > 360) {
                                    outputFormat = new SimpleDateFormat("E','MMMM dd yyyy KK:mm a");
                                } else {
                                    outputFormat = new SimpleDateFormat("E','dd MMMM  KK:mm a");
                                }
                                order_date.setText(outputFormat.format(inputFormat.parse(startDateString)));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            break;
                        case 40:
                            sended_order_status.setBackground(getResources().getDrawable(R.drawable.background_under_way));
                            sended_order_status.setText(getResources().getString(R.string.underWay));
                            order_id.setBackground(getResources().getDrawable(R.drawable.background_under_way));
                            order_id.setText(String.valueOf(REQUEST_ID));
                            //order_date.setBackground(getResources().getDrawable(R.drawable.shap_profile_word));
                            startDateString = request.getCreated_date();
                            try {
                                startDate = df.parse(startDateString);
                                long diff = currentDate.getTime() - startDate.getTime();
                                int days = (int) (diff / (1000 * 60 * 60 * 24));
                                DateFormat outputFormat;
                                DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                if (days == 0) {
                                    outputFormat = new SimpleDateFormat("KK:mm a");
                                } else if (days > 360) {
                                    outputFormat = new SimpleDateFormat("E','MMMM dd yyyy KK:mm a");
                                } else {
                                    outputFormat = new SimpleDateFormat("E','dd MMMM  KK:mm a");
                                }
                                order_date.setText(outputFormat.format(inputFormat.parse(startDateString)));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            break;

                        case 41:
                            sended_order_status.setBackground(getResources().getDrawable(R.drawable.background_recived));
                            sended_order_status.setText(getResources().getString(R.string.completed));
                            order_id.setBackground(getResources().getDrawable(R.drawable.background_recived));
                            order_id.setText(String.valueOf(REQUEST_ID));
                            //order_date.setBackground(getResources().getDrawable(R.drawable.shap_profile_word));
                            startDateString = request.getCreated_date();
                            try {
                                startDate = df.parse(startDateString);
                                long diff = currentDate.getTime() - startDate.getTime();
                                int days = (int) (diff / (1000 * 60 * 60 * 24));
                                DateFormat outputFormat;
                                DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                if (days == 0) {
                                    outputFormat = new SimpleDateFormat("KK:mm a");
                                } else if (days > 360) {
                                    outputFormat = new SimpleDateFormat("E','MMMM dd yyyy KK:mm a");
                                } else {
                                    outputFormat = new SimpleDateFormat("E','dd MMMM  KK:mm a");
                                }
                                order_date.setText(outputFormat.format(inputFormat.parse(startDateString)));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            break;

                        case 44:
                            sended_order_status.setBackground(getResources().getDrawable(R.drawable.background_closed));
                            sended_order_status.setText(getResources().getString(R.string.closed));
                            order_id.setBackground(getResources().getDrawable(R.drawable.background_closed));
                            order_id.setText(String.valueOf(REQUEST_ID));
                            //order_date.setBackground(getResources().getDrawable(R.drawable.shap_profile_word));
                            startDateString = request.getCreated_date();
                            try {
                                startDate = df.parse(startDateString);
                                long diff = currentDate.getTime() - startDate.getTime();
                                int days = (int) (diff / (1000 * 60 * 60 * 24));
                                DateFormat outputFormat;
                                DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                if (days == 0) {
                                    outputFormat = new SimpleDateFormat("KK:mm a");
                                } else if (days > 360) {
                                    outputFormat = new SimpleDateFormat("E','MMMM dd yyyy KK:mm a");
                                } else {
                                    outputFormat = new SimpleDateFormat("E','dd MMMM  KK:mm a");
                                }
                                order_date.setText(outputFormat.format(inputFormat.parse(startDateString)));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            break;

                        case 43:
                            sended_order_status.setBackground(getResources().getDrawable(R.drawable.background_rejected));
                            sended_order_status.setText(getResources().getString(R.string.rejected));
                            order_id.setBackground(getResources().getDrawable(R.drawable.background_rejected));
                            order_id.setText(String.valueOf(REQUEST_ID));
                            //order_date.setBackground(getResources().getDrawable(R.drawable.shap_profile_word));
                            startDateString = request.getCreated_date();
                            try {
                                startDate = df.parse(startDateString);
                                long diff = currentDate.getTime() - startDate.getTime();
                                int days = (int) (diff / (1000 * 60 * 60 * 24));
                                DateFormat outputFormat;
                                DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                if (days == 0) {
                                    outputFormat = new SimpleDateFormat("KK:mm a");
                                } else if (days > 360) {
                                    outputFormat = new SimpleDateFormat("E','MMMM dd yyyy KK:mm a");
                                } else {
                                    outputFormat = new SimpleDateFormat("E','dd MMMM  KK:mm a");
                                }
                                order_date.setText(outputFormat.format(inputFormat.parse(startDateString)));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            break;

                        case 35:
                            sended_order_status.setBackground(getResources().getDrawable(R.drawable.background_ended));
                            sended_order_status.setText(getResources().getString(R.string.ended));
                            order_id.setBackground(getResources().getDrawable(R.drawable.background_ended));
                            order_id.setText(String.valueOf(REQUEST_ID));
                            //order_date.setBackground(getResources().getDrawable(R.drawable.shap_profile_word));
                            startDateString = request.getCreated_date();
                            try {
                                startDate = df.parse(startDateString);
                                long diff = currentDate.getTime() - startDate.getTime();
                                int days = (int) (diff / (1000 * 60 * 60 * 24));
                                DateFormat outputFormat;
                                DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                if (days == 0) {
                                    outputFormat = new SimpleDateFormat("KK:mm a");
                                } else if (days > 360) {
                                    outputFormat = new SimpleDateFormat("E','MMMM dd yyyy KK:mm a");
                                } else {
                                    outputFormat = new SimpleDateFormat("E','dd MMMM  KK:mm a");
                                }
                                order_date.setText(outputFormat.format(inputFormat.parse(startDateString)));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            break;
                    }
                    ArrayList<String> tags = new ArrayList<>();// just show price to user not suscription

                    if (request.getRequirement() != null) {
                        for (int i = 0; i < request.getRequirement().size(); i++) {
                            tags.add(request.getRequirement().get(i).getName() + "(" + request.getRequirement().get(i).getQuantity() + ")");
                        }
                        order_supplies.setVisibility(View.VISIBLE);
                        order_supplies.setTags(tags);
                    }

                    order_cat.setText(request.getSubcategory());
                    order_service.setText(request.getService());

                    latLng = new LatLng(Double.parseDouble(request.getLatitude()), Double.parseDouble(request.getLongitude()));
                    if (latLng != null) {

                        mMapView.onResume();
                        googleMap.addMarker(new MarkerOptions()
                                .position(latLng)
                                .title("Location")
                                .snippet("First Marker")
                                .icon(BitmapDescriptorFactory.fromBitmap(UitilsFun.getBitmap(getBaseContext(), R.drawable.map_marker))));


                        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                                .target(latLng)
                                .zoom(15.5F)
                                .build()));


                    }

                    if (request.getImages() != null && request.getImages().size() != 0) {

                        order_layout_images.setVisibility(View.VISIBLE);
                        mageAdapter.loadData(request.getImages());

                    } else if (request.getVideos() != null && +request.getVideos().size() != 0) {
                        if (request.getVideos().get(0) != null) {

                            order_layout_video.setVisibility(View.VISIBLE);
                            order_layout_video.setVideoURI(Uri.parse(ApiUtils.BASE_URL + request.getVideos().get(0)));

                        }

                    } else {
                        order_no_attachment.setVisibility(View.VISIBLE);
                    }
                    order_technical_note.setEnabled(request.getTechnical_notes() != null?false:true);
                    order_supervise_note.setText(request.getSupervisor_notes() != null ? request.getSupervisor_notes() : "");
                    order_technical_note.setText(request.getTechnical_notes() != null ? request.getTechnical_notes() : "");

                    if (request.getStatusId() == 2) {
                        //show price
                        //** price **///
//                        if (request.getIsSubscription() == 0) {

                            // just show price to user not suscription
                            show_price_layout.setVisibility(View.VISIBLE);
                            end_job.setVisibility(View.VISIBLE);

                            if(request.getPrice() == null) {
                                order_price.setEnabled(true);
                            } else {

                                order_price.setEnabled(false);
                                //** price * ///


                                price_confirm.setVisibility(View.INVISIBLE);

                            }
                            order_price.setText("" + (request.getPrice() == null ? "" : request.getPrice()));

                            vd.ValidatePrice(R.id.order_price);

                            end_job.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    finishRequestByclient();

                                }
                            });


                            price_confirm.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    if (vd.isValid()) {
                                        sendPyament(REQUEST_ID, Integer.parseInt(order_price.getText().toString()));
                                    }
                                }
                            });



//                        } else if (request.getIsSubscription() == 1) {



//                        }
                        if  (request.getTechnical_notes() == null || request.getTechnical_notes().equals("")) {
                            order_technical_note.setEnabled(true);
                            technical_send_notes.setVisibility(View.VISIBLE);
                            technical_send_notes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    String note = order_technical_note.getText().toString();
                                    if (!note.trim().equals("")) {
                                        ApiInterface apiInterface = ApiUtils.getAPIService(getBaseContext());
                                        apiInterface.sendTechnicalNotes(user.getApiToken(), REQUEST_ID, order_technical_note.getText().toString()).enqueue(new Callback<JsonObject>() {
                                            @Override
                                            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                                Toast.makeText(Activity_ViewOrderTechnical.this, getString(R.string.sucess) + "", Toast.LENGTH_SHORT).show();
                                                finish();
                                            }

                                            @Override
                                            public void onFailure(Call<JsonObject> call, Throwable t) {
                                                dialog.ShowErrorInternetConnection("");
                                            }
                                        });
                                    }else
                                    {
                                        Toast.makeText(Activity_ViewOrderTechnical.this, getString(R.string.note_empty) + "", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        }

                    } else if (request.getStatusId() == 3) {
                        price_confirm.setVisibility(View.GONE);
                        order_price.setEnabled(false);

                        if (request.getIsSubscription() == 1) {
                            job_done.setVisibility(View.VISIBLE);

                        } else {

                            order_price.setEnabled(false);
                            if (request.getOrder_status_type()!=44) {
                                payment_done.setVisibility(View.VISIBLE);
                            } else {
                                payment_done.setVisibility(View.GONE);
                            }
                        }
                        if (request.getTechnical_notes() == null || request.getTechnical_notes().equals("")) {
                            order_technical_note.setEnabled(true);
                            technical_send_notes.setVisibility(View.VISIBLE);
                            technical_send_notes.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    ApiInterface apiInterface = ApiUtils.getAPIService(getBaseContext());
                                    apiInterface.sendTechnicalNotes(user.getApiToken(), REQUEST_ID, order_technical_note.getText().toString()).enqueue(new Callback<JsonObject>() {
                                        @Override
                                        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                            Toast.makeText(Activity_ViewOrderTechnical.this, getString(R.string.sucess) + "", Toast.LENGTH_SHORT).show();
                                            finish();
                                        }

                                        @Override
                                        public void onFailure(Call<JsonObject> call, Throwable t) {
                                            dialog.ShowErrorInternetConnection("");
                                        }
                                    });
                                }
                            });
                        }


                    }


//                    dialog.hide();

                } else {

                }
            }

            @Override
            public void onFailure(Call<ViewOrder> call, Throwable t) {
                //dialog.hide();
                dialog.ShowErrorInternetConnection("");
                mProgressBar.setVisibility(View.INVISIBLE);
                Log.d("getOrderData", "onFailure: " + t.getCause());
                Log.d("getOrderData", "onFailure: " + t.getMessage());

            }
        });


    }

    void sendPyament(int REQUEST_ID, int amount) {
        dialog.ShowProgress(getString(R.string.sending));
        ApiInterface apiInterface = ApiUtils.getAPIService(getBaseContext());
        apiInterface.sendPaymentToClinent(user.getApiToken(), REQUEST_ID, amount).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                dialog.hide();
                if (response.isSuccessful()) {


                    Intent intent = new Intent(getBaseContext(), TrasferPriceToClientDone.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    overridePendingTransition(R.anim.fadein, R.anim.fadeout);
                    startActivity(intent);
                    finish();


                } else {
                    helperRes.getErrorMessage(response.errorBody());
                }


            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                dialog.hide();
                dialog.ShowErrorInternetConnection("");
            }
        });

    }

    void finishRequestByclient() {

        ApiInterface apiInterface = ApiUtils.getAPIService(this);
        apiInterface.finishRequestbyClient(user.getApiToken(), REQUEST_ID).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                if (response.isSuccessful()) {

                    Intent intent = new Intent(getBaseContext(), JobDone.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    overridePendingTransition(R.anim.fadein, R.anim.fadeout);
                    startActivity(intent);
                    finish();

                    ///    helperRes.getSuccessMessage (response.body ());
                } else {
                    dialog.ShowError(getString(R.string.error));
                    ///helperRes.getErrorMessage (response.errorBody ());


                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    @Override
    public void onResume() {

        mMapView.onResume();

        super.onResume();

    }

    @Override
    public void onPause() {

        mMapView.onPause();
        super.onPause();

    }

    @Override
    public void onStop() {
        mMapView.onStop();
        super.onStop();

    }

    @Override
    public void onDestroy() {
        mMapView.onDestroy();

        super.onDestroy();


    }

    @Override
    public void onLowMemory() {
        mMapView.onLowMemory();
        super.onLowMemory();

    }

    private void getNotificationNumber() {

        ApiInterface apiInterface = ApiUtils.getAPIService(this);
        apiInterface.getNotifications(user.getApiToken()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                if (response.isSuccessful()) {
                    String number;
                    if (response.body().get("number") == null) {
                        number = "0";
                    } else {

                        number = response.body().get("number").getAsString();

                    }

                    number_of_notifications.setText(number);


                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });


    }
}
