package com.example.ibraheem.mirva.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.Model.Response.ManagerResonse.Store;
import com.example.ibraheem.mirva.Model.Response.ManagerResonse.StoreItem_StatusTypes;
import com.example.ibraheem.mirva.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by ibraheem on 29/05/2017.
 */

public class StoreItemAdapter extends RecyclerView.Adapter<StoreItemAdapter.MyViewHolder> {

    final String TAG = "Adapter";
    Context mContext;
    ArrayList<com.example.ibraheem.mirva.Model.Response.ManagerResonse.StoreItem> mLists;
    StoreItem_StatusTypes statusTypes;

    public StoreItemAdapter(Context context, ArrayList<com.example.ibraheem.mirva.Model.Response.ManagerResonse.StoreItem> items) {
        mContext = context;
        mLists = items;
        Log.d (TAG, "StoreItemAdapter: ");
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from (parent.getContext ()).inflate (R.layout.item_manager_store, parent, false);
       /* RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams (ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        view.setLayoutParams (lp);*/

        return new MyViewHolder (view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        com.example.ibraheem.mirva.Model.Response.ManagerResonse.StoreItem storeItem = mLists.get (position);

        Picasso.with (mContext)
                .load (storeItem.getImage ())
                .placeholder (R.drawable.avater_image)
                .error (R.drawable.avater_image)
                .into (holder.store_item_icon);

        holder.store_item_name.setText (storeItem.getService ());

        holder.manager_store_item_details.setText (storeItem.getRequirement () + " : " + storeItem.getQuantity ());
        switch (storeItem.getStatus ()) {

            case 1:
                holder.store_item_status.setText (mContext.getResources ().getText (R.string.available));
                holder.store_item_status.setBackground (mContext.getResources ().getDrawable (R.drawable.background_ended));
                break;
            case 0:
                holder.store_item_status.setText (mContext.getResources ().getText (R.string.needed));
                holder.store_item_status.setBackground (mContext.getResources ().getDrawable (R.drawable.shape_needed));
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mLists.size ();
    }

    public void loadData(Store store) {
        mLists.addAll (store.getData ());
        statusTypes = store.getStatusTypes ();
        notifyDataSetChanged ();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView store_item_icon;
        JFTextView store_item_name, manager_store_item_details, store_item_status;

        public MyViewHolder(View itemView) {
            super (itemView);

            Log.d (TAG, "MyViewHolder: ");
            store_item_icon = (ImageView) itemView.findViewById (R.id.manager_store_item_icon);
            store_item_name = (JFTextView) itemView.findViewById (R.id.manager_store_item_name);
            manager_store_item_details = (JFTextView) itemView.findViewById (R.id.manager_store_item_details);
            store_item_status = (JFTextView) itemView.findViewById (R.id.manager_store_item_status);
        }
    }
}
