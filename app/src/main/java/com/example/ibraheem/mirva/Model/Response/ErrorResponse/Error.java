package com.example.ibraheem.mirva.Model.Response.ErrorResponse;

/**
 * Created by ibraheem on 16/07/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Error {

    @SerializedName("email")
    @Expose
    private ArrayList<String> email = null;

    public ArrayList<String> getEmail() {
        return email;
    }

    public void setEmail(ArrayList<String> email) {
        this.email = email;
    }


    @SerializedName("mobile")
    @Expose
    private ArrayList<String> mobile = null;

    public ArrayList<String> getMobile() {
        return mobile;
    }

    public void setMobile(ArrayList<String> mobile) {
        this.mobile = mobile;
    }

    @SerializedName("name")
    @Expose
    private ArrayList<String> name = null;

    public ArrayList<String> getName() {
        return name;
    }

    public void setName(ArrayList<String> name) {
        this.name = name;
    }

    @SerializedName("password")
    @Expose
    private ArrayList<String> password = null;

    public ArrayList<String> getPassword() {
        return password;
    }

    public void setPassword(ArrayList<String> password) {
        this.password = password;
    }


    @SerializedName("message")
    @Expose
    private ArrayList<String> message = null;

    public ArrayList<String> getMessage() {
        return password;
    }

    public void setMessage(ArrayList<String> message) {
        this.message = message;
    }

}