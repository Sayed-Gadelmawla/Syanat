package com.example.ibraheem.mirva.Model.Response;

import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ibraheem on 10/06/2017.
 */

public class User {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("api_token")
    @Expose
    private String apiToken;

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("subscription")
    @Expose
    private Integer subscription;
    @SerializedName("birth_date")
    @Expose
    private String birthDate;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("statusNote")
    @Expose
    private String statusNote;
    @SerializedName("status")
    @Expose
    private Integer status;

    @SerializedName("subscription_type")
    @Expose
    private Integer subscription_type;


    public Integer getSubscription_type() {
        return subscription_type;
    }

    public void setSubscription_type(Integer subscription_type) {
        this.subscription_type = subscription_type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getApiToken() {
        return apiToken;
    }

    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImage() {
        return ApiUtils.BASE_URL + image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImageToServer() {
        return image;
    }

    public Integer getSubscription() {
        return subscription;
    }

    public void setSubscription(Integer subscription) {
        this.subscription = subscription;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "User{" +
                "message='" + message + '\'' +
                ", apiToken='" + apiToken + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", mobile='" + mobile + '\'' +
                ", type='" + type + '\'' +
                ", image='" + image + '\'' +
                ", subscription=" + subscription +
                ", birthDate='" + birthDate + '\'' +
                ", gender='" + gender + '\'' +
                ", status=" + status +
                ", subscription_type=" + subscription_type +
                '}';
    }

    public String getStatusNote() {
        return statusNote;
    }

    public void setStatusNote(String statusNote) {
        this.statusNote = statusNote;
    }
}