package com.example.ibraheem.mirva.Model.Response.ManagerResonse;

import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ibraheem on 04/07/2017.
 */

public class MOrder {

    @SerializedName("request_id")
    @Expose
    private Integer requestId;
    @SerializedName("service")
    @Expose
    private String service;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("details")
    @Expose
    private String details;
    @SerializedName("end_time")
    @Expose
    private String endTime;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("status_id")
    @Expose
    private Integer statusId;

    //for ended order
    @SerializedName("rate")
    @Expose
    private String rate;
    @SerializedName("name")
    @Expose
    private String techName;

    @SerializedName("job")
    @Expose
    private String jobTitle;


    @SerializedName("tec_image")
    @Expose
    private String techIamge;

    public Integer getRequestId() {
        return requestId;
    }

    public void setRequestId(Integer requestId) {
        this.requestId = requestId;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getImage() {
        return ApiUtils.BASE_URL + image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getTechName() {
        return techName;
    }

    public void setTechName(String techName) {
        this.techName = techName;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getTechIamge() {
        return ApiUtils.BASE_URL + techIamge;
    }

    public void setTechIamge(String techIamge) {
        this.techIamge = techIamge;
    }
}
