package com.example.ibraheem.mirva.Model.Response.Notification;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ibraheem on 04/07/2017.
 */

public class Notification {
    @SerializedName("request_id")
    @Expose
    private Integer requestId;
    @SerializedName("service")
    @Expose
    private String service;
    @SerializedName("status_name")
    @Expose
    private String statusName;
    @SerializedName("status_id")
    @Expose
    private Integer statusId;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("end_date")
    @Expose
    private String endDate;

    public Integer getRequestId() {
        return requestId;
    }

    public void setRequestId(Integer requestId) {
        this.requestId = requestId;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "Notification{" +
                "requestId=" + requestId +
                ", service='" + service + '\'' +
                ", statusName='" + statusName + '\'' +
                ", statusId=" + statusId +
                ", image='" + image + '\'' +
                ", endDate='" + endDate + '\'' +
                '}';
    }
}
