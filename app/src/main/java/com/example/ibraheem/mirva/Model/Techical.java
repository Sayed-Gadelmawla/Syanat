package com.example.ibraheem.mirva.Model;

import android.graphics.Bitmap;

import com.example.ibraheem.mirva.Enum.TECH_STATUS;


/**
 * Created by ibraheem on 28/05/2017.
 */

public class Techical {
    private Bitmap uImage;
    private String name;
    private TECH_STATUS status;

    public Techical(Bitmap uImage, String name, TECH_STATUS status) {
        this.uImage = uImage;
        this.name = name;
        this.status = status;
    }

    public Bitmap getuImage() {
        return uImage;
    }

    public void setuImage(Bitmap uImage) {
        this.uImage = uImage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TECH_STATUS getStatus() {
        return status;
    }

    public void setStatus(TECH_STATUS status) {
        this.status = status;
    }
}
