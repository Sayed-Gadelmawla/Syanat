package com.example.ibraheem.mirva.Fragments.Client;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ibraheem.mirva.Interfaces.Communicator;
import com.example.ibraheem.mirva.R;

/**
 * Created by ibraheem on 01/07/2017.
 */

public class Frag_OrderReceive extends Fragment {
    Communicator comm;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate (R.layout.fragment_check_done, container, false);

        comm = (Communicator) getActivity ();
        comm.selectVisibleToolbar (R.id.toolbar_clints);


        return view;

    }
}
