package com.example.ibraheem.mirva.Fragments.manager;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.VideoView;

import com.example.ibraheem.mirva.Activities.MyNotificationActivity;
import com.example.ibraheem.mirva.Adapter.ViewImageAdapter;
import com.example.ibraheem.mirva.Const.FragConst;
import com.example.ibraheem.mirva.CustomViews.FWTextView;
import com.example.ibraheem.mirva.CustomViews.JFButton;
import com.example.ibraheem.mirva.CustomViews.JFEditText;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.FragmentMessages.TrasferPriceToClientDone;
import com.example.ibraheem.mirva.Interfaces.Communicator;
import com.example.ibraheem.mirva.Model.Response.Order.ViewOrder.Request;
import com.example.ibraheem.mirva.Model.Response.Order.ViewOrder.ViewOrder;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.Model.Response.helperRes;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.UitilsFun;
import com.example.ibraheem.mirva.util.D;
import com.example.ibraheem.mirva.util.L;
import com.example.ibraheem.mirva.util.SharedPrefs;
import com.example.ibraheem.mirva.util.V;
import com.example.ibraheem.mirva.util.helperFun;
import com.github.rtoshiro.view.video.FullscreenVideoLayout;
import com.github.rtoshiro.view.video.FullscreenVideoView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Activity_ViewOrderManager extends AppCompatActivity {
    public final String TAG = Activity_ViewOrderManager.class.getSimpleName();
    Communicator comm;
    JFButton select_technical;
//    JFButton price_confirm;
    JFTextView deivery_time, order_cat, order_service, order_status, sended_order_status, order_details, payment_done;
    FullscreenVideoView order_layout_video;
    LinearLayout layout_case;
    //LinearLayout show_price_layout;
    MapView mMapView;
    GoogleMap googleMap;
    RecyclerView order_layout_images;
    ArrayList<String> imagePaths;
    ViewImageAdapter viewImageAdapter;
    JFTextView order_no_attachment, select_technical_done;
    View type_order_layout;
    D d;
    LatLng latLng;
    LatLng tempLng;

    JFTextView c_title_toolbar, number_of_notifications;
    FWTextView frag_back;
    Toolbar toolbar_order;
    V vd;
    D dialog;

    int REQUEST_ID;
    User user;
    private JFTextView order_id;
    private JFTextView order_date;
    private JFTextView technical_note;
    private LinearLayout technical_note_layout;
    private CircleImageView mClientImageView;
    private JFTextView mClientNameJfTextView,mClientMobileNumberJfTextView;
//    private JFEditText order_price;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__view_order_manager);
        user = helperFun.getUser(this);
        findViewById(R.id.frag_notification).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), MyNotificationActivity.class));

            }
        });
        findViewById(R.id.view_order_notification_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), MyNotificationActivity.class));

            }
        });


        vd = new V(this);
        dialog = new D(this);
        dialog.ShowProgress(getString(R.string.loading));
        user = helperFun.getUser(getBaseContext());
        toolbar_order = (Toolbar) findViewById(R.id.toolbar_view_order);
        c_title_toolbar = (JFTextView) toolbar_order.findViewById(R.id.c_title_toolbar);

        mClientImageView = (CircleImageView) findViewById(R.id.user_image);
        mClientNameJfTextView = (JFTextView) findViewById(R.id.client_name);
        mClientMobileNumberJfTextView = (JFTextView) findViewById(R.id.client_mobile);

        number_of_notifications = (JFTextView) toolbar_order.findViewById(R.id.number_of_notifications);
        frag_back = (FWTextView) toolbar_order.findViewById(R.id.frag_back);

        frag_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        Intent intent = getIntent();
        if (intent != null) {
            REQUEST_ID = intent.getIntExtra(FragConst.REQUEST_ID, -1);
        } else {
            finish();
        }

        c_title_toolbar.setText(getString(R.string.order_details));


        select_technical = (JFButton) findViewById(R.id.select_technical);
        type_order_layout = findViewById(R.id.type_order_layout);
        deivery_time = (JFTextView) findViewById(R.id.order_date);
        order_id = (JFTextView) findViewById(R.id.sended_order_id);
        order_date = (JFTextView) findViewById(R.id.label_order_date);
        technical_note_layout = (LinearLayout) findViewById(R.id.technical_note_layout);
        technical_note = (JFTextView) findViewById(R.id.technical_note);
        select_technical_done = (JFTextView) findViewById(R.id.select_technical_done);
        technical_note_layout.setVisibility(View.INVISIBLE);

        order_status = (JFTextView) findViewById(R.id.order_status);
        order_layout_video = (FullscreenVideoLayout) findViewById(R.id.order_layout_video);
        order_layout_video.setFullscreen(false);
        order_layout_video.setVisibility(View.INVISIBLE);
        layout_case = (LinearLayout) findViewById(R.id.layout_case);
        order_cat = (JFTextView) findViewById(R.id.order_cat);
        order_service = (JFTextView) findViewById(R.id.order_service);
        mMapView = (MapView) findViewById(R.id.mapView_new_order);
        sended_order_status = (JFTextView) findViewById(R.id.sended_order_status);
        mMapView.onCreate(savedInstanceState);
        order_no_attachment = (JFTextView) findViewById(R.id.order_no_attachment);
        order_layout_images = (RecyclerView) findViewById(R.id.order_layout_images);
        imagePaths = new ArrayList<>();
        order_details = (JFTextView) findViewById(R.id.order_details);

//        show_price_layout = (LinearLayout) findViewById(R.id.show_price_layout);
//        order_price = (JFEditText) findViewById(R.id.order_price);
//        price_confirm = (JFButton) findViewById(R.id.price_confirm);

        viewImageAdapter = new ViewImageAdapter(this, imagePaths);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getBaseContext());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        order_layout_images.setLayoutManager(layoutManager);
        order_layout_images.setAdapter(viewImageAdapter);


        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;


            }
        });


        getOrderData(REQUEST_ID);


    }

    /**
     * get  data for selected request
     *
     * @param REQUEST_ID
     */
    void getOrderData(final int REQUEST_ID) {

        dialog.hide();

        ApiInterface apiInterface = ApiUtils.getAPIService(this);
        apiInterface.getSODSupervise(user.getApiToken(), REQUEST_ID).enqueue(new Callback<ViewOrder>() {
            @Override
            public void onResponse(Call<ViewOrder> call, Response<ViewOrder> response) {

                if (response.isSuccessful()) {
                    ViewOrder viewOrder = response.body();
                    final Request request = viewOrder.getData();
                    final User mUser = viewOrder.getUser();
                    Picasso.with (Activity_ViewOrderManager.this)
                            .load (mUser.getImage ())
                            .placeholder (R.drawable.avatar)
                            .error (R.drawable.avatar)
                            .into (mClientImageView);
                    order_cat.setText(request.getSubcategory());
                    order_service.setText(request.getService());
                    order_details.setText(request.getDetails());
                    mClientMobileNumberJfTextView.setText(mUser.getMobile());
                    mClientNameJfTextView.setText(mUser.getName());
                    if (request.getStatusType().equals("19")) {
                        type_order_layout.setVisibility(View.VISIBLE);
                        deivery_time.setText(request.getEndTime());
                        order_status.setText(getString(R.string.type_case_schedule));

                    } else {
                        order_status.setText(getString(R.string.type_case_emergency));

                    }

                    DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date startDate;
                    Date currentDate = new Date();
                    String startDateString;
                    if(request.getStatusId()!=1){
                        technical_note_layout.setVisibility(View.VISIBLE);
                        technical_note.setText(request.getTechnical_notes()==null || request.getTechnical_notes()==""?"":request.getTechnical_notes());
                    }
                    switch (request.getStatusId()) {

                        case 1:
                            sended_order_status.setBackground(getResources().getDrawable(R.drawable.background_under_revision));
                            sended_order_status.setText(getResources().getString(R.string.new_order));
                            select_technical.setVisibility(View.VISIBLE);

                            select_technical.setOnClickListener(new View.OnClickListener() {
                                                                    @Override
                                                                    public void onClick(View v) {

                                                                        Intent intent = new Intent(getBaseContext(), Activity_SelectTechnicalToOrder.class);
                                                                        intent.putExtra(FragConst.REQUEST_ID, REQUEST_ID);
                                                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                                                        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
                                                                        startActivity(intent);
                                                                        finish();

                                                                    }
                                                                }
                            );
                            order_id.setBackground(getResources().getDrawable(R.drawable.background_under_revision));
                            order_id.setText(String.valueOf(REQUEST_ID));
                            startDateString = request.getCreated_date();
                            try {
                                startDate = df.parse(startDateString);
                                long diff = currentDate.getTime() - startDate.getTime();
                                int days = (int) (diff / (1000 * 60 * 60 * 24));
                                DateFormat outputFormat;
                                DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                if (days == 0) {
                                    outputFormat = new SimpleDateFormat("KK:mm a");
                                } else if (days > 360) {
                                    outputFormat = new SimpleDateFormat("E','MMMM dd yyyy KK:mm a");
                                } else {
                                    outputFormat = new SimpleDateFormat("E','dd MMMM  KK:mm a");
                                }
                                order_date.setText(outputFormat.format(inputFormat.parse(startDateString)));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            break;
                        case 2:

                            sended_order_status.setBackground(getResources().getDrawable(R.drawable.background_under_way));
                            sended_order_status.setText(getResources().getString(R.string.underWay));
                            select_technical_done.setVisibility(View.VISIBLE);
                            order_id.setBackground(getResources().getDrawable(R.drawable.background_under_way));
                            order_id.setText(String.valueOf(REQUEST_ID));
                            startDateString = request.getCreated_date();
                            try {
                                startDate = df.parse(startDateString);
                                long diff = currentDate.getTime() - startDate.getTime();
                                int days = (int) (diff / (1000 * 60 * 60 * 24));
                                DateFormat outputFormat;
                                DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                if (days == 0) {
                                    outputFormat = new SimpleDateFormat("KK:mm a");
                                } else if (days > 360) {
                                    outputFormat = new SimpleDateFormat("E','MMMM dd yyyy KK:mm a");
                                } else {
                                    outputFormat = new SimpleDateFormat("E','dd MMMM  KK:mm a");
                                }
                                order_date.setText(outputFormat.format(inputFormat.parse(startDateString)));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            break;

                        case 3:
                            sended_order_status.setBackground(getResources().getDrawable(R.drawable.background_ended));
                            sended_order_status.setText(getResources().getString(R.string.ended));
                            select_technical_done.setVisibility(View.VISIBLE);
                            order_id.setBackground(getResources().getDrawable(R.drawable.background_ended));
                            order_id.setText(String.valueOf(REQUEST_ID));
                            startDateString = request.getCreated_date();
                            try {
                                startDate = df.parse(startDateString);
                                long diff = currentDate.getTime() - startDate.getTime();
                                int days = (int) (diff / (1000 * 60 * 60 * 24));
                                DateFormat outputFormat;
                                DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                if (days == 0) {
                                    outputFormat = new SimpleDateFormat("KK:mm a");
                                } else if (days > 360) {
                                    outputFormat = new SimpleDateFormat("E','MMMM dd yyyy KK:mm a");
                                } else {
                                    outputFormat = new SimpleDateFormat("E','dd MMMM  KK:mm a");
                                }
                                order_date.setText(outputFormat.format(inputFormat.parse(startDateString)));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            break;
                    }


                    if(request.getHasTechnician()!=null) {
                        if (!request.getHasTechnician()) {
                            select_technical_done.setVisibility(View.GONE);
                            select_technical.setVisibility(View.VISIBLE);

                            select_technical.setOnClickListener(new View.OnClickListener() {
                                                                    @Override
                                                                    public void onClick(View v) {

                                                                        Intent intent = new Intent(getBaseContext(), Activity_SelectTechnicalToOrder.class);
                                                                        intent.putExtra(FragConst.REQUEST_ID, REQUEST_ID);
                                                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                                                        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
                                                                        startActivity(intent);
                                                                        finish();

                                                                    }
                                                                }
                            );
                        }
                    }



                    latLng = new LatLng(Double.parseDouble(request.getLatitude()), Double.parseDouble(request.getLongitude()));
                    if (latLng != null) {

                        mMapView.onResume();
                        googleMap.addMarker(new MarkerOptions()
                                .position(latLng)
                                .title("Location")
                                .snippet("First Marker")
                                .icon(BitmapDescriptorFactory.fromBitmap(UitilsFun.getBitmap(getBaseContext(), R.drawable.map_marker))));


                        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                                .target(latLng)
                                .zoom(15.5F)
                                .build()));


                    }

                    if (request.getImages() != null && request.getImages().size() != 0) {

                        order_layout_images.setVisibility(View.VISIBLE);
                        viewImageAdapter.loadData(request.getImages());
                        Log.d(TAG, "onResponse: " + request.getImages());

                    } else if (request.getVideos() != null && +request.getVideos().size() != 0) {
                        if (Uri.parse(request.getVideos().get(0)) != null) {

                            order_layout_video.setVisibility(View.VISIBLE);
                            L.d(TAG, "getVideos ()" + request.getVideos().get(0));
                            //order_layout_video.setVideoURI(Uri.parse(ApiUtils.BASE_URL + request.getVideos().get(0)));
                            order_layout_video.setActivity(Activity_ViewOrderManager.this);
                            try {
                                order_layout_video.setVideoURI(Uri.parse(ApiUtils.BASE_URL + request.getVideos().get(0)));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        }

                    } else {
                        order_no_attachment.setVisibility(View.VISIBLE);
                    }
//                    if(request.getPrice()==null) {
//                        show_price_layout.setVisibility(View.VISIBLE);
//                        if (request.getPrice() == null) {
//                            order_price.setEnabled(true);
//                        } else {
//                            order_price.setEnabled(false);
//                            price_confirm.setVisibility(View.INVISIBLE);
//                        }
//                        order_price.setText("" + (request.getPrice() == null ? "" : request.getPrice()));
//                        price_confirm.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//
//                                if (vd.isValid()) {
//                                    sendPyament(REQUEST_ID, Integer.parseInt(order_price.getText().toString()));
//                                }
//                            }
//                        });
//                    }

                } else

                {
                    finish();
                }
            }

            @Override
            public void onFailure(Call<ViewOrder> call, Throwable t) {
                dialog.hide();
                dialog.ShowErrorInternetConnection("");
            }
        });


    }

    void sendPyament(int REQUEST_ID, int amount) {
        dialog.ShowProgress(getString(R.string.sending));
        ApiInterface apiInterface = ApiUtils.getAPIService(getBaseContext());
        apiInterface.sendPaymentToClinent(user.getApiToken(), REQUEST_ID, amount).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                dialog.hide();
                if (response.isSuccessful()) {


                    Intent intent = new Intent(getBaseContext(), TrasferPriceToClientDone.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    overridePendingTransition(R.anim.fadein, R.anim.fadeout);
                    startActivity(intent);
                    finish();


                } else {
                    helperRes.getErrorMessage(response.errorBody());
                }


            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                dialog.hide();
                dialog.ShowErrorInternetConnection("");
            }
        });

    }

    @Override
    public void onResume() {

        mMapView.onResume();

        super.onResume();

    }

    @Override
    public void onPause() {

        mMapView.onPause();
        super.onPause();

    }

    @Override
    public void onStop() {
        mMapView.onStop();
        super.onStop();

    }

    @Override
    public void onDestroy() {
        mMapView.onDestroy();

        super.onDestroy();


    }

    @Override
    public void onLowMemory() {
        mMapView.onLowMemory();
        super.onLowMemory();

    }

    public void getLastSelectedLocation() {
        if (!SharedPrefs.getString(getBaseContext(), SharedPrefs.LATITUDE, "").isEmpty()) {

            double dragLat = Double.parseDouble(SharedPrefs.getString(getBaseContext(), SharedPrefs.LATITUDE));
            double dragLong = Double.parseDouble(SharedPrefs.getString(getBaseContext(), SharedPrefs.LONGITUDE));
            tempLng = new LatLng(dragLat, dragLong);
        } else {

            tempLng = new LatLng(31.5017672, 34.4665412);
        }
    }

}
