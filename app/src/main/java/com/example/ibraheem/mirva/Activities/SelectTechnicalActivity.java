package com.example.ibraheem.mirva.Activities;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ViewFlipper;

import com.example.ibraheem.mirva.Adapter.TechinalAdapter;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.Enum.TECH_STATUS;
import com.example.ibraheem.mirva.Model.Techical;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.UitilsFun;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;

import java.util.ArrayList;

public class SelectTechnicalActivity extends AppCompatActivity {
    JFTextView select_technical;

    ///


    JFTextView general, under_supervise;
    ViewFlipper viewFlipper;
    ArrayList<Techical> list, list_all;
    TechinalAdapter mTechinalAdapter, mTechinalAdapter_all;
    RecyclerView recyclerView, recyclerView_all;
    String TAG = "dialog";
    Dialog dialog = null;
    SupportMapFragment mapFragment;
    ///\
    private GoogleMap googleMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_manager_select_tech_to_order);

        select_technical = (JFTextView) findViewById(R.id.select_technical);
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapView_new_order);

        createDaialog(this);
        select_technical.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
            }
        });
    }


    //        googleMap.addMarker (markerOptions.position (sydney).title ("Marker Title").snippet ("Marker Description"));


    @Override
    public void onResume() {
        super.onResume();
//        mapView.onResume ();

        general();
        under_supervise();

        LoadData();

        Log.d(TAG, "onResume: ");
    }

    private void createDaialog(Context context) {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); //before
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.setContentView(R.layout.fragment_dialog);
        general = (JFTextView) dialog.findViewById(R.id.general);
        under_supervise = (JFTextView) dialog.findViewById(R.id.under_supervise);
        viewFlipper = (ViewFlipper) dialog.findViewById(R.id.choose_type);
        ImageView imageView = (ImageView) dialog.findViewById(R.id.exist);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        general.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                general.setBackground(getBaseContext().getDrawable(R.drawable.shape_dialog_selected));
                under_supervise.setBackground(getBaseContext().getResources().getDrawable(R.drawable.shape_dialog));

                viewFlipper.setDisplayedChild(0);
            }
        });

        under_supervise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                general.setBackground(getBaseContext().getDrawable(R.drawable.shape_dialog));

                under_supervise.setBackground(getBaseContext().getResources().getDrawable(R.drawable.shape_dialog_selected));

                viewFlipper.setDisplayedChild(1);
            }
        });
    }

    private void under_supervise() {
        list = new ArrayList<>();

        recyclerView = (RecyclerView) dialog.findViewById(R.id.select_tech_recyler_view);
        // mTechinalAdapter = new TechinalAdapter (dialog.getContext (), list);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(dialog.getContext(), 3);
        gridLayoutManager.setOrientation(GridLayout.VERTICAL);

        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(mTechinalAdapter);
    }

    private void general() {
        list_all = new ArrayList<>();

        recyclerView_all = (RecyclerView) dialog.findViewById(R.id.select_tech_recyler_view_all);
        //     mTechinalAdapter_all = new TechinalAdapter (dialog.getContext (), list_all);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(dialog.getContext(), 3);
        gridLayoutManager.setOrientation(GridLayout.VERTICAL);

        recyclerView_all.setLayoutManager(gridLayoutManager);
        recyclerView_all.setAdapter(mTechinalAdapter_all);
    }


    private void LoadData() {
// Techical(Bitmap uImage, String name, TECH_STATUS status)

        list.add(new Techical(UitilsFun.getBitmap(this, R.drawable.face), "ابراهيم  لبد", TECH_STATUS.BUSY));
        list.add(new Techical(UitilsFun.getBitmap(this, R.drawable.face), "ابراهيم  لبد", TECH_STATUS.AVAILABLE));

        list_all.add(new Techical(UitilsFun.getBitmap(this, R.drawable.face), "ابراهيم  لبد", TECH_STATUS.BUSY));
        list_all.add(new Techical(UitilsFun.getBitmap(this, R.drawable.face), "ابراهيم  لبد", TECH_STATUS.AVAILABLE));
        list.add(new Techical(UitilsFun.getBitmap(this, R.drawable.face), "ابراهيم  لبد", TECH_STATUS.NOT_AVAILABLE));
        list.add(new Techical(UitilsFun.getBitmap(this, R.drawable.face), "ابراهيم  لبد", TECH_STATUS.AVAILABLE));

        list_all.add(new Techical(UitilsFun.getBitmap(this, R.drawable.face), "ابراهيم  لبد", TECH_STATUS.BUSY));
        list_all.add(new Techical(UitilsFun.getBitmap(this, R.drawable.face), "ابراهيم  لبد", TECH_STATUS.NOT_AVAILABLE));

        list.add(new Techical(UitilsFun.getBitmap(this, R.drawable.face), "ابراهيم  لبد", TECH_STATUS.BUSY));
        list.add(new Techical(UitilsFun.getBitmap(this, R.drawable.face), "ابراهيم  لبد", TECH_STATUS.AVAILABLE));

        list_all.add(new Techical(UitilsFun.getBitmap(this, R.drawable.face), "ابراهيم  لبد", TECH_STATUS.BUSY));
        list_all.add(new Techical(UitilsFun.getBitmap(this, R.drawable.face), "ابراهيم  لبد", TECH_STATUS.AVAILABLE));

        list.add(new Techical(UitilsFun.getBitmap(this, R.drawable.face), "ابراهيم  لبد", TECH_STATUS.NOT_AVAILABLE));
        list.add(new Techical(UitilsFun.getBitmap(this, R.drawable.face), "ابراهيم  لبد", TECH_STATUS.AVAILABLE));

        list_all.add(new Techical(UitilsFun.getBitmap(this, R.drawable.face), "ابراهيم  لبد", TECH_STATUS.BUSY));
        list_all.add(new Techical(UitilsFun.getBitmap(this, R.drawable.face), "ابراهيم  لبد", TECH_STATUS.AVAILABLE));

        mTechinalAdapter_all.notifyDataSetChanged();
        mTechinalAdapter.notifyDataSetChanged();
    }

}
