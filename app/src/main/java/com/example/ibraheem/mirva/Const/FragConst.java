package com.example.ibraheem.mirva.Const;

/**
 * Created by ibraheem on 17/06/2017.
 */

public class FragConst {
    public final static String SERVICE_ID = "service_id";
    public final static String TICKET_ID = "ticket_id";
    public final static String SUBSCRIPTION_ID = "SUB_ID";
    public final static String SUBSCRIPTION_PRICE = "SUB_PRICE";
    public final static String SUBSCRIPTION_NAME = "SUB_NAME";
    public final static String REQUEST_ID = "request_id";
    public final static String USER_TYPE = "user_type";
    public final static String CONTENT = "content";
}
