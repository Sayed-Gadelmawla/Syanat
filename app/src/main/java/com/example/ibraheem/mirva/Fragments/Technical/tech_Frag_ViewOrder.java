package com.example.ibraheem.mirva.Fragments.Technical;

import android.Manifest;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.VideoView;

import com.example.ibraheem.mirva.Adapter.ViewImageAdapter;
import com.example.ibraheem.mirva.Const.FragConst;
import com.example.ibraheem.mirva.CustomViews.JFButton;
import com.example.ibraheem.mirva.CustomViews.JFEditText;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.FragmentMessages.TrasferPriceToClientDone;
import com.example.ibraheem.mirva.Interfaces.Communicator;
import com.example.ibraheem.mirva.Model.Response.Order.ViewOrder.Request;
import com.example.ibraheem.mirva.Model.Response.Order.ViewOrder.ViewOrder;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.Model.Response.helperRes;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.UitilsFun;
import com.example.ibraheem.mirva.util.D;
import com.example.ibraheem.mirva.util.L;
import com.example.ibraheem.mirva.util.V;
import com.example.ibraheem.mirva.util.helperFun;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.ArrayList;

import me.next.tagview.TagCloudView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by ibraheem on 27/05/2017.
 */

public class tech_Frag_ViewOrder extends Fragment {

    public final String TAG = tech_Frag_ViewOrder.class.getSimpleName ();
    D d;
    V vd;
    int REQUEST_ID;
    User user;
    JFTextView deivery_time, order_cat, order_service, order_status, sended_order_status, order_details, payment_done, order_supervise_note;
    VideoView order_layout_video;
    LinearLayout layout_case, show_price_layout;
    MapView mMapView;
    GoogleMap googleMap;
    RecyclerView order_layout_images;
    ArrayList<String> imagePaths;
    ViewImageAdapter viewImageAdapter;
    JFTextView order_no_attachment;
    View type_order_layout;
    TagCloudView order_supplies;
    JFButton price_confirm;
    JFEditText order_price;
    View transfer_price2client;
    JFButton end_job;
    Communicator comm;
    LatLng latLng;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);

        d = new D (getActivity ());
        vd = new V (getActivity ());
        user = helperFun.getUser (getActivity ());

        d.ShowProgress (getString (R.string.loading));
        Bundle arguments = getArguments ();
        if (arguments != null) {
            REQUEST_ID = arguments.getInt (FragConst.REQUEST_ID, 0);

        }

        Log.d (TAG, "onCreate:REQUEST_ID  " + REQUEST_ID);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate (R.layout.tech_fragment_check_order, container, false);
///
        transfer_price2client = view.findViewById (R.id.transfer_price2client);
        end_job = (JFButton) view.findViewById (R.id.end_job);

        type_order_layout = view.findViewById (R.id.type_order_layout);
        deivery_time = (JFTextView) view.findViewById (R.id.order_date);
        order_status = (JFTextView) view.findViewById (R.id.order_status);
        order_layout_video = (VideoView) view.findViewById (R.id.order_layout_video);
        layout_case = (LinearLayout) view.findViewById (R.id.layout_case);
        order_cat = (JFTextView) view.findViewById (R.id.order_cat);
        order_service = (JFTextView) view.findViewById (R.id.order_service);
        mMapView = (MapView) view.findViewById (R.id.mapView_new_order);
        sended_order_status = (JFTextView) view.findViewById (R.id.sended_order_status);
        mMapView.onCreate (savedInstanceState);
        order_no_attachment = (JFTextView) view.findViewById (R.id.order_no_attachment);
        order_layout_images = (RecyclerView) view.findViewById (R.id.order_layout_images);

        order_supervise_note = (JFTextView) view.findViewById (R.id.order_supervise_note);

        order_details = (JFTextView) view.findViewById (R.id.order_details);

        imagePaths = new ArrayList<> ();
        viewImageAdapter = new ViewImageAdapter (getActivity (), imagePaths);
        LinearLayoutManager layoutManager = new LinearLayoutManager (view.getContext ());
        layoutManager.setOrientation (LinearLayoutManager.HORIZONTAL);
        order_layout_images.setLayoutManager (layoutManager);
        order_layout_images.setAdapter (viewImageAdapter);
        //

        order_supplies = (TagCloudView) view.findViewById (R.id.order_supplies);


        ///


        //** price **///

        show_price_layout = (LinearLayout) view.findViewById (R.id.show_price_layout);
        order_price = (JFEditText) view.findViewById (R.id.order_price);
        price_confirm = (JFButton) view.findViewById (R.id.price_confirm);


        ///
        payment_done = (JFTextView) view.findViewById (R.id.payment_done);
        //** price * ///


        ///
     /*   tag_cloud_view.setOnTagClickListener (new TagCloudView.OnTagClickListener () {
            @Override
            public void onTagClick(int position) {

            }
        });
        tag_cloud_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });*/


        mMapView.getMapAsync (new OnMapReadyCallback () {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                // For showing a move to my location button
                if (ActivityCompat.checkSelfPermission (getActivity (), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission (getActivity (), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }


               /* googleMap.addMarker (new MarkerOptions ()
                        .position (tempLng)
                        .title ("Location")
                        .snippet ("First Marker")
                        .icon (BitmapDescriptorFactory.fromBitmap (UitilsFun.getBitmap (getActivity (), R.drawable.map_marker))))
                .showInfoWindow ();
*/

                /*INIT = new CameraPosition.Builder ()
                        .target (tempLng)
                        .zoom (17.5F)
                        .build ();*/
                // use map to move camera into position
                //moveToCurrentLocation (googleMap, tempLng);
                //  googleMap.moveCamera (CameraUpdateFactory.newCameraPosition (INIT));

                /*mk = googleMap.addMarker (marker);
                mk.showInfoWindow ();*/

                new Handler ().postDelayed (new Runnable () {
                    @Override
                    public void run() {
                        d.hide ();
                    }
                }, 2000);


            }
        });


        if (REQUEST_ID != 0) {
            getOrderData (REQUEST_ID);
        } else {
            d.ShowError ("");
        }

        comm = (Communicator) getActivity ();
        comm.setTitleToolbar (getActivity ().getString (R.string.order_details));
        comm.selectVisibleToolbar (R.id.toolbar_clints);
        return view;
    }

    /****\
     *get request  data for select
     * @param REQUEST_ID
     */
    void getOrderData(final int REQUEST_ID) {

        ApiInterface apiInterface = ApiUtils.getAPIService (getActivity ());
        apiInterface.getTechSpecificOrderData (user.getApiToken (), REQUEST_ID).enqueue (new Callback<ViewOrder> () {
            @Override
            public void onResponse(Call<ViewOrder> call, Response<ViewOrder> response) {

                if (response.isSuccessful ()) {
                    ViewOrder viewOrder = response.body ();

                    final Request request = viewOrder.getData ();
                    Log.d (TAG, "onResponse: " + request);

                    L.d (TAG, "getStatus " + request.getStatus ());

                    if (request.getStatusType ().equals ("19")) {
                        type_order_layout.setVisibility (View.VISIBLE);
                        deivery_time.setText (request.getEndTime ());
                        order_status.setText (getString (R.string.type_case_schedule));
                    } else {
                        order_status.setText (getString (R.string.type_case_emergency));

                    }
                    order_details.setText (request.getDetails ());

                    switch (request.getStatusId ()) {

                        case 1:
                            sended_order_status.setBackground (getResources ().getDrawable (R.drawable.background_under_revision));
                            sended_order_status.setText (getResources ().getString (R.string.underRevision));
                            break;
                        case 2:
                            sended_order_status.setBackground (getResources ().getDrawable (R.drawable.background_under_way));
                            sended_order_status.setText (getResources ().getString (R.string.underWay));
                            break;

                        case 3:
                            sended_order_status.setBackground (getResources ().getDrawable (R.drawable.background_ended));
                            sended_order_status.setText (getResources ().getString (R.string.ended));

                            break;
                    }
                    ArrayList<String> tags = new ArrayList<> ();// just show price to user not suscription

                    if (request.getRequirement () != null) {
                        for (int i = 0; i < request.getRequirement ().size (); i++) {
                            tags.add (request.getRequirement ().get (i).getName () + "(" + request.getRequirement ().get (i).getQuantity () + ")");
                        }
                        order_supplies.setVisibility (View.VISIBLE);
                        order_supplies.setTags (tags);
                    }

                    order_cat.setText (request.getSubcategory ());
                    order_service.setText (request.getService ());

                    latLng = new LatLng (Double.parseDouble (request.getLatitude ()), Double.parseDouble (request.getLongitude ()));
                    if (latLng != null) {

                        mMapView.onResume ();
                        googleMap.addMarker (new MarkerOptions ()
                                .position (latLng)
                                .title ("Location")
                                .snippet ("First Marker")
                                .icon (BitmapDescriptorFactory.fromBitmap (UitilsFun.getBitmap (getActivity (), R.drawable.map_marker))));


                        googleMap.moveCamera (CameraUpdateFactory.newCameraPosition (new CameraPosition.Builder ()
                                .target (latLng)
                                .zoom (17.5F)
                                .build ()));


                    }

                    if (request.getImages () != null && request.getImages ().size () != 0) {

                        order_layout_images.setVisibility (View.VISIBLE);
                        viewImageAdapter.loadData (request.getImages ());
                        Log.d (TAG, "onResponse: " + request.getImages ());

                    } else if (request.getVideos () != null && +request.getVideos ().size () != 0) {
                        if (request.getVideos ().get (0) != null) {

                            order_layout_video.setVisibility (View.VISIBLE);
                            L.d (TAG, "getVideos ()" + request.getVideos ().get (0));
                            order_layout_video.setVideoURI (Uri.parse (ApiUtils.BASE_URL + request.getVideos ().get (0)));

                        }

                    } else {
                        order_no_attachment.setVisibility (View.VISIBLE);
                    }
                    order_supervise_note.setText (request.getSupervisor_notes () != null ? request.getSupervisor_notes () : "");



                    if (request.getStatusId () == 2) {
                        //show price
                        //** price **///
                        if (request.getIsSubscription () == 0) {

                            // just show price to user not suscription
                            show_price_layout.setVisibility (View.VISIBLE);
                            order_price.setText ("" + (request.getPrice () == null ? "" : request.getPrice ()));

                            vd.ValidatePrice (R.id.order_price);

                            price_confirm.setOnClickListener (new View.OnClickListener () {
                                @Override
                                public void onClick(View v) {

                                    if (vd.isValid ()) {
                                        sendPyament (REQUEST_ID, Integer.parseInt (order_price.getText ().toString ()));
                                    }
                                }
                            });


                            //** price * ///

                        } else if (request.getIsSubscription () == 1) {
                            end_job.setVisibility (View.VISIBLE);
                            end_job.setOnClickListener (new View.OnClickListener () {
                                @Override
                                public void onClick(View v) {
                                    finishRequestByclient ();

                                }
                            });


                        }

                    } else if (request.getStatusId () == 3) {


                        price_confirm.setVisibility (View.GONE);
                        order_price.setEnabled (false);
                        payment_done.setVisibility (View.VISIBLE);


                    }


                    d.hide ();

                } else {

                    try {
                        d.hide ();
                        d.ShowError (response.errorBody ().string ());

                    } catch (IOException e) {
                        /// e.printStackTrace ();
                    }
                }
            }

            @Override
            public void onFailure(Call<ViewOrder> call, Throwable t) {
                d.hide ();
                d.ShowErrorInternetConnection ("");
            }
        });


    }

    void sendPyament(int REQUEST_ID, int amount) {
        d.ShowProgress (getString (R.string.sending));
        ApiInterface apiInterface = ApiUtils.getAPIService (getActivity ());
        apiInterface.sendPaymentToClinent (user.getApiToken (), REQUEST_ID, amount).enqueue (new Callback<JsonObject> () {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                d.hide ();
                if (response.isSuccessful ()) {


                    /*omm.setTitleToolbar (getString (R.string.transfer_done));
                    transfer_price2client.setVisibility (View.VISIBLE);*/
              //      comm.TopMessage (new TrasferPriceToClientDone ());

                } else {
                    helperRes.getErrorMessage (response.errorBody ());
                }


            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                d.hide ();
                d.ShowErrorInternetConnection ("");
            }
        });

    }

    void finishRequestByclient() {

        ApiInterface apiInterface = ApiUtils.getAPIService (getActivity ());
        apiInterface.finishRequestbyClient (user.getApiToken (), REQUEST_ID).enqueue (new Callback<JsonObject> () {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                if (response.isSuccessful ()) {

                    helperRes.getSuccessMessage (response.body ());
                } else {
                    d.ShowError (getString (R.string.error));
                    ///helperRes.getErrorMessage (response.errorBody ());


                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    @Override
    public void onResume() {

        mMapView.onResume ();

        super.onResume ();

    }

    @Override
    public void onPause() {

        mMapView.onPause ();
        super.onPause ();

    }

    @Override
    public void onStop() {
        mMapView.onStop ();
        super.onStop ();

    }

    @Override
    public void onDestroy() {
        mMapView.onDestroy ();

        super.onDestroy ();


    }

    @Override
    public void onLowMemory() {
        mMapView.onLowMemory ();
        super.onLowMemory ();

    }
}
