package com.example.ibraheem.mirva.Activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.example.ibraheem.mirva.CustomViews.JFButton;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.Enum.DialogAction;
import com.example.ibraheem.mirva.Model.Response.ErrorResponse.ErrorUtils;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.Model.Response.helperRes;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.util.SessionManager;
import com.example.ibraheem.mirva.util.SharedPrefs;
import com.example.ibraheem.mirva.util.helperFun;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ChooseLocationActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private static final String TAG = "ChooseLocationActivity";
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION_TWO = 2;
    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private Location mLastLocation;
    private Marker mCurrLocationMarker;
    private MarkerOptions currentPositionMarker;
    private int PLACE_PICKER_REQUEST = 1;
    private JFTextView mLocationTextJFTextView;
    private JFButton mChooseThisLocationBtn;
    private Context mContext;
    private SessionManager mSessionManager;
    private LatLng latLng;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_location);
        mContext = this;
        user = helperFun.getUser(this);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mLocationTextJFTextView = (JFTextView) findViewById(R.id.current_location_chane_location_tv);
        mLocationTextJFTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try {
                    startActivityForResult(builder.build(ChooseLocationActivity.this), PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });

        mSessionManager = new SessionManager(mContext);

        mChooseThisLocationBtn = (JFButton) findViewById(R.id.choose_this_location_btn);
        mChooseThisLocationBtn.setEnabled(false);
        mChooseThisLocationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ApiInterface apiInterface = ApiUtils.getAPIService(ChooseLocationActivity.this);
                apiInterface.upldateLocation(user.getApiToken(), String.valueOf(latLng.longitude), String.valueOf(latLng.latitude)).enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        SharedPrefs.save(mContext, SharedPrefs.LATITUDE, String.valueOf(latLng.latitude));
                        SharedPrefs.save(mContext, SharedPrefs.LONGITUDE, String.valueOf(latLng.longitude));
                        mSessionManager.setIsLocationChecked(true);
                        finish();
                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {


                    }
                });

            }
        });
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        mMap = googleMap;
        //mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        buildGoogleApiClient();

        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            }
        } else {

        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(3000);
        mLocationRequest.setFastestInterval(3000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION_TWO);

            }
        } else {
            Location mLocation = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient);


            if (mLocation == null) {
                //Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                //startActivity(intent);
            } else {
                mLastLocation = mLocation;
                if (mCurrLocationMarker != null) {
                    mCurrLocationMarker.remove();
                }

                //Place current location marker
                latLng = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
                //latLng = new LatLng(Double.valueOf("25.3190407"), Double.valueOf("51.4613873"));
                currentPositionMarker = new MarkerOptions();
                currentPositionMarker.position(latLng);
                currentPositionMarker.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_placeholder_point));
                mCurrLocationMarker = mMap.addMarker(currentPositionMarker);
                mChooseThisLocationBtn.setEnabled(true);

                //mCurrLocationMarker.showInfoWindow();
                //move map camera
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        mMap.animateCamera(CameraUpdateFactory.zoomTo(17), 1000, null);
                        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        mMap.setMyLocationEnabled(true);
                    }
                }, 1500);


                //stop location updates
                if (mGoogleApiClient != null) {
                    LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
                }

                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(this, Locale.getDefault());

                try {
                    addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                    if (addresses != null && !addresses.isEmpty()) {
                        String address = addresses.get(0).getAddressLine(0) == null ? "" : addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                        String city = addresses.get(0).getLocality() == null ? "" : addresses.get(0).getLocality();
                        String state = addresses.get(0).getAdminArea() == null ? "" : addresses.get(0).getAdminArea();
                        String country = addresses.get(0).getCountryName() == null ? "" : addresses.get(0).getCountryName();
                        String postalCode = addresses.get(0).getPostalCode() == null ? "" : addresses.get(0).getPostalCode();
                        String knownName = addresses.get(0).getFeatureName() == null ? "" : addresses.get(0).getFeatureName(); // Only if available else return NULL
                        String locationText = address + "," + city + "," + state + "," + country + "," + postalCode + "," + knownName;
                        mLocationTextJFTextView.setText(locationText);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }


            //}
        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(this, this)
                .build();
        mGoogleApiClient.connect();
    }


    @Override
    public void onLocationChanged(Location location) {
        /*mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        currentPositionMarker = new MarkerOptions();
        currentPositionMarker.position(latLng);
        currentPositionMarker.snippet("INFO");
        currentPositionMarker.title("Current Position");
        currentPositionMarker.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_placeholder_point));
        mMap.clear();
        mCurrLocationMarker = mMap.addMarker(currentPositionMarker);
        mCurrLocationMarker.showInfoWindow();

        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(11));

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }*/
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                break;

            case MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION_TWO:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                break;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                StringBuilder stBuilder = new StringBuilder();
                String placename = String.format("%s", place.getName());
                String latitude = String.valueOf(place.getLatLng().latitude);
                String longitude = String.valueOf(place.getLatLng().longitude);
                String address = String.format("%s", place.getAddress());
                stBuilder.append("Name: ");
                stBuilder.append(placename);
                stBuilder.append("\n");
                stBuilder.append("Latitude: ");
                stBuilder.append(latitude);
                stBuilder.append("\n");
                stBuilder.append("Logitude: ");
                stBuilder.append(longitude);
                stBuilder.append("\n");
                stBuilder.append("Address: ");
                stBuilder.append(address);
                Log.d(TAG, stBuilder.toString());
                mLocationTextJFTextView.setText(address);
                latLng = new LatLng(Double.valueOf(latitude), Double.valueOf(longitude));
                currentPositionMarker = new MarkerOptions();
                currentPositionMarker.position(latLng);
                currentPositionMarker.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_placeholder_point));
                mMap.clear();
                mCurrLocationMarker = mMap.addMarker(currentPositionMarker);
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
                mChooseThisLocationBtn.setEnabled(true);
            }
        }
    }

    public static Intent newIntent(Context mContext) {
        return new Intent(mContext, ChooseLocationActivity.class);
    }
}
