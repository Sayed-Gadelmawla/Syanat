package com.example.ibraheem.mirva.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.ibraheem.mirva.Const.FragConst;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.Fragments.Technical.Activity_ViewOrderTechnical;
import com.example.ibraheem.mirva.Interfaces.Communicator;
import com.example.ibraheem.mirva.Model.Response.Order.Technical.SAllOrders;
import com.example.ibraheem.mirva.Model.Response.Order.Technical.TOrder;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.util.helperFun;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by ibraheem on 28/05/2017.
 */

public class TechnicalRequestsAdapter extends RecyclerView.Adapter<TechnicalRequestsAdapter.MyViewHolder> {

    ArrayList<TOrder> mRequests;
    Context mContext;
    Communicator comm;
    String TAG = "Main";
    SAllOrders sAllOrders;

    public TechnicalRequestsAdapter(Context context, ArrayList<TOrder> mRequestArrayList) {
        mContext = context;
        mRequests = mRequestArrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      /*  RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams (ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);*/

        View view = LayoutInflater.from (parent.getContext ()).inflate (R.layout.item_technical_reaqest, parent, false);
        //  view.setLayoutParams (lp);
        return new MyViewHolder (view);

    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final TOrder sOrder = mRequests.get (position);
        holder.item_cat.setText (sOrder.getService ());
        Log.d (TAG, "onBindViewHolder: " + sOrder.toString ());


        holder.item_request_time.setText (sOrder.getEndTime ());

        Picasso.with (mContext)
                .load (sOrder.getImage ())
                .placeholder (R.drawable.icon_elect)
                .error (R.drawable.icon_elect)
                .into (holder.item_icon);

        boolean isDone = false;
        if (sOrder.getStatusId () == 35) {
            isDone = true;
            holder.item_status.setText (mContext.getString (R.string.ended));
            holder.item_status.setBackground (mContext.getResources ().getDrawable (R.drawable.background_ended));

        } else if (sOrder.getStatusId () == 34) {
            isDone = false;

            holder.item_status.setText (mContext.getString (R.string.underRevision));
            holder.item_status.setBackground (mContext.getResources ().getDrawable (R.drawable.background_under_revision));

        }else if (sOrder.getStatusId () == 40) {
            isDone = false;

            holder.item_status.setText (mContext.getString (R.string.underWay));
            holder.item_status.setBackground (mContext.getResources ().getDrawable (R.drawable.background_under_way));

        }else if (sOrder.getStatusId () == 41) {
            isDone = false;

            holder.item_status.setText (mContext.getString (R.string.completed));
            holder.item_status.setBackground (mContext.getResources ().getDrawable (R.drawable.background_recived));

        }else if (sOrder.getStatusId () == 43) {
            isDone = false;

            holder.item_status.setText (mContext.getString (R.string.rejected));
            holder.item_status.setBackground (mContext.getResources ().getDrawable (R.drawable.background_rejected));

        }else if (sOrder.getStatusId () == 44) {
            isDone = false;
            holder.item_status.setText (mContext.getString (R.string.closed));
            holder.item_status.setBackground (mContext.getResources ().getDrawable (R.drawable.background_closed));

        }

        if (sOrder.getEndTime () == null || sOrder.getEndTime ().isEmpty ()) {
            holder.item_request_time.setText (mContext.getString (R.string.type_case_emergency));
        } else {
            holder.item_request_time.setText (helperFun.getDifferenceDatesm (mContext, sOrder.getEndTime (), isDone));

        }

        holder.layout.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (mContext, Activity_ViewOrderTechnical.class);
                intent.putExtra (FragConst.REQUEST_ID, sOrder.getRequest_id ());

                mContext.startActivity (intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mRequests.size ();
    }

    public void LoadData(SAllOrders sAllOrders) {
        this.sAllOrders = sAllOrders;
        mRequests.addAll (sAllOrders.getData ());
        Log.d (TAG, "LoadData: " + sAllOrders.getData ().toString ());
        notifyDataSetChanged ();

    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        JFTextView item_cat, item_request_time, item_status;
        ImageView item_icon;
        View layout;

        public MyViewHolder(View itemView) {
            super (itemView);
            layout = itemView.findViewById (R.id.tech_order_item_layout);
            item_cat = (JFTextView) itemView.findViewById (R.id.tech_order_item_name);
            item_request_time = (JFTextView) itemView.findViewById (R.id.tech_order_item_details);
            item_status = (JFTextView) itemView.findViewById (R.id.tech_order_item_status);
            item_icon = (ImageView) itemView.findViewById (R.id.tech_order_item_icon);

        }
    }
}