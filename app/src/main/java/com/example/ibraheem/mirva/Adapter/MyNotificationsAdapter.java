package com.example.ibraheem.mirva.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.ibraheem.mirva.Const.FragConst;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.Fragments.Client.Activity_ViewOrderClient;
import com.example.ibraheem.mirva.Fragments.Technical.Activity_ViewOrderTechnical;
import com.example.ibraheem.mirva.Fragments.manager.Activity_ViewOrderManager;
import com.example.ibraheem.mirva.Model.Response.Notification.Notification;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.util.helperFun;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by ibraheem on 18/05/2017.
 */

public class MyNotificationsAdapter extends RecyclerView.Adapter<MyNotificationsAdapter.MyViewHolder> {

    private static final String TAG = "MyNotificationsAdapter";
    Context mContext;
    ArrayList<Notification> notificationArrayList;
    Notification notification;
    User user;
    Bundle bundle;
    Intent intent;

    public MyNotificationsAdapter(Context context, ArrayList<Notification> list) {

        mContext = context;
        notificationArrayList = list;
        user = helperFun.getUser(mContext);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        notification = notificationArrayList.get(position);
        // holder.my_notification_item_details.setText (notification.getEndDate ());


        holder.my_notification_item_name.setText(notification.getService());
        Picasso.with(mContext)
                .load(ApiUtils.BASE_URL + notification.getImage())
                .placeholder(R.drawable.icon_elect)
                .error(R.drawable.icon_elect)
                .into(holder.my_notification_item_icon);

        boolean isDone = false;

        switch (notification.getStatusId()) {

            case 35:
                isDone = true;

                holder.my_notification_item_status.setBackground(mContext.getResources().getDrawable(R.drawable.background_ended));
                holder.my_notification_item_status.setText(mContext.getResources().getString(R.string.ended));

                break;
            case 40:
                isDone = false;

                holder.my_notification_item_status.setBackground(mContext.getResources().getDrawable(R.drawable.background_under_way));
                holder.my_notification_item_status.setText(mContext.getResources().getString(R.string.underWay));
                break;
            case 34:
                isDone = false;

                holder.my_notification_item_status.setBackground(mContext.getResources().getDrawable(R.drawable.background_under_revision));
                holder.my_notification_item_status.setText(mContext.getResources().getString(R.string.underRevision));
                break;
            case 41:
                isDone = false;
                holder.my_notification_item_status.setBackground(mContext.getResources().getDrawable(R.drawable.background_recived));
                holder.my_notification_item_status.setText(mContext.getResources().getString(R.string.completed));
                break;
            case 43:
                isDone = false;
                holder.my_notification_item_status.setBackground(mContext.getResources().getDrawable(R.drawable.background_rejected));
                holder.my_notification_item_status.setText(mContext.getResources().getString(R.string.rejected));

                break;
            case 44:
                isDone = false;
                holder.my_notification_item_status.setBackground(mContext.getResources().getDrawable(R.drawable.background_closed));
                holder.my_notification_item_status.setText(mContext.getResources().getString(R.string.closed));

                break;
        }

        if (notification.getEndDate() == null || notification.getEndDate().isEmpty()) {
            holder.my_notification_item_details.setText(mContext.getString(R.string.type_case_emergency));
        } else {
            holder.my_notification_item_details.setText(notification.getEndDate());

        }
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                switch (user.getType()) {
                    case "Supervisor":
                        // intent = new Intent (getBaseContext (), MainPageManager.class);
                        intent = new Intent(mContext, Activity_ViewOrderManager.class);

                        break;
                    case "Technician":
                        //  intent = new Intent (getBaseContext (), MainPageTechnical.class);
                        intent = new Intent(mContext, Activity_ViewOrderTechnical.class);

                        break;
                    case "Client":
                        intent = new Intent(mContext, Activity_ViewOrderClient.class);

                        break;
                }
                intent.putExtra(FragConst.REQUEST_ID, notificationArrayList.get(position).getRequestId());
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                // mContext.overridePendingTransition (R.anim.fadein, R.anim.fadeout);
                mContext.startActivity(intent);

            }
        });

       /* if (notification.get () == null || order.getEndTime ().isEmpty ()) {
            holder.my_order_item_details.setText (mContext.getString (R.string.type_case_emergency));
        } else {
            holder.my_order_item_details.setText (helperFun.getDifferenceDatesm (mContext, order.getEndTime (), isDone));

        }*/

    }


    @Override
    public int getItemCount() {
        return notificationArrayList.size();
    }

    public void loadData(ArrayList<Notification> list) {
        notificationArrayList.clear();
        notificationArrayList.addAll(list);
        notifyDataSetChanged();

    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        JFTextView my_notification_item_name, my_notification_item_status, my_notification_item_details;
        ImageView my_notification_item_icon;
        View view;

        public MyViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            Log.d(TAG, "MyViewHolder: ");
            my_notification_item_icon = (ImageView) itemView.findViewById(R.id.my_notification_item_icon);
            my_notification_item_name = (JFTextView) itemView.findViewById(R.id.my_notification_item_name);
            my_notification_item_status = (JFTextView) itemView.findViewById(R.id.my_notification_item_status);
            my_notification_item_details = (JFTextView) itemView.findViewById(R.id.my_notification_item_details);
        }
    }


}
