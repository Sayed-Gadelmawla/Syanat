package com.example.ibraheem.mirva.Model.Response.Order.Technical;

import com.example.ibraheem.mirva.Model.Response.Order.OrderStatus;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by ibraheem on 02/07/2017.
 */


public class SAllOrders {
    @SerializedName("status")
    @Expose
    private OrderStatus status;
    @SerializedName("data")
    @Expose
    private ArrayList<TOrder> data = null;

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public ArrayList<TOrder> getData() {
        return data;
    }

    public void setData(ArrayList<TOrder> data) {
        this.data = data;
    }


}
