package com.example.ibraheem.mirva.Model;

import android.graphics.Bitmap;

import com.example.ibraheem.mirva.Enum.NOTIFICATION_STATUS;

/**
 * Created by ibraheem on 28/05/2017.
 */

public class mRequest {

    private Bitmap rImage, uImage;
    private String cat, sub_cat, techName, techTitle, rStatus, mDetails;
    private NOTIFICATION_STATUS status;
    private float Rate;

//    public mRequest(Bitmap rImage, Bitmap uImage, String rTitle, String techName, String techTitle, String rStatus, String mDetails, NOTIFICATION_STATUS status) {
//    public mRequest(Bitmap rImage, Bitmap uImage, String rTitle, String techName, String techTitle, NOTIFICATION_STATUS status, float rate) {

    //ended
    public mRequest(Bitmap rImage, Bitmap uImage, String cat, String techName, String techTitle, NOTIFICATION_STATUS status, float rate) {
        this.rImage = rImage;
        this.uImage = uImage;
        this.cat = cat;
        this.techName = techName;
        this.techTitle = techTitle;
        this.status = status;
        Rate = rate;
    }

    //underway
    public mRequest(Bitmap rImage, Bitmap uImage, String cat, String techName, String techTitle, String rStatus, String mDetails, NOTIFICATION_STATUS status) {
        this.rImage = rImage;
        this.uImage = uImage;
        this.cat = cat;
        this.techName = techName;
        this.techTitle = techTitle;
        this.rStatus = rStatus;
        this.mDetails = mDetails;
        this.status = status;
    }

    /**
     * "request_id": 21,
     * "service": "تنظييف",
     * "image": "/images/services/Shape4.png",
     * "details": "اختبار3",
     * "end_time": "2017-06-10 11:00:00",
     * "status": "جديد",
     * "status_id": 1
     */
    // new order for manager  or ended fro technical
    public mRequest(Bitmap rImage, String cat, String sub_cat, NOTIFICATION_STATUS status) {
        this.rImage = rImage;
        this.cat = cat;
        this.sub_cat = sub_cat;
        this.status = status;
    }


    public Bitmap getrImage() {
        return rImage;
    }

    public void setrImage(Bitmap rImage) {
        this.rImage = rImage;
    }

    public Bitmap getuImage() {
        return uImage;
    }

    public void setuImage(Bitmap uImage) {
        this.uImage = uImage;
    }

    public String getCat() {
        return cat;
    }

    public void setCat(String cat) {
        this.cat = cat;
    }

    public String getSub_cat() {
        return sub_cat;
    }

    public void setSub_cat(String sub_cat) {
        this.sub_cat = sub_cat;
    }

    public String getTechName() {
        return techName;
    }

    public void setTechName(String techName) {
        this.techName = techName;
    }

    public String getTechTitle() {
        return techTitle;
    }

    public void setTechTitle(String techTitle) {
        this.techTitle = techTitle;
    }

    public String getrStatus() {
        return rStatus;
    }

    public void setrStatus(String rStatus) {
        this.rStatus = rStatus;
    }

    public String getmDetails() {
        return mDetails;
    }

    public void setmDetails(String mDetails) {
        this.mDetails = mDetails;
    }

    public NOTIFICATION_STATUS getStatus() {
        return status;
    }

    public void setStatus(NOTIFICATION_STATUS status) {
        this.status = status;
    }

    public float getRate() {
        return Rate;
    }

    public void setRate(float rate) {
        Rate = rate;
    }
}
