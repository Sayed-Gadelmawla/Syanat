package com.example.ibraheem.mirva.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Eng.Mhmod on 8/28/2017.
 */

public class AllAdvertisment {
    @SerializedName("data")
    @Expose
    private ArrayList<Advertisment> data = null;

    public ArrayList<Advertisment> getData() {
        return data;
    }

    public void setData(ArrayList<Advertisment> data) {
        this.data = data;
    }
}
