package com.example.ibraheem.mirva.Fragments.Client;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ibraheem.mirva.R;

/**
 * Created by ibraheem on 21/06/2017.
 */

public class Frag_LostInternet extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView (inflater, container, savedInstanceState);

        return inflater.inflate (R.layout.lost_internet, container, false);
    }
}
