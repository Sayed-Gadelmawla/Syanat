package com.example.ibraheem.mirva.Fragments.Technical;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.example.ibraheem.mirva.Adapter.TechnicalRequestsAdapter;
import com.example.ibraheem.mirva.Interfaces.Communicator;
import com.example.ibraheem.mirva.Model.Response.Order.Technical.SAllOrders;
import com.example.ibraheem.mirva.Model.Response.Order.Technical.TOrder;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.util.helperFun;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class tech_Frag_MyOrders extends Fragment {
    RecyclerView recyclerView;
    TechnicalRequestsAdapter mRequestsAdapter;
    ArrayList<TOrder> arrayList;
    User user;
    // D d;
    private ProgressBar mProgressBar;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        user = helperFun.getUser (getActivity ());
        //   d = new D (getActivity ());
        //  d.ShowProgress (getString (R.string.loading));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        arrayList = new ArrayList<> ();
        View view = inflater.inflate (R.layout.tech_fragment_my_requests, container, false);
        mRequestsAdapter = new TechnicalRequestsAdapter (getActivity (), arrayList);
        recyclerView = (RecyclerView) view.findViewById (R.id.tech_request_recyler_view);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager (getActivity ());
        linearLayoutManager.setOrientation (LinearLayout.VERTICAL);
        recyclerView.setLayoutManager (linearLayoutManager);
        Communicator comm = (Communicator) getActivity ();


        recyclerView.setAdapter (mRequestsAdapter);


        comm.selectVisibleToolbar (R.id.toolbar_clints);
        comm.setTitleToolbar (getActivity ().getString (R.string.myOrder));

        getAllOrders ();

        return view;
    }

    private void loadData() {
//    public mRequest(Bitmap rImage, Bitmap uImage, String rTitle, String techName, String techTitle, String rStatus, String mDetails, NOTIFICATION_STATUS status) {
//    public mRequest(Bitmap rImage, String cat, String sub_cat, NOTIFICATION_STATUS status) {
/*
        arrayList.add (new mRequest (getBitmap (R.drawable.icon_elect), "كهربائيات", "انتهت منذ   15 س : 30 د : 50 ث", NOTIFICATION_STATUS.ENDED));//, "انتهت منذ   15 س : 30 د : 50 ث"));
        arrayList.add (new mRequest (getBitmap (R.drawable.icon_elect), "كهربائيات", "الوقت المتبقي   15 س : 30 د : 50 ث", NOTIFICATION_STATUS.UNDER_WAY));//, "انتهت منذ   15 س : 30 د : 50 ث"));

        arrayList.add (new mRequest (getBitmap (R.drawable.icon_elect), "كهربائيات", "الوقت المتبقي   15 س : 30 د : 50 ث", NOTIFICATION_STATUS.UNDER_WAY));//, "انتهت منذ   15 س : 30 د : 50 ث"));
        arrayList.add (new mRequest (getBitmap (R.drawable.icon_elect), "كهربائيات", "انتهت منذ  15 س : 30 د : 50 ث", NOTIFICATION_STATUS.ENDED));//, "انتهت منذ   15 س : 30 د : 50 ث"));
        arrayList.add (new mRequest (getBitmap (R.drawable.icon_elect), "كهربائيات", "الوقت المتبقي   15 س : 30 د : 50 ث", NOTIFICATION_STATUS.UNDER_WAY));//, "انتهت منذ   15 س : 30 د : 50 ث"));

        mRequestsAdapter.notifyDataSetChanged ();*/

    }

    /**
     * get all order  for technical
     */

    void getAllOrders() {
        ApiInterface apiInterface = ApiUtils.getAPIService (getActivity ());

        apiInterface.getTechnicalOrders (user.getApiToken ()).enqueue (new Callback<SAllOrders> () {
            @Override
            public void onResponse(Call<SAllOrders> call, Response<SAllOrders> response) {
                if (response.isSuccessful ()) {
                    SAllOrders body = response.body ();
                    ArrayList<TOrder> sOrders = body.getData ();
                    Log.d ("getAllOrders", "onResponse: " + sOrders.toString ());
                    if (sOrders.size () != 0) {
                        mRequestsAdapter.LoadData (body);

                    }

                    mProgressBar.setVisibility(View.INVISIBLE);
                } else {


                }
                //  d.hide ();
            }

            @Override
            public void onFailure(Call<SAllOrders> call, Throwable t) {
                //  d.ShowErrorInternetConnection ("");
                mProgressBar.setVisibility(View.INVISIBLE);
            }
        });


    }
}
