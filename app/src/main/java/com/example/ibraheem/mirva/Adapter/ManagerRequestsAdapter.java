package com.example.ibraheem.mirva.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;

import com.example.ibraheem.mirva.Const.FragConst;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.Fragments.Client.Activity_ViewOrderClient;
import com.example.ibraheem.mirva.Fragments.manager.Activity_ViewOrderManager;
import com.example.ibraheem.mirva.Interfaces.Communicator;
import com.example.ibraheem.mirva.Model.Response.ManagerResonse.MAllOrders;
import com.example.ibraheem.mirva.Model.Response.ManagerResonse.MOrder;
import com.example.ibraheem.mirva.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by ibraheem on 28/05/2017.
 */

public class ManagerRequestsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<MOrder> mRequests;
    Context mContext;
    Communicator comm;
    String TAG = "Main";

    public ManagerRequestsAdapter(Context context, ArrayList<MOrder> mOrderArrayList) {
        mContext = context;
        mRequests = mOrderArrayList;
    }

    @Override
    public int getItemViewType(int position) {
      /*  int status = -1;
        switch () {
            case 2:

                status = 2;
                break;
            case ENDED:
                status = 1;
                break;
            case NEW_ORDER:
                status = 2;
                break;
        }*/

        return mRequests.get (position).getStatusId ();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflate;
     /*   RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams (ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);*/

        switch (viewType) {
            case 1:
                inflate = LayoutInflater.from (parent.getContext ()).inflate (R.layout.item_manager_new_order, parent, false);
                //inflate.setLayoutParams (lp);
                return new ManagerRequestsAdapter.ViewHolderNewOrder (inflate);
            case 2:
                inflate = LayoutInflater.from (parent.getContext ()).inflate (R.layout.item_manger_my_requests, parent, false);
               // inflate.setLayoutParams (lp);
                return new ManagerRequestsAdapter.ViewHolderWithoutRate (inflate);
            case 3:
                inflate = LayoutInflater.from (parent.getContext ()).inflate (R.layout.item_manage_my_requests_rating, parent, false);
               // inflate.setLayoutParams (lp);
                return new ManagerRequestsAdapter.ViewHolderWithRate (inflate);


        }
        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final MOrder mOrder = mRequests.get (position);

        Log.d (TAG, "onBindViewHolder: getRequestId : " + mOrder.getRequestId ());
        switch (holder.getItemViewType ()) {

            case 1:
                ManagerRequestsAdapter.ViewHolderNewOrder viewHolderNewOrder = (ManagerRequestsAdapter.ViewHolderNewOrder) holder;
                viewHolderNewOrder.item_cat.setText (mOrder.getService ());
                viewHolderNewOrder.item_sub_cat.setText (mOrder.getDetails ());
                viewHolderNewOrder.item_status.setBackground (mContext.getResources ().getDrawable (R.drawable.background_under_revision));
                viewHolderNewOrder.item_status.setText (mContext.getResources ().getString (R.string.new_order));

                Picasso.with (mContext)
                        .load (mOrder.getImage ())
                        .placeholder (R.drawable.avatar)
                        .error (R.drawable.avatar)
                        .into (viewHolderNewOrder.item_icon);

                viewHolderNewOrder.itemView.setOnClickListener (new View.OnClickListener () {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent (mContext, Activity_ViewOrderManager.class);
                        intent.putExtra (FragConst.REQUEST_ID, mRequests.get (position).getRequestId ());

                        mContext.startActivity (intent);
                       /* Fragment fragment = new Manager_Frag_ViewOrderDetail ();
                        Bundle bundle = new Bundle ();
                        bundle.putInt (FragConst.REQUEST_ID, mRequests.get (position).getRequestId ());
                        fragment.setArguments (bundle);
                        ((MainPageManager) mContext).addFragment (fragment);*/
                    }
                });

                break;
            case 2:
                ManagerRequestsAdapter.ViewHolderWithoutRate viewHolderOWithoutRate = (ManagerRequestsAdapter.ViewHolderWithoutRate) holder;
                viewHolderOWithoutRate.request_title.setText (mOrder.getService ());

                viewHolderOWithoutRate.itemView.setOnClickListener (new View.OnClickListener () {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent (mContext, Activity_ViewOrderManager.class);
                        intent.putExtra (FragConst.REQUEST_ID, mRequests.get (position).getRequestId ());

                        mContext.startActivity (intent);
                       /* Fragment fragment = new Manager_Frag_ViewOrderDetail ();
                        Bundle bundle = new Bundle ();
                        bundle.putInt (FragConst.REQUEST_ID, mRequests.get (position).getRequestId ());
                        fragment.setArguments (bundle);
                        ((MainPageManager) mContext).addFragment (fragment);*/
                    }
                });


                Picasso.with (mContext)
                        .load (mOrder.getImage ())
                        .placeholder (R.drawable.avatar)
                        .error (R.drawable.avatar)
                        .into (viewHolderOWithoutRate.request_icon);
                Log.d (TAG, "onBindViewHolder: " + mOrder.getTechIamge ());
                Log.d (TAG, "onBindViewHolder: " + mOrder.getImage ());

                Picasso.with (mContext)
                        .load (mOrder.getTechIamge ())
                        .placeholder (R.drawable.avatar)
                        .error (R.drawable.avatar)
                        .into (viewHolderOWithoutRate.uImage);

                viewHolderOWithoutRate.item_order_technical_name.setText (mOrder.getTechName ());
                viewHolderOWithoutRate.item_order_technical_title.setText (mOrder.getJobTitle ());
                viewHolderOWithoutRate.item_order_time.setText (mOrder.getEndTime ());

                viewHolderOWithoutRate.status.setBackground (mContext.getResources ().getDrawable (R.drawable.background_under_way));
                viewHolderOWithoutRate.status.setText (mContext.getResources ().getString (R.string.underWay));
                viewHolderOWithoutRate.request_status.setText (mOrder.getStatus ());

            /*    switch (mOrder.getStatus ()) {
                    case UNDER_REVISON:
                        viewHolderOWithoutRate.status.setBackground (mContext.getResources ().getDrawable (R.drawable.background_under_revision));
                        viewHolderOWithoutRate.status.setText (mContext.getResources ().getString (R.string.underRevision));
                        viewHolderOWithoutRate.request_status.setText ("السبب");

                        break;
                    case UNDER_WAY:
                        viewHolderOWithoutRate.status.setBackground (mContext.getResources ().getDrawable (R.drawable.background_under_way));
                        viewHolderOWithoutRate.status.setText (mContext.getResources ().getString (R.string.underWay));
                        viewHolderOWithoutRate.request_status.setText (mOrder.getrStatus ());
                        viewHolderOWithoutRate.request_status.setText ("متبقي للتسليم");

                        break;
                }*/

                break;

            case 3:
                ManagerRequestsAdapter.ViewHolderWithRate viewHolderWithRate = (ManagerRequestsAdapter.ViewHolderWithRate) holder;
                viewHolderWithRate.request_title.setText (mOrder.getService ());

                Picasso.with (mContext)
                        .load (mOrder.getImage ())
                        .placeholder (R.drawable.avatar)
                        .error (R.drawable.avatar)
                        .into (viewHolderWithRate.request_icon);

                Picasso.with (mContext)
                        .load (mOrder.getTechIamge ())
                        .placeholder (R.drawable.avatar)
                        .error (R.drawable.avatar)
                        .into (viewHolderWithRate.uImage);

                viewHolderWithRate.order_status.setBackground (mContext.getResources ().getDrawable (R.drawable.background_ended));
                viewHolderWithRate.order_status.setText (mContext.getResources ().getString (R.string.ended));

                viewHolderWithRate.user_name.setText (mOrder.getTechName ());
                viewHolderWithRate.user_title.setText (mOrder.getJobTitle ());

                if (mOrder.getRate () != null) {
                    viewHolderWithRate.ratingBar.setRating (Float.parseFloat ((mOrder.getRate ())));

                }
                viewHolderWithRate.itemView.setOnClickListener (new View.OnClickListener () {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent (mContext, Activity_ViewOrderManager.class);
                        intent.putExtra (FragConst.REQUEST_ID, mRequests.get (position).getRequestId ());

                        mContext.startActivity (intent);
                       /* Fragment fragment = new Manager_Frag_ViewOrderDetail ();
                        Bundle bundle = new Bundle ();
                        bundle.putInt (FragConst.REQUEST_ID, mRequests.get (position).getRequestId ());
                        fragment.setArguments (bundle);
                        ((MainPageManager) mContext).addFragment (fragment);*/
                    }
                });


                break;

        }

    }

    @Override
    public int getItemCount() {
        return mRequests.size ();
    }

    public void loadData(MAllOrders orderses) {
        //   mRequests
        mRequests.addAll (orderses.getData ());

        notifyDataSetChanged ();

    }

    /**
     *
     * create layout for without rate not  finished
     */

    class ViewHolderWithoutRate extends RecyclerView.ViewHolder {
        JFTextView item_order_technical_name, item_order_time, request_status, request_title, item_order_technical_title, status;
        ImageView uImage, request_icon;

        public ViewHolderWithoutRate(View itemView) {
            super (itemView
            );
            item_order_technical_name = (JFTextView) itemView.findViewById (R.id.item_order_technical_name);
            item_order_technical_title = (JFTextView) itemView.findViewById (R.id.item_order_technical_title);
            request_title = (JFTextView) itemView.findViewById (R.id.store_item_name);
            uImage = (ImageView) itemView.findViewById (R.id.uImage);
            request_icon = (ImageView) itemView.findViewById (R.id.store_item_icon);

            status = (JFTextView) itemView.findViewById (R.id.store_item_status);
            request_status = (JFTextView) itemView.findViewById (R.id.request_status);
            item_order_time = (JFTextView) itemView.findViewById (R.id.item_order_time);


        }
    }

    /**
     *
     * create layout for ended with rate  in side manager side
     */
    class ViewHolderWithRate extends RecyclerView.ViewHolder {
        JFTextView user_name, request_title, user_title;
        ImageView uImage, request_icon;
        RatingBar ratingBar;
        View ViewWithRate;
        JFTextView order_status;

        public ViewHolderWithRate(View itemView) {
            super (itemView);
            ViewWithRate = itemView;

            request_icon = (ImageView) itemView.findViewById (R.id.order_icon);
            request_title = (JFTextView) itemView.findViewById (R.id.order_name);
            order_status = (JFTextView) itemView.findViewById (R.id.order_status);

            user_name = (JFTextView) itemView.findViewById (R.id.username);
            user_title = (JFTextView) itemView.findViewById (R.id.user_title);
            uImage = (ImageView) itemView.findViewById (R.id.uImage);
            ratingBar = (RatingBar) itemView.findViewById (R.id.my_rate);


        }
    }

    /**
     *
     * create layout for new order in side manager side
     */
    class ViewHolderNewOrder extends RecyclerView.ViewHolder {
        JFTextView item_cat, item_sub_cat, item_status;
        ImageView item_icon;
        View layout;
        View ViewNewOrder;

        public ViewHolderNewOrder(View itemView) {
            super (itemView);
            ViewNewOrder = itemView;
            layout = itemView.findViewById (R.id.newOrderLayout);
            item_cat = (JFTextView) itemView.findViewById (R.id.item_cat);
            item_sub_cat = (JFTextView) itemView.findViewById (R.id.item_sub_cat);
            item_status = (JFTextView) itemView.findViewById (R.id.item_status);
            item_icon = (ImageView) itemView.findViewById (R.id.item_icon);


        }
    }
}