package com.example.ibraheem.mirva.Fragments.manager;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.example.ibraheem.mirva.Adapter.ManagerRequestsAdapter;
import com.example.ibraheem.mirva.Interfaces.Communicator;
import com.example.ibraheem.mirva.Model.Response.ManagerResonse.MAllOrders;
import com.example.ibraheem.mirva.Model.Response.ManagerResonse.MOrder;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.util.D;
import com.example.ibraheem.mirva.util.L;
import com.example.ibraheem.mirva.util.SessionManager;
import com.example.ibraheem.mirva.util.helperFun;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

/**
 * Created by ibraheem on 05/06/2017.
 */

public class Manager_Frag_NewOrders extends Fragment {


    RecyclerView recyclerView;
    ManagerRequestsAdapter mRequestsAdapter;
    ArrayList<MOrder> mNewOrders;
    Communicator comm;
    User user;
    private ProgressBar mProgressBar;
    private SessionManager mSessionManager;
    private Context mContext;

    D d;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        user = helperFun.getUser (getActivity ().getApplicationContext ());
        d = new D (getActivity ());
      //  d.ShowProgress (getString (R.string.loading));

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mContext = getActivity();
        mSessionManager = new SessionManager(mContext);

        View view = inflater.inflate (R.layout.fragment_manager_my_requests, container, false);
        mNewOrders = new ArrayList<> ();
        mRequestsAdapter = new ManagerRequestsAdapter (getActivity (), mNewOrders);

        recyclerView = (RecyclerView) view.findViewById (R.id.my_request_recyler_view);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager (getActivity ());
        linearLayoutManager.setOrientation (LinearLayout.VERTICAL);
        recyclerView.setLayoutManager (linearLayoutManager);
        recyclerView.setAdapter (mRequestsAdapter);
        comm = (Communicator) getActivity ();
        comm.selectVisibleToolbar (R.id.manager_toolbar_fragments);
        comm.setTitleToolbar (1);
        //  loadData ();
        new FillAllNewOrderAsyncTask ().execute();

        return view;
    }

    @Override
    public void onStart() {
        super.onStart ();

    }

    private void loadData() {
//    public mRequest(Bitmap rImage, String cat, String sub_cat, NOTIFICATION_STATUS status)
   /*     arrayList.add (new mRequest (getBitmap (R.drawable.icon_elect), "كهربائيات", "إضاءات", NOTIFICATION_STATUS.NEW_ORDER));//, "انتهت منذ   15 س : 30 د : 50 ث"));
        arrayList.add (new mRequest (getBitmap (R.drawable.icon_elect), "كهربائيات", "إضاءات", NOTIFICATION_STATUS.NEW_ORDER));//, "انتهت منذ   15 س : 30 د : 50 ث"));
        arrayList.add (new mRequest (getBitmap (R.drawable.icon_elect), "كهربائيات", "إضاءات", NOTIFICATION_STATUS.NEW_ORDER));//, "انتهت منذ   15 س : 30 د : 50 ث"));
        arrayList.add (new mRequest (getBitmap (R.drawable.icon_elect), "كهربائيات", "إضاءات", NOTIFICATION_STATUS.NEW_ORDER));//, "انتهت منذ   15 س : 30 د : 50 ث"));
        arrayList.add (new mRequest (getBitmap (R.drawable.icon_elect), "كهربائيات", "إضاءات", NOTIFICATION_STATUS.NEW_ORDER));//, "انتهت منذ   15 س : 30 د : 50 ث"));
        arrayList.add (new mRequest (getBitmap (R.drawable.icon_elect), "كهربائيات", "إضاءات", NOTIFICATION_STATUS.NEW_ORDER));//, "انتهت منذ   15 س : 30 د : 50 ث"));
        arrayList.add (new mRequest (getBitmap (R.drawable.icon_elect), "كهربائيات", "إضاءات", NOTIFICATION_STATUS.NEW_ORDER));//, "انتهت منذ   15 س : 30 د : 50 ث"));
        mRequestsAdapter.notifyDataSetChanged ();
*/
    }

    Bitmap getBitmap(int res) {
        return BitmapFactory.decodeResource (getResources (), res);
    }


    /**
     *
     * get new order
     */

    public class FillAllNewOrderAsyncTask extends AsyncTask<Void, Void, Void> {

        String lang;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            lang = helperFun.getLanguage(getActivity().getApplicationContext());
        }

        @Override
        protected Void doInBackground(Void... params) {

            ApiInterface apiInterface = ApiUtils.getAPIService (getActivity ());
            apiInterface.getAllMNewOrders (user.getApiToken ()).enqueue (new Callback<MAllOrders> () {
                @Override
                public void onResponse(Call<MAllOrders> call, Response<MAllOrders> response) {
                    if (response.isSuccessful ()) {
                        mRequestsAdapter.loadData (response.body ());
                        mSessionManager.setNewMOrders(response.body (),lang);
                    } else {
                        L.d (TAG, "errorBody : " + response.errorBody ());
                    }
                    // d.hide ();

                    mProgressBar.setVisibility(View.INVISIBLE);

                }

                @Override
                public void onFailure(Call<MAllOrders> call, Throwable t) {
                    mProgressBar.setVisibility(View.INVISIBLE);
                    d.ShowErrorInternetConnection("");
                    if (mSessionManager.getMNewOrders(lang)!=null) {
                        mRequestsAdapter.loadData(mSessionManager.getMNewOrders(lang));
                    }
                }
            });

            return null;
        }
    }



}