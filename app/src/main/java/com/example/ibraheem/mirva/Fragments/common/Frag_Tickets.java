package com.example.ibraheem.mirva.Fragments.common;


import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.example.ibraheem.mirva.Adapter.TicketAdapter;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.Interfaces.Communicator;
import com.example.ibraheem.mirva.Interfaces.frag2Adapter;
import com.example.ibraheem.mirva.Model.Response.TicketsResonse.AllTickets;
import com.example.ibraheem.mirva.Model.Response.TicketsResonse.Tikect;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.util.D;
import com.example.ibraheem.mirva.util.L;
import com.example.ibraheem.mirva.util.SessionManager;
import com.example.ibraheem.mirva.util.helperFun;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class Frag_Tickets extends Fragment implements frag2Adapter {

    public static final String TAG = "tickets_fragment";
    ArrayList<Tikect> tickets;
    RecyclerView recyclerView;
    TicketAdapter ticketAdapter;
    JFTextView number_of_notifications;
    Communicator comm;
    ApiInterface mAPIService;

    /*SwipeLayout swipeLayout = (SwipeLayout)convertView.findViewById(R.id.swipe);
    //set show mode.
    swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
    swipeLayout.setDragEdges(SwipeLayout.DragEdge.Left,SwipeLayout.DragEdge.Right);
    swipeLayout.setBottomViewIds(R.id.bottom_wrapper, R.id.bottom_wrapper_2,R.id.bottom_wrapper, R.id.bottom_wrapper_2);

     */
    //JFTextView show_noData;
    User user;
    int currentPageNumber = 0;
    D d;
    private Context mContext;
    private SessionManager mSessionManager;
    private ProgressBar mProgressBar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user = helperFun.getUser(getActivity());
        mAPIService = ApiUtils.getAPIService(getActivity());
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tickets, container, false);
        mContext = getActivity();
        d = new D(getActivity());
        mSessionManager = new SessionManager(mContext);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        tickets = new ArrayList<>();

        number_of_notifications = (JFTextView) getActivity().findViewById(R.id.number_of_notifications);

        recyclerView = (RecyclerView) view.findViewById(R.id.rv_tickets);
        ticketAdapter = new TicketAdapter(this, getContext(), tickets);
        //show_noData = (JFTextView) view.findViewById (R.id.show_noData);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayout.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(ticketAdapter);


        comm = (Communicator) getActivity();
        comm.setTitleToolbar(getString(R.string.tickets_messages));
        comm.selectVisibleToolbar(R.id.toolbar_clints);

        new FillAllAsyncTask().execute();
        return view;
    }

    void getTtickets(int page) {

        Log.d(TAG, "onStart: user getApiToken" + user.getApiToken());

        mAPIService.getAllTickets(user.getApiToken(), page).enqueue(new Callback<AllTickets>() {
            @Override
            public void onResponse(Call<AllTickets> call, Response<AllTickets> response) {
                if (response.isSuccessful()) {
                    AllTickets tikects = response.body();
                    mSessionManager.setTickets(tikects);
                    L.d(TAG, "getData size ... " + tikects.getData().size());
                    ArrayList<Tikect> data = tikects.getData();
                    if (!data.isEmpty()) {
                        ticketAdapter.LoadData(data);
                        number_of_notifications.setText("" + ticketAdapter.getItemCount());
                        //show_noData.setVisibility (View.GONE);
                        if (tikects.getNextPageUrl() != null) {
                            ++currentPageNumber;

                        } else {
                            currentPageNumber = -1;

                        }
                    } else {
                        L.d(TAG, "errorBody : " + response.errorBody());

                    }
                    mProgressBar.setVisibility(View.INVISIBLE);
                }

                // d.hide ();
            }

            @Override
            public void onFailure(Call<AllTickets> call, Throwable t) {
                // d.hide ();
                d.ShowErrorInternetConnection("");
                AllTickets tikects = mSessionManager.getTickets();
                if (tikects != null) {
                    ArrayList<Tikect> data = tikects.getData();
                    if (!data.isEmpty()) {
                        ticketAdapter.LoadData(data);
                        number_of_notifications.setText("" + ticketAdapter.getItemCount());
                        //show_noData.setVisibility (View.GONE);
                        if (tikects.getNextPageUrl() != null) {
                            ++currentPageNumber;

                        } else {
                            currentPageNumber = -1;

                        }
                    }
                }
                mProgressBar.setVisibility(View.INVISIBLE);
            }

        });
    }

    private void sendMesssageSeen() {


        ApiInterface apiInterface = ApiUtils.getAPIService(getActivity());
        apiInterface.updateSeenMessage(user
                .getApiToken()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {

                    Log.d(TAG, "onResponse: " + response.body().toString());
                } else {
                    Log.d(TAG, "onResponse: " + response.errorBody().toString());

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {


            }
        });
    }

    @Override
    public void getNewTickets() {

        if (currentPageNumber != -1) {
            getTtickets(currentPageNumber);

        }

    }

    private class FillAllAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            sendMesssageSeen();

            getTtickets(currentPageNumber);
            return null;
        }
    }

}
