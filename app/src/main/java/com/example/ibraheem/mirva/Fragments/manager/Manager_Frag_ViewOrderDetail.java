package com.example.ibraheem.mirva.Fragments.manager;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.VideoView;

import com.example.ibraheem.mirva.Adapter.ViewImageAdapter;
import com.example.ibraheem.mirva.Const.FragConst;
import com.example.ibraheem.mirva.CustomViews.JFButton;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.Fragments.Client.Frag_ViewOrderDetail;
import com.example.ibraheem.mirva.Interfaces.Communicator;
import com.example.ibraheem.mirva.Interfaces.Actions;
import com.example.ibraheem.mirva.Model.Response.Order.ViewOrder.Request;
import com.example.ibraheem.mirva.Model.Response.Order.ViewOrder.ViewOrder;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.UitilsFun;
import com.example.ibraheem.mirva.util.D;
import com.example.ibraheem.mirva.util.L;
import com.example.ibraheem.mirva.util.helperFun;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ibraheem on 19/06/2017.
 */

public class Manager_Frag_ViewOrderDetail extends Fragment {

    public final String TAG = Frag_ViewOrderDetail.class.getSimpleName ();
    Communicator comm;
    JFButton select_technical;
    JFTextView deivery_time, order_cat, order_service, order_status, sended_order_status, order_details, order_price, payment_done;
    VideoView order_layout_video;
    LinearLayout layout_case, show_price_layout;
    MapView mMapView;
    GoogleMap googleMap;
    RecyclerView order_layout_images;
    ArrayList<String> imagePaths;
    ViewImageAdapter viewImageAdapter;
    JFTextView order_no_attachment, select_technical_done;
    View type_order_layout;
    User user;
    D d;
    int REQUEST_ID;
    Actions actions;
    LatLng latLng;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        user = helperFun.getUser (getActivity ());
        d = new D (getActivity ());
        d.ShowProgress (getString (R.string.loading));
        Bundle arguments = getArguments ();

        if (arguments != null) {
            REQUEST_ID = arguments.getInt (FragConst.REQUEST_ID, 0);

        }


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate (R.layout.manager_frag_view_order, container, false);
        select_technical = (JFButton) view.findViewById (R.id.select_technical);
        type_order_layout = view.findViewById (R.id.type_order_layout);
        deivery_time = (JFTextView) view.findViewById (R.id.order_date);

        select_technical_done = (JFTextView) view.findViewById (R.id.select_technical_done);

        order_status = (JFTextView) view.findViewById (R.id.order_status);
        order_layout_video = (VideoView) view.findViewById (R.id.order_layout_video);
        layout_case = (LinearLayout) view.findViewById (R.id.layout_case);
        order_cat = (JFTextView) view.findViewById (R.id.order_cat);
        order_service = (JFTextView) view.findViewById (R.id.order_service);
        mMapView = (MapView) view.findViewById (R.id.mapView_new_order);
        sended_order_status = (JFTextView) view.findViewById (R.id.sended_order_status);
        mMapView.onCreate (savedInstanceState);
        order_no_attachment = (JFTextView) view.findViewById (R.id.order_no_attachment);
        order_layout_images = (RecyclerView) view.findViewById (R.id.order_layout_images);
        imagePaths = new ArrayList<> ();
        order_details = (JFTextView) view.findViewById (R.id.order_details);


        viewImageAdapter = new ViewImageAdapter (getActivity (), imagePaths);
        LinearLayoutManager layoutManager = new LinearLayoutManager (view.getContext ());
        layoutManager.setOrientation (LinearLayoutManager.HORIZONTAL);
        order_layout_images.setLayoutManager (layoutManager);
        order_layout_images.setAdapter (viewImageAdapter);


        mMapView.getMapAsync (new OnMapReadyCallback () {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                mMapView.onResume();           //The methods
                mMapView.onEnterAmbient(null); // that made my map work
            }
        });


        if (REQUEST_ID != 0) {
            getOrderData (REQUEST_ID);
        } else {
            d.ShowError ("");
        }

        comm = (Communicator) getActivity ();
        comm.setTitleToolbar (getActivity ().getString (R.string.order_details));
        comm
                .selectVisibleToolbar (R.id.manager_toolbar_fragments);
        actions = (Actions) getActivity ();
        return view;
    }

    /**
     *
     * get request order  from manage side
     * @param REQUEST_ID
     */
    void getOrderData(final int REQUEST_ID) {

        Log.d (TAG, "getOrderData:getApiToken " + user.getApiToken ());
        Log.d (TAG, "getOrderData:getApiToken " + REQUEST_ID);
        ApiInterface apiInterface = ApiUtils.getAPIService (getActivity ());
        apiInterface.getSODSupervise (user.getApiToken (), REQUEST_ID).enqueue (new Callback<ViewOrder> () {
            @Override
            public void onResponse(Call<ViewOrder> call, Response<ViewOrder> response) {

                if (response.isSuccessful ()) {
                    ViewOrder viewOrder = response.body ();
                    final Request request = viewOrder.getData ();
                    Log.d (TAG, "onResponse: " + response.body ());

                    L.d (TAG, "getStatus " + request.getStatus ());

                    order_cat.setText (request.getSubcategory ());
                    order_service.setText (request.getService ());
                    order_details.setText (request.getDetails ());


                    if (request.getStatusType ().equals ("19")) {
                        type_order_layout.setVisibility (View.VISIBLE);
                        deivery_time.setText (request.getEndTime ());
                        order_status.setText (getString (R.string.type_case_schedule));

                    } else {
                        order_status.setText (getString (R.string.type_case_emergency));

                    }

                    switch (request.getStatusId ()) {

                        case 1:
                            sended_order_status.setBackground (getResources ().getDrawable (R.drawable.background_under_revision));
                            sended_order_status.setText (getResources ().getString (R.string.new_order));
                            select_technical.setVisibility (View.VISIBLE);
                            select_technical.setOnClickListener (new View.OnClickListener () {
                                                                     @Override
                                                                     public void onClick(View v) {
                                                                         Fragment fragment = new Manager_Frag_SelectTechnicalToOrder ();
                                                                         Bundle bundle = new Bundle ();
                                                                         Log.d (TAG, "onClick: REQUEST_ID : " + REQUEST_ID);
                                                                         bundle.putInt (FragConst.REQUEST_ID, REQUEST_ID);
                                                                         fragment.setArguments (bundle);
                                                                         comm.addFragment (fragment);
                                                                     }
                                                                 }
                            );

                            break;
                        case 2:
                            sended_order_status.setBackground (getResources ().getDrawable (R.drawable.background_under_way));
                            sended_order_status.setText (getResources ().getString (R.string.underWay));

                            select_technical_done.setVisibility (View.VISIBLE);

                            break;

                        case 3:
                            sended_order_status.setBackground (getResources ().getDrawable (R.drawable.background_ended));
                            sended_order_status.setText (getResources ().getString (R.string.ended));


                            select_technical_done.setVisibility (View.VISIBLE);

                            break;
                    }
                    // sended_order_status.setBackground (getActivity ().getResources ().getDrawable (R.drawable.background_under_revision));


                    // sended_order_status.setText (request.getStatus ());
                    latLng = new LatLng (Double.parseDouble (request.getLatitude ()), Double.parseDouble (request.getLongitude ()));
                    if (latLng != null) {

                        mMapView.onResume ();
                        googleMap.addMarker (new MarkerOptions ()
                                .position (latLng)
                                .title ("Location")
                                .snippet ("First Marker")
                                .icon (BitmapDescriptorFactory.fromBitmap (UitilsFun.getBitmap (getActivity (), R.drawable.map_marker))));


                        googleMap.moveCamera (CameraUpdateFactory.newCameraPosition (new CameraPosition.Builder ()
                                .target (latLng)
                                .zoom (17.5F)
                                .build ()));


                    }

                    if (request.getImages () != null && request.getImages ().size () != 0) {

                        order_layout_images.setVisibility (View.VISIBLE);
                        viewImageAdapter.loadData (request.getImages ());
                        Log.d (TAG, "onResponse: " + request.getImages ());

                    } else if (request.getVideos () != null && +request.getVideos ().size () != 0) {
                        if (Uri.parse (request.getVideos ().get (0)) != null) {

                            order_layout_video.setVisibility (View.VISIBLE);
                            L.d (TAG, "getVideos ()" + request.getVideos ().get (0));
                            order_layout_video.setVideoURI (Uri.parse (ApiUtils.BASE_URL + request.getVideos ().get (0)));

                        }

                    } else {
                        order_no_attachment.setVisibility (View.VISIBLE);
                    }


 /*  type_order_layout = view.findViewById (R.id.type_order_layout);
        deivery_time = (JFTextView) view.findViewById (R.id.order_date);
        order_layout_video = (VideoView) view.findViewById (R.id.order_layout_video);
        layout_case =(LinearLayout)view.findViewById (R.id.layout_case);
        order_cat = (JFTextView) view.findViewById (R.id.order_cat);
        order_service = (JFTextView) view.findViewById (R.id.order_service);
        mMapView = (MapView) view.findViewById (R.id.mapView_new_order);
        mMapView.onCreate (savedInstanceState);
 */
                    d.hide ();

                } else

                {

                    try {
                        d.hide ();
                        d.ShowError (response.errorBody ().string ());

                    } catch (IOException e) {
                        /// e.printStackTrace ();
                    }
                }
            }

            @Override
            public void onFailure(Call<ViewOrder> call, Throwable t) {
                d.hide ();
                d.ShowErrorInternetConnection ("");
            }
        });


    }

    @Override
    public void onResume() {

        mMapView.onResume ();

        super.onResume ();

    }

    @Override
    public void onPause() {

        mMapView.onPause ();
        super.onPause ();

    }

    @Override
    public void onStop() {
        mMapView.onStop ();
        super.onStop ();

    }

    @Override
    public void onDestroy() {
        mMapView.onDestroy ();

        super.onDestroy ();


    }

    @Override
    public void onLowMemory() {
        mMapView.onLowMemory ();
        super.onLowMemory ();

    }
}