package com.example.ibraheem.mirva.Fragments.Client;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.location.Location;
import android.location.LocationListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.VideoView;

import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.example.ibraheem.mirva.Activities.AllSubscriptionsActivity;
import com.example.ibraheem.mirva.Activities.ClientNewOrder;
import com.example.ibraheem.mirva.Activities.EditProfileActivity;
import com.example.ibraheem.mirva.Activities.MyNotificationActivity;
import com.example.ibraheem.mirva.Adapter.ImageAdapter;
import com.example.ibraheem.mirva.Const.FragConst;
import com.example.ibraheem.mirva.CustomViews.FWTextView;
import com.example.ibraheem.mirva.CustomViews.JFButton;
import com.example.ibraheem.mirva.CustomViews.JFEditText;
import com.example.ibraheem.mirva.CustomViews.JFSpinnerAdapter;
import com.example.ibraheem.mirva.CustomViews.JFSwitch;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.FragmentMessages.SendOrderDone;
import com.example.ibraheem.mirva.Interfaces.Communicator;
import com.example.ibraheem.mirva.Model.Advertisment;
import com.example.ibraheem.mirva.Model.AllAdvertisment;
import com.example.ibraheem.mirva.Model.IamgeModle;
import com.example.ibraheem.mirva.Model.Response.Tickets.OrderCat;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.Model.Response.WorkingHour;
import com.example.ibraheem.mirva.Model.Response.helperRes;
import com.example.ibraheem.mirva.Model.SendOrder;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.UitilsFun;
import com.example.ibraheem.mirva.util.D;
import com.example.ibraheem.mirva.util.L;
import com.example.ibraheem.mirva.util.SessionManager;
import com.example.ibraheem.mirva.util.SharedPrefs;
import com.example.ibraheem.mirva.util.V;
import com.example.ibraheem.mirva.util.ViewPagerAdapter;
import com.example.ibraheem.mirva.util.helperFun;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.JsonObject;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by ibraheem on 16/07/2017.
 */

public class ActivityNewOrder extends AppCompatActivity implements AdapterView.OnItemSelectedListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, ViewPager.OnPageChangeListener

{
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 99;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS_Media = 100;
    final static int RESULT_LOAD_IMAGE = 100;
    final static int RESULT_LOAD_Vedio = 200;
    static final int OPEN_MEDIA_PICKER = 1; // The request code
    private static final int REQUEST_CODE_PICKER = 220;
    public final String TAG = ActivityNewOrder.class.getSimpleName();
    JFSpinnerAdapter appinmentAdapter;
    String[] mServiceSubCategories;
    String[] mWorkingHoursStrings;
    //Spinner spinner_type_of_order;
    Spinner spinner_select_appointment;
    Communicator comm;
    //JFTextView tv_select_subscrbtion;
    JFSwitch case_switch;
    View timing_layout;
    int serviceId;
    Handler mHandler;
    Runnable mPendingRunnable;
    JFButton btn_addMore;
    User user;
    String lang;
    D dialog;
    //Dialog DGM;
    LinearLayout btn_addIamges, btn_add_video, container_add_media;
    RelativeLayout container_add_video;
    LinearLayout container_add_images;
    RecyclerView rv_add_images;
    ImageAdapter imageAdapter;
    ArrayList<IamgeModle> imagePaths;
    ProgressDialog progressDialog;
    VideoView videoView;
    Uri videoPath;
    JFButton deleteVideo;

    MapView mMapView;
    LatLng tempLng;
    Marker d_mk;
    CameraPosition INIT;
    V vd;
    JFEditText order_details;
    // MapView d_map_view;
    View sendOKMessage;
    ImageView btn_zoom_map;
    Marker mk;
    MarkerOptions marker, d_marker;
    ApiInterface mAPIService;
    ArrayList<OrderCat> orderCats;

    ArrayList<WorkingHour> workingHours;
    ArrayList<WorkingHour> workingHoursAvailable;
    MultipartBody.Part requestImage;
    ArrayList<String> urlImages;
    RequestBody requestFile;
    String urlVideo;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    private SupportMapFragment mSupportMapFragment;
    private MediaController mediaControls;
    private GoogleMap googleMap;
    private WorkingHoursAdapter mWorkingHoursAdapter;
    //d_googleMap;

    private String[] permissions = new String[]{
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
    };
    private String[] permissions_media = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.MEDIA_CONTENT_CONTROL
    };


    JFTextView c_title_toolbar, number_of_notifications, type_of_order_tv;
    FWTextView frag_back;
    Toolbar toolbar_view_order;
    int selectedCase = 18;

    ImageView myLocation;
    SweetAlertDialog showProgress;
    View mapViewLayout;
    boolean isZoom = false;
    private ProgressBar mProgressBar, mWorkingHourDialogProgress;
    private Context mContext;
    private int PLACE_PICKER_REQUEST = 1;
    //private MarkerOptions currentPositionMarker;
    private JFButton mLocationTextJfButton;

    private LinearLayout pager_indicator;
    private int dotsCount;
    private ImageView[] dots;
    private ViewPagerAdapter mAdapter;
    private ViewPager intro_images;

    private Dialog mWorkingHourDialog;
    private RecyclerView mWorkingHoursRecyclerView;
    private int mSelectedDayId = -1;
    Timer timer;
    int page = 0;
    private int imagesSize = 0, spinner_type_of_order_position = -1;
    private MaterialDialog singleChoiceDialog;
    private MaterialDialog mWorkingHoursSingleChoiceDialog;
    private ScrollView mNewOrderScrollView;

    private SessionManager mSessionManager;

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_new_order);
        mContext = this;
        mSessionManager = new SessionManager(mContext);
        mNewOrderScrollView = (ScrollView) findViewById(R.id.new_order_scroll);
        mNewOrderScrollView.setVisibility(View.INVISIBLE);
        workingHours = new ArrayList<>();
        mWorkingHoursAdapter = new WorkingHoursAdapter(workingHours, R.layout.time_custom_layout);

        intro_images = (ViewPager) findViewById(R.id.pager_introduction);
        pager_indicator = (LinearLayout) findViewById(R.id.viewPagerCountDots);


        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mapViewLayout = findViewById(R.id.mapViewLayout);
        mLocationTextJfButton = (JFButton) findViewById(R.id.new_location_change_location_btn);
        mLocationTextJfButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try {
                    startActivityForResult(builder.build(ActivityNewOrder.this), PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
        });

        vd = new V(this);
        urlImages = new ArrayList<>();
        dialog = new D(ActivityNewOrder.this);
        showProgress = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        /*showProgress = this.dialog.ShowProgress(getString(R.string.loading));
        showProgress.hide();*/
        findViewById(R.id.frag_notification).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), MyNotificationActivity.class));

            }
        });
        findViewById(R.id.view_order_notification_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), MyNotificationActivity.class));

            }
        });

        toolbar_view_order = (Toolbar) findViewById(R.id.toolbar_view_order);
        c_title_toolbar = (JFTextView) toolbar_view_order.findViewById(R.id.c_title_toolbar);
        number_of_notifications = (JFTextView) toolbar_view_order.findViewById(R.id.number_of_notifications);
        type_of_order_tv = (JFTextView) findViewById(R.id.type_of_order_tv);
        type_of_order_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSingleListChoiceDialog();
            }
        });

        frag_back = (FWTextView) toolbar_view_order.findViewById(R.id.frag_back);
        frag_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Intent intent = getIntent();
        if (intent != null) {
            serviceId = intent.getIntExtra(FragConst.SERVICE_ID, -1);
        } else {

            finish();
        }


        lang = helperFun.getLanguage(this.getApplicationContext());
        user = helperFun.getUser(this.getApplicationContext());
        new FillAdvertismentsAsyncTask().execute();

        imagePaths = new ArrayList<>();

        getLastSelectedLocation();

        btn_zoom_map = (ImageView) findViewById(R.id.btn_zoom_map);
        order_details = (JFEditText) findViewById(R.id.order_details);
        //  sendOKMessage = findViewById (R.id.sendOKMessage);

        mMapView = (MapView) findViewById(R.id.mapView_new_order);

        btn_addMore = (JFButton) findViewById(R.id.btn_addMore);
        btn_addMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE);*/
                //ImagePicker.pickImage(ActivityNewOrder.this, getString(R.string.choose_image));
                ImagePicker.create(ActivityNewOrder.this)
                        .returnAfterFirst(true) // set whether pick or camera action should return immediate result or not. For pick image only work on single mode
                        .imageTitle(getString(R.string.choose_image)) // image selection title
                        .single() // single mode
                        .showCamera(true) // show camera or not (true by default)
                        .theme(R.style.AppTheme) // must inherit ef_BaseTheme. please refer to sample
                        .start(REQUEST_CODE_PICKER); // start image picker activity with request code
            }
        });

        findViewById(R.id.btn_Subscriptions).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, AllSubscriptionsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                mContext.startActivity(intent);
            }
        });

        btn_addIamges = (LinearLayout) findViewById(R.id.btn_addIamges);
        //  btn_addIamges.setVisibility (GONE);
        btn_add_video = (LinearLayout) findViewById(R.id.btn_add_video);
        //   btn_add_video.setVisibility (GONE);
        container_add_video = (RelativeLayout) findViewById(R.id.container_add_video);
        container_add_images = (LinearLayout) findViewById(R.id.container_add_images);
        container_add_media = (LinearLayout) findViewById(R.id.container_add_media);

        rv_add_images = (RecyclerView) findViewById(R.id.rv_add_images);
        imageAdapter = new ImageAdapter(this, imagePaths);
        deleteVideo = (JFButton) findViewById(R.id.deleteVideo);

        deleteVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                container_add_media.setVisibility(View.VISIBLE);
                container_add_video.setVisibility(View.GONE);
                container_add_images.setVisibility(View.GONE);
                videoView.setVideoURI(null);
                videoView.stopPlayback();
                videoPath = null;
            }
        });

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getBaseContext());
        linearLayoutManager.setOrientation(LinearLayout.HORIZONTAL);
        rv_add_images.setLayoutManager(linearLayoutManager);
        rv_add_images.setAdapter(imageAdapter);


        videoView = (VideoView) findViewById(R.id.add_video);
        if (mediaControls == null) {
            mediaControls = new MediaController(this);
            videoView.setMediaController(mediaControls);
            mediaControls.setMinimumWidth(100);
        }


        btn_addIamges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Initialize Google Play Services
                /*if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(getBaseContext(),
                            Manifest.permission.READ_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED &&
                            ContextCompat.checkSelfPermission(getBaseContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        Log.d(TAG, "onClick: btn_addIamges + permsston");
                        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, RESULT_LOAD_IMAGE);

                    } else {
                        Log.d(TAG, "onClick: btn_addIamges + checkMediaPermission");

                        checkMediaPermission();
                    }
                } else {

                    Log.d(TAG, "onClick: btn_addIamges + not M <");
                    Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(Intent.createChooser(i, "Select Picture"), RESULT_LOAD_IMAGE);
                }*/
                //ImagePicker.pickImage(ActivityNewOrder.this, getString(R.string.choose_image));
                ImagePicker.create(ActivityNewOrder.this)
                        .returnAfterFirst(true) // set whether pick or camera action should return immediate result or not. For pick image only work on single mode
                        .imageTitle(getString(R.string.choose_image)) // image selection title
                        .single() // single mode
                        .showCamera(true) // show camera or not (true by default)
                        .theme(R.style.AppTheme) // must inherit ef_BaseTheme. please refer to sample
                        .start(REQUEST_CODE_PICKER); // start image picker activity with request code

            }
        });


        btn_add_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Initialize Google Play Services
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(getBaseContext(),
                            Manifest.permission.READ_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED &&
                            ContextCompat.checkSelfPermission(getBaseContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        //Location Permission already granted

                        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        intent.setType("video/*");
                        startActivityForResult(intent, RESULT_LOAD_Vedio);


                    } else {

                        checkMediaPermission();
                    }
                } else {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("video/*");
                    startActivityForResult(intent, RESULT_LOAD_Vedio);

                }


                /*videoView.setOnPreparedListener (new MediaPlayer.OnPreparedListener () {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        //   progressDialog.dismiss ();

                    }
                });*/
            }
        });


        //spinner_type_of_order = (Spinner) findViewById(R.id.spinner_type_of_order);
        //customAdapter = new JFSpinnerAdapter(getBaseContext(), R.layout.item_spinner);
        /*spinner_type_of_order.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                new MaterialDialog.Builder(ActivityNewOrder.this)
                        .title(R.string.about)
                        .items(mServiceSubCategories)
                        .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                            @Override
                            public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {

                                return true;
                            }
                        })
                        .positiveText(R.string.accept)
                        .show();
                return false;
            }
        });*/
        //spinner_type_of_order.setAdapter(customAdapter);

        /*tv_select_subscrbtion = (JFTextView) findViewById(R.id.tv_select_subscrbtion);
        tv_select_subscrbtion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getBaseContext(), Account_Subscription.class));


            }
        });*/
        timing_layout = findViewById(R.id.timing_layout);


        spinner_select_appointment = (Spinner) findViewById(R.id.spinner_select_appointment);
        spinner_select_appointment.setOnItemSelectedListener(ActivityNewOrder.this);
        case_switch = (JFSwitch) findViewById(R.id.case_switch);


        appinmentAdapter = new JFSpinnerAdapter(getBaseContext(), R.layout.item_spinner);
        spinner_select_appointment.setAdapter(appinmentAdapter);
        spinner_select_appointment.setSelection(0);

        case_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    //timing_layout.setVisibility(View.VISIBLE);
                    /*mWorkingHourDialog = new Dialog(mContext);
                    mWorkingHourDialog.setContentView(R.layout.working_hours_custom_dialog);
                    mWorkingHoursRecyclerView = (RecyclerView) mWorkingHourDialog.findViewById(R.id.working_hour_recycler);
                    mWorkingHourDialogProgress = (ProgressBar) mWorkingHourDialog.findViewById(R.id.progressBar);
                    LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(mContext);
                    mWorkingHoursRecyclerView.setLayoutManager(mLinearLayoutManager);*/

                    //showWorkingHoursSingleListChoiceDialog();
                    startActivity(ClientNewOrder.newIntent(mContext,serviceId));

                    /*mWorkingHourDialog.show();*/
                    selectedCase = 19;


                } else {

                    timing_layout.setVisibility(View.GONE);
                    selectedCase = 18;


                }
            }
        });


       /* DGM = new Dialog (this);
        DGM.requestWindowFeature (Window.FEATURE_NO_TITLE);
        DGM.setContentView (R.layout.dialog_google_map);
        d_map_view = (MapView) DGM.findViewById (R.id.google_map);
*/
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                buildGoogleApiClient();

                mMapView.setEnabled(true);
                mk = googleMap.addMarker(new MarkerOptions()
                        .position(tempLng)
                        .draggable(true)
                        .title("Selected Postion")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_placeholder_point)));

                googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                        .target(tempLng)
                        .zoom(15.6F)
                        .build()));

                btn_zoom_map.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //DGM.show ();

                        Log.d(TAG, "onClick: rezie iamge ");
                        if (!isZoom) {
                            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);


                            mapViewLayout.setLayoutParams(layoutParams);

                            // btn_zoom_map.setText ("-");
                            btn_zoom_map.setBackgroundResource(R.drawable.icon_forward_button);

                            // btn_zoom_map.setBackground (getResources ().getDrawable (R.drawable.icon_forward_button));
                            isZoom = true;
                        } else {

                            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, getResources().getDimensionPixelSize(R.dimen.hight_map));

                            btn_zoom_map.setBackgroundResource(R.drawable.icon_full_screen_filled);

                            layoutParams.setMargins(getResources().getDimensionPixelSize(R.dimen.margin_view),
                                    getResources().getDimensionPixelSize(R.dimen.margin_view), getResources().getDimensionPixelSize(R.dimen.margin_view), getResources().getDimensionPixelSize(R.dimen.margin_view));
                            //  btn_zoom_map.setText ();


                            mapViewLayout.setLayoutParams(layoutParams);
                            isZoom = false;

                        }


                    }
                });
              /*  googleMap.setOnMarkerDragListener (new GoogleMap.OnMarkerDragListener () {
                    @Override
                    public void onMarkerDragStart(Marker marker) {

                    }

                    @Override
                    public void onMarkerDrag(Marker marker) {

                        LatLng dragPosition = marker.getPosition ();
                        double dragLat = dragPosition.latitude;
                        double dragLong = dragPosition.longitude;
                        tempLng = new LatLng (dragLat, dragLong);
                        animateMarker (marker, tempLng, false);
                    }

                    @Override
                    public void onMarkerDragEnd(Marker marker) {


                    }
                });*/
                googleMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
                    @Override
                    public void onCameraMove() {
                        LatLng centerOfMap = googleMap.getCameraPosition().target;
                        mk.setPosition(centerOfMap);

                    }
                });

            }
        });


        //JFButton btn_selectPostion = (JFButton) DGM.findViewById (R.id.btn_selectPostion);

      /*  btn_selectPostion.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {

              /*  if (mk != null) {
                    //     mk.setPosition (d_mk.getPosition ());
                    animateMarker (mk, d_mk.getPosition (), false);
                }

                mk.setPosition (d_mk.getPosition ());

                googleMap.moveCamera (CameraUpdateFactory.newCameraPosition (new CameraPosition.Builder ()
                        .target (mk.getPosition ())
                        .zoom (15.6F)
                        .build ()));

              //  DGM.dismiss ();

            }
        });
        //mMapView.setBackgroundColor (getResources ().getColor (R.color.float_transparent));
    //    d_map_view.setBackgroundColor (getResources ().getColor (R.color.float_transparent));
*/

        //d_map_view.onCreate (DGM.onSaveInstanceState ());
     /*   d_map_view.onResume ();// needed to get the map to display immediately
        d_map_view.getMapAsync (new OnMapReadyCallback () {
            @Override
            public void onMapReady(final GoogleMap map) {

                d_googleMap = map;
                buildGoogleApiClient ();
                ///
                // d_map_view.setEnabled (true);
                d_map_view.setActivated (true);

                d_mk = d_googleMap.addMarker (new MarkerOptions ()
                        .position (tempLng)
                        .draggable (true)
                        .title ("Selected Postion")
                        .icon (BitmapDescriptorFactory.fromBitmap (UitilsFun.getBitmap (getBaseContext (), R.drawable.map_marker))));

                d_googleMap.moveCamera (CameraUpdateFactory.newCameraPosition (new CameraPosition.Builder ()
                        .target (tempLng)
                        .zoom (15.6F)
                        .build ()));
                d_googleMap.setOnMarkerDragListener (new GoogleMap.OnMarkerDragListener () {
                    @Override
                    public void onMarkerDragStart(Marker marker) {

                    }

                    @Override
                    public void onMarkerDrag(Marker marker) {

                        LatLng dragPosition = marker.getPosition ();
                        double dragLat = dragPosition.latitude;
                        double dragLong = dragPosition.longitude;
                        tempLng = new LatLng (dragLat, dragLong);
                        animateMarker (marker, tempLng, false);
                    }

                    @Override
                    public void onMarkerDragEnd(Marker marker) {


                    }
                });

            }
        });*/


        findViewById(R.id.btn_sendOrder).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (spinner_type_of_order_position == -1) {
                    Toast.makeText(mContext, getString(R.string.choose_order_type) + "", Toast.LENGTH_SHORT).show();
                    //type_of_order_tv.setError(getString(R.string.err_selectOrderType));
                    //dialog.ShowError (getString (R.string.err_selectOrderType));
                    showSingleListChoiceDialog();
                    return;
                }
                mSelectedDayId = mSessionManager.getCurrentOrderWorkingId();

                if (selectedCase == 19 && mSelectedDayId == -1) {
                    Toast.makeText(mContext, getString(R.string.err_selectTime), Toast.LENGTH_SHORT).show();

                    return;
                }
                if (TextUtils.isEmpty(order_details.getText())) {
                    order_details.setError(getString(R.string.err_empty));
                    order_details.requestFocus();
                    return;
                }


                //  vd.validateTextArea (R.id.order_details);

                if (UitilsFun.isNetworkAvailable(getBaseContext())) {

                    if (d_marker != null) {
                        SharedPrefs.save(getBaseContext(), SharedPrefs.LATITUDE, String.valueOf(d_marker.getPosition().latitude));
                        SharedPrefs.save(getBaseContext(), SharedPrefs.LONGITUDE, String.valueOf(d_marker.getPosition().longitude));
                    }


                    sendOrder();
                }
            }

        });


        new FillAllAsyncTask().execute();
        //  myLocation = (ImageView) DGM.findViewById (R.id.myLocation);


    }

    private void showSingleListChoiceDialog() {
        singleChoiceDialog = new MaterialDialog.Builder(ActivityNewOrder.this)
                .title(R.string.choose_order_type)
                .items(mServiceSubCategories)
                .widgetColorRes(R.color.colorPrimaryDark)
                .titleColorRes(R.color.colorPrimaryDark)
                .positiveColorRes(R.color.colorPrimaryDark)
                .titleGravity(GravityEnum.CENTER)
                .typeface("jf_flat_regular.ttf", "jf_flat_regular.ttf")
                .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        if (which >= 0) {
                            spinner_type_of_order_position = which;
                            type_of_order_tv.setText(orderCats.get(spinner_type_of_order_position).getName());
                        }
                        return true;
                    }
                })
                .positiveText(R.string.ok)
                .show();

    }

       /*private void myLocationAction() {

            if (ContextCompat.checkSelfPermission (getBaseContext (),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission (getBaseContext (), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                myLocation.setVisibility (View.GONE);

                d_googleMap.setMyLocationEnabled (true);

            } else {
                myLocation.setVisibility (View.VISIBLE);


            }
        }
        */

    @Override
    protected void onPostResume() {
       /* if (mMapView != null) {
            mMapView.onResume ();

        }
        if (d_map_view != null) {
            d_map_view.onResume ();

        }*/
        super.onPostResume();
    }

    private class FillAdvertismentsAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            ApiInterface apiInterface = ApiUtils.getAPIService(ActivityNewOrder.this);
            apiInterface.getAllAdvertisment(user.getApiToken(), lang).enqueue(new Callback<AllAdvertisment>() {
                @Override
                public void onResponse(Call<AllAdvertisment> call, Response<AllAdvertisment> response) {
                    if (response.isSuccessful()) {
                        AllAdvertisment mAllAdvertisment = response.body();
                        ArrayList<Advertisment> mAdvertismentsArrayList = mAllAdvertisment.getData();
                        imagesSize = mAdvertismentsArrayList.size();
                        Log.d(TAG, "mAdvertismentsArrayList size = " + mAdvertismentsArrayList.size());
                        mAdapter = new ViewPagerAdapter(ActivityNewOrder.this, mAdvertismentsArrayList);
                        intro_images.setAdapter(mAdapter);
                        intro_images.setCurrentItem(0);
                        intro_images.setOnPageChangeListener(ActivityNewOrder.this);
                        setUiPageViewController();
                    } else {

                    }
                }

                @Override
                public void onFailure(Call<AllAdvertisment> call, Throwable t) {

                }
            });
            return null;
        }
    }

    public void animateMarker(final Marker marker, final LatLng toPosition,
                              final boolean hideMarker) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = googleMap.getProjection();
        Point startPoint = proj.toScreenLocation(marker.getPosition());
        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 500;

        marker.setPosition(toPosition);

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                marker.setPosition(toPosition);

          /*      long elapsed = SystemClock.uptimeMillis () - start;
                float t = interpolator.getInterpolation ((float) elapsed
                        / duration);
                double lng = t * toPosition.longitude + (1 - t)
                        * startLatLng.longitude;
                double lat = t * toPosition.latitude + (1 - t)
                        * startLatLng.latitude;
                marker.setPosition (new LatLng (lat, lng));

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed (this, 16);
                } else {
                    if (hideMarker) {
                        marker.setVisible (false);
                    } else {
                        marker.setVisible (true);
                    }
                }*/
            }
        });
    }

    private void setUiPageViewController() {

        dotsCount = mAdapter.getCount();
        dots = new ImageView[dotsCount];

        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.nonselecteditem_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            params.setMargins(4, 0, 4, 0);

            pager_indicator.addView(dots[i], params);
        }

        if (dots.length > 0) {
            dots[0].setImageDrawable(getResources().getDrawable(R.drawable.selecteditem_dot));
            timer = new Timer(); // At this line a new Thread will be created
            timer.scheduleAtFixedRate(new RemindTask(), 0, 2000);
        }
    }

    @Override
    public void onPause() {

      /*  if (mMapView != null) {
            mMapView.onPause ();

        }
        if (d_map_view != null) {
            d_map_view.onPause ();
        }*/
        super.onPause();

    }

    @Override
    public void onStop() {
  /*      if (mMapView != null) {
            mMapView.onStop ();

        }
        if (d_map_view != null) {
            d_map_view.onStop ();
        }*/

        super.onStop();

    }

    @Override
    public void onDestroy() {
    /*    if (mMapView != null) {
            mMapView.onDestroy ();

        }
        if (d_map_view != null) {
            d_map_view.onDestroy ();
        }
*/

        super.onDestroy();


    }

    @Override
    public void onLowMemory() {
  /*      if (mMapView != null) {
            mMapView.onLowMemory ();

        }
        if (d_map_view != null) {
            d_map_view.onLowMemory ();
        }*/

        super.onLowMemory();

    }

    String picturePath;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            // When an Image is picked
            if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK
                    && null != data) {
                Log.d(TAG, "onActivityResult: " + data);
                // Get the Image from data
                ;
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                if (data.getData() != null) {
                    container_add_video.setVisibility(View.GONE);
                    container_add_images.setVisibility(View.VISIBLE);

                    Uri mImageUri = data.getData();

                    // Get the cursor
                    Cursor cursor = getContentResolver().query(mImageUri,
                            filePathColumn, null, null, null);
                    // Move to first row
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    picturePath = cursor.getString(columnIndex);

                    cursor.close();
                    if (picturePath != null && !picturePath.isEmpty()) {
                        IamgeModle iamgeModle = new IamgeModle(ActivityNewOrder.this);
                        iamgeModle.setName(picturePath);
                        try {
                            iamgeModle.setBitmap(getThumbnail(mImageUri));
                            iamgeModle.setPath(picturePath);

                        } catch (IOException e) {

                            Log.d(TAG, "onActivityResult: " + e.getMessage());
                        }
                        imagePaths.add(iamgeModle);

                        imageAdapter.notifyDataSetChanged();
                    }

                } else {
                    if (data.getClipData() != null) {
                        container_add_video.setVisibility(View.GONE);
                        container_add_images.setVisibility(View.VISIBLE);

                        ClipData mClipData = data.getClipData();
                        Log.d(TAG, "onActivityResult: " + mClipData);
                        for (int i = 0; i < mClipData.getItemCount(); i++) {

                            ClipData.Item item = mClipData.getItemAt(i);
                            Uri uri = item.getUri();
                            // Get the cursor
                            Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
                            // Move to first row
                            cursor.moveToFirst();

                            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                            picturePath = cursor.getString(columnIndex);

                            cursor.close();

                            if (picturePath != null && !picturePath.isEmpty()) {

                                IamgeModle iamgeModle = new IamgeModle(ActivityNewOrder.this);
                                iamgeModle.setName(picturePath);
                                try {
                                    iamgeModle.setBitmap(getThumbnail(uri));
                                    iamgeModle.setPath(picturePath);

                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                imagePaths.add(iamgeModle);

                            }
                            Log.v("LOG_TAG", "Selected Images" + mClipData.getItemCount());
                        }

                        imageAdapter.notifyDataSetChanged();
                    }
                }
            } else if (requestCode == RESULT_LOAD_Vedio && resultCode == RESULT_OK
                    && null != data) {


                if (data.getData() != null) {
                    videoPath = data.getData();
                    File mFile = new File(getRealPathFromURIPath(videoPath, ActivityNewOrder.this));
                    if (mFile.length() < 4194304) {
                        container_add_video.setVisibility(View.VISIBLE);
                        container_add_images.setVisibility(View.GONE);
                        videoView.setVideoURI(data.getData());
                        videoView.requestFocus();
                    } else {
                        Toast.makeText(mContext, getString(R.string.exceeded_the_maximum_video_size), Toast.LENGTH_SHORT).show();
                    }

                }
            } else if (requestCode == PLACE_PICKER_REQUEST) {
                if (resultCode == RESULT_OK) {
                    Place place = PlacePicker.getPlace(data, this);
                    StringBuilder stBuilder = new StringBuilder();
                    String placename = String.format("%s", place.getName());
                    String latitude = String.valueOf(place.getLatLng().latitude);
                    String longitude = String.valueOf(place.getLatLng().longitude);
                    String address = String.format("%s", place.getAddress());
                    stBuilder.append("Name: ");
                    stBuilder.append(placename);
                    stBuilder.append("\n");
                    stBuilder.append("Latitude: ");
                    stBuilder.append(latitude);
                    stBuilder.append("\n");
                    stBuilder.append("Logitude: ");
                    stBuilder.append(longitude);
                    stBuilder.append("\n");
                    stBuilder.append("Address: ");
                    stBuilder.append(address);
                    Log.d(TAG, stBuilder.toString());
                    mLocationTextJfButton.setText(address);
                    tempLng = new LatLng(Double.valueOf(latitude), Double.valueOf(longitude));
                    googleMap.clear();
                    mk = googleMap.addMarker(new MarkerOptions()
                            .position(tempLng)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_placeholder_point)));
                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(tempLng));
                    googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
                }
            } else if (requestCode == REQUEST_CODE_PICKER && resultCode == RESULT_OK && data != null) {
                ArrayList<Image> images = (ArrayList<Image>) ImagePicker.getImages(data);
                picturePath = images.get(0).getPath();
                if (picturePath != null && !picturePath.isEmpty()) {
                    File mFile = new File(picturePath);
                    if (mFile.length() < 2097152) {
                        container_add_video.setVisibility(View.GONE);
                        container_add_images.setVisibility(View.VISIBLE);
                        picturePath = images.get(0).getPath();
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inJustDecodeBounds = true;
                        BitmapFactory.decodeFile(picturePath, options);

                        options.inJustDecodeBounds = false;
                        options.inDither = false;
                        options.inPurgeable = true;
                        options.inInputShareable = true;
                        options.inPreferQualityOverSpeed = true;

                        Bitmap bitmap = BitmapFactory.decodeFile(picturePath, options);

                        IamgeModle iamgeModle = new IamgeModle(ActivityNewOrder.this);
                        iamgeModle.setName(picturePath);
                        iamgeModle.setBitmap(bitmap);
                        iamgeModle.setPath(picturePath);

                        imagePaths.add(iamgeModle);
                        imageAdapter.notifyDataSetChanged();
                    } else {
                        Toast.makeText(mContext, getString(R.string.exceeded_the_maximum_image_size), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                    .show();
        }


    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    void getOrderCatData() {
   /*     runOnUiThread (new Runnable () {
            @Override
            public void run() {


            }
        });*/
        //customAdapter.add(getString(R.string.select_cat));


        mAPIService = ApiUtils.getAPIService(ActivityNewOrder.this);
        mAPIService.getOrderCat(lang, user.getApiToken(), serviceId).enqueue(new Callback<ArrayList<OrderCat>>() {
            @Override
            public void onResponse(Call<ArrayList<OrderCat>> call, Response<ArrayList<OrderCat>> response) {
                Log.d(TAG, "onResponse: size " + response.body().size());

                if (response.isSuccessful()) {
                    orderCats = response.body();
                    mServiceSubCategories = new String[orderCats.size()];
                    for (int i = 0; i < orderCats.size(); i++) {
                        //customAdapter.add(orderCats.get(i).getName());
                        mServiceSubCategories[i] = orderCats.get(i).getName();
                    }
                    //customAdapter.notifyDataSetChanged();
                }
                mProgressBar.setVisibility(View.INVISIBLE);
                mNewOrderScrollView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Call<ArrayList<OrderCat>> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage().toString());
                mProgressBar.setVisibility(View.INVISIBLE);

            }
        });

    }

    void getWorkingHour() {
        appinmentAdapter.add(getString(R.string.select_appinment));

        mAPIService = ApiUtils.getAPIService(ActivityNewOrder.this);
        mAPIService.getWorkingHour(lang, user.getApiToken(), serviceId,"").enqueue(new Callback<ArrayList<WorkingHour>>() {
            @Override
            public void onResponse(Call<ArrayList<WorkingHour>> call, Response<ArrayList<WorkingHour>> response) {

                if (response.isSuccessful()) {
                    workingHours = response.body();
                    workingHoursAvailable = new ArrayList<WorkingHour>();
                    Log.d(TAG, "onResponse:  workingHours.size ()" + workingHours.size());

                    for (int i = 0; i < workingHours.size(); i++) {
                        if (workingHours.get(i).getStatus() == 5) {
                            workingHoursAvailable.add(workingHours.get(i));
                        }
                    }
                    mWorkingHoursStrings = new String[workingHoursAvailable.size()];
                    for (int i = 0; i < workingHoursAvailable.size(); i++) {
                        appinmentAdapter.add(workingHoursAvailable.get(i).getDay() + "  " + workingHoursAvailable.get(i).getFrom() + " - " + workingHoursAvailable.get(i).getTo());
                        mWorkingHoursStrings[i] = workingHoursAvailable.get(i).getDay() + "  " + getFormatedDateTime(String.valueOf(workingHoursAvailable.get(i).getFrom()), "HH:mm:ss", "hh:mm a") + " - " + getFormatedDateTime(String.valueOf(workingHoursAvailable.get(i).getTo()), "HH:mm:ss", "hh:mm a");

                        Log.d(TAG, "onResponse:  workingHours.size ()" + workingHours.size());

                    }

                    appinmentAdapter.notifyDataSetChanged();
                    /*mWorkingHoursAdapter = new WorkingHoursAdapter(workingHours, R.layout.time_custom_layout);
                    mWorkingHoursRecyclerView.setAdapter(mWorkingHoursAdapter);*/
                    if (mWorkingHourDialogProgress != null) {
                        mWorkingHourDialogProgress.setVisibility(View.INVISIBLE);
                    }
                    if (mProgressBar != null) {
                        mProgressBar.setVisibility(View.INVISIBLE);
                        mNewOrderScrollView.setVisibility(View.VISIBLE);
                    }
                    if (showProgress != null) {
                        showProgress.hide();
                    }
                    mProgressBar.setVisibility(View.INVISIBLE);
                    mNewOrderScrollView.setVisibility(View.VISIBLE);


                } else {
                    helperRes.getErrorMessage(response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<WorkingHour>> call, Throwable t) {
                call.cancel();
            }
        });
    }

    private void showWorkingHoursSingleListChoiceDialog() {
        mWorkingHoursSingleChoiceDialog = new MaterialDialog.Builder(ActivityNewOrder.this)
                .title(R.string.select_appinment)
                .items(mWorkingHoursStrings)
                .widgetColorRes(R.color.colorPrimaryDark)
                .titleColorRes(R.color.colorPrimaryDark)
                .positiveColorRes(R.color.colorPrimaryDark)
                .titleGravity(GravityEnum.CENTER)
                .typeface("jf_flat_regular.ttf", "jf_flat_regular.ttf")
                .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        if (which >= 0) {
                            mSelectedDayId = workingHoursAvailable.get(which).getId();
                        }
                        return true;
                    }
                })
                .positiveText(R.string.ok)
                .show();
    }

    void uploadImage(final int num) {


        if (num == -1) {
            sendCompleteOrder();

            return;
        }

        File imageFile = imagePaths.get(num).getFile();
        if (imageFile == null) {
            Log.d(TAG, "uploadImage: " + imageFile);
            return;
        }
        showProgress.setTitleText(getString(R.string.image_upload, "" + (imagePaths.size() - num), "" + imagePaths.size()));
        /// showProgress.setTitleText ("upload image " +  + " of " + );


       /* Bitmap thumbnail = getThumbnail (Uri.parse (imagePath));
        persistImage (thumbnail, imagePath.split ("/")[])

        File file = new File (imagePath);*/


            /*Bitmap resized = Bitmap.createScaledBitmap (bitmap, (int) (bitmap.getWidth () * 0.8), (int) (bitmap.getHeight () * 0.8), true);*/

        //  if (file != null) {
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), imageFile);
        requestImage = MultipartBody.Part.createFormData("image", imageFile.getName(), requestFile);
        //  }


        ApiInterface up = ApiUtils.getLongTimeAPIService(ActivityNewOrder.this);

        up
                .uploadImage(user.getApiToken(), requestImage).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {


                if (response.isSuccessful()) {
                    urlImages.add(response.body().get("url").getAsString());
                    uploadImage(num - 1);


                } else {

                    L.d(TAG, "Image errory " + response.errorBody());

                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                L.d(TAG, "call image isExecuted" + call.isExecuted());
                L.d(TAG, "call image isCanceled" + call.isCanceled());
                showProgress.hide();


            }


        });

    }

    private String getRealPathFromURIPath(Uri contentURI, Activity activity) {
        Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            return contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    void uploadVideo(Uri videoPath) {

        dialog.ShowProgress("");
        if (videoPath == null) {
            return;
        }

        File file = new File(getRealPathFromURIPath(videoPath, ActivityNewOrder.this));

        if (file != null) {


            RequestBody videoBody = RequestBody.create(MediaType.parse("video/*"), file);
            requestImage = MultipartBody.Part.createFormData("video", file.getName(), videoBody);


        } else {
            return;
        }
        ApiInterface up = ApiUtils.getLongTimeAPIService(ActivityNewOrder.this);

        up
                .uploadVideo(user.getApiToken(), requestImage).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                if (response.isSuccessful()) {

                    urlVideo = response.body().get("url").getAsString();

                    sendCompleteOrder();

                } else {

                    L.d(TAG, "Image errory " + response.errorBody());

                }

                dialog.hide();

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                L.d(TAG, "call video isExecuted" + call.isExecuted());
                L.d(TAG, "call video isCanceled" + call.isCanceled());
                dialog.hide();
                L.d(TAG, "call video getMessage" + t.getMessage());
                L.d(TAG, "call video getCause" + t.getCause());


            }
        });


    }

    private void sendOrder() {

        showProgress.setTitleText(getString(R.string.sending_order));
        showProgress.show();

        if (imagePaths.size() > 0) {
            uploadImage(imagePaths.size() - 1);
        } else if (videoPath != null) {
            uploadVideo(videoPath);
        } else {
            sendCompleteOrder();
        }


    }

    private void sendCompleteOrder() {


        L.d(TAG, "send order");
        Log.d(TAG, "sendCompleteOrder: " + urlImages.toString());
        showProgress.setTitleText(getString(R.string.sending_order));

        Log.d(TAG, "onResponse: size : " + urlImages.size());

        SendOrder sendOrder = new SendOrder();
        sendOrder.setRequestType("" + orderCats.get(spinner_type_of_order_position).getId());
        sendOrder.setDescription(order_details.getText().toString());
        sendOrder.setLatitude("" + Double.parseDouble(SharedPrefs.getString(ActivityNewOrder.this, SharedPrefs.LATITUDE)));
        sendOrder.setLongitude("" + Double.parseDouble(SharedPrefs.getString(ActivityNewOrder.this, SharedPrefs.LONGITUDE)));
        sendOrder.setStatusType("" + selectedCase);
        if (!urlImages.isEmpty()) {
            sendOrder.setImageUrl(urlImages);
        } else if (urlVideo != null) {
            sendOrder.setVideoUrl(urlVideo);
        }

        if (case_switch.isChecked()) {
            sendOrder.setWorkingId("" + mSelectedDayId);

        }

        mAPIService.sendOrder("application/json", lang, user.getApiToken(), sendOrder).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                showProgress.hide();
                if (response.isSuccessful()) {
                    L.d(TAG, "send body" + response.body().toString());

                    mMapView.onDestroy();
                    //      d_map_view.onDestroy ();
                    mMapView.invalidate();
                    //  d_map_view.invalidate ();


                    Intent intent = new Intent(getBaseContext(), SendOrderDone.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    overridePendingTransition(R.anim.fadein, R.anim.fadeout);
                    startActivity(intent);
                    finish();


                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                showProgress.hide();
            }
        });


    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();


        mGoogleApiClient.connect();

    /*    mk = googleMap.addMarker (new MarkerOptions ()
                .position (tempLng)
                .title ("Selected Postion")
                .icon (BitmapDescriptorFactory.fromBitmap (UitilsFun.getBitmap (this, R.drawable.map_marker))));

        googleMap.moveCamera (CameraUpdateFactory.newCameraPosition (new CameraPosition.Builder ()
                .target (tempLng)
                .zoom (15.6F)
                .build ()));*/
    }

    @Override
    public void onConnected(Bundle bundle) {
        // mLocationRequest = new LocationRequest ();
        // mLocationRequest.setInterval (1000);
        // mLocationRequest.setFastestInterval (1000);
        // mLocationRequest.setPriority (LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        //FusedLocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest,getActivity ());


    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    private void checkMediaPermission() {

        if (ContextCompat.checkSelfPermission(getBaseContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(ActivityNewOrder.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(ActivityNewOrder.this)
                        .setTitle("Read Storage Permission Needed")
                        .setMessage("This app needs the Read Storage, please accept to use  Storage functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(ActivityNewOrder.this,
                                        permissions_media,
                                        REQUEST_ID_MULTIPLE_PERMISSIONS_Media);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(ActivityNewOrder.this,
                        permissions_media,
                        REQUEST_ID_MULTIPLE_PERMISSIONS_Media);
            }
        }
    }

    /*

    && ContextCompat.checkSelfPermission (getBaseContext (),
                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
     */
    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getBaseContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(ActivityNewOrder.this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(ActivityNewOrder.this,
                    Manifest.permission.ACCESS_FINE_LOCATION) && ActivityCompat.shouldShowRequestPermissionRationale(ActivityNewOrder.this,
                    Manifest.permission.ACCESS_COARSE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(ActivityNewOrder.this)
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(ActivityNewOrder.this,
                                        permissions,
                                        REQUEST_ID_MULTIPLE_PERMISSIONS);
                            }
                        })
                        .create()
                        .show();
                myLocation.setVisibility(View.VISIBLE);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(ActivityNewOrder.this,
                        permissions,
                        REQUEST_ID_MULTIPLE_PERMISSIONS);

                myLocation.setVisibility(View.VISIBLE);

            }
        } else {
            myLocation.setVisibility(View.GONE);

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//
                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(ActivityNewOrder.this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }


                        // googleMap.setMyLocationEnabled (true);
                        mk = googleMap.addMarker(new MarkerOptions()
                                .position(tempLng)
                                .title("Selected Postion")
                                .draggable(true)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_placeholder_point)));

                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(ActivityNewOrder.this, "permission denied", Toast.LENGTH_LONG).show();
                }

            }
            break;

            case REQUEST_ID_MULTIPLE_PERMISSIONS_Media: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//
                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(ActivityNewOrder.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(ActivityNewOrder.this, "PERMISSION_GRANTED ", Toast.LENGTH_LONG).show();


                    } else {

                        // permission denied, boo! Disable the
                        // functionality that depends on this permission.
                        Toast.makeText(ActivityNewOrder.this, "permission denied", Toast.LENGTH_LONG).show();
                    }

                }


                break;


                // other 'case' lines to check for other
                // permissions this app might request
            }

        }
    }

    @Override
    public void onLocationChanged(Location location) {
        //Place current location marker

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public void getLastSelectedLocation() {
        if (!SharedPrefs.getString(ActivityNewOrder.this, SharedPrefs.LATITUDE, "").isEmpty()) {

            double dragLat = Double.parseDouble(SharedPrefs.getString(ActivityNewOrder.this, SharedPrefs.LATITUDE));
            double dragLong = Double.parseDouble(SharedPrefs.getString(ActivityNewOrder.this, SharedPrefs.LONGITUDE));
            tempLng = new LatLng(dragLat, dragLong);
        } else {

            tempLng = new LatLng(31.5017672, 34.4665412);
        }
    }

    private void getNotificationNumber() {

        ApiInterface apiInterface = ApiUtils.getAPIService(this);
        apiInterface.getNotifications(user.getApiToken()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                if (response.isSuccessful()) {
                    String number;
                    if (response.body().get("number") == null) {
                        number = "0";
                    } else {

                        number = response.body().get("number").getAsString();

                    }

                    number_of_notifications.setText(number);


                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });


    }


    public Bitmap getThumbnail(Uri uri) throws FileNotFoundException, IOException {
        InputStream input = this.getContentResolver().openInputStream(uri);

        BitmapFactory.Options onlyBoundsOptions = new BitmapFactory.Options();
        onlyBoundsOptions.inJustDecodeBounds = true;
        onlyBoundsOptions.inDither = true;//optional
        onlyBoundsOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//optional
        BitmapFactory.decodeStream(input, null, onlyBoundsOptions);
        input.close();

        if ((onlyBoundsOptions.outWidth == -1) || (onlyBoundsOptions.outHeight == -1)) {
            return null;
        }

        int originalSize = (onlyBoundsOptions.outHeight > onlyBoundsOptions.outWidth) ? onlyBoundsOptions.outHeight : onlyBoundsOptions.outWidth;

        double ratio = (originalSize > 450) ? (originalSize / 450) : 1.0;

        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        bitmapOptions.inSampleSize = (int) ratio;
        bitmapOptions.inDither = true; //optional
        bitmapOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;//
        input = this.getContentResolver().openInputStream(uri);
        Bitmap bitmap = BitmapFactory.decodeStream(input, null, bitmapOptions);
        input.close();
        return bitmap;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        for (int i = 0; i < dotsCount; i++) {
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.nonselecteditem_dot));
        }

        dots[position].setImageDrawable(getResources().getDrawable(R.drawable.selecteditem_dot));

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public class FillAllAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            getOrderCatData();


            //getWorkingHour();

            getNotificationNumber();
            return null;
        }
    }

    private class WorkingHoursAdapter extends RecyclerView.Adapter<WorkingHoursViewHolder> {
        private int resource; // the layout we will fill it in recycler (list_item_row)
        private List<WorkingHour> mItemsList; // this is the copy from MainList

        public WorkingHoursAdapter(List<WorkingHour> itemList, int res) {
            mItemsList = itemList;
            this.resource = res;
        }

        @Override
        public WorkingHoursViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(mContext); // move the layout controller from this activity layout (mContext)
            View view = layoutInflater.inflate(this.resource, parent, false); // move int resource (list_item_row)
            return new WorkingHoursViewHolder(view); // take parameter of the view (layout) we will fill in it in the recycler
        }

        @Override
        public void onBindViewHolder(WorkingHoursViewHolder holder, int position) {
            WorkingHour mWorkingHour = mItemsList.get(position);
            holder.bindItem(mWorkingHour); // this is the item of (kids) we will fill it in the view on the recycler
        }

        @Override
        public int getItemCount() {
            return mItemsList.size();
        }
    }


    private class WorkingHoursViewHolder extends RecyclerView.ViewHolder {
        private JFTextView mWorkingHourDay, mWorkingHourFrom, mWorkingHourTo, mWorkingHourStatus;

        public WorkingHoursViewHolder(View itemView) {
            super(itemView);
            mWorkingHourDay = (JFTextView) itemView.findViewById(R.id.working_hour_day);
            mWorkingHourFrom = (JFTextView) itemView.findViewById(R.id.working_hour_time_from);
            mWorkingHourTo = (JFTextView) itemView.findViewById(R.id.working_hour_time_to);
            mWorkingHourStatus = (JFTextView) itemView.findViewById(R.id.working_hour_item_status);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (workingHours.get(getAdapterPosition()).getStatus() == 5) {
                        mSelectedDayId = workingHours.get(getAdapterPosition()).getId();
                        mWorkingHourDialog.dismiss();
                    }
                }
            });

        }

        public void bindItem(final WorkingHour mWorkingHour) {
            mWorkingHourDay.setText(String.valueOf(mWorkingHour.getDay()));
            mWorkingHourFrom.setText(getFormatedDateTime(String.valueOf(mWorkingHour.getFrom()), "HH:mm:ss", "hh:mm a"));
            mWorkingHourTo.setText(getFormatedDateTime(String.valueOf(mWorkingHour.getTo()), "HH:mm:ss", "hh:mm a"));

            if (mWorkingHour.getStatus() == 5) {
                mWorkingHourStatus.setBackground(mContext.getResources().getDrawable(R.drawable.background_ended));
                mWorkingHourStatus.setText(getString(R.string.available));
            } else {
                mWorkingHourStatus.setBackground(mContext.getResources().getDrawable(R.drawable.background_under_way));
                mWorkingHourStatus.setText(getString(R.string.busy));
            }

        }

    }

    public static String getFormatedDateTime(String dateStr, String strReadFormat, String strWriteFormat) {

        String formattedDate = dateStr;

        DateFormat readFormat = new SimpleDateFormat(strReadFormat, Locale.getDefault());
        DateFormat writeFormat = new SimpleDateFormat(strWriteFormat, Locale.getDefault());

        Date date = null;

        try {
            date = readFormat.parse(dateStr);
        } catch (ParseException e) {
        }

        if (date != null) {
            formattedDate = writeFormat.format(date);
        }

        return formattedDate;
    }


    // this is an inner class...
    class RemindTask extends TimerTask {

        @Override
        public void run() {

            // As the TimerTask run on a seprate thread from UI thread we have
            // to call runOnUiThread to do work on UI thread.
            runOnUiThread(new Runnable() {
                public void run() {

                    if (page > imagesSize) { // In my case the number of pages are 5
                        page = 0;
                        intro_images.setCurrentItem(page);
                    } else {
                        intro_images.setCurrentItem(page++);
                    }
                }
            });

        }
    }
}
