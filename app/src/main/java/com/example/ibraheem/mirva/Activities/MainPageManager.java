package com.example.ibraheem.mirva.Activities;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.ibraheem.mirva.Const.FragConst;
import com.example.ibraheem.mirva.CustomViews.Drawer.DataModel;
import com.example.ibraheem.mirva.CustomViews.Drawer.DrawerItemCustomAdapter;
import com.example.ibraheem.mirva.CustomViews.FWTextView;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.CustomViews.ResideMenu.ResideMenu;
import com.example.ibraheem.mirva.CustomViews.ResideMenu.ResideMenuItem;
import com.example.ibraheem.mirva.Fragments.common.Frag_Chat;
import com.example.ibraheem.mirva.Fragments.common.Frag_Tickets;
import com.example.ibraheem.mirva.Fragments.manager.Manager_Frag_EndedOrders;
import com.example.ibraheem.mirva.Fragments.manager.Manager_Frag_MyOrders;
import com.example.ibraheem.mirva.Fragments.manager.Manager_Frag_NewOrders;
import com.example.ibraheem.mirva.Fragments.manager.Manager_Frag_Profile;
import com.example.ibraheem.mirva.Fragments.manager.Manager_Frag_Store;
import com.example.ibraheem.mirva.Fragments.manager.Manager_Frag_ViewOrderDetail;
import com.example.ibraheem.mirva.Interfaces.Communicator;
import com.example.ibraheem.mirva.Interfaces.Actions;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.Model.Response.helperRes;
import com.example.ibraheem.mirva.NetworkChangeReceiver;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.UitilsFun;
import com.example.ibraheem.mirva.UpdateLocationAsyncTask;
import com.example.ibraheem.mirva.util.D;
import com.example.ibraheem.mirva.util.SharedPrefs;
import com.example.ibraheem.mirva.util.helperFun;
import com.google.gson.JsonObject;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainPageManager extends AppCompatActivity
        implements View.OnClickListener, Communicator, Actions {
    public static int mSelectedItem;
    public String TAG = "MainPageManager";
    Toolbar toolbar, toolbar_clints, toolbar_profile;
    android.support.v7.app.ActionBarDrawerToggle mDrawerToggle;
    FWTextView control_navigation_drawer, c_control_navigation_drawer, p_control_navigation_drawer;
    JFTextView user_profile, m_title_toolbar, p_tv_title, user_name;
    FragmentManager fragmentManager;
    FWTextView tv_back;
    Handler mHandler;
    de.hdodenhof.circleimageview.CircleImageView user_image;

    View show_notification;
    InputMethodManager inputMethodManager;
    boolean locked = false;
    NetworkChangeReceiver mNetworkChangeReceiver;
    Runnable mPendingRunnable;
    D d;
    User user;
    JFTextView pro_number_of_notifications, client_number_of_notifications, main_number_of_notifications;
    public ResideMenu resideMenu;
    String[] titles;
    int Direction = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.m_activity_main_page);
        Direction = getResources().getBoolean(R.bool.is_right_to_left) ? ResideMenu.DIRECTION_RIGHT : ResideMenu.DIRECTION_LEFT;

        initView();

        d = new D(this);
        mHandler = new Handler();
        getUserData();
    }

    @Override
    public void TopMessage(Fragment fragment) {
        fragmentManager.beginTransaction()
                .replace(R.id.container, fragment)
                .setCustomAnimations(R.anim.enter, R.anim.exit)
                .commit();
    }

    private void getUserData() {
        user = helperFun.getUser(this);


        Picasso.with(this)
                .load(user.getImage())
                .placeholder(R.drawable.avatar)
                .error(R.drawable.avatar)
                .into(user_image);
        user_name.setText(user.getName());
    }

    @Override
    public void updateUserData() {
        getUserData();
    }


    void initToolbar() {

        toolbar = (Toolbar) findViewById(R.id.manager_toolbar_main);
        toolbar_clints = (Toolbar) findViewById(R.id.manager_toolbar_fragments);
        toolbar_profile = (Toolbar) findViewById(R.id.toolbar_profile);
        toolbar_clints.setVisibility(View.GONE);
        toolbar_profile.setVisibility(View.GONE);

        resideMenu = new ResideMenu(this);
        resideMenu.setUse3D(true);
        resideMenu.setScaleValue(0.9f);
        resideMenu.setBackground(R.drawable.menu_background);
        resideMenu.attachToActivity(this);
        resideMenu.setMenuListener(menuListener);
        // create menu items;
        titles = getResources().getStringArray(R.array.m_navigation_drawer_items_array);

        for (int i = 0; i < titles.length; i++){
            ResideMenuItem item = new ResideMenuItem(this, titles[i]);
            item.setOnClickListener(this);
            resideMenu.addMenuItem(item,  Direction); // or  ResideMenu.DIRECTION_RIGHT
        }

        toolbar.findViewById(R.id.main_nav).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                if (resideMenu.isOpened())
                {
                    resideMenu.closeMenu();
                }else
                {
                    resideMenu.openMenu(Direction);
                }
            }
        });

        toolbar.findViewById(R.id.main_notification).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), MyNotificationActivity.class));
            }
        });

        toolbar.findViewById(R.id.main_notification_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), MyNotificationActivity.class));

            }
        });
        toolbar_clints.findViewById(R.id.frag_nav).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                resideMenu.openMenu(Direction);
            }
        });


        toolbar_clints.findViewById(R.id.frag_notification).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), MyNotificationActivity.class));
            }
        });

        toolbar_clints.findViewById(R.id.manager_notification_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), MyNotificationActivity.class));
            }
        });
        toolbar_clints.findViewById(R.id.frag_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (resideMenu.isOpened()) {
                    resideMenu.closeMenu();
                }

                if (fragmentManager.getBackStackEntryCount() == 1) {
                    //  addFragment (new Manager_Frag_MyOrders ());
                    finish();

                } else {
                    fragmentManager.popBackStackImmediate();


                }

            }
        });


        toolbar_profile.findViewById(R.id.profile_nav).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                resideMenu.openMenu(Direction);
            }
        });


        toolbar_profile.findViewById(R.id.profile_notification).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), MyNotificationActivity.class));

            }
        });
        toolbar_profile.findViewById(R.id.profile_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (resideMenu.isOpened()) {
                    resideMenu.closeMenu();
                }

                addFragment(new Manager_Frag_MyOrders());


          /*   if (fragmentManager.getBackStackEntryCount () == 1) {
                  //  addFragment (new Manager_Frag_MyOrders ());
                  finish ();
                } else {
                    fragmentManager.popBackStackImmediate ();


                }*/

            }
        });
        toolbar_profile.findViewById(R.id.edit_profile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), EditProfileActivity.class));

            }
        });


        pro_number_of_notifications = (JFTextView) toolbar_profile.findViewById(R.id.number_of_notifications);
        client_number_of_notifications = (JFTextView) toolbar_clints.findViewById(R.id.number_of_notifications);
        main_number_of_notifications = (JFTextView) toolbar.findViewById(R.id.number_of_notifications);
    }

    private ResideMenu.OnMenuListener menuListener = new ResideMenu.OnMenuListener() {
        @Override
        public void openMenu() {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }

        @Override
        public void closeMenu() {
            if (mPendingRunnable != null) {
                mHandler.post(mPendingRunnable);
                mPendingRunnable = null;
            }
        }
    };

    private void initView() {
        mHandler = new Handler();
        initToolbar();


        fragmentManager = getSupportFragmentManager();
        user_profile = (JFTextView) findViewById(R.id.user_profile);
        user_profile.setOnClickListener(this);

        m_title_toolbar = (JFTextView) findViewById(R.id.m_title_toolbar);

        user_image = (de.hdodenhof.circleimageview.CircleImageView) findViewById(R.id.user_image);
        user_name = (JFTextView) findViewById(R.id.user_name);
        inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);

        Intent intent = getIntent();
        if (intent.getIntExtra(FragConst.REQUEST_ID, 0) == 0 && intent.getIntExtra(FragConst.TICKET_ID, 0) == 0) {

            addFragment(new Manager_Frag_MyOrders());

        } else if (intent.hasExtra(FragConst.REQUEST_ID)) {
            Bundle bundle = new Bundle();
            bundle.putInt(FragConst.REQUEST_ID, intent.getIntExtra(FragConst.REQUEST_ID, 0));

            Fragment fragment = new Manager_Frag_ViewOrderDetail();
            fragment.setArguments(bundle);

            addFragment(fragment);

        } else if (intent.hasExtra(FragConst.TICKET_ID)) {
            Bundle bundle = new Bundle();
            bundle.putInt(FragConst.TICKET_ID, intent.getIntExtra(FragConst.TICKET_ID, 0));
            Fragment fragment = new Frag_Chat();
            fragment.setArguments(bundle);

            addFragment(fragment);
        }
    }

    @Override
    public void setTitleToolbar(int titleToolbar) {
        m_title_toolbar.setText(titles[titleToolbar]);
    }

    @Override
    public void setTitleToolbar(String title) {
        m_title_toolbar.setText(title);

    }

    @Override
    public void addFragment(Fragment fragment) {
        ReplaceFragement(fragment);

        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
            mPendingRunnable = null;
        }
    }

    @Override
    public void actionToselectedPostion(int postion) {

    }

    @Override
    public void setToolbatLock(boolean locke) {
        this.locked = locke;
    }

    private void selectItem(int position) {

        Fragment fragment = null;

        switch (position) {
            case 0:
                fragment = new Manager_Frag_MyOrders();
                break;
            case 1:
                fragment = new Manager_Frag_NewOrders();

                break;
            case 2:
                fragment = new Manager_Frag_EndedOrders();
                break;
            case 3:
                fragment = new Manager_Frag_Store();

                break;
            case 4:
                fragment = new Frag_Tickets();

                break;

            case 5:
                startActivity(new Intent(getBaseContext(), About.class));
                resideMenu.closeMenu();

                break;

            case 6:
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("text/plain");
                i.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
                String shareText = getString(R.string.download_syanat) + " https://play.google.com/store/apps/details?id=" + getPackageName();
                i.putExtra(Intent.EXTRA_TEXT, shareText);
                startActivity(Intent.createChooser(i, getString(R.string.share_via)));
                resideMenu.closeMenu();
                break;

            case 7:
                UitilsFun.changeLanguage(this);
                resideMenu.closeMenu();

                break;
            case 8:
                // startActivity (new Intent (getBaseContext (), LoginActivity.class));
                sendLogout();

            default:
                break;
        }

        if (fragment != null) {
            ReplaceFragement(fragment);
            resideMenu.closeMenu();

        } else {
            Log.e("MainActivity", "Error in creating fragment");
        }
    }

    @Override
    public void selectVisibleToolbar(int t) {

        switch (t) {
            case R.id.manager_toolbar_main:
                if (!locked) {
                    toolbar.setVisibility(View.VISIBLE);
                    toolbar_clints.setVisibility(View.GONE);
                    toolbar_profile.setVisibility(View.GONE);

                }

                break;
            case R.id.manager_toolbar_fragments:
                if (!locked) {
                    toolbar.setVisibility(View.GONE);
                    toolbar_clints.setVisibility(View.VISIBLE);
                    toolbar_profile.setVisibility(View.GONE);

                }
                break;

            case R.id.toolbar_profile:
                toolbar_profile.setVisibility(View.VISIBLE);
                toolbar.setVisibility(View.GONE);
                toolbar_clints.setVisibility(View.GONE);
                locked = true;

                break;


        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.user_profile:
                ReplaceFragement(new Manager_Frag_Profile());
                resideMenu.closeMenu();
                break;
            case R.id.edit_profile:
                startActivity(new Intent(getBaseContext(), EditProfileActivity.class));
                break;
            default:
                selectItem(Arrays.asList(titles).indexOf(((ResideMenuItem)v).getTitle()));
                break;
        }
    }

    @Override
    public void onBackPressed() {
        ///  super.onBackPressed ();
        if (resideMenu.isOpened()) {
            resideMenu.closeMenu();
        }

        if (fragmentManager.getBackStackEntryCount() != 1) {
            fragmentManager.popBackStackImmediate();
        } else {

            finish();
        }
/*
        if (fragmentManager.getBackStackEntryCount () < 2|| fragmentManager.getFragments ().isEmpty ()
               ) {
            addFragment (new Manager_Frag_MyOrders ());

            Log.d (TAG, "onBackPressed: 0"+fragmentManager.getBackStackEntryAt (0));
        } else {

            Log.d (TAG, "onBackPressed:1 "+fragmentManager.getBackStackEntryAt (1));

            fragmentManager.popBackStackImmediate ();
        }*/
    }

    @Override
    protected void onResume() {
        super.onResume();


        mNetworkChangeReceiver = new NetworkChangeReceiver();
        mNetworkChangeReceiver.setNetworkLostCallback(new NetworkChangeReceiver.NetworkLostCallback() {
            @Override
            public void onNetworkLost() {

                UitilsFun.ShowAlertDialog(MainPageManager.this);

            }

            @Override
            public void onNetworkRegained() {

            }
        });

        IntentFilter networkFilter = new IntentFilter();
        networkFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);

        this.registerReceiver(mNetworkChangeReceiver, networkFilter);
        /////

        if (UitilsFun.isNetworkAvailable(MainPageManager.this)) {

            if (mPendingRunnable != null) {
                mHandler.post(mPendingRunnable);
                mPendingRunnable = null;
            }

        } else {
            UitilsFun.ShowAlertDialog(MainPageManager.this);
        }


    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mNetworkChangeReceiver != null) {
            unregisterReceiver(mNetworkChangeReceiver);
            mNetworkChangeReceiver = null;
        }
    }

    private void ReplaceFragement(final Fragment fragment) {
        if (resideMenu.isOpened()) {
            resideMenu.closeMenu();
        }

        mPendingRunnable = new Runnable() {
            @Override
            public void run() {

                fragmentManager.beginTransaction()
                        .replace(R.id.container, fragment)
                        .addToBackStack("more")
                        .setCustomAnimations(R.anim.enter, R.anim.exit)
                        .commit();

            }
        };


    }

    @Override
    public void beckFragment() {
        fragmentManager.popBackStack();
    }

    void sendLogout() {
        Intent intent = new Intent(getBaseContext(), LoginActivity.class);
        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(new Intent(getBaseContext(), LoginActivity.class));
        finish();

        ApiInterface apiInterface = ApiUtils.getAPIService(this);
        apiInterface.sendLogout(user.getApiToken()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    SharedPrefs.revokeShared(getBaseContext());


                } else {

                    // helperRes.showMessageError (getBaseContext (), response.errorBody ());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

                //  d.ShowErrorInternetConnection ("");
            }
        });


    }

    private void getNotificationNumber() {

        ApiInterface apiInterface = ApiUtils.getAPIService(this);
        apiInterface.getNotifications(user.getApiToken()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                if (response.isSuccessful()) {
                    String number;
                    if (response.body().get("number") == null) {
                        number = "0";
                    } else {

                        number = response.body().get("number").getAsString();

                    }

                    pro_number_of_notifications.setText(number);
                    client_number_of_notifications.setText(number);
                    main_number_of_notifications.setText(number);

                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();
        getNotificationNumber();
    }
}

