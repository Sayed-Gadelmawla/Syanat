package com.example.ibraheem.mirva.Fragments.common;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ibraheem.mirva.Adapter.MyEvalutionsAdapter;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.Model.Response.EevaluteOrderData.MyEveluation;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.util.helperFun;

import java.util.ArrayList;

/**
 * Created by ibraheem on 21/05/2017.
 */

public class Frag_Profile_MyData extends Fragment {

    public final String TAG = Frag_Profile_MyData.class.getSimpleName ();
    ArrayList<MyEveluation> myEveluations;
    MyEvalutionsAdapter myEvalutionsAdapter;
    RecyclerView recyclerView;
    JFTextView mydata_user_email, mydata_user_birthdate, mydata_user_mobile, mydata_user_gender;
    User user;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);


    }

    @Override
    public void onStart() {
        super.onStart ();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate (R.layout.fragment_my_data, container, false);

        mydata_user_birthdate = (JFTextView) view.findViewById (R.id.mydata_user_birthdate);
        mydata_user_email = (JFTextView) view.findViewById (R.id.mydata_user_email);
        mydata_user_mobile = (JFTextView) view.findViewById (R.id.mydata_user_mobile);
        mydata_user_gender = (JFTextView) view.findViewById (R.id.mydata_user_gender);

        getUserData ();

        return view;
    }

    private void getUserData() {
        user = helperFun.getUser (getContext ());
        mydata_user_birthdate.setText (user.getBirthDate ());
        mydata_user_email.setText (user.getEmail ());
        mydata_user_mobile.setText (user.getMobile ());
        mydata_user_gender.setText (user.getGender ()=="12"?getString (R.string.male):getString (R.string.femal));

    }

}
