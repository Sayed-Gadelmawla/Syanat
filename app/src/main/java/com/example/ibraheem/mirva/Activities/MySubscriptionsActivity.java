package com.example.ibraheem.mirva.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.example.ibraheem.mirva.Const.FragConst;
import com.example.ibraheem.mirva.CustomViews.FWTextView;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.Model.Subscription;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.util.helperFun;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MySubscriptionsActivity extends AppCompatActivity {

    private static final String TAG = "AllSubscriptionsActivity";
    private JFTextView c_title_toolbar;
    private Toolbar toolbar_view_order;
    private JFTextView number_of_notifications;
    private FWTextView frag_back;
    private int REQUEST_ID;
    private ProgressBar mProgressBar;
    private RecyclerView mSubscriptionsRecyclerView;
    private Context mContext;
    private LinearLayoutManager mLinearLayoutManager;
    private String lang;
    private String apiToken;
    private ArrayList<Subscription> subscriptions;
    private SubscriptionsAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_subscriptions);
        mContext = this;
        findViewById(R.id.frag_notification).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), MyNotificationActivity.class));

            }
        });

        findViewById(R.id.view_order_notification_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), MyNotificationActivity.class));

            }
        });

        toolbar_view_order = (Toolbar) findViewById(R.id.toolbar_view_order);
        c_title_toolbar = (JFTextView) toolbar_view_order.findViewById(R.id.c_title_toolbar);
        number_of_notifications = (JFTextView) toolbar_view_order.findViewById(R.id.number_of_notifications);

        c_title_toolbar.setText(getString(R.string.my_subscriptions));

        frag_back = (FWTextView) toolbar_view_order.findViewById(R.id.frag_back);
        frag_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
//        mSubscriptionsRecyclerView = (RecyclerView) findViewById(R.id.subscriptions_recycler);
//        mLinearLayoutManager = new LinearLayoutManager(mContext);
//        mSubscriptionsRecyclerView.setLayoutManager(mLinearLayoutManager);
        new FillSubscriptionsAsyncTask().execute();

    }

    private class FillSubscriptionsAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            lang = helperFun.getLanguage(mContext);
            apiToken = helperFun.getUser(mContext).getApiToken();
            ApiInterface mAPIService = ApiUtils.getAPIService(mContext);
            mAPIService.getUserSubscriptionData(lang, apiToken).enqueue(new Callback<ArrayList<Subscription>>() {
                @Override
                public void onResponse(Call<ArrayList<Subscription>> call, Response<ArrayList<Subscription>> response) {

                    if (response.isSuccessful()) {
                        subscriptions = response.body();
                        Log.d(TAG, "onResponse:  size" + subscriptions.size());
                        Log.d(TAG, "onResponse: " + response.body().toString());
                        Subscription bronz = subscriptions.get(0);
                        mAdapter = new SubscriptionsAdapter(subscriptions, R.layout.subscription_item_list_row);
                        mSubscriptionsRecyclerView.setAdapter(mAdapter);
                    }

                    mProgressBar.setVisibility(View.INVISIBLE);
                }


                @Override
                public void onFailure(Call<ArrayList<Subscription>> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.getMessage());
                    mProgressBar.setVisibility(View.INVISIBLE);

                }
            });
            return null;
        }
    }

    private class SubscriptionsAdapter extends RecyclerView.Adapter<SubscriptionsViewHolder> {
        private int resource; // the layout we will fill it in recycler (list_item_row)
        private List<Subscription> mItemsList; // this is the copy from MainList

        public SubscriptionsAdapter(List<Subscription> itemList, int res) {
            mItemsList = itemList;
            this.resource = res;
        }

        @Override
        public SubscriptionsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(mContext); // move the layout controller from this activity layout (mContext)
            View view = layoutInflater.inflate(this.resource, parent, false); // move int resource (list_item_row)
            return new SubscriptionsViewHolder(view); // take parameter of the view (layout) we will fill in it in the recycler
        }

        @Override
        public void onBindViewHolder(SubscriptionsViewHolder holder, int position) {
            Subscription mSubscription = mItemsList.get(position);
            holder.bindItem(mSubscription); // this is the item of (kids) we will fill it in the view on the recycler
        }

        @Override
        public int getItemCount() {
            return mItemsList.size();
        }
    }


    private class SubscriptionsViewHolder extends RecyclerView.ViewHolder {
        private JFTextView mNameJfTextView, mPriceJfTextView, mDurationJfTextView, mDescriptionJfTextView, mMaintenanceCountJfTextView, mMoreJfTextView;
        private ImageView mIconImageView;

        public SubscriptionsViewHolder(View itemView) {
            super(itemView);
//            mNameJfTextView = (JFTextView) itemView.findViewById(R.id.subscription_item_name);
//            mPriceJfTextView = (JFTextView) itemView.findViewById(R.id.subscription_item_price);
//            mDurationJfTextView = (JFTextView) itemView.findViewById(R.id.subscription_item_duration);
//            mDescriptionJfTextView = (JFTextView) itemView.findViewById(R.id.subscription_item_description);
//            mMaintenanceCountJfTextView = (JFTextView) itemView.findViewById(R.id.subscription_item_maintenance_count);
//            mIconImageView = (ImageView) itemView.findViewById(R.id.subscription_item_image_view);
        }

        public void bindItem(final Subscription mSubscription) {
            mNameJfTextView.setText(mSubscription.getName());
            mPriceJfTextView.setText(String.valueOf(mSubscription.getPrice()));
            mDurationJfTextView.setText(mSubscription.getDuration());

        }

    }
}
