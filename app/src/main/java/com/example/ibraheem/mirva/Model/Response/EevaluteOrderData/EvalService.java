package com.example.ibraheem.mirva.Model.Response.EevaluteOrderData;

import com.example.ibraheem.mirva.Model.Response.evaluationData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by ibraheem on 05/07/2017.
 */

public class EvalService {

    @SerializedName("evaluation")
    @Expose
    private ArrayList<EvalType> evaluation = null;
    @SerializedName("service")
    @Expose
    private Service service;
    @SerializedName("review")
    @Expose
    private String review;
    @SerializedName("evaluation_data")
    @Expose
    private ArrayList<evaluationData> evaluationData = null;

    public ArrayList<EvalType> getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(ArrayList<EvalType> evaluation) {
        this.evaluation = evaluation;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public ArrayList<com.example.ibraheem.mirva.Model.Response.evaluationData> getEvaluationData() {
        return evaluationData;
    }

    public void setEvaluationData(ArrayList<com.example.ibraheem.mirva.Model.Response.evaluationData> evaluationData) {
        this.evaluationData = evaluationData;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }
}
