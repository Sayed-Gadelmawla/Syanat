package com.example.ibraheem.mirva.Fragments.common;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;

import com.example.ibraheem.mirva.Const.FragConst;
import com.example.ibraheem.mirva.CustomViews.JFButton;
import com.example.ibraheem.mirva.CustomViews.JFEditText;
import com.example.ibraheem.mirva.CustomViews.JFSpinnerAdapter;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.Interfaces.Communicator;
import com.example.ibraheem.mirva.Model.Response.Tickets.TicketType;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.Model.Response.helperRes;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.UitilsFun;
import com.example.ibraheem.mirva.util.D;
import com.example.ibraheem.mirva.util.L;
import com.example.ibraheem.mirva.util.V;
import com.example.ibraheem.mirva.util.helperFun;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ibraheem on 02/06/2017.
 */

public class Frag_SelectMessageType extends Fragment {

    public final String TAG = Frag_SelectMessageType.class.getSimpleName ();
    JFButton btn_send_message;
    Communicator comm;
    JFSpinnerAdapter customAdapter;
    Spinner spinner_type_of_ticket;
    String lang;
    User user;
    JFEditText ticket_content;
    V vd;
    ApiInterface mAPIService;
    ArrayList<TicketType> ticketTypes;
    D d;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        lang = helperFun.getLanguage (getActivity ());
        user = helperFun.getUser (getActivity ());
        vd = new V (getActivity ());

        return inflater.inflate (R.layout.fragment_select_message, container, false);

    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated (view, savedInstanceState);
        btn_send_message = (JFButton) view.findViewById (R.id.btn_send_message);
        ticket_content = (JFEditText) view.findViewById (R.id.ticket_content);

        spinner_type_of_ticket = (Spinner) view.findViewById (R.id.spinner_type_of_ticket);
        customAdapter = new JFSpinnerAdapter (getContext (), R.layout.item_spinner);
        spinner_type_of_ticket.setAdapter (customAdapter);


        vd.validateTextArea (R.id.ticket_content);


        btn_send_message.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {


                if (spinner_type_of_ticket.getSelectedItemPosition () == 0) {

                    JFTextView selectedView = (JFTextView) spinner_type_of_ticket.getSelectedView ();
                    selectedView.setError (getString (R.string.err_selection));
                    return;
                }

                if (ticket_content.getText ().length () < 1) {
                    ticket_content.setError (getString (R.string.err_empty));
                    ticket_content.requestFocus ();
                    return;
                }

                if (UitilsFun.isNetworkAvailable (getActivity ())) {
                    int tiket_type_id = ticketTypes.get (spinner_type_of_ticket.getSelectedItemPosition () - 1).getId ();
                    String content = ticket_content.getText ().toString ();
                    createNewTicket (tiket_type_id, content);
                }


            }
        });
        comm = (Communicator) getActivity ();
        comm.setTitleToolbar (getContext ().getResources ().getString (R.string.select_message_type));
        comm.selectVisibleToolbar (R.id.toolbar_clints);

        getTicketTypes ();
    }

    @Override
    public void onStart() {
        super.onStart ();

    }

    void getTicketTypes() {

        customAdapter.add (getString (R.string.select_message_type));
        mAPIService = ApiUtils.getAPIService (getActivity ());
        mAPIService.getTicketTypes (lang, user.getApiToken ()).enqueue (new Callback<ArrayList<TicketType>> () {
            @Override
            public void onResponse(Call<ArrayList<TicketType>> call, Response<ArrayList<TicketType>> response) {

                if (response.isSuccessful ()) {
                    ticketTypes = response.body ();
                    for (int i = 0; i < ticketTypes.size (); i++) {
                        customAdapter.add (ticketTypes.get (i).getName ());
                    }
                    customAdapter.notifyDataSetChanged ();
                } else {
                    helperRes.showMessageError (getActivity (), response.errorBody ());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<TicketType>> call, Throwable t) {

            }
        });
    }

    void createNewTicket(int type_id, final String content) {
        d = new D (getActivity ());
        d.ShowProgress ("");
        mAPIService = ApiUtils.getAPIService (getActivity ());
        mAPIService.createTicket (lang, user.getApiToken (), type_id, content).enqueue (new Callback<JsonObject> () {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                d.hide ();
                if (response.isSuccessful ()) {

                    Frag_Chat frag_chat = new Frag_Chat ();

                    int ticket_id = response.body ().get ("ticket_id").getAsInt ();
                    String message = response.body ().get ("message").getAsString ();
                    d.showMessageSuccess (message, frag_chat);

                    L.d (TAG, "");
                    Bundle arguments = new Bundle ();
                    arguments.putInt (FragConst.TICKET_ID, ticket_id);
                    arguments.putString (FragConst.CONTENT, content);
                    frag_chat.setArguments (arguments);

                    comm.addFragment (frag_chat);
                    // helperRes.showMessageSuccess (getActivity (), message, null);

                  /*  if (getActivity () instanceof MainPageManager) {
                        ((MainPageManager) getActivity ()).addFragment (frag_chat);

                    } else if ((getActivity () instanceof MainPageClient)) {
                        ((MainPageClient) getActivity ()).addFragment (frag_chat);

                    } else if (getActivity () instanceof MainPageTechnical) {
                        ((MainPageTechnical) getActivity ()).addFragment (frag_chat);
                    }*/

                } else {
                    helperRes.showMessageError (getActivity (), response.errorBody ());


                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }
}
