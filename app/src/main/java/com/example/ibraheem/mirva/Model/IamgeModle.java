package com.example.ibraheem.mirva.Model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

/**
 * Created by ibraheem on 27/07/2017.
 */

public class IamgeModle {

    String name;
    String path;
    Bitmap bitmap;
    File file;
    Context context;

    public IamgeModle(Context context) {
        this.context = context;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;


    }

    public File getFile() {
        return file = persistImage (getBitmap (), getName ());

    }

      //  BitmapFactory.decodeFile ()
    private File persistImage(Bitmap bitmap, String name) {


      //  File filesDir = context.getFilesDir ();
        File imageFile = new File (getPath ());

      //  Bitmap resized = Bitmap.createScaledBitmap (bitmap, (int) (bitmap.getWidth () * 0.6), (int) (bitmap.getHeight () * 0.6), true);


        OutputStream os;
        try {
            os = new FileOutputStream (imageFile);
            bitmap.compress (Bitmap.CompressFormat.JPEG, 100, os);
            os.flush ();
            os.close ();
        } catch (Exception e) {
            // Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
        }
        return imageFile;
    }
}
