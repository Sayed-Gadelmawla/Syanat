package com.example.ibraheem.mirva.Model.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ibraheem on 20/06/2017.
 */

public class Success {
    @SerializedName("message")
    @Expose
    private String message;

    public Success(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
