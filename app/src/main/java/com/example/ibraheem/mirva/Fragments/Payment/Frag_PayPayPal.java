package com.example.ibraheem.mirva.Fragments.Payment;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.example.ibraheem.mirva.Const.FragConst;
import com.example.ibraheem.mirva.Fragments.Client.Frag_PaymentWay;
//import com.paypal.android.sdk.payments.PayPalConfiguration;
//import com.paypal.android.sdk.payments.PayPalService;

/**
 * Created by ibraheem on 28/06/2017.
 */

public class Frag_PayPayPal extends Fragment {

    private static final String PAYPAL_CLIENT_ID = "AYAq3lwOQxdHwmtHSaMrlF9m4BHdbbwQCaEszHuPHfb_xscGvEmVN_fO4xI2iEUjO5uRbTgsaah9wJkh";
    public final String TAG = Frag_PaymentWay.class.getSimpleName ();
    int SUBSCRIPTION_ID, SUBSCRIPTION_PRICE;
    String SUBSCRIPTION_NAME;

    //Paypal Configuration Object
    /*private static final PayPalConfiguration config = new PayPalConfiguration ()
            // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
            // or live (ENVIRONMENT_PRODUCTION)
            .environment (PayPalConfiguration.ENVIRONMENT_SANDBOX)
            .clientId (PAYPAL_CLIENT_ID);*/

    /**
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        Bundle arguments = getArguments ();

        if (arguments != null) {
            SUBSCRIPTION_NAME = arguments.getString (FragConst.SUBSCRIPTION_NAME);
            SUBSCRIPTION_ID = arguments.getInt (FragConst.SUBSCRIPTION_ID);
            SUBSCRIPTION_PRICE = Integer.parseInt (arguments.getString (FragConst.SUBSCRIPTION_PRICE));
        }



    /*    Intent intent = new Intent (getActivity (), PayPalService.class);
        intent.putExtra (PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        Log.d (TAG, "onStart: " + "payapal start  1");
        getActivity ().startService (intent);
        Log.d (TAG, "onStart: " + "payapal start 2");*/
    }
}
