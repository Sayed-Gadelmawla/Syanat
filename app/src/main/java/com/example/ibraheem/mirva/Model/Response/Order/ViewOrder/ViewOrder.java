package com.example.ibraheem.mirva.Model.Response.Order.ViewOrder;

import com.example.ibraheem.mirva.Model.Response.User;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ibraheem on 02/07/2017.
 */

public class ViewOrder {
    @SerializedName("status")
    @Expose
    private Status status;
    @SerializedName("data")
    @Expose
    private Request data;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("request_client_acceptance")
    @Expose
    private String request_client_acceptance;


    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Request getData() {
        return data;
    }

    public void setData(Request data) {
        this.data = data;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getRequest_client_acceptance() {
        return request_client_acceptance;
    }

    public void setRequest_client_acceptance(String request_client_acceptance) {
        this.request_client_acceptance = request_client_acceptance;
    }
}


