package com.example.ibraheem.mirva.util;

import android.content.Context;
import android.graphics.Point;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import com.example.ibraheem.mirva.CustomViews.FWTextView;
import com.example.ibraheem.mirva.CustomViews.JFEditText;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.R;
import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * Created by ibraheem on 17/06/2017.
 */

public class helperFun {

    public static User getUser(Context context) {
        String userData = SharedPrefs.getString (context, SharedPrefs.USER);
        if (userData == null) return null;

        Gson gson = new Gson ();
        return gson.fromJson (userData, User.class);
    }

    public static String getLanguage(Context context) {
        return SharedPrefs.getString (context, SharedPrefs.LANGUAGE);
    }

    public static RequestBody getRequestBody(JFEditText editText) {
        return RequestBody.create (MediaType.parse ("multipart/form-data"), editText.getText ().toString ());
    }

    public static RequestBody getRequestBody(FWTextView editText) {
        return RequestBody.create (MediaType.parse ("multipart/form-data"), editText.getText ().toString ());
    }

    public static String getDifferenceDatesm(Context mContext, String strDate, boolean isDone) {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");
        Date endDate;
        try {
            endDate = simpleDateFormat.parse (strDate);
            Log.d ("helperFun ", "endDate: " + endDate);
            Log.d ("helperFun ", "Time : " + new Date ());
            long different = isDone ? (endDate.getTime () - new Date ().getTime ()) : (new Date ().getTime () - endDate.getTime ());

            Log.d ("helperFun ", "different: " + different);

            long secondsInMilli = 1000;
            long minutesInMilli = secondsInMilli * 60;
            long hoursInMilli = minutesInMilli * 60;
            long daysInMilli = hoursInMilli * 24;

            long elapsedDays = different / daysInMilli;
            different = different % daysInMilli;

            long elapsedHours = different / hoursInMilli;
            different = different % hoursInMilli;

            long elapsedMinutes = different / minutesInMilli;
            different = different % minutesInMilli;

            long elapsedSeconds = different / secondsInMilli;

        } catch (ParseException e) {
            e.printStackTrace ();
            Log.d ("helperFun ", "getDifferenceDatesm: " + e.getMessage ());
        }
        return strDate;
    }

}
