package com.example.ibraheem.mirva.Model.Response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ibraheem on 21/06/2017.
 */

public class BankAccount {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("bank")
    @Expose
    private String bank;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("owner")
    @Expose
    private String owner;
    @SerializedName("iban_number")
    @Expose
    private String ibanNumber;
    @SerializedName("account_number")
    @Expose
    private String accountNumber;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getIbanNumber() {
        return ibanNumber;
    }

    public void setIbanNumber(String ibanNumber) {
        this.ibanNumber = ibanNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

}