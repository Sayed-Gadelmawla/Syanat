package com.example.ibraheem.mirva.Adapter;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.ibraheem.mirva.CustomViews.JFButton;
import com.example.ibraheem.mirva.Fragments.Client.ActivityNewOrder;
import com.example.ibraheem.mirva.Model.IamgeModle;
import com.example.ibraheem.mirva.R;

import java.util.ArrayList;

/**
 * Created by ibraheem on 22/06/2017.
 */

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.myHolder> {

    public final String TAG = ImageAdapter.class.getSimpleName ();
    Context context;
    ArrayList<IamgeModle> imagesPath;

    public ImageAdapter(Context context, ArrayList<IamgeModle> imagesPath) {

        this.context = context;
        this.imagesPath = imagesPath;

    }

    @Override
    public myHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new myHolder (LayoutInflater.from (parent.getContext ()).inflate (R.layout.item_add_image, parent, false));
    }

    @Override
    public void onBindViewHolder(myHolder holder, final int position) {
        String path = imagesPath.get (position).getPath ();
        holder.added_image.setImageBitmap (BitmapFactory.decodeFile (path));

        Log.d (TAG, "onBindViewHolder: ");

        holder.deleteImage.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                imagesPath.remove (position);
                if (imagesPath.size () == 0) {
                    ((ActivityNewOrder) (context)).findViewById (R.id.container_add_images).setVisibility (View.GONE);
                    ((ActivityNewOrder) (context)).findViewById (R.id.container_add_media).setVisibility (View.VISIBLE);
                }
                notifyDataSetChanged ();
            }
        });

    }

    @Override
    public int getItemCount() {
        return imagesPath.size ();
    }

    class myHolder extends RecyclerView.ViewHolder {
        ImageView added_image;
        JFButton deleteImage;

        public myHolder(View itemView) {
            super (itemView);

            deleteImage = (JFButton) itemView.findViewById (R.id.deleteVideo);
            added_image = (ImageView) itemView.findViewById (R.id.added_image);

        }

    }
}
