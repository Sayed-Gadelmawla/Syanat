package com.example.ibraheem.mirva.CustomViews;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;

import com.example.ibraheem.mirva.R;

/**
 * Created by ibraheem on 25/07/2017.
 */

public class SelectListener implements RecyclerView.OnClickListener {

    private Animation grow = null;
    private View lastView = null;

    public SelectListener(Context c) {
        grow = AnimationUtils.loadAnimation (c, R.anim.grow);
    }

    public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {

        // Shrink the view that was zoomed
        try {
            if (null != lastView) lastView.clearAnimation ();
        } catch (Exception clear) {
        }

        // Zoom the new selected view
        try {
            v.startAnimation (grow);
        } catch (Exception animate) {
        }

        // Set the last view so we can clear the animation
        lastView = v;
    }

    public void onNothingSelected(AdapterView<?> parent) {
    }

    @Override
    public void onClick(View v) {

    }
}