package com.example.ibraheem.mirva.Fragments.manager;


import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.example.ibraheem.mirva.Adapter.ManagerRequestsAdapter;
import com.example.ibraheem.mirva.Interfaces.Communicator;
import com.example.ibraheem.mirva.Model.Response.ManagerResonse.MAllOrders;
import com.example.ibraheem.mirva.Model.Response.ManagerResonse.MOrder;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.util.D;
import com.example.ibraheem.mirva.util.L;
import com.example.ibraheem.mirva.util.SessionManager;
import com.example.ibraheem.mirva.util.helperFun;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class Manager_Frag_MyOrders extends Fragment {


    RecyclerView recyclerView;
    ManagerRequestsAdapter mRequestsAdapter;
    ArrayList<MOrder> arrayList;
    Communicator comm;
    private ProgressBar mProgressBar;
    private SessionManager mSessionManager;
    private Context mContext;

    User user;
    private SwipyRefreshLayout mSwiper;
    //D d;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user = helperFun.getUser(getActivity().getApplicationContext());
        // d = new D (getActivity ());


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContext = getActivity();
        mSessionManager = new SessionManager(mContext);

        View view = inflater.inflate(R.layout.fragment_manager_my_requests, container, false);
        mSwiper = (SwipyRefreshLayout) view.findViewById(R.id.swiperefresh);
        arrayList = new ArrayList<>();

        mRequestsAdapter = new ManagerRequestsAdapter(getActivity(), arrayList);
        recyclerView = (RecyclerView) view.findViewById(R.id.my_request_recyler_view);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayout.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(mRequestsAdapter);
        comm = (Communicator) getActivity();
        comm.selectVisibleToolbar(R.id.manager_toolbar_main);
        comm.setTitleToolbar(0);
        new FillAllMOrdersAsyncTask().execute();
        mSwiper.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                arrayList = new ArrayList<>();
                mRequestsAdapter = new ManagerRequestsAdapter(getActivity(), arrayList);
                recyclerView.setAdapter(mRequestsAdapter);
                new FillAllMOrdersAsyncTask().execute();
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mSwiper.setRefreshing(false);
                    }
                }, 650);

            }
        });
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    private void loadData() {
       /* arrayList.add (new mRequest (getBitmap (R.drawable.icon_elect), getBitmap (R.drawable.face), "كهربائيات", "احمد", "فني محترف", null, "تاخر ", NOTIFICATION_STATUS.UNDER_WAY));//, "انتهت منذ   15 س : 30 د : 50 ث"));
        arrayList.add (new mRequest (getBitmap (R.drawable.icon_elect), getBitmap (R.drawable.face), "كهربائيات", "احمد", "فني محترف", null, " 15 س : 30 د : 50 ث ", NOTIFICATION_STATUS.UNDER_WAY));//, "انتهت منذ   15 س : 30 د : 50 ث"));
        arrayList.add (new mRequest (getBitmap (R.drawable.icon_elect), getBitmap (R.drawable.face), "كهربائيات", "احمد", "فني محترف", NOTIFICATION_STATUS.UNDER_WAY, 3.5f));//, "انتهت منذ   15 س : 30 د : 50 ث"));
        arrayList.add (new mRequest (getBitmap (R.drawable.icon_elect), getBitmap (R.drawable.face), "كهربائيات", "احمد", "فني محترف", null, " 15 س : 30 د : 50 ث ", NOTIFICATION_STATUS.UNDER_WAY));//, "انتهت منذ   15 س : 30 د : 50 ث"));
        arrayList.add (new mRequest (getBitmap (R.drawable.icon_elect), getBitmap (R.drawable.face), "كهربائيات", "احمد", "فني محترف", null, "تاخر ", NOTIFICATION_STATUS.UNDER_WAY));//, "انتهت منذ   15 س : 30 د : 50 ث"));
        mRequestsAdapter.notifyDataSetChanged ();*/
    }

    /**
     * get all order
     */

    public class FillAllMOrdersAsyncTask extends AsyncTask<Void, Void, Void> {
        String lang;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            lang = helperFun.getLanguage(getActivity().getApplicationContext());
        }

        @Override
        protected Void doInBackground(Void... params) {

            ApiInterface apiInterface = ApiUtils.getAPIService(getActivity());
            apiInterface.getAllMOrders(user.getApiToken()).enqueue(new Callback<MAllOrders>() {
                @Override
                public void onResponse(Call<MAllOrders> call, Response<MAllOrders> response) {
                    if (response.isSuccessful()) {
                        mRequestsAdapter.loadData(response.body());
                        mSessionManager.setMAllOrders(response.body(), lang);
                    } else {
                        L.d(TAG, "errorBody : " + response.errorBody());
                    }
                    //d.hide ();

                    mProgressBar.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onFailure(Call<MAllOrders> call, Throwable t) {
                    mProgressBar.setVisibility(View.INVISIBLE);
                    if (mSessionManager.getMAllOrders(lang) != null) {


                        mRequestsAdapter.loadData(mSessionManager.getMAllOrders(lang));

                        // d.ShowErrorInternetConnection ("");
                    }
                }
            });

            return null;
        }
    }


}
