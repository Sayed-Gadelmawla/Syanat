package com.example.ibraheem.mirva.CustomViews;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.List;

/**
 * Created by ibraheem on 11/05/2017.
 */

public class JFSpinnerAdapter extends ArrayAdapter {
    public JFSpinnerAdapter(@NonNull Context context, @LayoutRes int resource) {
        super (context, resource);
    }

    public JFSpinnerAdapter(@NonNull Context context, @LayoutRes int resource, @IdRes int textViewResourceId) {
        super (context, resource, textViewResourceId);
    }

    public JFSpinnerAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull Object[] objects) {
        super (context, resource, objects);
    }

    public JFSpinnerAdapter(@NonNull Context context, @LayoutRes int resource, @IdRes int textViewResourceId, @NonNull Object[] objects) {
        super (context, resource, textViewResourceId, objects);
    }

    public JFSpinnerAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List objects) {
        super (context, resource, objects);
    }

    public JFSpinnerAdapter(@NonNull Context context, @LayoutRes int resource, @IdRes int textViewResourceId, @NonNull List objects) {
        super (context, resource, textViewResourceId, objects);
    }

    private void init() {


    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        JFTextView view = (JFTextView) super.getView (position, convertView, parent);
        //    view.setTextColor(convertView.getResources().getColor(R.color.colorAccent));
/*
        view.setTypeface (Typeface.createFromAsset (getContext ().getAssets (), "fonts/jf_flat_regular.ttf"));
        view.setGravity (Gravity.RIGHT);*/

        return view;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        JFTextView view = (JFTextView) super.getDropDownView (position, convertView, parent);
        //  view.setTextColor(convertView.getResources().getColor(R.color.fontColorDark));
       /* view.setTypeface (Typeface.createFromAsset (getContext ().getAssets (), "fonts/jf_flat_regular.ttf"));
        view.setGravity (Gravity.RIGHT);*/
        return view;
    }

    @Override
    public boolean isEnabled(int position) {
        return position != 0;
    }

}