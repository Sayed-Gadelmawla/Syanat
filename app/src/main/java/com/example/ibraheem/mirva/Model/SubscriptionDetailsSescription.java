package com.example.ibraheem.mirva.Model;

import com.google.android.gms.drive.metadata.SearchableCollectionMetadataField;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Eng.Mhmod on 9/11/2017.
 */

public class SubscriptionDetailsSescription implements Serializable {
    @SerializedName("maintenance_count")
    @Expose
    private Integer maintenance_count;
    @SerializedName("sub_service_name")
    @Expose
    private String sub_service_name;

    public Integer getMaintenance_count() {
        return maintenance_count;
    }

    public void setMaintenance_count(Integer maintenance_count) {
        this.maintenance_count = maintenance_count;
    }

    public String getSub_service_name() {
        return sub_service_name;
    }

    public void setSub_service_name(String sub_service_name) {
        this.sub_service_name = sub_service_name;
    }
}
