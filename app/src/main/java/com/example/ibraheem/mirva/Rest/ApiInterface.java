package com.example.ibraheem.mirva.Rest;

/**
 * Created by ibraheem on 10/06/2017.
 */

import com.example.ibraheem.mirva.Model.Advertisment;
import com.example.ibraheem.mirva.Model.AllAdvertisment;
import com.example.ibraheem.mirva.Model.ManagerSendOrder;
import com.example.ibraheem.mirva.Model.Response.BankAccount;
import com.example.ibraheem.mirva.Model.Response.EevaluteOrderData.AllEvalutions;
import com.example.ibraheem.mirva.Model.Response.EevaluteOrderData.EvalService;
import com.example.ibraheem.mirva.Model.Response.ManagerResonse.AllThechnican;
import com.example.ibraheem.mirva.Model.Response.ManagerResonse.MAllOrders;
import com.example.ibraheem.mirva.Model.Response.ManagerResonse.Store;
import com.example.ibraheem.mirva.Model.Response.Notification.Notfications;
import com.example.ibraheem.mirva.Model.Response.Order.AllOrders;
import com.example.ibraheem.mirva.Model.Response.Order.Technical.Requirement;
import com.example.ibraheem.mirva.Model.Response.Order.Technical.SAllOrders;
import com.example.ibraheem.mirva.Model.Response.Order.ViewOrder.ViewOrder;
import com.example.ibraheem.mirva.Model.Response.Service;
import com.example.ibraheem.mirva.Model.Response.Tickets.OrderCat;
import com.example.ibraheem.mirva.Model.Response.Tickets.TicketType;
import com.example.ibraheem.mirva.Model.Response.TicketsResonse.AllTickets;
import com.example.ibraheem.mirva.Model.Response.TicketsResonse.Chat;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.Model.Response.WorkingHour;
import com.example.ibraheem.mirva.Model.SendOrder;
import com.example.ibraheem.mirva.Model.Subscription;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("v1/login")
    Call<User> getAccessToken(@Header("Accept-Language") String lang, @Header("Device-Token")
            String token, @Field("email") String email, @Field("password") String password);

    @POST("v1/logout")
    Call<JsonObject> sendLogout(@Header("Api-Token") String token);

    @Multipart
    @POST("v1/register")
    Call<User> createUser(@Header("Accept-Language") String lang, @Header("Device-Token") String token, @Part("name")
            RequestBody name, @Part("email") RequestBody email, @Part("password") RequestBody password, @Part("confirm") RequestBody confirm,
                                @Part MultipartBody.Part image);



    @GET("v1/user/profile")
    Call<JsonObject> getUserPorfileData(@Header("Accept-Language") String lang, @Header("Api-Token") String token);

    //for clinet to get service
    @GET("v1/services")
    Call<ArrayList<Service>> getServices(@Header("Accept-Language") String lang, @Header("Api-Token") String api_token);


    // get subscriptions when subsciption account and pay
    @GET("v1/subscriptions")
    Call<ArrayList<Subscription>> getSubscriptionData(@Header("Accept-Language") String lang, @Header("Api-Token") String api_token);

    @GET("v1/user/subscriptions")
    Call<ArrayList<Subscription>> getUserSubscriptionData(@Header("Accept-Language") String lang, @Header("Api-Token") String api_token);

    @Multipart
    @POST("v1/user/profile/update")
//profile  class
    Call<User> updateProfileDate(@Header("Accept-Language") String lang, @Header("Api-Token") String api_token,
                                 @Part("name") RequestBody name, @Part("email") RequestBody email, @Part("mobile") RequestBody mobile, @Part("gender") int gender,
                                 @Part("birth_date") RequestBody birth_date,
                                 @Part MultipartBody.Part image,@Part("statusNote") int statusNote);

    @FormUrlEncoded
    @POST("v1/activate/resend")
//activaition class
    Call<JsonObject> resendActiviation(@Header("Accept-Language") String lang, @Field("mobile") String mobile);

    @FormUrlEncoded
    @POST("v1/activate/send")
//activaition class
    Call<JsonObject> sendActiviation(@Header("Accept-Language") String lang, @Field("email") String email , @Field("mobile") String mobile);

    @FormUrlEncoded
    @POST("v1/register/activate")
//activaition class
    Call<User> activiation(@Header("Accept-Language") String lang, @Field("activation_code") String activation_code, @Field("mobile") String mobile);


    @GET("v1/ticket/show")
    Call<AllTickets> getAllTickets(@Header("Api-Token") String api_token, @Query("page") int page);

    @GET("v1/evaluations")
    Call<AllEvalutions> getAllEvaluations(@Header("Api-Token") String api_token);

    @GET("v1/request/show")
    Call<AllOrders> getAllUserOrder(@Header("Api-Token") String api_token);


    @GET("v1/ticket/details")
    Call<Chat> getSpecificTicketData(@Header("Api-Token") String api_token, @Query("ticket_id") int ticket_id);

    @FormUrlEncoded
    @POST("v1/ticket/message/store")
    Call<JsonObject> sendMessage(@Header("Api-Token") String api_token, @Field("ticket_id") int ticket_id, @Field("message") String message);

    @Multipart
    @POST("v1/ticket/image/upload")
    Call<JsonObject> sendImageMessage(@Header("Api-Token") String api_token, @Part("ticket_id") RequestBody ticket_id, @Part MultipartBody.Part image);


    @FormUrlEncoded
    @POST("v1/user/paypal/check")
    Call<JsonObject> payPayPal(@Header("Api-Token") String api_token, @Field("payment_id") String payment_id, @Field("subscription_id") int subscription_id);

    // get data about
    @GET("v1/about")
    Call<JsonObject> getAbout();

    // get data about
    @GET("v1/policy")
    Call<JsonObject> getPolicy(@Header("Accept-Language") String lang);

    //get data transfer bank when user  want to pay
    @GET("v1/request/bank/transfer")
    Call<ArrayList<BankAccount>> getBankAccount(@Header("Accept-Language") String lang, @Header("Api-Token") String api_token);

    // forget password
    @FormUrlEncoded
    @POST("v1/password/forget")
    Call<JsonObject> forgetPassword(@Header("Accept-Language") String lang, @Field("email") String email);


    ///get category to selected request on new order
    @GET("v1/request")
    Call<ArrayList<OrderCat>> getOrderCat(@Header("Accept-Language") String lang, @Header("Api-Token") String api_token, @Query("serviceId") int serviceId);

    ///get category to selected request on new order
    // TODO: 27/06/2017 remove date from time , and send it ordrded by day
    @FormUrlEncoded
    @POST("v1/working/hour/get")
    Call<ArrayList<WorkingHour>> getWorkingHour(@Header("Accept-Language") String lang, @Header("Api-Token") String api_token, @Field("service_id") int service_id,@Field("request_date") String date);

    @FormUrlEncoded
    @POST("v1/ticket/create")
//  create new tickets when select type
    Call<JsonObject> createTicket(@Header("Accept-Language") String lang, @Header("Api-Token") String api_token, @Field("type_id") int type_id, @Field("content") String content);

    //     //for clinet to get tickets type when  he want to send ,message
    @GET("v1/ticket/get")
    Call<ArrayList<TicketType>> getTicketTypes(@Header("Accept-Language") String lang, @Header("Api-Token") String api_token);

    @GET("v1/requirements/supervisor/inventory")
    Call<Store> getStoreItems(@Header("Api-Token") String api_token);


    @FormUrlEncoded
    @POST("v1/request/bank/transfer/store")
    Call<JsonObject> payByBankTransfer(@Header("Api-Token") String api_token, @Field("bank_transfer_id") int bank_transfer_id, @Field("transfer_name") String transfer_name, @Field("transfer_number") String transfer_number, @Field("subscription_id") int supscription_id
    );


    @Multipart
    @POST("v1/request/image/upload")
    Call<JsonObject> uploadImage(@Header("Api-Token") String api_token, @Part MultipartBody.Part image);

    @Multipart
    @POST("v1/request/video/upload")
    Call<JsonObject> uploadVideo(@Header("Api-Token") String api_token, @Part MultipartBody.Part video);

    @POST("v1/request")
    Call<JsonObject> sendOrder(@Header("Content-Type") String contetnType, @Header("Accept-Language") String lang, @Header("Api-Token") String api_token, @Body SendOrder orderBody);


    @FormUrlEncoded
    @POST("v1/request/client/price/choose")
//accept price or reject by client
    Call<JsonObject> acceptOrRejectPrice(@Header("Api-Token") String api_token, @Field("request_id") int request_id, @Field("status") int status);

    @FormUrlEncoded
    @POST("v1/request/client/confirm_completion")
//accept pice or reject by client
    Call<JsonObject> ConfirmOrder(@Header("Api-Token") String api_token, @Field("request_id") int request_id,@Field("status") int status);


    ///technical

    @GET("v1/request/technician/requests")
//getAllOrder for technical
    Call<SAllOrders> getTechnicalOrders(@Header("Api-Token") String api_token);

    @GET("v1/advertisement/show")
    Call<AllAdvertisment> getAllAdvertisment(@Header("Api-Token") String api_token, @Header("Accept-Language") String lang);


    @GET("v1/request/technicians/requests")
//getAllOrder for technical
    Call<SAllOrders> getTechnicalUnderWayOrders(@Header("Api-Token") String api_token);

    @GET("v1/request/technician/details")
//request details for thecnician
    Call<ViewOrder> getTechSpecificOrderData(@Header("Api-Token") String api_token, @Query("request_id") int request_id);

    @FormUrlEncoded
    @POST("v1/request/client/payment/cash")
// send price to client  by supervise
    Call<JsonObject> sendPaymentToClinent(@Header("Api-Token") String api_token, @Field("request_id") int request_id, @Field("amount") int amount);

    @FormUrlEncoded
    @POST("v1/request/technicians/notes/update")
// send price to client  by supervise
    Call<JsonObject> sendTechnicalNotes(@Header("Api-Token") String api_token, @Field("request_id") int request_id, @Field("note") String note);

    ///manager
    @GET("v1/request/supervisors/technicians/show")
//request details for thecnician
    Call<AllThechnican> getAllThechnicans(@Header("Api-Token") String api_token,@Header("Accept-Language") String lang);

    ///manager
    @GET("v1/request/supervisor/technician/choose")
    Call<ArrayList<Requirement>> geRequirments(@Header("Api-Token") String api_token);


    @POST("v1/request/supervisor/technician/store")
    Call<JsonObject> sendViewedbyManagerOrder(@Header("Content-Type") String contetnType, @Header("Api-Token") String api_token, @Body ManagerSendOrder orderBody);


    ///managet orders


    @GET("v1/request/new/requests")
//all new orders for superviser
    Call<MAllOrders> getAllMNewOrders(@Header("Api-Token") String api_token);


    @GET("v1/request/all/requests")
//all  orders for superviser
    Call<MAllOrders> getAllMOrders(@Header("Api-Token") String api_token);

    @GET("v1/request/process/requests")
//all  orders for superviser
    Call<MAllOrders> getAllMUndreWayOrders(@Header("Api-Token") String api_token);

    @GET("v1/request/ended/requests")
//all  orders for superviser
    Call<MAllOrders> getAllEndedMOrders(@Header("Api-Token") String api_token);


    //get all user notifications
    @GET("v1/notifications")
//all  orders for superviser
    Call<Notfications> getAllUserNotfications(@Header("Api-Token") String api_token);

    @GET("v1/evaluation")
//get evalution data for service when it's complete
    Call<EvalService> getEvalutionServiceData(@Header("Api-Token") String api_token, @Query("request_id") int request_id,@Header("Accept-Language") String lang);


    @FormUrlEncoded
    @POST("v1/evaluation")
//send evalution  order that inserted by user

    Call<JsonObject> sendEvalutionOrder(@Header("Api-Token") String api_token, @Field("request_id") int request_id, @Field("review") String review
            , @Field("eval1_id") int eval1_id, @Field("eval1_val") int eval1_val, @Field("eval2_id") int eval2_id, @Field("eval2_val") int eval2_val
            , @Field("eval3_id") int eval3_id, @Field("eval3_val") int eval3_val, @Field("eval4_id") int eval4_id, @Field("eval4_val") int eval4_val);


    //Technical .... show details for specific technical request
    @GET("v1/request/technician/details")
    Call<ViewOrder> getSODTechnical(@Header("Api-Token") String api_token, @Query("request_id") int request_id);

    //supervise .... show details for specific user request
    @GET("v1/request/supervisor/details")
    Call<ViewOrder> getSODSupervise(@Header("Api-Token") String api_token, @Query("request_id") int request_id);

    //client .... show details for specific user request
    @GET("v1/request/client/details")
    Call<ViewOrder> getSODClient(@Header("Api-Token") String api_token, @Query("request_id") int request_id);

    @FormUrlEncoded
    @POST("v1/request/client/payment/subscription")
    Call<JsonObject> finishRequestbyClient(@Header("Api-Token") String api_token, @Field("request_id") int request_id);

    //Technical .... of number of non completed requests
    @FormUrlEncoded
    @POST("v1/request/technician/count")
    Call<JsonObject> getTechnicalRequests(@Header("Api-Token") String api_token, @Field("technician_id") int technician_id);

    //get notification number
    @GET("v1/notifications/user/count")
    Call<JsonObject> getNotifications(@Header("Api-Token") String api_token);

    @PATCH("v1/notifications/user/update")
    Call<JsonObject> updateSeenNotification(@Header("Api-Token") String api_token);

    @GET("v1/ticket/message/count")
    Call<JsonObject> getMessagesnumber(@Header("Api-Token") String api_token);

    @PATCH("v1/ticket/message/update")
    Call<JsonObject> updateSeenMessage(@Header("Api-Token") String api_token);

    @FormUrlEncoded
    @PATCH("v1/user/password/change")
    Call<JsonObject> upldatePassword(@Header("Api-Token") String api_token, @Field("old") String old, @Field("password") String password, @Field("confirm") String confirm);

    @FormUrlEncoded
    @PATCH("v1/user/location/change")
    Call<JsonObject> upldateLocation(@Header("Api-Token") String api_token, @Field("longitude") String longitude, @Field("latitude") String latitude);

    @FormUrlEncoded
    @PATCH("v1/user/language/reset")
    Call<JsonObject> restLanguage(@Header("Api-Token") String api_token, @Field("language") String lang);

}
