package com.example.ibraheem.mirva.Adapter;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ibraheem.mirva.CustomViews.JFEditText;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.Model.Response.Order.Technical.Requirement;
import com.example.ibraheem.mirva.R;

import java.util.ArrayList;

/**
 * Created by ibraheem on 26/07/2017.
 */

public class SelectedRequirmentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context mContext;
    ArrayList<Requirement> requirements, selectedRequirments;
    int avilabile;

    public SelectedRequirmentAdapter(Context mContext, ArrayList<Requirement> requirements) {
        this.mContext = mContext;
        selectedRequirments = new ArrayList<> ();
        this.requirements = requirements;
    }



    @Override
    public int getItemViewType(int position) {
        Log.d ("RequirmentAdapter", "getItemViewType: " + requirements.get (position));

        return 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams (ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        View inflate;
        switch (viewType) {

            case 0:
                inflate = LayoutInflater.from (parent.getContext ()).inflate (R.layout.item_requirment_selected, null, false);
                inflate.setLayoutParams (lp);

                return new SelectedRequirmentAdapter.MyViewHolderRequired (inflate);



        }
        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        final Requirement requirement = requirements.get (position);


        switch (holder.getItemViewType ()) {

            case 0:
                SelectedRequirmentAdapter.MyViewHolderRequired myViewHolderRequired = (SelectedRequirmentAdapter.MyViewHolderRequired) holder;
                myViewHolderRequired.requirment_required.setText (requirement.getQuantity ());
                myViewHolderRequired.requirmnet_name.setText (requirement.getRequirement ());
                break;





        }


    }

    @Override
    public int getItemCount() {
        return requirements != null ? requirements.size () : 0;
    }

    public void loadData(ArrayList<Requirement> dataFiltered) {

        requirements = dataFiltered;
        notifyDataSetChanged ();

    }

    public void clearData() {

        selectedRequirments.clear ();
    }



    public class MyViewHolderRequired extends RecyclerView.ViewHolder {

        JFEditText requirment_required;
        JFTextView requirmnet_name;

        public MyViewHolderRequired(View itemView) {
            super (itemView);
            requirment_required = (JFEditText) itemView.findViewById (R.id.requirment_required);
            requirmnet_name = (JFTextView) itemView.findViewById (R.id.requirmnet_name);

        }
    }


}
