package com.example.ibraheem.mirva.Activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;

import com.eftimoff.androipathview.PathView;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.UitilsFun;
import com.example.ibraheem.mirva.util.helperFun;

public class MainSplash extends AppCompatActivity {
    private ImageView mSplashLogoImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UitilsFun.setLocale (this, helperFun.getLanguage (this));
        setContentView(R.layout.activity_main_splsh);
        mSplashLogoImageView = (ImageView) findViewById(R.id.splash_logo_iv);
        AlphaAnimation anim = new AlphaAnimation(1.0f, 0.0f);
        anim.setDuration(1000);
        anim.setRepeatCount(3);
        anim.setRepeatMode(Animation.REVERSE);
        mSplashLogoImageView.startAnimation(anim);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(getApplicationContext(),Splash.class));
                finish();
            }
        }, 2000);
    }
}
