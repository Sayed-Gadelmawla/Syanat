package com.example.ibraheem.mirva.Model.Response.TicketsResonse;

import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ibraheem on 29/06/2017.
 */

public class Tikect {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("ticket_id")
    @Expose
    private Integer ticketId;
    @SerializedName("type_id")
    @Expose
    private Integer typeId;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("image")
    @Expose
    private String image;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getTicketId() {
        return ticketId;
    }

    public void setTicketId(Integer ticketId) {
        this.ticketId = ticketId;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImage() {
        return ApiUtils.BASE_URL + image;
    }

    public void setImage(String image) {
        this.image = image;


    }
}
