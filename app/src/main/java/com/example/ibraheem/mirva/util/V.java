package com.example.ibraheem.mirva.util;

import android.app.Activity;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.example.ibraheem.mirva.CustomViews.Farsi;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.UitilsFun;

import static com.basgeekball.awesomevalidation.ValidationStyle.BASIC;

/**
 * Created by ibraheem on 29/06/2017.
 */

public class V {

    /**
     *custpom class to make validation
     *
     */
    private static String regexPassword = "^(?=.*).{6,50}";
    private static String regexBoxNumber = "^[0-9]$";

    private static String regexDate = "[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])";
    private static String reqexPhone = "(((009|\\+9)|(?=.*[1-9]))).{9,18}";
    private static String regexTextArea = "^(?!\\s*$).+";
    private static String regexTransferNumber = "(?=.*[0-9])";
    private static String regexName = "^(?!\\s*$).+";

    private static String regexPrice = "^[+]?([.]\\d+|\\d+([.]\\d+)?)$";

    ///private static String regexPrice = "^[1-9]\\d{0,7}(?:\\.\\d{1,4})?$";
    //private static String reqexPhone = "^(?:0|\\(?\\+966\\)?\\s?|00966\\s?)[1-79](?:[\\.\\-\\s]?\\d\\d){4}$";
    //  public static String regexPassword = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[\\d])(?=.*[~`!@#\\$%\\^&\\*\\(\\)\\-_\\+=\\{\\}\\[\\]\\|\\;:\"<>,./\\?]).{6,}";
    AwesomeValidation mAwesomeValidation;
    Activity mContext;


    boolean isArabic;

    public V(Activity context, ValidationStyle validationStyle) {
        mAwesomeValidation = new AwesomeValidation (validationStyle);
        mAwesomeValidation.setContext (context);
        mContext = context;
        isArabic= helperFun.getLanguage (mContext).equals ("ar");



    }

    public V(Activity context) {
        mAwesomeValidation = new AwesomeValidation (BASIC);
        mContext = context;
     //   mAwesomeValidation.setContext (context);
        isArabic= helperFun.getLanguage (mContext).equals ("ar");



    }

    public void ValidatePrice(int id) {
        mAwesomeValidation.addValidation (mContext, id, regexPrice, R.string.err_price);
    }

    public void ValidateUserName(int id) {
        mAwesomeValidation.addValidation (mContext, id, regexName, R.string.err_name);
    }

    public void validateEmail(int id) {
        mAwesomeValidation.addValidation (mContext, id, android.util.Patterns.EMAIL_ADDRESS, R.string.err_email);
    }

    public void validatePassword(int id) {
        mAwesomeValidation.addValidation (mContext, id, regexPassword, R.string.err_password);
    }

    public void validateConfirmPassword(int idPassword, int idConfirm) {
        mAwesomeValidation.addValidation (mContext, idConfirm, idPassword, R.string.err_password_confirmation);

    }

    public void validatePhone(int id) {
        mAwesomeValidation.addValidation (mContext, id, reqexPhone, R.string.err_phone);

    }

    public void validateDate(int id) {
        mAwesomeValidation.addValidation (mContext, id, regexDate, R.string.err_date);

    }

    public void validateBoxNumber(int id) {
        mAwesomeValidation.addValidation (mContext, id, regexBoxNumber, R.string.err_box_number);

    }

    public void validateTextArea(int id) {
        mAwesomeValidation.addValidation (mContext, id, regexTextArea, R.string.err_empty);

    }

    public boolean isValid() {
        return mAwesomeValidation.validate ();
    }

    public void validateTransferNumber(int id) {
        mAwesomeValidation.addValidation (mContext, id, regexTransferNumber, R.string.err_box_number);

    }
}
