package com.example.ibraheem.mirva.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.example.ibraheem.mirva.Model.Response.EevaluteOrderData.AllEvalutions;
import com.example.ibraheem.mirva.Model.Response.ManagerResonse.MAllOrders;
import com.example.ibraheem.mirva.Model.Response.ManagerResonse.Store;
import com.example.ibraheem.mirva.Model.Response.Order.AllOrders;
import com.example.ibraheem.mirva.Model.Response.Order.Technical.SAllOrders;
import com.example.ibraheem.mirva.Model.Response.Order.Technical.TOrder;
import com.example.ibraheem.mirva.Model.Response.Service;
import com.example.ibraheem.mirva.Model.Response.TicketsResonse.AllTickets;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Mhmod on 06/10/2016.
 */

public class SessionManager {
    private static String TAG = "SessionManager";
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context mContext;
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_APP_NAME = "preferences_mirva";
    private static final String KEY_POLICY = "key_policy";
    private static final String KEY_ABOUT = "key_about";
    private static final String KEY_ARABIC_SERVICES = "key_services_ar";
    private static final String KEY_ENGLISH_SERVICES = "key_services_en";
    private static final String KEY_ARABIC_SAllOrders = "key_SAllOrders_ar";
    private static final String KEY_ENGLISH_SAllOrders = "key_SAllOrders_en";

    private static final String KEY_ARABIC_MAllOrders = "key_MAllOrders_ar";
    private static final String KEY_ENGLISH_MAllOrders = "key_MAllOrders_en";

    private static final String KEY_ARABIC_MEndedOrders = "key_MEndedOrders_ar";
    private static final String KEY_ENGLISH_MEndedOrders = "key_MEndedOrders_en";

    private static final String KEY_ARABIC_MNewOrders = "key_MNewOrders_ar";
    private static final String KEY_ENGLISH_MNewOrders = "key_MNewOrders_en";

    private static final String KEY_ARABIC_MStoreOrders = "key_MStoreOrders_ar";
    private static final String KEY_ENGLISH_MStoreOrders= "key_MStoreOrders_en";

    private static final String KEY_MY_ORDERS = "key_MY_ORDERS";
    private static final String KEY_MY_EVALUATIONS = "key_MY_EVALUATIONS";
    private static final String KEY_TICKETS = "key_TICKETS";
    private static final String KEY_PROFILE_DATA = "key_profile_data";
    private static final String KEY_IS_LOCATION_CHECKED = "key_IS_LOCATION_CHECKED";
    private static final String KEY_CURRENT_WORKING_HOUR_ID = "KEY_CURRENT_WORKING_HOUR_ID";

    public SessionManager(Context context) {
        this.mContext = context;
        pref = mContext.getSharedPreferences(PREF_APP_NAME, PRIVATE_MODE);

        editor = pref.edit();
    }


    public String getPolicy() {
        return pref.getString(KEY_POLICY, "");
    }

    public void setIsLocationChecked(boolean isLocationChecked) {
        editor.putBoolean(KEY_IS_LOCATION_CHECKED, isLocationChecked);
        editor.commit();
    }

    public boolean isLocationChecked() {
        return pref.getBoolean(KEY_IS_LOCATION_CHECKED, false);
    }

    public void setPolicy(String policy) {
        editor.putString(KEY_POLICY, policy);
        editor.commit();
    }

    public String getAbout() {
        return pref.getString(KEY_ABOUT, "");
    }

    public void setAbout(String about) {
        editor.putString(KEY_ABOUT, about);
        editor.commit();
    }
    public int getCurrentOrderWorkingId() {
        return pref.getInt(KEY_CURRENT_WORKING_HOUR_ID, -1);
    }

    public void setCurrentOrderWorkingId(int id) {
        editor.putInt(KEY_CURRENT_WORKING_HOUR_ID, id);
        editor.commit();
    }

    public void setServices(ArrayList<Service> services,String lang) {
        Gson gson = new Gson();
        String json = gson.toJson(services);
        Log.d(TAG,"Services = "+json);
        if(lang.equals("en")){
            editor.putString(KEY_ENGLISH_SERVICES, json);
        }else {
            editor.putString(KEY_ARABIC_SERVICES, json);
        }

        editor.commit();
    }
    public void setSAllOrders(SAllOrders mSAllOrderses,String lang) {
        Gson gson = new Gson();
        String json = gson.toJson(mSAllOrderses);
        if(lang.equals("en")){
            editor.putString(KEY_ENGLISH_SAllOrders, json);
        }else {
            editor.putString(KEY_ARABIC_SAllOrders, json);
        }

        editor.commit();
    }
    public void setMAllOrders(MAllOrders mMAllOrders, String lang) {
        Gson gson = new Gson();
        String json = gson.toJson(mMAllOrders);
        if(lang.equals("en")){
            editor.putString(KEY_ENGLISH_MAllOrders, json);
        }else {
            editor.putString(KEY_ARABIC_MAllOrders, json);
        }

        editor.commit();
    }
    public void setNewMOrders(MAllOrders MNewOrders, String lang) {
        Gson gson = new Gson();
        String json = gson.toJson(MNewOrders);
        if(lang.equals("en")){
            editor.putString(KEY_ENGLISH_MNewOrders, json);
        }else {
            editor.putString(KEY_ARABIC_MNewOrders, json);
        }

        editor.commit();
    }
    public void setEndedOrders(MAllOrders mEndedOrders, String lang) {
        Gson gson = new Gson();
        String json = gson.toJson(mEndedOrders);
        if(lang.equals("en")){
            editor.putString(KEY_ENGLISH_MEndedOrders, json);
        }else {
            editor.putString(KEY_ARABIC_MEndedOrders, json);
        }

        editor.commit();
    }
    public void setStoreOrders(Store mStoreOrders, String lang) {
        Gson gson = new Gson();
        String json = gson.toJson(mStoreOrders);
        if(lang.equals("en")){
            editor.putString(KEY_ENGLISH_MStoreOrders, json);
        }else {
            editor.putString(KEY_ARABIC_MStoreOrders, json);
        }

        editor.commit();
    }


    public ArrayList<Service> getServices(String lang) {
        String json;
        if(lang.equals("en")){
            json = pref.getString(KEY_ENGLISH_SERVICES, "");
        }else{
            json = pref.getString(KEY_ARABIC_SERVICES, "");
        }

        Gson gson = new Gson();
        Type type = new TypeToken<List<Service>>(){}.getType();
        ArrayList<Service> services = gson.fromJson(json, type);
        return  services;
    }
    public SAllOrders getSAllOrders(String lang) {
        String json;
        if(lang.equals("en")){
            json = pref.getString(KEY_ENGLISH_SAllOrders, "");
        }else{
            json = pref.getString(KEY_ARABIC_SAllOrders, "");
        }

        Gson gson = new Gson();
        Type type = new TypeToken<SAllOrders>(){}.getType();
        SAllOrders mSAllOrderses = gson.fromJson(json, type);
        return  mSAllOrderses;
    }
    public MAllOrders getMAllOrders(String lang) {
        String json;
        if(lang.equals("en")){
            json = pref.getString(KEY_ENGLISH_MAllOrders, "");
        }else{
            json = pref.getString(KEY_ARABIC_MAllOrders, "");
        }

        Gson gson = new Gson();
        Type type = new TypeToken<MAllOrders>(){}.getType();
        MAllOrders mMAllOrders = gson.fromJson(json, type);
        return  mMAllOrders;
    }
    public MAllOrders getMNewOrders(String lang) {
        String json;
        if(lang.equals("en")){
            json = pref.getString(KEY_ENGLISH_MNewOrders, "");
        }else{
            json = pref.getString(KEY_ARABIC_MNewOrders, "");
        }

        Gson gson = new Gson();
        Type type = new TypeToken<MAllOrders>(){}.getType();
        MAllOrders mNewOrders = gson.fromJson(json, type);
        return  mNewOrders;
    }
    public MAllOrders getEndedOrders(String lang) {
        String json;
        if(lang.equals("en")){
            json = pref.getString(KEY_ENGLISH_MEndedOrders, "");
        }else{
            json = pref.getString(KEY_ARABIC_MEndedOrders, "");
        }

        Gson gson = new Gson();
        Type type = new TypeToken<MAllOrders>(){}.getType();
        MAllOrders mEndedOrders = gson.fromJson(json, type);
        return  mEndedOrders;
    }
    public Store getStoreOrders(String lang) {
        String json;
        if(lang.equals("en")){
            json = pref.getString(KEY_ENGLISH_MStoreOrders, "");
        }else{
            json = pref.getString(KEY_ARABIC_MStoreOrders, "");
        }

        Gson gson = new Gson();
        Type type = new TypeToken<Store>(){}.getType();
        Store mStoreOrders = gson.fromJson(json, type);
        return  mStoreOrders;
    }

    public void setMyOrders(AllOrders orders) {
        Gson gson = new Gson();
        String json = gson.toJson(orders);
        editor.putString(KEY_MY_ORDERS, json);
        editor.commit();
    }

    public AllOrders getMyOrders() {
        String json = pref.getString(KEY_MY_ORDERS, "");
        Gson gson = new Gson();
        AllOrders orders = gson.fromJson(json, AllOrders.class);
        return  orders;
    }

    public void setMyProfile(JsonObject object) {
        Gson gson = new Gson();
        String json = gson.toJson(object);
        editor.putString(KEY_PROFILE_DATA, json);
        editor.commit();
    }

    public JsonObject getMyProfile() {
        String json = pref.getString(KEY_PROFILE_DATA, "");
        Gson gson = new Gson();
        JsonObject object = gson.fromJson(json, JsonObject.class);
        return  object;
    }

    public void setMyEvaluations(AllEvalutions evaluations) {
        Gson gson = new Gson();
        String json = gson.toJson(evaluations);
        editor.putString(KEY_MY_EVALUATIONS, json);
        editor.commit();
    }

    public AllEvalutions getMyEvaluations() {
        String json = pref.getString(KEY_MY_EVALUATIONS, "");
        Gson gson = new Gson();
        AllEvalutions evalutions = gson.fromJson(json, AllEvalutions.class);
        return  evalutions;
    }
    public void setTickets(AllTickets tickets) {
        Gson gson = new Gson();
        String json = gson.toJson(tickets);
        editor.putString(KEY_TICKETS, json);
        editor.commit();
    }

    public AllTickets getTickets() {
        String json = pref.getString(KEY_TICKETS, "");
        Gson gson = new Gson();
        AllTickets tickets = gson.fromJson(json, AllTickets.class);
        return  tickets;
    }






}
