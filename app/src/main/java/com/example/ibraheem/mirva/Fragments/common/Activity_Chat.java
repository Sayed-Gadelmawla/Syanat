package com.example.ibraheem.mirva.Fragments.common;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.example.ibraheem.mirva.Activities.MyNotificationActivity;
import com.example.ibraheem.mirva.Adapter.MessageAdpter;
import com.example.ibraheem.mirva.Const.FragConst;
import com.example.ibraheem.mirva.CustomViews.FWTextView;
import com.example.ibraheem.mirva.CustomViews.JFEditText;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.Interfaces.Communicator;
import com.example.ibraheem.mirva.Model.Response.TicketsResonse.Chat;
import com.example.ibraheem.mirva.Model.Response.TicketsResonse.Message;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.UitilsFun;
import com.example.ibraheem.mirva.util.D;
import com.example.ibraheem.mirva.util.L;
import com.example.ibraheem.mirva.util.V;
import com.example.ibraheem.mirva.util.helperFun;
import com.google.gson.JsonObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ibraheem on 30/07/2017.
 */

public class Activity_Chat extends AppCompatActivity {


    final static int RESULT_LOAD_IMAGE = 1;
    public final String TAG = Frag_Chat.class.getSimpleName();
    JFEditText text_chat_body;
    LinearLayout layout_icons;
    FWTextView text_message_icon, text_cam_icon, text_send_icon;
    ArrayList<com.example.ibraheem.mirva.Model.Response.TicketsResonse.Message> messageArrayList;
    MessageAdpter messageAdpter;
    RecyclerView recyclerView;
    Communicator comm;
    User user;
    String content;
    ApiInterface apiInterface;
    V vd;
    ArrayList<Message> messages;
    D d;
    MultipartBody.Part requestImage;
    Toolbar toolbar_view_order;
    JFTextView c_title_toolbar, number_of_notifications;
    FWTextView frag_back;
    int TICKET_ID;
    final static int REQUEST_CODE_PICKER = 101;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        d = new D(this);

        findViewById(R.id.frag_notification).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), MyNotificationActivity.class));

            }
        });
        toolbar_view_order = (Toolbar) findViewById(R.id.toolbar_view_order);
        c_title_toolbar = (JFTextView) toolbar_view_order.findViewById(R.id.c_title_toolbar);
        number_of_notifications = (JFTextView) toolbar_view_order.findViewById(R.id.number_of_notifications);

        c_title_toolbar.setText(getString(R.string.chat));


        frag_back = (FWTextView) toolbar_view_order.findViewById(R.id.frag_back);
        frag_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Intent intent = getIntent();
        if (intent != null && intent.hasExtra(FragConst.TICKET_ID)) {

            TICKET_ID = intent.getIntExtra(FragConst.TICKET_ID, -1);
            Log.d(TAG, "onCreate: TICKET_ID " + TICKET_ID);

        } else {
            finish();
        }

        user = helperFun.getUser(this);
        apiInterface = ApiUtils.getAPIService(this);

        vd = new V(this);

        text_chat_body = (JFEditText) findViewById(R.id.text_chat_body);
        text_message_icon = (FWTextView) findViewById(R.id.text_message_icon);

        text_cam_icon = (FWTextView) findViewById(R.id.text_cam_icon);
        text_send_icon = (FWTextView) findViewById(R.id.text_send_icon);
        text_send_icon.setVisibility(View.GONE);
        layout_icons = (LinearLayout) findViewById(R.id.layout_icons);
        recyclerView = (RecyclerView) findViewById(R.id.message_recycler_view);
        messageArrayList = new ArrayList<>();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayout.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        messageAdpter = new MessageAdpter(this, messageArrayList, recyclerView);
        recyclerView.setAdapter(messageAdpter);


        text_message_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent i = new Intent (Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                startActivityForResult (i, RESULT_LOAD_IMAGE);
                ImagePicker.create(Activity_Chat.this)
                        .returnAfterFirst(true) // set whether pick or camera action should return immediate result or not. For pick image only work on single mode
                        .imageTitle(getString(R.string.choose_image)) // image selection title
                        .single() // single mode
                        .showCamera(true) // show camera or not (true by default)
                        .theme(R.style.AppTheme) // must inherit ef_BaseTheme. please refer to sample
                        .start(REQUEST_CODE_PICKER); // start image picker activity with request code

            }
        });

        /**
         *
         * add action when user add some text in message box ..
         */

        text_chat_body.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!text_chat_body.getText().toString().trim().isEmpty()) {

                    text_message_icon.setVisibility(View.GONE);
                    text_cam_icon.setVisibility(View.GONE);

                    text_send_icon.setVisibility(View.VISIBLE);


                } else {

                    text_message_icon.setVisibility(View.VISIBLE);
                    text_cam_icon.setVisibility(View.VISIBLE);
                    text_send_icon.setVisibility(View.GONE);
                }

            }
        });


        vd.validateTextArea(R.id.text_chat_body);
/**
 *
 * action to send message with text type  firstly send it diractly
 */
        text_send_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (vd.isValid() && UitilsFun.isNetworkAvailable(Activity_Chat.this)) {

                    com.example.ibraheem.mirva.Model.Response.TicketsResonse.Message message = new com.example.ibraheem.mirva.Model.Response.TicketsResonse.Message();
                    message.setContent(text_chat_body.getText().toString());
                    message.setDate(getString(R.string.now));
                    message.setIsUserSend("1");
                    message.setImage(user.getImageToServer());

                    messageAdpter.addMessage(message);
                    text_message_icon.setVisibility(View.VISIBLE);
                    text_cam_icon.setVisibility(View.VISIBLE);
                    text_send_icon.setVisibility(View.GONE);
                    text_chat_body.setText("");
                    recyclerView.scrollToPosition(messageAdpter.getItemCount() - 1);

                    sendMessage(text_chat_body.getText().toString());


                }

            }
        });

        getData();
    }


    /**
     * get all messages to user
     */
    void getData() {

        //d.ShowProgress(getString(R.string.loading));
        mProgressBar.setVisibility(View.VISIBLE);
        apiInterface = ApiUtils.getAPIService(this);
        apiInterface.getSpecificTicketData(user.getApiToken(), TICKET_ID).enqueue(new Callback<Chat>() {
            @Override
            public void onResponse(Call<Chat> call, Response<Chat> response) {
                if (response.isSuccessful()) {
                    Chat body = response.body();
                    messages = body.getData();
                    messageAdpter.loadData(messages);
                    Log.d(TAG, "onResponse: " + TICKET_ID);


                }

                //d.hide();
                mProgressBar.setVisibility(View.INVISIBLE);

            }

            @Override
            public void onFailure(Call<Chat> call, Throwable t) {
                //d.hide();
                mProgressBar.setVisibility(View.INVISIBLE);
                d.ShowErrorInternetConnection("");

            }
        });


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);

            com.example.ibraheem.mirva.Model.Response.TicketsResonse.Message message = new com.example.ibraheem.mirva.Model.Response.TicketsResonse.Message();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), Uri.parse(picturePath));
                message.setTempImage(bitmap);
            } catch (Exception e) {
                //handle exception
            }
            message.setUrl(null);
            message.setDate(new Date().toString());
            message.setImage(user.getImageToServer());
            message.setIsUserSend("1");

            messageAdpter.addMessage(message);
            text_message_icon.setVisibility(View.VISIBLE);
            text_cam_icon.setVisibility(View.VISIBLE);
            text_send_icon.setVisibility(View.GONE);
            text_chat_body.setText("");

            recyclerView.scrollToPosition(messageAdpter.getItemCount() - 1);

            sendImageMessage(picturePath);


        } else if (requestCode == REQUEST_CODE_PICKER && resultCode == RESULT_OK && data != null) {
            ArrayList<Image> images = (ArrayList<Image>) ImagePicker.getImages(data);
            String picturePath = images.get(0).getPath();

            com.example.ibraheem.mirva.Model.Response.TicketsResonse.Message message = new com.example.ibraheem.mirva.Model.Response.TicketsResonse.Message();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), Uri.parse(picturePath));
                message.setTempImage(bitmap);
            } catch (Exception e) {
                //handle exception
            }
            message.setUrl(null);
            message.setDate(new Date().toString());
            message.setImage(user.getImageToServer());
            message.setIsUserSend("1");

            messageAdpter.addMessage(message);
            text_message_icon.setVisibility(View.VISIBLE);
            text_cam_icon.setVisibility(View.VISIBLE);
            text_send_icon.setVisibility(View.GONE);
            text_chat_body.setText("");

            recyclerView.scrollToPosition(messageAdpter.getItemCount() - 1);

            sendImageMessage(picturePath);
        }
    }

    /**
     * send message has text type
     *
     * @param content
     */
    void sendMessage(final String content) {


        apiInterface = ApiUtils.getAPIService(this);
        apiInterface.sendMessage(user.getApiToken(), TICKET_ID, content).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                L.d(TAG, "ticket_id" + TICKET_ID);
                if (response.isSuccessful()) {

                    Log.d("chat", "onResponse: body :" + response.body());

                } else {

                    Log.d("chat", "onResponse: errorBody :" + response.errorBody().toString());

                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d("chat", "onResponse: isExecuted " + call.isExecuted());

            }
        });
    }

    /**
     * send message woth image type
     *
     * @param imagePath
     */
    void sendImageMessage(final String imagePath) {
        mProgressBar.setVisibility(View.VISIBLE);
        //d.ShowProgress(getString(R.string.sending));

        if (imagePath == null || imagePath.isEmpty()) {
            return;
        }
        File file = new File(imagePath);
        if (file != null) {
            RequestBody requestFile =
                    RequestBody.create(MediaType.parse("multipart/form-data"), file);
            requestImage = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
        }

        RequestBody ticket_idRequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), "" + TICKET_ID);

        apiInterface = ApiUtils.getAPIService(this);
        apiInterface.sendImageMessage(user.getApiToken(), ticket_idRequestBody, requestImage).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                L.d(TAG, "ticket_id" + TICKET_ID);
                if (response.isSuccessful()) {


                } else {

                    Log.d("chat", "onResponse: errorBody :" + response.errorBody());

                }
                //d.hide();
                mProgressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                //d.hide();
                mProgressBar.setVisibility(View.VISIBLE);
                d.ShowErrorInternetConnection("");
            }
        });
    }

}
