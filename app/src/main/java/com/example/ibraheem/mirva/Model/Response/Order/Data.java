package com.example.ibraheem.mirva.Model.Response.Order;

/**
 * Created by ibraheem on 01/07/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Data {

    @SerializedName("status")
    @Expose
    private OrderStatus requestStatus;
    @SerializedName("data")
    @Expose
    private ArrayList<Order> requestData = null;

    public OrderStatus getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(OrderStatus requestStatus) {
        this.requestStatus = requestStatus;
    }

    public ArrayList<Order> getRequestData() {
        return requestData;
    }

    public void setRequestData(ArrayList<Order> requestData) {
        this.requestData = requestData;
    }

}