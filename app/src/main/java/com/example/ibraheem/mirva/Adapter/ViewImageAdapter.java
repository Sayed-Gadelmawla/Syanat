package com.example.ibraheem.mirva.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.jsibbold.zoomage.ZoomageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by ibraheem on 02/07/2017.
 */

/**
 * Created by ibraheem on 22/06/2017.
 */

public class ViewImageAdapter extends RecyclerView.Adapter<com.example.ibraheem.mirva.Adapter.ViewImageAdapter.myHolder> {

    public final String TAG = com.example.ibraheem.mirva.Adapter.ImageAdapter.class.getSimpleName ();
    Context context;
    ArrayList<String> imagesPath;
    Dialog dialog;

    ImageView  exist;
    ZoomageView viewFullImage;

    public ViewImageAdapter(Context context, ArrayList<String> imagesPath) {

        this.context = context;
        this.imagesPath = imagesPath;
        dialog = new android.app.Dialog (context);

        dialog.requestWindowFeature (Window.FEATURE_NO_TITLE); //before
        dialog.getWindow ().setBackgroundDrawable (new ColorDrawable (android.graphics.Color.TRANSPARENT));
        dialog.setContentView (R.layout.view_full_screen_order);
        viewFullImage = (ZoomageView) dialog.findViewById (R.id.viewFullImage);
        exist = (ImageView) dialog.findViewById (R.id.exist);
        exist.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                dialog.dismiss ();
            }
        });


    }

    @Override
    public com.example.ibraheem.mirva.Adapter.ViewImageAdapter.myHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new com.example.ibraheem.mirva.Adapter.ViewImageAdapter.myHolder (LayoutInflater.from (parent.getContext ()).inflate (R.layout.item_add_image, parent, false));
    }

    String image;

    @Override
    public void onBindViewHolder(com.example.ibraheem.mirva.Adapter.ViewImageAdapter.myHolder holder, final int position) {

        image = ApiUtils.BASE_URL + imagesPath.get (position);

        // holder.added_image.setImageBitmap (BitmapFactory.decodeFile (path));

        Log.d (TAG, "onBindViewHolder: " + image);
        Picasso.with (context)
                .load (image)
                .placeholder (R.drawable.avater_image)
                .error (R.drawable.avater_image)
                .into (holder.added_image);

        holder.itemView.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {

                DisplayMetrics dm = context.getResources().getDisplayMetrics();

                Picasso.with (context)
                        .load (ApiUtils.BASE_URL + imagesPath.get (position))
                        .placeholder (R.drawable.avater_image)
                        .resize(dm.widthPixels,dm.heightPixels-100)
                        .error (R.drawable.avater_image)
                        .into (viewFullImage);
                dialog.show ();
            }
        });


    }

    @Override
    public int getItemCount() {
        return imagesPath.size ();
    }

    public void loadData(ArrayList<String> list) {
        imagesPath.addAll (list);
        notifyDataSetChanged ();

    }

    class myHolder extends RecyclerView.ViewHolder {
        ImageView added_image;


        public myHolder(View itemView) {
            super (itemView);

            itemView.findViewById (R.id.deleteVideo).setVisibility (View.GONE);

            added_image = (ImageView) itemView.findViewById (R.id.added_image);

        }

    }

}
