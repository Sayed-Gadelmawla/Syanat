package com.example.ibraheem.mirva.CustomViews.Drawer;

/**
 * Created by ibraheem on 01/06/2017.
 */


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.R;

public class DrawerItemCustomAdapter extends ArrayAdapter<DataModel> {

    Context mContext;
    int layoutResourceId;
    DataModel data[] = null;
    int mSelectedItem;

    public DrawerItemCustomAdapter(Context mContext, int layoutResourceId, DataModel[] data) {

        super (mContext, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.data = data;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View listItem = convertView;

        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater ();
        listItem = inflater.inflate (layoutResourceId, parent, false);

        JFTextView textViewName = (JFTextView) listItem.findViewById (R.id.item_tv_drawer);

        if (position == mSelectedItem) {

            textViewName.setBackgroundColor (getContext ().getResources ().getColor (R.color.nd_background));
        } /*else {

            textViewName.setBackgroundColor (getContext ().getResources ().getColor (R.color.colorPrimary));

        }*/

        listItem.setOnTouchListener (new View.OnTouchListener () {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mSelectedItem = position
                ;

                notifyDataSetChanged ();
                return false;
            }
        });
        DataModel folder = data[position];

        textViewName.setText (folder.name);

        return listItem;
    }
}