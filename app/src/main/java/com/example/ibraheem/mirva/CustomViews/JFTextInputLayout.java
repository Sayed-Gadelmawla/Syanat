package com.example.ibraheem.mirva.CustomViews;

import android.content.Context;
import android.graphics.Typeface;
import android.support.design.widget.TextInputLayout;
import android.util.AttributeSet;

/**
 * Created by ibraheem on 24/05/2017.
 */

public class JFTextInputLayout extends TextInputLayout {
    public JFTextInputLayout(Context context) {
        super (context);
        init ();
    }

    public JFTextInputLayout(Context context, AttributeSet attrs) {
        super (context, attrs);
        init ();
    }

    public JFTextInputLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super (context, attrs, defStyleAttr);
        init ();
    }

    void init() {


        setTypeface (Typeface.createFromAsset (getContext ().getAssets (), "fonts/jf_flat_regular.ttf"));


    }
}
