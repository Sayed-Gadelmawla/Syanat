package com.example.ibraheem.mirva.Model;

import android.graphics.Bitmap;

import com.example.ibraheem.mirva.Enum.NOTIFICATION_STATUS;

/**
 * Created by ibraheem on 18/05/2017.
 */

public class Notification {
    private Bitmap icon;
    private String type;
    private NOTIFICATION_STATUS status;
    private String descTime;

    public Notification(Bitmap icon, String type, NOTIFICATION_STATUS status, String descTime) {
        this.icon = icon;
        this.type = type;
        this.status = status;
        this.descTime = descTime;
    }

    public Bitmap getIcon() {
        return icon;
    }

    public void setIcon(Bitmap icon) {
        this.icon = icon;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public NOTIFICATION_STATUS getStatus() {
        return status;
    }

    public void setStatus(NOTIFICATION_STATUS status) {
        this.status = status;
    }

    public String getDescTime() {
        return descTime;
    }

    public void setDescTime(String descTime) {
        this.descTime = descTime;
    }
}
