package com.example.ibraheem.mirva.Interfaces;

import android.support.v4.app.Fragment;

/**
 * Created by ibraheem on 03/06/2017.
 */

public interface Communicator {

    void setTitleToolbar(int title);

    void setTitleToolbar(String title);
    void addFragment(Fragment fragment);

    void selectVisibleToolbar(int postion);

    void actionToselectedPostion(int postion);

    //to prevent change toolbar in profile
    void setToolbatLock(boolean locke);

    void updateUserData();

    void TopMessage(Fragment fragment);
}
