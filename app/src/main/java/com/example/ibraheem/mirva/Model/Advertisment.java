package com.example.ibraheem.mirva.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Eng.Mhmod on 8/28/2017.
 */

public class Advertisment {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("image_ar")
    @Expose
    private String image_ar;
    @SerializedName("image_en")
    @Expose
    private String image_en;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("type_id")
    @Expose
    private Integer type_id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImage_ar() {
        return image_ar;
    }

    public void setImage_ar(String image_ar) {
        this.image_ar = image_ar;
    }

    public String getImage_en() {
        return image_en;
    }

    public void setImage_en(String image_en) {
        this.image_en = image_en;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getType_id() {
        return type_id;
    }

    public void setType_id(Integer type_id) {
        this.type_id = type_id;
    }

    @Override
    public String toString() {
        return "Advertisment{" +
                "id=" + id +
                ", image_ar='" + image_ar + '\'' +
                ", image_en='" + image_en + '\'' +
                ", url='" + url + '\'' +
                ", type_id=" + type_id +
                '}';
    }
}
