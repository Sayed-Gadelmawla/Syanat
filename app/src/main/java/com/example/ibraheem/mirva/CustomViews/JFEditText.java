package com.example.ibraheem.mirva.CustomViews;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;

/**
 * Created by ibraheem on 5/10/2017.
 */

public class JFEditText extends android.support.v7.widget.AppCompatEditText {
    public JFEditText(Context context) {
        super (context);
        init ();
    }

    public JFEditText(Context context, AttributeSet attrs) {
        super (context, attrs);
        init ();
    }

    public JFEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super (context, attrs, defStyleAttr);
        init ();

    }

    private void init() {

        //    setBackground(getResources().getDrawable(R.drawable.border_tv_ev));
        //  setTextColor(getResources().getColor(R.color.colorAccent));
        setTypeface (Farsi.GetFarsiFont (getContext ()));

        setTextDirection (TEXT_DIRECTION_RTL);
       // setGravity (Gravity.RIGHT);
        setSelection (0);


    }


}
