package com.example.ibraheem.mirva.Model;

import com.example.ibraheem.mirva.Model.Response.Order.Technical.Requirement;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by ibraheem on 03/07/2017.
 */

public class ManagerSendOrder {
    @SerializedName("request_id")
    @Expose
    private String requestId;
    @SerializedName("technician_id")
    @Expose
    private String technicianId;
    @SerializedName("notes")
    @Expose
    private String notes;
    @SerializedName("requirements")
    @Expose
    private ArrayList<Requirement> requirements;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getTechnicianId() {
        return technicianId;
    }

    public void setTechnicianId(String technicianId) {
        this.technicianId = technicianId;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public ArrayList<Requirement> getRequirements() {
        return requirements;
    }

    public void setRequirements(ArrayList<Requirement> requirements) {
        this.requirements = requirements;
    }

    @Override
    public String toString() {
        return "ManagerSendOrder{" +
                "requestId='" + requestId + '\'' +
                ", technicianId='" + technicianId + '\'' +
                ", notes='" + notes + '\'' +
                ", requirements=" + requirements +
                '}';
    }
}