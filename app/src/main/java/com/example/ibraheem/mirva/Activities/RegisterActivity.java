package com.example.ibraheem.mirva.Activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import com.example.ibraheem.mirva.CustomViews.FWTextView;
import com.example.ibraheem.mirva.CustomViews.JFButton;
import com.example.ibraheem.mirva.CustomViews.JFEditText;
import com.example.ibraheem.mirva.Model.Response.ErrorResponse.ErrorUtils;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.Model.Response.helperRes;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.UitilsFun;
import com.example.ibraheem.mirva.util.D;
import com.example.ibraheem.mirva.util.SessionManager;
import com.example.ibraheem.mirva.util.SharedPrefs;
import com.example.ibraheem.mirva.util.V;
import com.example.ibraheem.mirva.util.helperFun;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.onesignal.OSSubscriptionObserver;
import com.onesignal.OSSubscriptionStateChanges;
import com.onesignal.OneSignal;

import org.json.JSONException;

import java.io.File;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener, OSSubscriptionObserver {

    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 99;
    final static int RESULT_LOAD_IMAGE = 1;
    public final String TAG = RegisterActivity.class.getSimpleName();
    public Intent intent;

    FWTextView tv_back, btn_add_image;
    de.hdodenhof.circleimageview.CircleImageView user_image;
    JFButton btn_register;
    String language;
    String picturePath;
    JFEditText user_name, user_email, user_password, user_password_again, user_phone_number;
    SweetAlertDialog sweetAlertDialog;
    V vd;
    User user;
    MultipartBody.Part body;
    D dilog;
    String token;
    private ApiInterface mAPIService;
    private String[] permissions = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE,
    };
    //private JFEditText mPolicyEditText;
    //private CheckBox mPolicyCheckBox;
    private SessionManager mSessionManager;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        mContext = this;
        mSessionManager = new SessionManager(mContext);
        //new FillPolicyAsyncTask().execute();

        OneSignal.setSubscription(true);
        language = helperFun.getLanguage(this);
        mAPIService = ApiUtils.getAPIService(getBaseContext());
        SharedPrefs.revokeShared(getApplicationContext());
        init_view();
    }

    /**
     * get refrence to view from xml
     */

    private void init_view() {
        btn_register = (JFButton) findViewById(R.id.btn_register);
        tv_back = (FWTextView) findViewById(R.id.tv_back);
        user_image = (de.hdodenhof.circleimageview.CircleImageView) findViewById(R.id.user_image);
        btn_add_image = (FWTextView) findViewById(R.id.btn_add_image);
        btn_register.setOnClickListener(this);
        btn_add_image.setOnClickListener(this);
        tv_back.setOnClickListener(this);

        user_name = (JFEditText) findViewById(R.id.user_name);
        user_email = (JFEditText) findViewById(R.id.user_email);
        user_password = (JFEditText) findViewById(R.id.user_password);
        user_password_again = (JFEditText) findViewById(R.id.user_password_again);
        user_phone_number = (JFEditText) findViewById(R.id.user_phone_number);

        //mPolicyEditText = (JFEditText) findViewById(R.id.policy_et);
       //mPolicyCheckBox = (CheckBox) findViewById(R.id.policy_check_box);
        //mPolicyEditText.setText(mSessionManager.getPolicy());
        //mPolicyEditText.setEnabled(false);

        vd = new V(RegisterActivity.this);
        vd.ValidateUserName(R.id.user_name);
        vd.validateEmail(R.id.user_email);
        vd.validatePassword(R.id.user_password);
        vd.validateConfirmPassword(R.id.user_password, R.id.user_password_again);
        vd.validatePhone(R.id.user_phone_number);



//        ApiInterface apiInterface = ApiUtils.getAPIService(getBaseContext());
//        apiInterface.getPolicy(language).enqueue(new Callback<JsonObject>() {
//            @Override
//            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//                if (response.isSuccessful()) {
//                    JsonObject object = response.body();
//                    mPolicyEditText.setText(object.get("policy").toString());
//                }
//            }
//
//            @Override
//            public void onFailure(Call<JsonObject> call, Throwable t) {
//            }
//        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);

            user_image.setImageBitmap(BitmapFactory.decodeFile(picturePath));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /**
             *
             * click on add iamge
             */
            case R.id.btn_add_image:
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED) {

                        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, RESULT_LOAD_IMAGE);

                    } else {
                        //Request Location Permission
                        checkLocationPermission();
                    }
                } else {
                    Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, RESULT_LOAD_IMAGE);

                }
                break;/**
             *
             * click on back on  toolbar
             */
            case R.id.tv_back:
                finish();

                break;
            case R.id.btn_register:

                if (vd.isValid() && UitilsFun.isNetworkAvailable(this)) {
                    creatUser();
                }


        }
    }

    private void creatUser() {
        dilog = new D(this);
        dilog.ShowProgress(getString(R.string.processing_reg));

        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                if (token == null) {
                    token = userId;
                }
            }
        });

        if (picturePath != null) {
            File file = new File(picturePath);
            if (file != null) {
                RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                body = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
            }


        }
        RequestBody userName = helperFun.getRequestBody(user_name);
        RequestBody useEmail = helperFun.getRequestBody(user_email);
        //RequestBody userPhoneNumber = helperFun.getRequestBody(user_phone_number);
        RequestBody userPassword = helperFun.getRequestBody(user_password);
        RequestBody userPasswprdAgain = helperFun.getRequestBody(user_password_again);

        mAPIService.createUser(language, token, userName, useEmail
                , userPassword, userPasswprdAgain, body).enqueue(new Callback<User>() {

            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                dilog.hide();
                if (response.isSuccessful()) {

                    Log.d(TAG, "onResponse: " + response.body().toString().toString());

                    user = response.body();

                    Gson gson = new Gson ();

                    SharedPrefs.save (getApplicationContext (), SharedPrefs.USER, gson.toJson (user));

                    switch (user.getType ()) {
                        case "Supervisor":
                            intent = new Intent (getApplicationContext (), MainPageManager.class);

                            break;
                        case "Technician":
                            intent = new Intent (getApplicationContext (), MainPageTechnical.class);

                            break;
                        case "Client":
                            intent = new Intent (getApplicationContext (), MainPageClient.class);

                            break;

                    }

                    intent.setFlags (Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    overridePendingTransition (R.anim.fadein, R.anim.fadeout);
                    startActivity (intent);
                    finish ();

                } else {
                    ErrorUtils errorMessage = helperRes.getErrorMessage(response.errorBody());

                    //  user_name.setError (errorMessage.getError ().getEmail ());
                    user_email.setError(getErrorMessage(errorMessage.getError().getEmail()));
                    user_password.setError(getErrorMessage(errorMessage.getError().getPassword()));
                    //user_phone_number.setError(getErrorMessage(errorMessage.getError().getMobile()));
                }


            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                dilog.hide();
                Log.d(TAG, "getMessage: " + t.getMessage());
                Log.d(TAG, "getCause: " + t.getCause());

                dilog.ShowErrorInternetConnection("");

            }
        });
    }

    private String getErrorMessage(ArrayList<String> type) {
        return type == null ? null : type.get(0);
    }

    private void checkLocationPermission() {
        final Activity activity = this;
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(activity,
                                        permissions,
                                        REQUEST_ID_MULTIPLE_PERMISSIONS);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        permissions,
                        REQUEST_ID_MULTIPLE_PERMISSIONS);
            }
        }
    }


    @Override
    public void onOSSubscriptionChanged(OSSubscriptionStateChanges stateChanges) {
        if (!stateChanges.getFrom().getSubscribed() &&
                stateChanges.getTo().getSubscribed()) {

          /*  new AlertDialog.Builder (this)
                    .setMessage ("You've successfully subscribed to push notifications!")
                    .show ();*/
        }

        try {
            Log.i("Debug", "onOSPermissionChanged: " + stateChanges.toJSONObject().getString("userId"));
            token = stateChanges.toJSONObject().getString("userId");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class FillPolicyAsyncTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }

}
