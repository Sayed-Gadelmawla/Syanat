package com.example.ibraheem.mirva.Fragments.Client;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;

import com.example.ibraheem.mirva.Const.FragConst;
import com.example.ibraheem.mirva.CustomViews.JFButton;
import com.example.ibraheem.mirva.CustomViews.JFEditText;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.Interfaces.Communicator;
import com.example.ibraheem.mirva.Model.Response.EevaluteOrderData.EvalService;
import com.example.ibraheem.mirva.Model.Response.EevaluteOrderData.Service;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.Model.Response.helperRes;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.UitilsFun;
import com.example.ibraheem.mirva.util.D;
import com.example.ibraheem.mirva.util.V;
import com.example.ibraheem.mirva.util.helperFun;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ibraheem on 27/06/2017.
 */

public class Frag_EvaluteOrder extends Fragment {
    public final String TAG = "sendEvalutionOrder";
    User user;
    int request_id;
    V vd;
    D d;
    ImageView order_icon;
    JFTextView order_desc, Order_subCat, order_location, order_details;
    RatingBar rate1, rate2, rate3, rate4;
    JFEditText order_review;
    JFButton order_send_evalution;

    View evalution_done;
    private String lang;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        Bundle bundle = this.getArguments ();
        vd = new V (getActivity ());
        d = new D (getActivity ());

        if (bundle != null) {
            request_id = bundle.getInt (FragConst.REQUEST_ID, -1);
        }
        user = helperFun.getUser (getContext ());


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from (container.getContext ()).inflate (R.layout.fragment_evaluate_order, container, false);
        order_icon = (ImageView) view.findViewById (R.id.order_icon);
        order_desc = (JFTextView) view.findViewById (R.id.order_desc);
        Order_subCat = (JFTextView) view.findViewById (R.id.Order_subCat);
        order_location = (JFTextView) view.findViewById (R.id.order_location);
        evalution_done = view.findViewById (R.id.evalution_done);
        order_details = (JFTextView) view.findViewById (R.id.order_details);


        rate1 = (RatingBar) view.findViewById (R.id.rate1);
        rate2 = (RatingBar) view.findViewById (R.id.rate2);
        rate3 = (RatingBar) view.findViewById (R.id.rate3);
        rate4 = (RatingBar) view.findViewById (R.id.rate4);
        order_review = (JFEditText) view.findViewById (R.id.order_review);

        order_send_evalution = (JFButton) view.findViewById (R.id.order_send_evalution);

        vd.validateTextArea (R.id.order_review);

        order_send_evalution.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {

                if (vd.isValid () && UitilsFun.isNetworkAvailable (getContext ())) {
                    sendEvalutionOrder ();
                }

            }
        });

        comm = (Communicator) getActivity ();
        comm.setTitleToolbar (getString (R.string.evaluat_order));
        comm.selectVisibleToolbar (R.id.toolbar_clints);
        lang = helperFun.getLanguage(getActivity().getApplicationContext());
        getEvalutionData ();

        return view;


    }

    Communicator comm;

    private void getEvalutionData() {

        ApiInterface apiInterface = ApiUtils.getAPIService (getActivity ());
        apiInterface.getEvalutionServiceData (user.getApiToken (), request_id,lang).enqueue (new Callback<EvalService> () {
            @Override
            public void onResponse(Call<EvalService> call, Response<EvalService> response) {
                if (response.isSuccessful ()) {
                    EvalService evalService = response.body ();
                    Service serviceData = evalService.getService ();

                    Picasso.with (getActivity ())
                            .load (serviceData.getImage ())
                            .into (order_icon);

                    order_desc.setText (serviceData.getService ());
                    //   order_location.setText (getAddress (serviceData.getLatitude (), serviceData.getLongitude ()));

                    Order_subCat.setText (serviceData.getSubcategory ());


                }
            }

            @Override
            public void onFailure(Call<EvalService> call, Throwable t) {
                d.ShowErrorInternetConnection ("");

            }
        });


    }

    public String getAddress(String Latitude, String Longitude) {
        double lat = Double.parseDouble (Latitude);
        double lng = Double.parseDouble (Longitude);
        Geocoder geocoder = new Geocoder (getActivity (), Locale.getDefault ());
        try {
            List<Address> addresses = geocoder.getFromLocation (lat, lng, 1);
            Address obj = addresses.get (0);
            String add = obj.getAddressLine (0);
            add = add + "\n" + obj.getCountryName ();
            add = add + "\n" + obj.getCountryCode ();
            add = add + "\n" + obj.getAdminArea ();
            add = add + "\n" + obj.getPostalCode ();
            add = add + "\n" + obj.getSubAdminArea ();
            add = add + "\n" + obj.getLocality ();
            add = add + "\n" + obj.getSubThoroughfare ();

            // Log.v("IGA", "Address" + add);
            // Toast.makeText(this, "Address=>" + add,
            // Toast.LENGTH_SHORT).show();
            if (obj.getSubAdminArea () != null) {
                return obj.getSubAdminArea ();
            }


            // TennisAppActivity.showDialog(add);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace ();
            // Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return "Unkown";

    }

    private void sendEvalutionOrder() {
        user = helperFun.getUser (getContext ());
        d.ShowError (getString (R.string.sending));
        ApiInterface apiInterface = ApiUtils.getAPIService (getActivity ());
        apiInterface.sendEvalutionOrder (user.getApiToken (), request_id, order_review.getText ().toString ()
                , 1, rate1.getProgress (), 2, rate2.getProgress (), 3, rate3.getProgress (), 4, rate4.getProgress ()).enqueue (new Callback<JsonObject> () {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                d.hide ();
                if (response.isSuccessful ()) {
                    // evalution_done.setVisibility (View.VISIBLE);

                    //  comm.TopMessage (new EvalutionOrderDone ());

                } else {

                    helperRes.showMessageError (getActivity (), response.errorBody ());
                    Log.d (TAG, "onResponse:errorBody " + response.errorBody ().toString ());

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                d.ShowErrorInternetConnection ("");
            }
        });


    }


}
