package com.example.ibraheem.mirva.Fragments.Technical;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.example.ibraheem.mirva.Adapter.TechnicalRequestsAdapter;
import com.example.ibraheem.mirva.Interfaces.Communicator;
import com.example.ibraheem.mirva.Model.Response.Order.Technical.SAllOrders;
import com.example.ibraheem.mirva.Model.Response.Order.Technical.TOrder;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.util.D;
import com.example.ibraheem.mirva.util.SessionManager;
import com.example.ibraheem.mirva.util.helperFun;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class tech_Frag_UnderwayOrder extends Fragment {
    RecyclerView recyclerView;
    TechnicalRequestsAdapter mRequestsAdapter;
    ArrayList<TOrder> arrayList;
    D d;
    User user;
    private Context mContext;
    private ProgressBar mProgressBar;
    private SessionManager mSessionManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        d = new D (getActivity ());
        user = helperFun.getUser (getActivity ());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContext = getActivity();
        // Inflate the layout for this fragment
        arrayList = new ArrayList<> ();
        View view = inflater.inflate (R.layout.tech_fragment_my_requests, container, false);
        mRequestsAdapter = new TechnicalRequestsAdapter (getActivity (), arrayList);
        recyclerView = (RecyclerView) view.findViewById (R.id.tech_request_recyler_view);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        mSessionManager = new SessionManager(mContext);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager (getActivity ());
        linearLayoutManager.setOrientation (LinearLayout.VERTICAL);
        recyclerView.setLayoutManager (linearLayoutManager);

        recyclerView.setAdapter (mRequestsAdapter);
        // loadData ();
        Communicator comm = (Communicator) getActivity ();
        comm.selectVisibleToolbar (R.id.toolbar_main);

        new FillUnderwayOrdersAsyncTask().execute();
        return view;
    }

    private class FillUnderwayOrdersAsyncTask extends AsyncTask<Void, Void, Void> {
        String lang;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            lang = helperFun.getLanguage(getActivity().getApplicationContext());
        }

        @Override
        protected Void doInBackground(Void... params) {
            ApiInterface apiInterface = ApiUtils.getAPIService (getActivity ());
            apiInterface.getTechnicalUnderWayOrders (user.getApiToken ()).enqueue (new Callback<SAllOrders> () {
                @Override
                public void onResponse(Call<SAllOrders> call, Response<SAllOrders> response) {
                    if (response.isSuccessful ()) {
                        SAllOrders body = response.body ();
                        ArrayList<TOrder> sOrders = body.getData ();
                        mSessionManager.setSAllOrders(body,lang);
                        Log.d ("getUnderwayOrders", "onResponse: " + sOrders.toString ());
                        if (sOrders.size () != 0) {

                            mRequestsAdapter.LoadData (body);

                        }


                    } else {


                    }
                    mProgressBar.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onFailure(Call<SAllOrders> call, Throwable t) {

                    mProgressBar.setVisibility(View.INVISIBLE);

                    SAllOrders body =  mSessionManager.getSAllOrders(lang);
                    if(body!=null) {
                        ArrayList<TOrder> sOrders = body.getData();
                        if (sOrders.size() != 0) {

                            mRequestsAdapter.LoadData(body);

                        }
                    }

                    d.ShowErrorInternetConnection ("");
                }
            });
            return null;
        }
    }




}
