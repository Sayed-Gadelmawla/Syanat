package com.example.ibraheem.mirva.Model;

import android.graphics.Bitmap;

import com.example.ibraheem.mirva.Enum.STORE_STATUS;

/**
 * Created by ibraheem on 29/05/2017.
 */

public class StoreItem {
    Bitmap Icon;
    String Title, quantity;
    STORE_STATUS Status;

    public StoreItem(Bitmap icon, String title, String quantity, STORE_STATUS status) {
        Icon = icon;
        Title = title;
        this.quantity = quantity;
        Status = status;
    }

    public Bitmap getIcon() {
        return Icon;
    }

    public void setIcon(Bitmap icon) {
        Icon = icon;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public STORE_STATUS getStatus() {
        return Status;
    }

    public void setStatus(STORE_STATUS status) {
        Status = status;
    }
}
