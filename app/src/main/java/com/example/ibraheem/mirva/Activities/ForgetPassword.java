package com.example.ibraheem.mirva.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.example.ibraheem.mirva.CustomViews.JFButton;
import com.example.ibraheem.mirva.CustomViews.JFEditText;
import com.example.ibraheem.mirva.Model.Response.ErrorResponse.Error;
import com.example.ibraheem.mirva.Model.Response.ErrorResponse.ErrorUtils;
import com.example.ibraheem.mirva.Model.Response.helperRes;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.util.D;
import com.example.ibraheem.mirva.util.V;
import com.example.ibraheem.mirva.util.helperFun;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ibraheem on 21/06/2017.
 */

public class ForgetPassword extends AppCompatActivity {
    public final String TAG = ForgetPassword.class.getSimpleName ();
    String lang;
    ApiInterface apiInterface;
    JFEditText user_email;
    V vd;
    D d;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_forget_password);

        user_email = (JFEditText) findViewById (R.id.user_email);
        lang = helperFun.getLanguage (this);
        vd = new V (this);

        vd.validateEmail (R.id.user_email);

        JFButton btn_forget_password = (JFButton) findViewById (R.id.btn_forget);
        btn_forget_password.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                if (vd.isValid ()) {
                    forgetPassword ();
                }
            }
        });

    }

    /**
     * send request to forgget password
     */
    void forgetPassword() {

        d = new D (this);
        d.ShowProgress (getString (R.string.check));

        apiInterface = ApiUtils.getAPIService (this);

        apiInterface.forgetPassword (lang, user_email.getText ().toString ()).enqueue (new Callback<JsonObject> () {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                d.hide ();
                if (response.isSuccessful ()) {
                    helperRes.showMessageSuccess (ForgetPassword.this, response.body (), LoginActivity.class);
                } else {
                    ErrorUtils errorMessage = helperRes.getErrorMessage (response.errorBody ());
                    Log.d (TAG, "onResponse: "+errorMessage.getError ());
                    if (errorMessage != null)
                    {
                        Log.d (TAG, "onResponse: "+errorMessage.getError ().getEmail ().get (0));
                        d.ShowError (errorMessage.getError ().getEmail ().get (0));
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                d.hide ();
                d.ShowErrorInternetConnection ("");
            }
        });
    }
}
