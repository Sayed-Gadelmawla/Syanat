package com.example.ibraheem.mirva.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.ibraheem.mirva.Activities.AllSubscriptionsActivity;
import com.example.ibraheem.mirva.Activities.MainPageClient;
import com.example.ibraheem.mirva.Const.FragConst;
import com.example.ibraheem.mirva.Fragments.Client.ActivityNewOrder;
import com.example.ibraheem.mirva.Fragments.common.Frag_Activation;
import com.example.ibraheem.mirva.Model.Response.Service;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.util.helperFun;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by ibraheem on 5/11/2017.
 */

public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.MyViewHoder> {

    int row_index = 0;
    Service service;
    int selectedPostion = -1;
    private ArrayList<Service> mServiceList;
    private SparseBooleanArray selectedItems;
    private Context mContext;

    public ServiceAdapter(Context context, ArrayList<Service> list) {
        mServiceList = list;
        mContext = context;

    }

    @Override
    public MyViewHoder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        return new MyViewHoder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_service, parent, false));
    }

    @Override

    public void onBindViewHolder(final MyViewHoder holder, final int position) {
        service = mServiceList.get(position);
        holder.tv_desc.setText(service.getName());
        Picasso.with(mContext)
                .load(service.getImage())
                .into(holder.iv_icon);

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User user = helperFun.getUser ( mContext);
                if (user.getStatus () == 0) {
                    FragmentTransaction ft = ((MainPageClient) mContext).getSupportFragmentManager().beginTransaction();
                    Frag_Activation newFragment = Frag_Activation.newInstance(user.getEmail(),mServiceList.get(position).getId());
                    newFragment.show(ft , "Dialog");

                } else {
                    Intent intent = new Intent(mContext, ActivityNewOrder.class);
                    intent.putExtra(FragConst.SERVICE_ID, mServiceList.get(position).getId());
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    mContext.startActivity(intent);
                }
            }
        });

        if (position == selectedPostion) {
            holder.itemView.getBackground().setColorFilter(mContext.getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
            holder.iv_icon.setColorFilter(mContext.getResources().getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
            holder.tv_desc.setTextColor(mContext.getResources().getColor(R.color.colorWhite));
        } else {
            holder.itemView.getBackground().clearColorFilter();
            holder.iv_icon.clearColorFilter();
            holder.tv_desc.setTextColor(mContext.getResources().getColor(R.color.fontColorNormal));
        }
    }

    @Override
    public int getItemCount() {
        return mServiceList != null ? mServiceList.size() : null;
    }

    public void loadData(ArrayList<Service> list) {

        this.mServiceList = list;
        notifyDataSetChanged();
    }

    public class MyViewHoder extends RecyclerView.ViewHolder {

        public TextView tv_desc;
        public ImageView iv_icon;
        View prevView;
        private LinearLayout linearLayout;

        public MyViewHoder(final View itemView) {
            super(itemView);

            prevView = itemView;
            iv_icon = (ImageView) itemView.findViewById(R.id.service_icon);
            tv_desc = (TextView) itemView.findViewById(R.id.service_name);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.service_layout);


        }
    }
}
