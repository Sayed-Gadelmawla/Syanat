package com.example.ibraheem.mirva.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.ibraheem.mirva.Const.FragConst;
import com.example.ibraheem.mirva.CustomViews.JFButton;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.util.helperFun;

//import com.paypal.android.sdk.payments.PaymentActivity;
//import com.paypal.android.sdk.payments.PaymentConfirmation;
//import paytabs.project.PayTabActivity;

public class PayBYVisa extends AppCompatActivity {
    public static final int VISA_REQUEST_CODE = 455;
    String lang;
    User user;
    JFButton pay_visa;
    String SUBSCRIPTION_ID;
    String SUBSCRIPTION_PRICE, SUBSCRIPTION_NAME;
    JFTextView type_subcription, amount, user_account;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_pay_byvisa);
        lang = helperFun.getLanguage (this);
        user = helperFun.getUser (this);
        Intent intent = getIntent ();
        type_subcription = (JFTextView) findViewById (R.id.type_subcription);
        amount = (JFTextView) findViewById (R.id.amount);
        user_account = (JFTextView) findViewById (R.id.user_account);

        if (intent != null) {
            SUBSCRIPTION_ID = getIntent ().getStringExtra (FragConst.SUBSCRIPTION_ID);
            SUBSCRIPTION_PRICE = getIntent ().getStringExtra (FragConst.SUBSCRIPTION_PRICE);
            SUBSCRIPTION_NAME = getIntent ().getStringExtra (FragConst.SUBSCRIPTION_NAME);
        }

        type_subcription.setText (SUBSCRIPTION_NAME);
        amount.setText (SUBSCRIPTION_PRICE);
        user_account.setText (user.getName ());

        pay_visa = (JFButton) findViewById (R.id.pay_visa);
        pay_visa.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                PayByVisa (SUBSCRIPTION_PRICE, SUBSCRIPTION_NAME, SUBSCRIPTION_ID);
            }
        });
    }

    /**
     * payment by paytabs
     * @param price
     * @param subscription
     * @param id
     */
    void PayByVisa(String price, String subscription, String id) {

        /*Intent in = new Intent (PayBYVisa.this, PayTabActivity.class);
        in.putExtra ("pt_merchant_email", "info@adeksa.com"); //this a demo account for
        in.putExtra ("pt_secret_key", "hL1qwUK2OnQKo75q0oQK6p3Z0hq934alQbMm5woTTeEimKIn9y7gYafZsG8vLiQBtDXz9mt3B8lXT3k2CsiBDyA5eIBZ0rBu5z4e");//Add your Secret Key Here
        in.putExtra ("pt_transaction_title", "Mirva Subscription");
        in.putExtra ("pt_amount", price);
        in.putExtra ("pt_currency_code", "SAR"); //Use Standard 3 character ISO
        // in.putExtra ("pt_shared_prefs_name", "myapp_shared");
        in.putExtra ("pt_customer_email", user.getEmail ());
        in.putExtra ("pt_customer_phone_number", user.getMobile ());
        //in.putExtra ("pt_order_id", "1234567");
        in.putExtra ("pt_product_name", "Mirva Subscription " + subscription + "type account");
        in.putExtra ("pt_timeout_in_seconds", "3000"); //Optional
//Billing Address
      /*  in.putExtra ("pt_address_billing", "Flat 1,Building 123, Road 2345");
        in.putExtra ("pt_city_billing", "Juffair");
        in.putExtra ("pt_state_billing", "Manama");
        in.putExtra ("pt_country_billing", "Bahrain");
        in.putExtra ("pt_postal_code_billing", "00973"); //Put Country Phone code if Postal
        code not available '00973'*/
//Shipping Address
      /*  in.putExtra ("pt_address_shipping", "Flat 1,Building 123, Road 2345");
        in.putExtra ("pt_city_shipping", "Juffair");
        in.putExtra ("pt_state_shipping", "Manama");
        in.putExtra ("pt_country_shipping", "Bahrain");
        in.putExtra ("pt_postal_code_shipping", "00973"); //Put Country Phone code if Postal
*/

        //   startActivityForResult (in, VISA_REQUEST_CODE);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK && data != null) {
            //If the result is from paypal
            //If the result is OK i.e. user has not canceled the payment
            if (requestCode == VISA_REQUEST_CODE) {

                SharedPreferences shared_prefs = getSharedPreferences ("myapp_shared", Context.MODE_PRIVATE);
                String pt_response_code = shared_prefs.getString ("pt_response_code", "");
                String pt_transaction_id = shared_prefs.getString ("pt_transaction_id", "");
                Toast.makeText (this, "PayTabs Response Code : " + pt_response_code,
                        Toast.LENGTH_LONG).show ();
                Toast.makeText (this, "Paytabs transaction ID after payment : " +
                        pt_transaction_id, Toast.LENGTH_LONG).show ();

            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            Log.i ("paymentExample", "The user canceled.");
        } /*else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
            Log.i ("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
        }*/


    }

}
