package com.example.ibraheem.mirva.Fragments.manager;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ibraheem.mirva.Adapter.TechinalAdapter;
import com.example.ibraheem.mirva.Enum.TECH_STATUS;
import com.example.ibraheem.mirva.Model.Techical;
import com.example.ibraheem.mirva.R;

import java.util.ArrayList;

/**
 * Created by ibraheem on 28/05/2017.
 */

public class Frag_Dialog_General extends Fragment {

    ArrayList<Techical> list;
    TechinalAdapter mTechinalAdapter;
    RecyclerView recyclerView;
    String TAG = "dialog";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView (inflater, container, savedInstanceState);

        View view = LayoutInflater.from (container.getContext ()).inflate (R.layout.fragment_dialog_general, container, false);
        list = new ArrayList<> ();

        recyclerView = (RecyclerView) view.findViewById (R.id.select_tech_recyler_view_all);
        // mTechinalAdapter = new TechinalAdapter (getActivity (), list);

        GridLayoutManager gridLayoutManager = new GridLayoutManager (getActivity (), 2);
        gridLayoutManager.setOrientation (GridLayout.VERTICAL);

        recyclerView.setLayoutManager (gridLayoutManager);
        recyclerView.setAdapter (mTechinalAdapter);


        return view;
    }

    @Override
    public void onResume() {
        super.onResume ();
        LoadData ();
        Log.d (TAG, "onResume 2: ");
    }


    private void LoadData() {
// Techical(Bitmap uImage, String name, TECH_STATUS status)
        getActivity ().runOnUiThread (new Runnable () {
            @Override
            public void run() {
                list.add (new Techical (getBitmap (R.drawable.face), "ابراهيم  لبد", TECH_STATUS.BUSY));

                list.add (new Techical (getBitmap (R.drawable.face), "ابراهيم  لبد", TECH_STATUS.AVAILABLE));


                mTechinalAdapter.notifyDataSetChanged ();
            }
        });

    }

    Bitmap getBitmap(int res) {
        return BitmapFactory.decodeResource (getResources (), res);
    }

    String getStr(int res) {
        return getResources ().getString (res);
    }

}
