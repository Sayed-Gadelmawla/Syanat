package com.example.ibraheem.mirva.Fragments.Client;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.example.ibraheem.mirva.Activities.MyNotificationActivity;
import com.example.ibraheem.mirva.Const.FragConst;
import com.example.ibraheem.mirva.CustomViews.FWTextView;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.Interfaces.Communicator;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.Model.Subscription;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.util.D;
import com.example.ibraheem.mirva.util.helperFun;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Account_Subscription extends AppCompatActivity implements View.OnClickListener {

    public final String TAG = Frag_Account_Subscription.class.getSimpleName ();
    Communicator comm;
    JFTextView title_pronz, amount_pronz, title_pheda, amount_pheda, title_dahap, amount_dahap, title_masy, amount_masy;
    ApiInterface mAPIService;
    User user;
    String lang, apiToken;
    ArrayList<Subscription> subscriptions;

    JFTextView c_title_toolbar, number_of_notifications;
    FWTextView frag_back;
    Toolbar toolbar_view_order;
    D dialog;
    private ProgressBar mProgressBar;
    private LinearLayout mSubscriptionsLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_account__subscription);

        dialog = new D (this);
        //dialog.ShowProgress (getString (R.string.loading));

        user = helperFun.getUser (getApplicationContext ());
        findViewById (R.id.frag_notification).setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                startActivity (new Intent (getBaseContext (), MyNotificationActivity.class));

            }
        });

        toolbar_view_order = (Toolbar) findViewById (R.id.toolbar_view_order);

        c_title_toolbar = (JFTextView) toolbar_view_order.findViewById (R.id.c_title_toolbar);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        number_of_notifications = (JFTextView) toolbar_view_order.findViewById (R.id.number_of_notifications);
        frag_back = (FWTextView) toolbar_view_order.findViewById (R.id.frag_back);
        frag_back.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                finish ();
            }
        });
        c_title_toolbar.setText (getString (R.string.subscribe));

        mSubscriptionsLayout = (LinearLayout) findViewById(R.id.subscriptions_layout);
        mSubscriptionsLayout.setVisibility(View.INVISIBLE);
        title_pronz = (JFTextView) findViewById (R.id.title_pronz);
        amount_pronz = (JFTextView) findViewById (R.id.amount_pronz);

        title_pheda = (JFTextView) findViewById (R.id.title_pheda);
        amount_pheda = (JFTextView) findViewById (R.id.amount_pheda);

        title_dahap = (JFTextView) findViewById (R.id.title_dahap);
        amount_dahap = (JFTextView) findViewById (R.id.amount_dahap);

        title_masy = (JFTextView) findViewById (R.id.title_masy);
        amount_masy = (JFTextView) findViewById (R.id.amount_masy);

        View pronz_account = findViewById (R.id.pronz_account);
        View silver_account = findViewById (R.id.silver_account);
        View golden_account = findViewById (R.id.golden_account);
        View diamond_account = findViewById (R.id.diamond_account);


        pronz_account.setOnClickListener (this);
        silver_account.setOnClickListener (this);
        golden_account.setOnClickListener (this);
        diamond_account.setOnClickListener (this);
        getSubscription ();
        getNotificationNumber ();
    }


    @Override
    public void onClick(View v) {
        if (subscriptions != null) {

            Log.d (TAG, "onClick: " + subscriptions.get (0).getName ());
            int selectItem = -1;
            switch (v.getId ()) {

                case R.id.pronz_account:
                    selectItem = 0;
                    break;
                case R.id.silver_account:
                    selectItem = 1;
                    break;
                case R.id.golden_account:
                    selectItem = 2;
                    break;
                case R.id.diamond_account:
                    selectItem = 3;
                    break;

            }
            Intent intent = new Intent (this, ActivityPayment.class);

            intent.putExtra (FragConst.SUBSCRIPTION_NAME, subscriptions.get (selectItem).getName ());
            intent.putExtra (FragConst.SUBSCRIPTION_PRICE, subscriptions.get (selectItem).getPrice ());
            intent.putExtra (FragConst.SUBSCRIPTION_ID, subscriptions.get (selectItem).getId ());
            overridePendingTransition (R.anim.fadein, R.anim.fadeout);
            startActivity (intent);
            finish ();

        }

    }

    void getSubscription() {

        lang = helperFun.getLanguage (getApplicationContext ());
        apiToken = helperFun.getUser (getApplicationContext ()).getApiToken ();
        mAPIService = ApiUtils.getAPIService (getApplicationContext ());
        mAPIService.getSubscriptionData (lang, apiToken).enqueue (new Callback<ArrayList<Subscription>> () {
            @Override
            public void onResponse(Call<ArrayList<Subscription>> call, Response<ArrayList<Subscription>> response) {
                //dialog.hide ();
                mProgressBar.setVisibility(View.INVISIBLE);

                if (response.isSuccessful ()) {
                    subscriptions = response.body ();
                    Log.d (TAG, "onResponse:  size" + subscriptions.size ());
                    Log.d (TAG, "onResponse: " + response.body ().toString ());
                    Subscription bronz = subscriptions.get (0);
                    title_pronz.setText (bronz.getName ());
                    amount_pronz.setText (getString (R.string.subsciption_amount, bronz.getDuration (), bronz.getPrice ()));

                    bronz = subscriptions.get (1);
                    title_pheda.setText (bronz.getName ());
                    amount_pheda.setText (getString (R.string.subsciption_amount, bronz.getDuration (), bronz.getPrice ()));

                    bronz = subscriptions.get (2);
                    title_dahap.setText (bronz.getName ());
                    amount_dahap.setText (getString (R.string.subsciption_amount, bronz.getDuration (), bronz.getPrice ()));

                    bronz = subscriptions.get (3);
                    title_masy.setText (bronz.getName ());
                    amount_masy.setText (getString (R.string.subsciption_amount, bronz.getDuration (), bronz.getPrice ()));
                    mSubscriptionsLayout.setVisibility(View.VISIBLE);
                }


            }


            @Override
            public void onFailure(Call<ArrayList<Subscription>> call, Throwable t) {
                mProgressBar.setVisibility(View.INVISIBLE);
                dialog.ShowErrorInternetConnection ("");

            }
        });
    }

    private void getNotificationNumber() {

        ApiInterface apiInterface = ApiUtils.getAPIService (this);
        apiInterface.getNotifications (user.getApiToken ()).enqueue (new Callback<JsonObject> () {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                if (response.isSuccessful ()) {
                    String number;
                    if (response.body ().get ("number") == null) {
                        number = "0";
                    } else {

                        number = response.body ().get ("number").getAsString ();

                    }

                    number_of_notifications.setText (number);


                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });


    }
}
