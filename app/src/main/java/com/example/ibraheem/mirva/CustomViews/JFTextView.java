package com.example.ibraheem.mirva.CustomViews;


import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

/**
 * Created by ibraheem on 5/10/2017.
 */

public class JFTextView extends android.support.v7.widget.AppCompatTextView {

    public JFTextView(Context context) {
        super (context);
        init ();
    }

    public JFTextView(Context context, @Nullable AttributeSet attrs) {
        super (context, attrs);
        init ();
    }

    public JFTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super (context, attrs, defStyleAttr);
        init ();
    }

    void init() {

        Typeface fc = Typeface.createFromAsset (getContext ().getAssets (), "fonts/jf_flat_regular.ttf");
        setTypeface (fc);
        setTextDirection (TEXT_DIRECTION_RTL);

    }
}
