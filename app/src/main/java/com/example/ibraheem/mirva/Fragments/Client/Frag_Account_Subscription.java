package com.example.ibraheem.mirva.Fragments.Client;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ibraheem.mirva.Const.FragConst;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.Interfaces.Communicator;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.Model.Subscription;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.util.helperFun;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static cn.pedant.SweetAlert.SweetAlertDialog.PROGRESS_TYPE;


/**
 * A simple {@link Fragment} subclass.
 */
public class Frag_Account_Subscription extends Fragment implements View.OnClickListener {


    public final String TAG = Frag_Account_Subscription.class.getSimpleName ();
    Communicator comm;
    JFTextView title_pronz, amount_pronz, title_pheda, amount_pheda, title_dahap, amount_dahap, title_masy, amount_masy;
    ApiInterface mAPIService;
    User user;
    String lang, apiToken;
    ArrayList<Subscription> subscriptions;
    SweetAlertDialog dialog;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate (R.layout.fragment_subscription_membership, container, false);

        comm = (Communicator) getActivity ();
        comm.setTitleToolbar (getString (R.string.subscribe));

        title_pronz = (JFTextView) view.findViewById (R.id.title_pronz);
        amount_pronz = (JFTextView) view.findViewById (R.id.amount_pronz);

        title_pheda = (JFTextView) view.findViewById (R.id.title_pheda);
        amount_pheda = (JFTextView) view.findViewById (R.id.amount_pheda);

        title_dahap = (JFTextView) view.findViewById (R.id.title_dahap);
        amount_dahap = (JFTextView) view.findViewById (R.id.amount_dahap);

        title_masy = (JFTextView) view.findViewById (R.id.title_masy);
        amount_masy = (JFTextView) view.findViewById (R.id.amount_masy);
        dialog = new SweetAlertDialog (getContext (), PROGRESS_TYPE);
        dialog.setTitleText (getActivity ().getString (R.string.loading));


        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated (view, savedInstanceState);

        View pronz_account = view.findViewById (R.id.pronz_account);
        View silver_account = view.findViewById (R.id.silver_account);
        View golden_account = view.findViewById (R.id.golden_account);
        View diamond_account = view.findViewById (R.id.diamond_account);

        pronz_account.setOnClickListener (this);
        silver_account.setOnClickListener (this);
        golden_account.setOnClickListener (this);
        diamond_account.setOnClickListener (this);
        getSubscription ();
    }


    @Override
    public void onClick(View v) {
        if (subscriptions != null) {

            Log.d (TAG, "onClick: " + subscriptions.get (0).getName ());
            int selectItem = -1;
            switch (v.getId ()) {

                case R.id.pronz_account:
                    selectItem = 0;
                    break;
                case R.id.silver_account:
                    selectItem = 1;
                    break;
                case R.id.golden_account:
                    selectItem = 2;
                    break;
                case R.id.diamond_account:
                    selectItem = 3;
                    break;

            }
            Fragment fragment = new Frag_PaymentWay ();
            Bundle bundle = new Bundle ();
            bundle.putString (FragConst.SUBSCRIPTION_NAME, subscriptions.get (selectItem).getName ());
            bundle.putString (FragConst.SUBSCRIPTION_PRICE, subscriptions.get (selectItem).getPrice ());
            bundle.putInt (FragConst.SUBSCRIPTION_ID, subscriptions.get (selectItem).getId ());
            fragment.setArguments (bundle);
            comm.addFragment (fragment);


        }

    }

    void getSubscription() {
        dialog.show ();
        lang = helperFun.getLanguage (getActivity ());
        apiToken = helperFun.getUser (getActivity ()).getApiToken ();
        mAPIService = ApiUtils.getAPIService (getActivity ());
        mAPIService.getSubscriptionData (lang, apiToken).enqueue (new Callback<ArrayList<Subscription>> () {
            @Override
            public void onResponse(Call<ArrayList<Subscription>> call, Response<ArrayList<Subscription>> response) {

                if (response.isSuccessful ()) {
                    subscriptions = response.body ();
                    Log.d (TAG, "onResponse:  size" + subscriptions.size ());
                    Log.d (TAG, "onResponse: " + response.body ().toString ());
                    Subscription bronz = subscriptions.get (0);
                    title_pronz.setText (bronz.getName ());
                    amount_pronz.setText (getString (R.string.subsciption_amount, bronz.getDuration (), bronz.getPrice ()));

                    bronz = subscriptions.get (1);
                    title_pheda.setText (bronz.getName ());
                    amount_pheda.setText (getString (R.string.subsciption_amount, bronz.getDuration (), bronz.getPrice ()));

                    bronz = subscriptions.get (2);
                    title_dahap.setText (bronz.getName ());
                    amount_dahap.setText (getString (R.string.subsciption_amount, bronz.getDuration (), bronz.getPrice ()));

                    bronz = subscriptions.get (3);
                    title_masy.setText (bronz.getName ());
                    amount_masy.setText (getString (R.string.subsciption_amount, bronz.getDuration (), bronz.getPrice ()));

                }

                dialog.dismiss ();
            }


            @Override
            public void onFailure(Call<ArrayList<Subscription>> call, Throwable t) {
                Log.d (TAG, "onFailure: " + t.getMessage ());

            }
        });
    }
}
