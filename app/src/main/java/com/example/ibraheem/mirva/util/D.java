package com.example.ibraheem.mirva.util;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Button;

import com.example.ibraheem.mirva.Activities.MainPageClient;
import com.example.ibraheem.mirva.Activities.MainPageManager;
import com.example.ibraheem.mirva.Activities.MainPageTechnical;
import com.example.ibraheem.mirva.Enum.DialogAction;
import com.example.ibraheem.mirva.R;

import cn.pedant.SweetAlert.SweetAlertDialog;

//import com.paypal.android.sdk.m;

/**
 * Created by ibraheem on 29/06/2017.
 */

/**
 *
 * for create custom sweet dialog  and use



 */
public class D {
    SweetAlertDialog sweetAlertDialog;
    private Activity mActivity;

    private Context mContext;

    public D(Activity activity) {
        mActivity = activity;
        mContext = activity;
    }

    public D(Context context) {
        mContext = context;

    }

    public SweetAlertDialog ShowProgress(String content) {

        sweetAlertDialog = new SweetAlertDialog (mContext, SweetAlertDialog.PROGRESS_TYPE);
        sweetAlertDialog.setContentText (content);
        sweetAlertDialog.setTitleText ("");
        sweetAlertDialog.setCancelable (false);
        sweetAlertDialog.setCanceledOnTouchOutside (false);
        sweetAlertDialog.show ();
        return sweetAlertDialog;
    }

    public void hide() {
        sweetAlertDialog.dismiss ();
    }

    public void ShowSuccess( String title , String content ) {

        sweetAlertDialog = new SweetAlertDialog (mContext, SweetAlertDialog.SUCCESS_TYPE);
        sweetAlertDialog.setContentText (content);
        sweetAlertDialog.setTitleText (title);
        sweetAlertDialog.setCancelable (true);
        sweetAlertDialog.setCanceledOnTouchOutside (true);
        sweetAlertDialog.show ();
//        sweetAlertDialog.findViewById (R.id.confirm_button).setVisibility (View.GONE);
        View viewById = sweetAlertDialog.findViewById (R.id.confirm_button);
        if (viewById != null) {
            viewById.setVisibility (View.GONE);
        }
        View viewById2 = sweetAlertDialog.findViewById (R.id.cancel_button);
        if (viewById2 != null) {
            viewById2.setVisibility (View.GONE);
        }


    }

    public void ShowError(String content) {
        sweetAlertDialog = new SweetAlertDialog (mContext, SweetAlertDialog.ERROR_TYPE);
        sweetAlertDialog.setContentText (content);
        sweetAlertDialog.setTitleText ("");
//        sweetAlertDialog.findViewById (R.id.confirm_button).setVisibility (View.GONE);
        View viewById = sweetAlertDialog.findViewById (R.id.confirm_button);
        if (viewById != null) {
            viewById.setVisibility (View.GONE);
        }
        View viewById2 = sweetAlertDialog.findViewById (R.id.cancel_button);
        if (viewById2 != null) {
            viewById2.setVisibility (View.GONE);
        }


        sweetAlertDialog.setCancelable (true);
        sweetAlertDialog.setCanceledOnTouchOutside (true);
        sweetAlertDialog.show ();
    }

    public void showMessageSuccess(String content, final Intent intent) {

        sweetAlertDialog = new SweetAlertDialog (mContext, SweetAlertDialog.SUCCESS_TYPE);

        sweetAlertDialog.setContentText (content);
        sweetAlertDialog.setCancelable (true);
        sweetAlertDialog.setCanceledOnTouchOutside (true);
        sweetAlertDialog.showCancelButton (false);
        sweetAlertDialog.show ();

        Button confirm_button = (Button) sweetAlertDialog.findViewById (R.id.confirm_button);
        if (confirm_button != null) {

            confirm_button.setVisibility (View.GONE);
        }


        sweetAlertDialog.setOnDismissListener (new DialogInterface.OnDismissListener () {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mActivity.overridePendingTransition (R.anim.fadein, R.anim.fadeout);
                mActivity.startActivity (intent);
                mActivity.finish ();
            }
        });


    }

    public void showMessageSuccess(String content, DialogAction action) {

        sweetAlertDialog = new SweetAlertDialog (mContext, SweetAlertDialog.SUCCESS_TYPE);
        sweetAlertDialog.setTitleText (content);
        sweetAlertDialog.setContentText ("");
        sweetAlertDialog.setCancelable (true);
        sweetAlertDialog.setCanceledOnTouchOutside (true);
        sweetAlertDialog.showCancelButton (false);
        sweetAlertDialog.show ();

        Button confirm_button = (Button) sweetAlertDialog.findViewById (R.id.confirm_button);
        if (confirm_button != null) {

            confirm_button.setVisibility (View.GONE);
        }

        if (action == DialogAction.CLOSE) {
            sweetAlertDialog.setOnDismissListener (new DialogInterface.OnDismissListener () {
                @Override
                public void onDismiss(DialogInterface dialog) {

                    mActivity.finish ();
                }
            });
        }


    }

    public void showMessageSuccess(String content, final Fragment fragment) {

        sweetAlertDialog = new SweetAlertDialog (mContext, SweetAlertDialog.SUCCESS_TYPE);
        sweetAlertDialog.setTitleText (content);
        sweetAlertDialog.setContentText ("");
        sweetAlertDialog.setCancelable (true);
        sweetAlertDialog.setCanceledOnTouchOutside (true);
        sweetAlertDialog.showCancelButton (false);
        sweetAlertDialog.show ();

        Button confirm_button = (Button) sweetAlertDialog.findViewById (R.id.confirm_button);
        if (confirm_button != null) {

            confirm_button.setVisibility (View.GONE);
        }


        sweetAlertDialog.setOnDismissListener (new DialogInterface.OnDismissListener () {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (mActivity instanceof MainPageManager) {
                    ((MainPageManager) mActivity).addFragment (fragment);

                } else if ((mActivity instanceof MainPageClient)) {
                    ((MainPageClient) mActivity).addFragment (fragment);

                } else if (mActivity instanceof MainPageTechnical) {
                    ((MainPageTechnical) mActivity).addFragment (fragment);
                }

            }
        });


    }

    public void ShowErrorInternetConnection(String content) {
        sweetAlertDialog = new SweetAlertDialog (mContext, SweetAlertDialog.ERROR_TYPE);
        sweetAlertDialog.setContentText (mContext.getString (R.string.err_quality_internet));
        sweetAlertDialog.setTitleText ("");
//        sweetAlertDialog.findViewById (R.id.confirm_button).setVisibility (View.GONE);
        View viewById = sweetAlertDialog.findViewById (R.id.confirm_button);
        if (viewById != null) {
            viewById.setVisibility (View.GONE);
        }
        View viewById2 = sweetAlertDialog.findViewById (R.id.cancel_button);
        if (viewById2 != null) {
            viewById2.setVisibility (View.GONE);
        }


        sweetAlertDialog.setCancelable (true);
        sweetAlertDialog.setCanceledOnTouchOutside (true);
        sweetAlertDialog.show ();
    }


}
