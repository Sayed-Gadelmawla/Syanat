package com.example.ibraheem.mirva.Fragments.Client;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;

import com.example.ibraheem.mirva.Activities.MyNotificationActivity;
import com.example.ibraheem.mirva.Const.FragConst;
import com.example.ibraheem.mirva.CustomViews.FWTextView;
import com.example.ibraheem.mirva.CustomViews.Farsi;
import com.example.ibraheem.mirva.CustomViews.JFButton;
import com.example.ibraheem.mirva.CustomViews.JFEditText;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.FragmentMessages.EvalutionOrderDone;
import com.example.ibraheem.mirva.Model.Response.EevaluteOrderData.EvalService;
import com.example.ibraheem.mirva.Model.Response.EevaluteOrderData.Service;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.Model.Response.evaluationData;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.UitilsFun;
import com.example.ibraheem.mirva.util.D;
import com.example.ibraheem.mirva.util.V;
import com.example.ibraheem.mirva.util.helperFun;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EvalutionOrder extends AppCompatActivity {

    public final String TAG = "sendEvalutionOrder";
    User user;

    D d;
    ImageView order_icon;
    JFTextView order_desc, Order_subCat, order_details;
    RatingBar rate1, rate2, rate3, rate4;
    JFEditText order_review;
    JFButton order_send_evalution;


    JFTextView c_title_toolbar, number_of_notifications;
    FWTextView frag_back;
    Toolbar toolbar_view_order;
    int selectedCase = 18;
    V vd;
    D dialog;

    int REQUEST_ID;
    static boolean isShowJust = false;
    private ProgressBar mProgressBar;
    private String lang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evalution_order);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        vd = new V(this);
        dialog = new D(this);
        user = helperFun.getUser(this);
        toolbar_view_order = (Toolbar) findViewById(R.id.toolbar_view_order);
        c_title_toolbar = (JFTextView) toolbar_view_order.findViewById(R.id.c_title_toolbar);
        number_of_notifications = (JFTextView) toolbar_view_order.findViewById(R.id.number_of_notifications);
        findViewById(R.id.frag_notification).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), MyNotificationActivity.class));

            }
        });
        c_title_toolbar.setText(getString(R.string.order_details));

        frag_back = (FWTextView) toolbar_view_order.findViewById(R.id.frag_back);
        frag_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Intent intent = getIntent();

        if (intent != null) {
            REQUEST_ID = intent.getIntExtra(FragConst.REQUEST_ID, -1);
            if (intent.getBooleanExtra("isJustShow", false)) {
                isShowJust = true;
            }
        } else {

            finish();
        }

        ///

        order_icon = (ImageView) findViewById(R.id.order_icon);
        order_desc = (JFTextView) findViewById(R.id.order_desc);
        Order_subCat = (JFTextView) findViewById(R.id.Order_subCat);
        /// order_location = (JFTextView) findViewById (R.id.order_location);
        order_details = (JFTextView) findViewById(R.id.order_details);


        rate1 = (RatingBar) findViewById(R.id.rate1);
        rate2 = (RatingBar) findViewById(R.id.rate2);
        rate3 = (RatingBar) findViewById(R.id.rate3);
        rate4 = (RatingBar) findViewById(R.id.rate4);
        order_review = (JFEditText) findViewById(R.id.order_review);

        order_send_evalution = (JFButton) findViewById(R.id.order_send_evalution);

        //vd.validateTextArea (R.id.order_review);
        if (!intent.getBooleanExtra("isJustShow", false)) {
            order_send_evalution.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (vd.isValid() && UitilsFun.isNetworkAvailable(getBaseContext())) {
                        sendEvalutionOrder();
                    }

                }
            });

        } else {
            rate1.setEnabled(false);
            rate2.setEnabled(false);
            rate3.setEnabled(false);
            rate4.setEnabled(false);
            order_review.setEnabled(false);
            order_send_evalution.setVisibility(View.INVISIBLE);
        }
        lang = helperFun.getLanguage(this.getApplicationContext());
        getEvalutionData();


    }

    private void getEvalutionData() {
        //dialog.ShowProgress(getString(R.string.loading));

        ApiInterface apiInterface = ApiUtils.getAPIService(this);
        apiInterface.getEvalutionServiceData(user.getApiToken(), REQUEST_ID,lang).enqueue(new Callback<EvalService>() {
            @Override
            public void onResponse(Call<EvalService> call, Response<EvalService> response) {
                mProgressBar.setVisibility(View.INVISIBLE);
                if (response.isSuccessful()) {
                    EvalService evalService = response.body();
                    Service serviceData = evalService.getService();

                    Picasso.with(getBaseContext())
                            .load(serviceData.getImage())
                            .error(R.drawable.avater_image)
                            .placeholder(R.drawable.avater_image)
                            .into(order_icon);

                    order_desc.setText(serviceData.getService());
                    //   order_location.setText (getAddress (serviceData.getLatitude (), serviceData.getLongitude ()));
                    Order_subCat.setText(serviceData.getSubcategory());

                    if(isShowJust){
                        List<evaluationData> mEvaluationData = evalService.getEvaluationData();
                        if(!mEvaluationData.isEmpty()){
                            rate1.setProgress((mEvaluationData.get(0).getValue()));
                            rate2.setProgress(mEvaluationData.get(1).getValue());
                            rate3.setProgress(mEvaluationData.get(2).getValue());
                            rate4.setProgress(mEvaluationData.get(3).getValue());
                            order_review.setText(evalService.getReview());
                        }
                    }


                }
            }

            @Override
            public void onFailure(Call<EvalService> call, Throwable t) {
                mProgressBar.setVisibility(View.INVISIBLE);

                d.ShowErrorInternetConnection("");

            }
        });


    }

    public String getAddress(String Latitude, String Longitude) {
        double lat = Double.parseDouble(Latitude);
        double lng = Double.parseDouble(Longitude);
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            Address obj = addresses.get(0);
            String add = obj.getAddressLine(0);
            add = add + "\n" + obj.getCountryName();
            add = add + "\n" + obj.getCountryCode();
            add = add + "\n" + obj.getAdminArea();
            add = add + "\n" + obj.getPostalCode();
            add = add + "\n" + obj.getSubAdminArea();
            add = add + "\n" + obj.getLocality();
            add = add + "\n" + obj.getSubThoroughfare();

            // Log.v("IGA", "Address" + add);
            // Toast.makeText(this, "Address=>" + add,
            // Toast.LENGTH_SHORT).show();
            if (obj.getSubAdminArea() != null) {
                return obj.getSubAdminArea();
            }


            // TennisAppActivity.showDialog(add);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            // Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return "Unkown";

    }

    private void sendEvalutionOrder() {
        user = helperFun.getUser(this);
        String orderReview = order_review.getText().toString() + "";
        //dialog.(getString(R.string.sending));
        ApiInterface apiInterface = ApiUtils.getAPIService(this);
        apiInterface.sendEvalutionOrder(user.getApiToken(), REQUEST_ID, orderReview
                , 1, rate1.getProgress(), 2, rate2.getProgress(), 3, rate3.getProgress(), 4, rate4.getProgress()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                //dialog.hide();
                if (response.isSuccessful()) {

                    Intent intent = new Intent(getBaseContext(), EvalutionOrderDone.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    overridePendingTransition(R.anim.fadein, R.anim.fadeout);
                    startActivity(intent);
                    finish();

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                //dialog.hide();
                dialog.ShowErrorInternetConnection("");
            }
        });


    }
}
