package com.example.ibraheem.mirva.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.daimajia.swipe.SwipeLayout;
import com.example.ibraheem.mirva.Const.FragConst;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.Fragments.common.Activity_Chat;
import com.example.ibraheem.mirva.Fragments.common.Frag_Chat;
import com.example.ibraheem.mirva.Fragments.common.Frag_Tickets;
import com.example.ibraheem.mirva.Model.Response.TicketsResonse.Tikect;
import com.example.ibraheem.mirva.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


/**
 * Created by ibraheem on 17/05/2017.
 */

public class TicketAdapter extends RecyclerView.Adapter<TicketAdapter.MyViewHolder> {
    public static final String TAG = "TicketAdapter";
    ArrayList<Tikect> mList;
    Context mContext;
    Fragment fragment;
    Frag_Chat frag_chat;
    Bundle bundle;

    public TicketAdapter(Fragment fragment, Context context, ArrayList<Tikect> list) {
        mContext = context;
        mList = list;
        this.fragment = fragment;
    }

    @Override


    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        return new MyViewHolder (LayoutInflater.from (parent.getContext ()).inflate (R.layout.item_ticket, parent, false));


    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Tikect tikect = mList.get (position);
        holder.ticket_desc.setText (tikect.getContent ());
        holder.ticket_person_name.setText (tikect.getName ());

        switch (tikect.getTypeId ()) {
            case 22:
                holder.ticket_type.setText (mContext.getString (R.string.complaint));
                holder.ticket_type.setTextColor (mContext.getResources ().getColor (R.color.colorWhite));
                holder.ticket_type.setBackground (mContext.getResources ().getDrawable (R.drawable.shap_complaint));

                break;
            case 23:
                holder.ticket_type.setText (mContext.getString (R.string.enquiry));
                holder.ticket_type.setTextColor (mContext.getResources ().getColor (R.color.fontEnquiry));
                holder.ticket_type.setBackground (mContext.getResources ().getDrawable (R.drawable.shap_enquiry));

                break;
        }

        Picasso.with (mContext)
                .load (tikect.getImage ())
                .placeholder (R.drawable.avatar)
                .error (R.drawable.avatar)
                .into (holder.ticket_person_image);
        frag_chat = new Frag_Chat ();
        bundle = new Bundle ();

        holder.wrapper.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                Log.d (TAG, "onClick: "+mList.get (position).getTicketId ());

                Intent intent = new Intent (mContext, Activity_Chat.class);
                intent.putExtra (FragConst.TICKET_ID, mList.get (position).getTicketId ());
                intent.setFlags (Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                mContext.startActivity (intent);

                /*if (itemView.getContext () instanceof MainPageManager) {
                    ((MainPageManager) itemView.getContext ()).addFragment (new Frag_Chat ());
                } else if ((itemView.getContext () instanceof MainPageClient)) {
                    ((MainPageClient) itemView.getContext ()).addFragment (new Frag_Chat ());
                } else if (itemView.getContext () instanceof MainPageTechnical) {
                    ((MainPageTechnical) itemView.getContext ()).addFragment (new Frag_Chat ());

                }*/

            }
        });


        holder.view.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {

                Log.d (TAG, "onClick: "+mList.get (position).getTicketId ());

                Intent intent = new Intent (mContext, Activity_Chat.class);
                intent.putExtra (FragConst.TICKET_ID, mList.get (position).getTicketId ());
                intent.setFlags (Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                mContext.startActivity (intent);
                /*if (holder.view.getContext () instanceof MainPageManager) {

                    bundle.putString (FragConst.USER_TYPE, "Supervisor");
                    frag_chat.setArguments (bundle);
                    ((MainPageManager) holder.view.getContext ()).addFragment (frag_chat);

                    Log.d (TAG, "onClick: MainPageManager");


                } else if ((holder.view.getContext () instanceof MainPageClient)) {


                    bundle.putInt (FragConst.TICKET_ID, tikect.getTicketId ());
                    bundle.putString (FragConst.USER_TYPE, "Client");
                    frag_chat.setArguments (bundle);
                    Log.d (TAG, "onClick: MainPageClient");
                    ((MainPageClient) holder.view.getContext ()).addFragment (frag_chat);


                } else if (holder.view.getContext () instanceof MainPageTechnical) {
                    bundle.putString (FragConst.USER_TYPE, "Technician");
                    frag_chat.setArguments (bundle);
                    Log.d (TAG, "onClick: MainPageTechnical");

                    ((MainPageTechnical) holder.view.getContext ()).addFragment (frag_chat);


                }*/

            }
        });

        if (position == getItemCount () - 1) {
            ((Frag_Tickets) fragment).getNewTickets ();
        }

    }

    @Override
    public int getItemCount() {
        return mList.size ();
    }


    public void LoadData(ArrayList<Tikect> list) {
        mList.addAll (list);
        notifyDataSetChanged ();

    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        JFTextView ticket_desc, ticket_person_name, ticket_type;
        de.hdodenhof.circleimageview.CircleImageView ticket_person_image;
        SwipeLayout swipeLayout;

        View view, wrapper;

        public MyViewHolder(final View itemView) {
            super (itemView);

            view = itemView;
            swipeLayout = (SwipeLayout) itemView.findViewById (R.id.swipe);
            swipeLayout.addDrag (SwipeLayout.DragEdge.Right, swipeLayout.findViewById (R.id.wrapper));

            ticket_desc = (JFTextView) itemView.findViewById (R.id.ticket_item_content);
            ticket_person_name = (JFTextView) itemView.findViewById (R.id.ticket_item_technical_name);
            ticket_type = (JFTextView) itemView.findViewById (R.id.ticket_item_status);
            ticket_person_image = (de.hdodenhof.circleimageview.CircleImageView) itemView.findViewById (R.id.ticket_item_icon);

            wrapper = swipeLayout.findViewById (R.id.wrapper);
        }
    }
}
