package com.example.ibraheem.mirva.Model.Response.ManagerResonse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by ibraheem on 03/07/2017.
 */

public class AllThechnican {

    @SerializedName("all")
    @Expose
    private ArrayList<Thechnican> all = null;
    @SerializedName("specific")
    @Expose
    private ArrayList<Thechnican> specific = null;

    public ArrayList<Thechnican> getAll() {
        return all;
    }

    public void setAll(ArrayList<Thechnican> all) {
        this.all = all;
    }

    public ArrayList<Thechnican> getSpecific() {
        return specific;
    }

    public void setSpecific(ArrayList<Thechnican> specific) {
        this.specific = specific;
    }

}