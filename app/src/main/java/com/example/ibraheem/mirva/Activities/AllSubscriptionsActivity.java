package com.example.ibraheem.mirva.Activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ibraheem.mirva.Const.FragConst;
import com.example.ibraheem.mirva.CustomViews.FWTextView;
import com.example.ibraheem.mirva.CustomViews.JFButton;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.Model.Subscription;
import com.example.ibraheem.mirva.Model.SubscriptionDetailsSescription;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.util.D;
import com.example.ibraheem.mirva.util.V;
import com.example.ibraheem.mirva.util.helperFun;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllSubscriptionsActivity extends AppCompatActivity {

    private static final String TAG = "AllSubscriptionsActivity";
    private static final String SubscriptionTAG = "Subscription";
    private JFTextView c_title_toolbar;
    private Toolbar toolbar_view_order;
    private LinearLayout sub_layout ;
    private JFTextView number_of_notifications;
    private FWTextView frag_back;
    private int REQUEST_ID;
    private ProgressBar mProgressBar;
    private RecyclerView mSubscriptionsRecyclerView;
    private Context mContext;
    private LinearLayoutManager mLinearLayoutManager;
    private String lang;
    private String apiToken;
    private ArrayList<Subscription> subscriptions;
//    private SubscriptionsAdapter mAdapter;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_subscriptions);
        mContext = this;
        findViewById(R.id.frag_notification).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), MyNotificationActivity.class));

            }
        });

        findViewById(R.id.view_order_notification_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), MyNotificationActivity.class));

            }
        });

        toolbar_view_order = (Toolbar) findViewById(R.id.toolbar_view_order);
        sub_layout = (LinearLayout) findViewById(R.id.subscription_layout);
        c_title_toolbar = (JFTextView) toolbar_view_order.findViewById(R.id.c_title_toolbar);
        number_of_notifications = (JFTextView) toolbar_view_order.findViewById(R.id.number_of_notifications);

        c_title_toolbar.setText(getString(R.string.subscriptions));

        frag_back = (FWTextView) toolbar_view_order.findViewById(R.id.frag_back);
        frag_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Intent intent = getIntent();
        if (intent != null) {
            REQUEST_ID = intent.getIntExtra(FragConst.REQUEST_ID, -1);
        } else {
            finish();
        }

        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
//        mSubscriptionsRecyclerView = (RecyclerView) findViewById(R.id.subscriptions_recycler);
//        mLinearLayoutManager = new LinearLayoutManager(mContext);
//        mSubscriptionsRecyclerView.setLayoutManager(mLinearLayoutManager);
        new FillSubscriptionsAsyncTask().execute(new DataCallback() {
            @Override
            public void onSuccess(final ArrayList<Subscription> subscriptions) {

                final Subscription diamond = subscriptions.get(0);
                final Subscription gold = subscriptions.get(1);
                final Subscription silver = subscriptions.get(2);
                final Subscription bronze = subscriptions.get(3);

                ((TextView) findViewById(R.id.bronze_name)).setText(bronze.getName());
                ((TextView) findViewById(R.id.bronze_duration)).setText(String.format(getResources().getString(R.string.subscription_duration), bronze.getDuration()));
                ((TextView) findViewById(R.id.bronze_price)).setText(String.format(getResources().getString(R.string.subscription_price), bronze.getPrice()));

                ((TextView) findViewById(R.id.silver_name)).setText(silver.getName());
                ((TextView) findViewById(R.id.silver_duration)).setText(String.format(getResources().getString(R.string.subscription_duration), silver.getDuration()));
                ((TextView) findViewById(R.id.silver_price)).setText(String.format(getResources().getString(R.string.subscription_price), silver.getPrice()));

                ((TextView) findViewById(R.id.gold_name)).setText(gold.getName());
                ((TextView) findViewById(R.id.gold_duration)).setText(String.format(getResources().getString(R.string.subscription_duration), gold.getDuration()));
                ((TextView) findViewById(R.id.gold_price)).setText(String.format(getResources().getString(R.string.subscription_price), gold.getPrice()));

                ((TextView) findViewById(R.id.diamond_name)).setText(diamond.getName());
                ((TextView) findViewById(R.id.diamond_duration)).setText(String.format(getResources().getString(R.string.subscription_duration), diamond.getDuration()));
                ((TextView) findViewById(R.id.diamond_price)).setText(String.format(getResources().getString(R.string.subscription_price), diamond.getPrice()));

                findViewById(R.id.bronze).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getBaseContext(), SubscriptionDetailsActivity.class);
                        intent.putExtra(SubscriptionTAG , bronze);
                        startActivity(intent);
                    }
                });

                findViewById(R.id.gold).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getBaseContext(), SubscriptionDetailsActivity.class);
                        intent.putExtra(SubscriptionTAG , gold);
                        startActivity(intent);
                    }
                });

                findViewById(R.id.silver).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getBaseContext(), SubscriptionDetailsActivity.class);
                        intent.putExtra(SubscriptionTAG , silver);
                        startActivity(intent);
                    }
                });

                findViewById(R.id.diamond).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getBaseContext(), SubscriptionDetailsActivity.class);
                        intent.putExtra(SubscriptionTAG , diamond);
                        startActivity(intent);
                    }
                });

                sub_layout.setVisibility(View.VISIBLE);
                Log.d("Synchronous" ,subscriptions.get(0).getDescription() );
            }
        });

    }

    private class FillSubscriptionsAsyncTask extends AsyncTask<DataCallback, Void, Void> {

        @Override
        protected Void doInBackground(final DataCallback... callback) {
            lang = helperFun.getLanguage(mContext);
            apiToken = helperFun.getUser(mContext).getApiToken();
            ApiInterface mAPIService = ApiUtils.getAPIService(mContext);

            mAPIService.getSubscriptionData(lang, apiToken).enqueue(new Callback<ArrayList<Subscription>>() {
                @Override
                public void onResponse(Call<ArrayList<Subscription>> call, Response<ArrayList<Subscription>> response) {

                    if (response.isSuccessful()) {
                        subscriptions = response.body();
                        callback[0].onSuccess(subscriptions);
                        //Subscription bronz = subscriptions.get(0);
                        //mAdapter = new SubscriptionsAdapter(subscriptions, R.layout.subscription_item_list_row);
                        //mSubscriptionsRecyclerView.setAdapter(mAdapter);
                    }

                    mProgressBar.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onFailure(Call<ArrayList<Subscription>> call, Throwable t) {
                    Log.d(TAG, "onFailure: " + t.getMessage());
                    mProgressBar.setVisibility(View.INVISIBLE);

                }
            });
            return null;
        }



    }

//    private class SubscriptionsAdapter extends RecyclerView.Adapter<SubscriptionsViewHolder> {
//        private int resource; // the layout we will fill it in recycler (list_item_row)
//        private List<Subscription> mItemsList; // this is the copy from MainList
//
//        public SubscriptionsAdapter(List<Subscription> itemList, int res) {
//            mItemsList = itemList;
//            this.resource = res;
//        }
//
//        @Override
//        public SubscriptionsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//            LayoutInflater layoutInflater = LayoutInflater.from(mContext); // move the layout controller from this activity layout (mContext)
//            View view = layoutInflater.inflate(this.resource, parent, false); // move int resource (list_item_row)
//            return new SubscriptionsViewHolder(view); // take parameter of the view (layout) we will fill in it in the recycler
//        }
//
//        @Override
//        public void onBindViewHolder(SubscriptionsViewHolder holder, int position) {
//            Subscription mSubscription = mItemsList.get(position);
//            holder.bindItem(mSubscription); // this is the item of (kids) we will fill it in the view on the recycler
//        }
//
//        @Override
//        public int getItemCount() {
//            return mItemsList.size();
//        }
//    }

    public interface DataCallback {
        void onSuccess(ArrayList<Subscription> result);
    }

//    private class SubscriptionsViewHolder extends RecyclerView.ViewHolder {
//        private JFTextView mNameJfTextView, mPriceJfTextView, mDurationJfTextView, mDescriptionJfTextView, mMaintenanceCountJfTextView, mMoreJfTextView,mVisitsDetails;
//        private ImageView mIconImageView;
//        private JFButton mDetailsButton,mGitJfButton;
//
//        public SubscriptionsViewHolder(View itemView) {
//            super(itemView);
//            mNameJfTextView = (JFTextView) itemView.findViewById(R.id.subscription_item_name);
//            mPriceJfTextView = (JFTextView) itemView.findViewById(R.id.subscription_item_price);
//            mDurationJfTextView = (JFTextView) itemView.findViewById(R.id.subscription_item_duration);
//            mDescriptionJfTextView = (JFTextView) itemView.findViewById(R.id.subscription_item_description);
//            mMaintenanceCountJfTextView = (JFTextView) itemView.findViewById(R.id.subscription_item_maintenance_count);
//            mVisitsDetails = (JFTextView) itemView.findViewById(R.id.subscription_item_description_details);
//            mIconImageView = (ImageView) itemView.findViewById(R.id.subscription_item_image_view);
//            mDetailsButton = (JFButton) itemView.findViewById(R.id.btn_details);
//            mGitJfButton = (JFButton) itemView.findViewById(R.id.btn_get);
//        }
//
//        public void bindItem(final Subscription mSubscription) {
//            mNameJfTextView.setText(mSubscription.getName());
//            if(mSubscription.getColor()!=null){
//                mNameJfTextView.setTextColor(Color.parseColor("#"+mSubscription.getColor()));
//                mPriceJfTextView.setTextColor(Color.parseColor("#"+mSubscription.getColor()));
//                mDurationJfTextView.setTextColor(Color.parseColor("#"+mSubscription.getColor()));
//                mDescriptionJfTextView.setTextColor(Color.parseColor("#"+mSubscription.getColor()));
//                mMaintenanceCountJfTextView.setTextColor(Color.parseColor("#"+mSubscription.getColor()));
//                mVisitsDetails.setTextColor(Color.parseColor("#"+mSubscription.getColor()));
//            }
//
//            mPriceJfTextView.setText(String.valueOf(mSubscription.getPrice()));
//
//            mDurationJfTextView.setText(mSubscription.getDuration() + " " + getString(R.string.month));
//
//            mDescriptionJfTextView.setText(mSubscription.getDescription());
//
//            mMaintenanceCountJfTextView.setText(String.valueOf(mSubscription.getMaintenanceCount()));
//
//            /*itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    startActivity(new Intent(getApplicationContext(),BankTransferActivity.class));
//                }
//            });*/
//            mDetailsButton.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    if(mSubscription.getUrl()!=null) {
//                        Intent i = new Intent(Intent.ACTION_VIEW);
//                        i.setData(Uri.parse(mSubscription.getUrl()));
//                        mContext.startActivity(i);
//                    }
//                }
//            });
//            mGitJfButton.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Intent bundle = new Intent (getApplicationContext(), BankTransferActivity.class);
//                    bundle.putExtra (FragConst.SUBSCRIPTION_NAME, mSubscription.getName ());
//                    bundle.putExtra (FragConst.SUBSCRIPTION_PRICE, mSubscription.getId());
//                    bundle.putExtra (FragConst.SUBSCRIPTION_ID, mSubscription.getId ());
//                    startActivity(bundle);
//                }
//            });
//
//            ArrayList<SubscriptionDetailsSescription> mDetailsSescriptions = mSubscription.getDetails_description();
//            String description = "";
//            for (int i = 0; i < mDetailsSescriptions.size(); i++) {
//                description+="\u25C9 "+mDetailsSescriptions.get(i).getSub_service_name()+" : "+mDetailsSescriptions.get(i).getMaintenance_count()+"\n\n";
//            }
//            mVisitsDetails.setText(description);
//            Picasso.with (mContext)
//                    .load (ApiUtils.BASE_URL+(lang.trim().equals("ar")?"/images/subscriptions/Ar/"+mSubscription.getImage():"/images/subscriptions/En/"+mSubscription.getImage()))
//                    .placeholder (R.drawable.pheda_icon)
//                    .error (R.drawable.pheda_icon)
//                    .into (mIconImageView);
//
//        }
//
//    }
}
