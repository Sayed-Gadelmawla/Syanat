package com.example.ibraheem.mirva.Rest;

import android.content.Context;

/**
 * Created by ibraheem on 12/06/2017.
 */

public class ApiUtils {
    public static final String BASE_URL = "http://apps.mirva.syanat.sa";
    private static final String BASE_URL_API = BASE_URL + "/api/";

    private ApiUtils() {
    }

    public static ApiInterface getAPIService(Context context) {
        return ApiClient.getClient (context, BASE_URL_API).create (ApiInterface.class);
    }

    public static ApiInterface getLongTimeAPIService(Context context) {
        return ApiClient.getLongTimeClient (context, BASE_URL_API).create (ApiInterface.class);
    }
}
