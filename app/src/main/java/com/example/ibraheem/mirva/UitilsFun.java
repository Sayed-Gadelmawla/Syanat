package com.example.ibraheem.mirva;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.DisplayMetrics;
import android.util.TypedValue;

import com.example.ibraheem.mirva.Activities.Splash;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.util.D;
import com.example.ibraheem.mirva.util.SharedPrefs;
import com.example.ibraheem.mirva.util.helperFun;
import com.google.gson.JsonObject;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ibraheem on 29/05/2017.
 */

public class UitilsFun {
    /**
     *
     * create custom function that will use it ina application
     * @param context
     * @param res
     * @return
     */
    public static Bitmap getBitmap(Context context, int res) {
        return BitmapFactory.decodeResource (context.getResources (), res);

    }

    public static String getStr(Context context, int res) {
        return context.getResources ().getString (res);
    }

    public static float dpToPx(Context context, float valueInDp) {
        DisplayMetrics metrics = context.getResources ().getDisplayMetrics ();
        return TypedValue.applyDimension (TypedValue.COMPLEX_UNIT_DIP, valueInDp, metrics);
    }

    public static Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth ();
        int height = image.getHeight ();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap (image, width, height, true);
    }

    public static Drawable resize(Context context, Drawable image) {
        Bitmap bitmap = ((BitmapDrawable) image).getBitmap ();
        Bitmap bitmapResized = Bitmap.createScaledBitmap (bitmap, (int) (bitmap.getWidth () * 0.6), (int) (bitmap.getHeight () * 0.5), false);
        return new BitmapDrawable (context.getResources (), bitmapResized);
    }


    /**
     * get current app context, and select english.
     * change  app language .
     */
    public static void setLocale(Context mContext, String lang) {
        SharedPrefs.save (mContext, SharedPrefs.LANGUAGE, lang);
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        mContext.getResources().updateConfiguration(config,
                mContext.getResources().getDisplayMetrics());
    }

    static int status = 0;

    public static int changeLanguage(final Context mContext) {
        status = 0;

        final D dialog = new D (mContext);
        dialog.ShowProgress (mContext.getString (R.string.changing_langauge));


        String lang = SharedPrefs.getString (mContext, SharedPrefs.LANGUAGE);
        User user = helperFun.getUser (mContext);

        lang = lang.equals ("en") ? "ar" : "en";

        SharedPrefs.save (mContext, SharedPrefs.LANGUAGE, lang);

        ApiInterface apiInterface = ApiUtils.getAPIService (mContext);


        apiInterface.restLanguage (user.getApiToken (), lang).enqueue (new Callback<JsonObject> () {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful ()) {
                    mContext.startActivity (new Intent (mContext, Splash.class));
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                dialog.hide ();
                mContext.startActivity (new Intent (mContext, Splash.class));
            }
        });

        return status;

    }

    public static boolean isNetworkAvailable(Context mContext) {
        ConnectivityManager connectivity = (ConnectivityManager) mContext.getSystemService (Context.CONNECTIVITY_SERVICE);
        D d;
        if (connectivity == null) {
            d = new D (mContext);
            d.ShowError (mContext.getString (R.string.err_no_internet));
            return false;

        } else {
            NetworkInfo[] info = connectivity.getAllNetworkInfo ();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState () == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        d = new D (mContext);
        return false;
    }

    public static void ShowAlertDialog(Context context) {
       /* try {
            AlertDialog alertDialog = new AlertDialog.Builder (context).create ();
            alertDialog.setTitle ("Info");
            alertDialog.setMessage ("Internet not available, Cross check your internet connectivity and try again");

            // Alert dialog button
            alertDialog.setButton (AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener () {
                        public void onClick(DialogInterface dialog, int which) {
                            // Alert dialog action goes here
                            // onClick button code here
                            dialog.dismiss ();// use dismiss to cancel alert dialog
                        }
                    });
            alertDialog.show ();

        } catch (Exception e) {
        }*/
    }
}
