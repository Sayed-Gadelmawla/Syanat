package com.example.ibraheem.mirva.Model.Response.Order;

import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ibraheem on 01/07/2017.
 */

public class Order {
    @SerializedName("request_id")
    @Expose
    private int request_id;
    @SerializedName("service")
    @Expose
    private String service;
    @SerializedName("status_id")
    @Expose
    private Integer status;
    @SerializedName("end_time")
    @Expose
    private String endTime;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("status_type")
    @Expose
    private Integer status_type;

    public int getRequest_id() {
        return request_id;
    }

    public void setRequest_id(int request_id) {
        this.request_id = request_id;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getImage() {
        return ApiUtils.BASE_URL + image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getStatus_type() {
        return status_type;
    }

    public void setStatus_type(Integer status_type) {
        this.status_type = status_type;
    }

    @Override
    public String toString() {
        return "Order{" +
                "request_id=" + request_id +
                ", service='" + service + '\'' +
                ", status=" + status +
                ", endTime='" + endTime + '\'' +
                ", image='" + image + '\'' +
                '}';
    }
}
