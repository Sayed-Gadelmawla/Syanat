package com.example.ibraheem.mirva;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.AsyncTask;

import com.example.ibraheem.mirva.Activities.ChooseLocationActivity;
import com.example.ibraheem.mirva.Model.Response.User;

import android.location.LocationManager;
import android.location.LocationListener;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.util.SharedPrefs;
import com.example.ibraheem.mirva.util.helperFun;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Eng.Mhmod on 9/4/2017.
 */

public class UpdateLocationAsyncTask extends AsyncTask<Void, Void, Void> {
    private Context mContext;
    private User user;
    private String longitude,latitude;

    public UpdateLocationAsyncTask(Context context, String s) {
        this.mContext = context;
        user = helperFun.getUser(mContext);
        String[] split = s.split(",");
        latitude = split[0];
        longitude = split[1];
    }

    @Override
    protected Void doInBackground(Void... voids) {
        ApiInterface apiInterface = ApiUtils.getAPIService(mContext);
        apiInterface.upldateLocation(user.getApiToken(), longitude,latitude).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                SharedPrefs.save(mContext, SharedPrefs.LATITUDE, latitude);
                SharedPrefs.save(mContext, SharedPrefs.LONGITUDE, longitude);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {


            }
        });
        return null;
    }




}
