package com.example.ibraheem.mirva.Fragments.manager;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ViewFlipper;

import com.example.ibraheem.mirva.Activities.MyNotificationActivity;
import com.example.ibraheem.mirva.Adapter.RequirmentAdapter;
import com.example.ibraheem.mirva.Adapter.SelectedRequirmentAdapter;
import com.example.ibraheem.mirva.Adapter.TechinalAdapter;
import com.example.ibraheem.mirva.Const.FragConst;
import com.example.ibraheem.mirva.CustomViews.FWTextView;
import com.example.ibraheem.mirva.CustomViews.JFButton;
import com.example.ibraheem.mirva.CustomViews.JFEditText;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.FragmentMessages.TrasferToTechnicalDone;
import com.example.ibraheem.mirva.Interfaces.Communicator;
import com.example.ibraheem.mirva.Model.ManagerSendOrder;
import com.example.ibraheem.mirva.Model.Response.ManagerResonse.AllThechnican;
import com.example.ibraheem.mirva.Model.Response.ManagerResonse.Thechnican;
import com.example.ibraheem.mirva.Model.Response.Order.Technical.Requirement;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.util.D;
import com.example.ibraheem.mirva.util.V;
import com.example.ibraheem.mirva.util.helperFun;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Activity_SelectTechnicalToOrder extends AppCompatActivity {
    JFTextView general, under_supervise;
    ViewFlipper viewFlipper;
    ArrayList<Thechnican> list, list_all;
    TechinalAdapter mTechinalAdapter, mTechinalAdapter_all;
    RecyclerView recyclerView, recyclerView_all;
    String TAG = "dialog";
    JFTextView select_technical, btn_select_requirments, btn_select_technical;
    JFEditText addtional_notes;
    JFButton btn_snd_order;
    RecyclerView rv_selected_requirments;
    View not_selected_tools, transfer2technical_done;
    Communicator comm;
    Thechnican selectedTechnical;
    android.app.Dialog reqDialog;
    RecyclerView rv_requirment;
    RequirmentAdapter requirmentAdapter;
    SelectedRequirmentAdapter selectedRequirmentAdapter;
    ArrayList<Requirement> requirements;
    JFButton select_requirments;
    ArrayList<Requirement> dataFiltered;
    View no_tools_message;

    JFTextView c_title_toolbar, number_of_notifications;
    FWTextView frag_back;
    Toolbar toolbar_order;
    V vd;
    android.app.Dialog dialog = null;

    int REQUEST_ID;
    User user;
    D d;
    private String lang;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.fragment_manager_select_tech_to_order);
        user = helperFun.getUser (this);
        lang = helperFun.getLanguage(getApplicationContext());
        findViewById (R.id.frag_notification).setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                startActivity (new Intent (getBaseContext (), MyNotificationActivity.class));

            }
        });
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);


        vd = new V (this);
        d = new D (this);

        //d.ShowProgress (getString (R.string.loading));

        user = helperFun.getUser (getBaseContext ());
        toolbar_order = (Toolbar) findViewById (R.id.toolbar_view_order);
        c_title_toolbar = (JFTextView) toolbar_order.findViewById (R.id.c_title_toolbar);
        number_of_notifications = (JFTextView) toolbar_order.findViewById (R.id.number_of_notifications);
        frag_back = (FWTextView) toolbar_order.findViewById (R.id.frag_back);

        frag_back.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                finish ();
            }
        });


        Intent intent = getIntent ();
        if (intent != null) {
            REQUEST_ID = intent.getIntExtra (FragConst.REQUEST_ID, -1);
        } else {
            finish ();
        }

        c_title_toolbar.setText (getString (R.string.select_technical));


        requirements = new ArrayList<> ();
        dataFiltered = new ArrayList<> ();

        not_selected_tools = findViewById (R.id.not_selected_tools);

        rv_selected_requirments = (RecyclerView) findViewById (R.id.rv_selected_requirments);
        rv_selected_requirments.setLayoutManager (new LinearLayoutManager (getBaseContext (), LinearLayout.VERTICAL, false));
        selectedRequirmentAdapter = new SelectedRequirmentAdapter (getBaseContext (), dataFiltered);
        rv_selected_requirments.setAdapter (selectedRequirmentAdapter);


        btn_select_requirments = (JFTextView) findViewById (R.id.btn_select_requirments);
        btn_select_technical = (JFTextView) findViewById (R.id.btn_select_technical);

        addtional_notes = (JFEditText) findViewById (R.id.addtional_notes);
        btn_snd_order = (JFButton) findViewById (R.id.btn_snd_order);

        vd.validateTextArea (R.id.addtional_notes);

        btn_snd_order.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {

                if (selectedTechnical != null) {

                    if (vd.isValid ()) {
                        sendViewedbyManagerOrder ();
                    }

                } else {
                    btn_select_technical.setError (getString (R.string.error));

                }


            }
        });




      /*  general ();
        under_supervise ();*/


        getAllThechnican ();
        getREquirments ();

        createDaialog (this);

        createRequirmentDaialog (this);


        btn_select_technical.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {

                dialog.show ();

            }
        });

        btn_select_requirments.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {

                reqDialog.show ();

            }
        });

    }

    JFEditText ed_search;

    private void createDaialog(Context context) {
        dialog = new android.app.Dialog (context);
        dialog.requestWindowFeature (Window.FEATURE_NO_TITLE); //before
        dialog.getWindow ().setBackgroundDrawable (new ColorDrawable (android.graphics.Color.TRANSPARENT));
        dialog.setContentView (R.layout.fragment_dialog);


        general = (JFTextView) dialog.findViewById (R.id.general);
        under_supervise = (JFTextView) dialog.findViewById (R.id.under_supervise);
        viewFlipper = (ViewFlipper) dialog.findViewById (R.id.choose_type);
        ImageView exist = (ImageView) dialog.findViewById (R.id.exist);
        exist.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                dialog.dismiss ();
            }
        });
        general ();
        ed_search = (JFEditText) dialog.findViewById (R.id.ed_search);

        under_supervise ();
        ed_search.addTextChangedListener (new TextWatcher () {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {

                mTechinalAdapter_all.filter (ed_search.getText ().toString ().toLowerCase(), list_all);

            }
        });

        mTechinalAdapter_all.setDialog (dialog);

        mTechinalAdapter.setDialog (dialog);

        dialog.setOnDismissListener (new DialogInterface.OnDismissListener () {
            @Override
            public void onDismiss(DialogInterface dialog) {

                if (viewFlipper.getDisplayedChild () == 0) {

                    Log.d (TAG, "onDismiss: " + mTechinalAdapter_all.getSelectedTechnical ());
                    selectedTechnical = mTechinalAdapter_all.getSelectedTechnical ();

                } else {
                    Log.d (TAG, "onDismiss: " + mTechinalAdapter.getSelectedTechnical ());

                    selectedTechnical = mTechinalAdapter.getSelectedTechnical ();
                }

                if (selectedTechnical != null) {
                    btn_select_technical.setText (selectedTechnical.getName ());
                }

            }
        });
       /* imageView.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {

                if (viewFlipper.getDisplayedChild () == 0) {

                    selectedTechnical = mTechinalAdapter_all.getSelectedTechnical ();

                } else {
                    selectedTechnical = mTechinalAdapter.getSelectedTechnical ();


                }

                btn_select_technical.setText (selectedTechnical.getName ());


                dialog.dismiss ();
            }
        });*/


        general.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {

                general.setBackground (getResources ().getDrawable (R.drawable.shape_dialog_selected));
                under_supervise.setBackground (getResources ().getDrawable (R.drawable.shape_dialog));
                viewFlipper.setDisplayedChild (0);
            }
        });

        under_supervise.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                general.setBackground (getResources ().getDrawable (R.drawable.shape_dialog));

                under_supervise.setBackground (getResources ().getDrawable (R.drawable.shape_dialog_selected));

                viewFlipper.setDisplayedChild (1);
            }
        });

    }

    private void under_supervise() {
        list = new ArrayList<> ();

        recyclerView = (RecyclerView) dialog.findViewById (R.id.select_tech_recyler_view);
        mTechinalAdapter = new TechinalAdapter (dialog.getContext (), list);
        GridLayoutManager gridLayoutManager = new GridLayoutManager (dialog.getContext (), 3);
        gridLayoutManager.setOrientation (GridLayout.VERTICAL);
        recyclerView.setLayoutManager (gridLayoutManager);
        recyclerView.setAdapter (mTechinalAdapter);
    }

    private void general() {
        list_all = new ArrayList<> ();

        recyclerView_all = (RecyclerView) dialog.findViewById (R.id.select_tech_recyler_view_all);
        mTechinalAdapter_all = new TechinalAdapter (dialog.getContext (), list_all);
        GridLayoutManager gridLayoutManager = new GridLayoutManager (dialog.getContext (), 3);
        gridLayoutManager.setOrientation (GridLayout.VERTICAL);
        recyclerView_all.setLayoutManager (gridLayoutManager);
        recyclerView_all.setAdapter (mTechinalAdapter_all);
    }

    private void getAllThechnican() {

        ApiInterface apiInterface = ApiUtils.getAPIService (this);
        apiInterface.getAllThechnicans (user.getApiToken (),lang).enqueue (new Callback<AllThechnican> () {
            @Override
            public void onResponse(Call<AllThechnican> call, Response<AllThechnican> response) {
                if (response.isSuccessful ()) {

                    AllThechnican body = response.body ();
                    list_all = body.getAll ();
                    list = body.getSpecific ();
                    mTechinalAdapter_all.loadData (list_all);
                    mTechinalAdapter.loadData (list);
                }else{
                    Log.d(TAG,response.toString());
                }
                mProgressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<AllThechnican> call, Throwable t) {
                mProgressBar.setVisibility(View.INVISIBLE);

                d.ShowErrorInternetConnection ("");

            }
        });
    }

    private void createRequirmentDaialog(Context context) {
        reqDialog = new android.app.Dialog (context);
        reqDialog.requestWindowFeature (Window.FEATURE_NO_TITLE); //before
        reqDialog.getWindow ().setBackgroundDrawable (new ColorDrawable (android.graphics.Color.TRANSPARENT));

        reqDialog.setContentView (R.layout.dialog_requirment);
        no_tools_message = reqDialog.findViewById (R.id.no_tools_message);

        rv_requirment = (RecyclerView) reqDialog.findViewById (R.id.rv_requirment);


        rv_requirment.setLayoutManager (new LinearLayoutManager (context, LinearLayout.VERTICAL, false));
        requirmentAdapter = new RequirmentAdapter (context, requirements);
        rv_requirment.setAdapter (requirmentAdapter);

        select_requirments = (JFButton) reqDialog.findViewById (R.id.select_requirments);
        select_requirments.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {

                dataFiltered = requirmentAdapter.getSelectedRequirments ();

                if (!dataFiltered.isEmpty ()) {

                    not_selected_tools.setVisibility (View.GONE);

                    selectedRequirmentAdapter.loadData (dataFiltered);

                }

                reqDialog.dismiss ();
            }
        });

    }


    private void getREquirments() {

        ApiInterface apiInterface = ApiUtils.getAPIService (this);
        apiInterface.geRequirments (user.getApiToken ()).enqueue (new Callback<ArrayList<Requirement>> () {
            @Override
            public void onResponse(Call<ArrayList<Requirement>> call, Response<ArrayList<Requirement>> response) {
                if (response.isSuccessful ()) {
                    ArrayList<Requirement> req = response.body ();

                    if (req == null || req.isEmpty ()) {
                        no_tools_message.setVisibility (View.GONE);

                    } else {

                        requirements.addAll (req);
                        requirmentAdapter.notifyDataSetChanged ();
                    }

                }

            }

            @Override
            public void onFailure(Call<ArrayList<Requirement>> call, Throwable t) {

            }
        });
    }

    public void sendViewedbyManagerOrder() {

        mProgressBar.setVisibility(View.VISIBLE);

        ApiInterface apiInterface = ApiUtils.getAPIService (this);
        ManagerSendOrder sendOrder = new ManagerSendOrder ();
        sendOrder.setNotes (addtional_notes.getText ().toString ());
        sendOrder.setRequestId ("" + REQUEST_ID);
        if (dataFiltered != null && !dataFiltered.isEmpty ()) {
            sendOrder.setRequirements (dataFiltered);

        }
        sendOrder.setTechnicianId ("" + selectedTechnical.getId ());

        apiInterface.sendViewedbyManagerOrder ("application/json", user.getApiToken (), sendOrder).enqueue (new Callback<JsonObject> () {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful ()) {
                    Intent intent = new Intent (getBaseContext (), TrasferToTechnicalDone.class);
                    intent.setFlags (Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    overridePendingTransition (R.anim.fadein, R.anim.fadeout);
                    startActivity (intent);
                    finish ();
                } else {

                    Log.d (TAG, "onResponse: " + response.errorBody ());
                }

                mProgressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {


                d.ShowErrorInternetConnection ("");

            }
        });


    }


}