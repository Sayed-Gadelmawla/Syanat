package com.example.ibraheem.mirva.CustomViews;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import com.example.ibraheem.mirva.R;

/**
 * Created by ibraheem on 5/10/2017.
 */

public class FWTextView extends android.support.v7.widget.AppCompatTextView {
    public FWTextView(Context context) {
        super (context);
        init ();

    }

    public FWTextView(Context context, @Nullable AttributeSet attrs) {
        super (context, attrs);
        init ();

    }

    public FWTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super (context, attrs, defStyleAttr);
        init ();
    }

    private void init() {

         //setTextColor(getResources().getColor( R.color.colorAccent));

        setTypeface (Typeface.createFromAsset (getContext ().getAssets (), "fonts/fontawesome.ttf"));


    }
}

