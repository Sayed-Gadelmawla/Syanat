package com.example.ibraheem.mirva.Fragments.manager;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.ibraheem.mirva.CustomViews.JFButton;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.Fragments.common.Activity_SelectMessages;
import com.example.ibraheem.mirva.Fragments.common.Frag_Profile_MyData;
import com.example.ibraheem.mirva.Fragments.common.Frag_SelectMessageType;
import com.example.ibraheem.mirva.Fragments.common.Frag_Tickets;
import com.example.ibraheem.mirva.Interfaces.Communicator;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.util.helperFun;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ibraheem on 19/06/2017.
 */

public class Manager_Frag_Profile extends Fragment {
    FragmentManager supportFragmentManager;
    String TAG = "fragment_my_profile";
    JFButton btn_send_message;
    Communicator comm;
    JFTextView profile_username;
    de.hdodenhof.circleimageview.CircleImageView profile_userImage;
    User user;
    JFTextView number_of_messages;
    MyPagerAdapter adapterViewPager;
    ViewPager vpPager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate (R.layout.manager_frag_profile, container, false);
        profile_userImage = (de.hdodenhof.circleimageview.CircleImageView) view.findViewById (R.id.profile_userImage);
        profile_username = (JFTextView) view.findViewById (R.id.profile_username);
        number_of_messages = (JFTextView) view.findViewById (R.id.number_of_messages);

        vpPager = (ViewPager) view.findViewById (R.id.vpPager);
        adapterViewPager = new MyPagerAdapter (getActivity ().getSupportFragmentManager ());
        vpPager.setAdapter (adapterViewPager);

        getUserData ();
        return view;
    }

    private void getUserData() {

        user = helperFun.getUser (getContext ());
        Log.d (TAG, "onCreate: " + user.getImage ());

        Picasso.with (getActivity ())
                .load (user.getImage ())
                .placeholder (R.drawable.avatar)
                .error (R.drawable.avatar)
                .into (profile_userImage);
        profile_username.setText (user.getName ());

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated (view, savedInstanceState);
        Log.d (TAG, "onCreateView: sasas");
        comm = (Communicator) getActivity ();
        comm.selectVisibleToolbar (R.id.toolbar_profile);

        supportFragmentManager = getActivity ().getSupportFragmentManager ();
        btn_send_message = (JFButton) view.findViewById (R.id.btn_send_message);
        final LinearLayout menu_my_request = (LinearLayout) view.findViewById (R.id.menu_my_request);
        final LinearLayout menu_my_messages = (LinearLayout) view.findViewById (R.id.menu_my_messages);
        final LinearLayout menu_my_data = (LinearLayout) view.findViewById (R.id.menu_my_data);
        vpPager.setCurrentItem (0);


        //supportFragmentManager.beginTransaction ().add (R.id.container_menu_selected, new Frag_Tickets ()).commit ();
        btn_send_message.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {

                startActivity (new Intent (getActivity (), Activity_SelectMessages.class));
                /*comm.setToolbatLock (false);
                comm.addFragment (new Frag_SelectMessageType ());*/
                /*comm.setToolbatLock (false);
                comm.addFragment (new Frag_SelectMessageType ());*/
            }
        });
        menu_my_request.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                menu_my_request.setBackgroundColor (getResources ().getColor (R.color.selected_menu));
                menu_my_messages.setBackgroundColor (getResources ().getColor (R.color.unselected_menu));
                menu_my_data.setBackgroundColor (getResources ().getColor (R.color.unselected_menu));
                vpPager.setCurrentItem (0);
                //    supportFragmentManager.beginTransaction ().replace (R.id.container_menu_selected, new Manager_Frag_MyOrders ()).commit ();

            }
        });
        menu_my_messages.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                //   supportFragmentManager.beginTransaction ().replace (R.id.container_menu_selected, new Frag_Tickets ()).commit ();
                menu_my_request.setBackgroundColor (getResources ().getColor (R.color.unselected_menu));
                menu_my_messages.setBackgroundColor (getResources ().getColor (R.color.selected_menu));
                menu_my_data.setBackgroundColor (getResources ().getColor (R.color.unselected_menu));
                vpPager.setCurrentItem (1);

            }
        });
        menu_my_data.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                //  supportFragmentManager.beginTransaction ().replace (R.id.container_menu_selected, new Frag_Profile_MyData ()).commit ();
                menu_my_request.setBackgroundColor (getResources ().getColor (R.color.unselected_menu));
                menu_my_messages.setBackgroundColor (getResources ().getColor (R.color.unselected_menu));
                menu_my_data.setBackgroundColor (getResources ().getColor (R.color.selected_menu));
                vpPager.setCurrentItem (2);

            }
        });
        getMeessageNumber ();


    }

    @Override
    public void onStop() {
        super.onStop ();
        comm.setToolbatLock (false);
    }

    /**
     * get message number
     */
    private void getMeessageNumber() {

        ApiInterface apiInterface = ApiUtils.getAPIService (getActivity ());
        apiInterface.getMessagesnumber (user.getApiToken ()).enqueue (new Callback<JsonObject> () {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                String number = "0";

                if (response.isSuccessful ()) {

                    if (response.body ().get ("number") != null) {
                        number = "" + response.body ().get ("number").getAsNumber ();

                        Log.d (TAG, "onResponse:number " + number);
                        number_of_messages.setText (number);
                    }

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    /***
     * view pager for selected adapter
     */
    private static class MyPagerAdapter extends FragmentPagerAdapter {
        private static int NUM_ITEMS = 3;

        public MyPagerAdapter(FragmentManager fragmentManager) {
            super (fragmentManager);
        }

        // Returns total number of pages
        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        // Returns the fragment to display for that page
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0: // Fragment # 0 - This will show FirstFragment
                    return (new Manager_Frag_MyOrders ());
                case 1: // Fragment # 0 - This will show FirstFragment different title
                    return (new Frag_Tickets ());
                case 2: // Fragment # 1 - This will show SecondFragment
                    return (new Frag_Profile_MyData ());
                default:
                    return null;
            }
        }

        // Returns the page title for the top indicator
        @Override
        public CharSequence getPageTitle(int position) {
            return "Page " + position;
        }

    }

}