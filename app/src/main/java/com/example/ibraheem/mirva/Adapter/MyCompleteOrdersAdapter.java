package com.example.ibraheem.mirva.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.example.ibraheem.mirva.Const.FragConst;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.Fragments.Client.Activity_ViewOrderClient;
import com.example.ibraheem.mirva.Model.Response.Order.AllOrders;
import com.example.ibraheem.mirva.Model.Response.Order.Order;
import com.example.ibraheem.mirva.Model.Response.Order.OrderStatus;
import com.example.ibraheem.mirva.R;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by ibraheem on 17/06/2017.
 */

public class MyCompleteOrdersAdapter extends RecyclerView.Adapter<MyCompleteOrdersAdapter.MyViewHolder> {

    private static final String TAG = "MyOrdersAdapter";
    Context mContext;
    ArrayList<Order> mList;
    ArrayList<Order> mCompletedList;
    Order order;
    Fragment fragment;
    OrderStatus requestStatus;

    public MyCompleteOrdersAdapter(Context context, ArrayList<Order> list) {
        mContext = context;
        mList = list;

    }

    @Override
    public MyCompleteOrdersAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from (parent.getContext ()).inflate (R.layout.item_my_order, parent, false);

        return new MyCompleteOrdersAdapter.MyViewHolder (v);
    }

    @Override
    public void onBindViewHolder(MyCompleteOrdersAdapter.MyViewHolder holder, final int position) {
        order = mList.get (position);

        Log.d (TAG, "onBindViewHolder: " + order);


        holder.my_order_item_name.setText (order.getService ());
        // holder.my_order_item_icon.setImageBitmap (notification.getIcon ());
        Picasso.with (mContext)
                .load (order.getImage ())
                .placeholder (R.drawable.icon_elect)
                .error (R.drawable.icon_elect)
                .into (holder.my_order_item_icon);

        holder.my_order_item_layout.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {

                Intent intent= new Intent (mContext, Activity_ViewOrderClient.class);
                intent.putExtra (FragConst.REQUEST_ID, mList.get (position).getRequest_id ());

                intent.setFlags (Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                mContext.startActivity (intent);

               /* fragment = new Frag_ViewOrderDetail ();
                Bundle bundle = new Bundle ();
                bundle.putInt (FragConst.REQUEST_ID, mList.get (position).getRequest_id ());
                fragment.setArguments (bundle);
                ((MainPageClient) mContext).addFragment (fragment);*/
            }
        });

        boolean isDone = false;
        switch (mList.get(position).getStatus_type()) {

            case 35:
                isDone = true;

                holder.my_order_item_status.setBackground(mContext.getResources().getDrawable(R.drawable.background_ended));
                holder.my_order_item_status.setText(mContext.getResources().getString(R.string.ended));

                break;
            case 40:
                isDone = false;

                holder.my_order_item_status.setBackground(mContext.getResources().getDrawable(R.drawable.background_under_revision));
                holder.my_order_item_status.setText(mContext.getResources().getString(R.string.underRevision));
                break;
            case 34:
                isDone = false;

                holder.my_order_item_status.setBackground(mContext.getResources().getDrawable(R.drawable.background_under_way));
                holder.my_order_item_status.setText(mContext.getResources().getString(R.string.underWay));
                break;
            case 41:
                isDone = false;
                holder.my_order_item_status.setBackground(mContext.getResources().getDrawable(R.drawable.background_recived));
                holder.my_order_item_status.setText(mContext.getResources().getString(R.string.completed));
                break;
            case 43:
                isDone = false;
                holder.my_order_item_status.setBackground(mContext.getResources().getDrawable(R.drawable.background_rejected));
                holder.my_order_item_status.setText(mContext.getResources().getString(R.string.rejected));

                break;
            case 44:
                isDone = false;
                holder.my_order_item_status.setBackground(mContext.getResources().getDrawable(R.drawable.background_closed));
                holder.my_order_item_status.setText(mContext.getResources().getString(R.string.closed));

                break;
        }

        if (order.getEndTime() == null || order.getEndTime().trim().equals("")) {
            holder.my_order_item_details.setText(mContext.getString(R.string.type_case_emergency));
        } else {
            holder.my_order_item_details.setText(getFormatedDateTime(String.valueOf(order.getEndTime()), "yyyy-MM-dd HH:mm:ss", "E',' ' 'dd','MMMM','yyyy KK:mm a"));

        }

    }

    @Override
    public int getItemCount() {
        return mList.size ();
    }

    public void loadData(AllOrders orderData) {
        mCompletedList = new ArrayList<>();
        requestStatus = orderData.getStatus ();
        mList = orderData.getData ();
        for (int i = 0; i < mList.size(); i++) {
            if (mList.get(i).getStatus() == 4) {
                mCompletedList.add(mList.get(i));
            }
        }
        mList = mCompletedList;
        notifyDataSetChanged ();


    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        JFTextView my_order_item_details, my_order_item_status, my_order_item_name;
        ImageView my_order_item_icon;
        RelativeLayout my_order_item_layout;

        public MyViewHolder(View itemView) {
            super (itemView);
            my_order_item_layout = (RelativeLayout) itemView.findViewById (R.id.my_order_item_layout);
            my_order_item_icon = (ImageView) itemView.findViewById (R.id.my_order_item_icon);
            my_order_item_details = (JFTextView) itemView.findViewById (R.id.my_order_item_details);
            my_order_item_status = (JFTextView) itemView.findViewById (R.id.my_order_item_status);
            my_order_item_name = (JFTextView) itemView.findViewById (R.id.my_order_item_name);


        }
    }

    public static String getFormatedDateTime(String dateStr, String strReadFormat, String strWriteFormat) {

        String formattedDate = dateStr;

        DateFormat readFormat = new SimpleDateFormat(strReadFormat, Locale.getDefault());
        DateFormat writeFormat = new SimpleDateFormat(strWriteFormat, Locale.getDefault());

        Date date = null;

        try {
            date = readFormat.parse(dateStr);
        } catch (ParseException e) {
        }

        if (date != null) {
            formattedDate = writeFormat.format(date);
        }

        return formattedDate;
    }
}