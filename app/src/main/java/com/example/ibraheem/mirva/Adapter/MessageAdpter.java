package com.example.ibraheem.mirva.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.Model.Response.TicketsResonse.Message;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.util.helperFun;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by ibraheem on 22/05/2017.
 */

public class MessageAdpter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static int selectedItem = -1;
    ArrayList<com.example.ibraheem.mirva.Model.Response.TicketsResonse.Message> messageArrayList;
    Context mContext;
    RecyclerView recyclerView;
    String TAG = "MessageAdpter";
    User user;


    public MessageAdpter(Context context, ArrayList<com.example.ibraheem.mirva.Model.Response.TicketsResonse.Message> messages, RecyclerView recyclerView) {
        messageArrayList = messages;
        mContext = context;
        this.recyclerView = recyclerView;
        user = helperFun.getUser (context);

    }

    public void setSelectedItem(int position) {
        selectedItem = position;
    }

    @Override
    public int getItemViewType(int position) {

        //   int type_message;
        Message message = messageArrayList.get (position);
        Log.d (TAG, "getItemViewType: toString :" + message.toString ());
        if (message.getIsUserSend ().equals ("0")) {


            return message.getContent () == null || message.getContent ().equals ("null") ? 1 : 0;


        } else {
            //    Log.d (TAG, "getItemViewType:getUrl " + message.getUrl ().isEmpty ());

            return message.getContent () == null || message.getContent ().equals ("null") ? 3 : 2;

        }
        //0,2 message
        //1,3 image;


        //   return type_message;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflate;

        Log.d (TAG, "onCreateViewHolder: viewType" + viewType);
        switch (viewType) {


            case 0:
                inflate = LayoutInflater.from (parent.getContext ()).inflate (R.layout.item_recived_message, parent, false);
                Log.d (TAG, "onCreateViewHolder: myViewHolderR");

                return new myViewHolderR (inflate);

            case 1:
                inflate = LayoutInflater.from (parent.getContext ()).inflate (R.layout.item_recived_iamge, parent, false);
                Log.d (TAG, "onCreateViewHolder: myViewHolderR");

                return new myViewHolderRIamge (inflate);

            case 2:

                inflate = LayoutInflater.from (parent.getContext ()).inflate (R.layout.item_send_message, parent, false);
                Log.d (TAG, "onCreateViewHolder: myViewHolderS");
                return new myViewHolderS (inflate);

            case 3:
                inflate = LayoutInflater.from (parent.getContext ()).inflate (R.layout.item_send_image, parent, false);
                Log.d (TAG, "onCreateViewHolder: myViewHolderSImage");
                return new myViewHolderSImage (inflate);

        }
        Log.d (TAG, "onCreateViewHolder: typer" + viewType);
        return null;

    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        com.example.ibraheem.mirva.Model.Response.TicketsResonse.Message message = messageArrayList.get (position);
        if (selectedItem == position) {
            holder.itemView.setSelected (true);

        }
        Log.d (TAG, "onBindViewHolder: " + message.toString ());


        if (position == getItemCount () - 1) {
            setSelectedItem (position);
            recyclerView.scrollToPosition (position);

        }
        switch (holder.getItemViewType ()) {
            case 0:
                myViewHolderR viewHolderR = (myViewHolderR) holder;
                viewHolderR.message_body.setText (message.getContent ());
                Picasso.with (mContext)
                        .load (message.getImage ())
                        .placeholder (R.drawable.avatar)
                        .error (R.drawable.avatar)
                        .into (viewHolderR.message_icon);
                viewHolderR.message_time.setText (message.getDate ());

                break;
            case 1:
                myViewHolderRIamge myViewHolderRIamge = (myViewHolderRIamge) holder;

                Picasso.with (mContext)
                        .load (message.getUrl ())
                        .placeholder (R.drawable.avater_image)
                        .error (R.drawable.avater_image)
                        .into (myViewHolderRIamge.message_image);

                Picasso.with (mContext)
                        .load (message.getImage ())
                        .placeholder (R.drawable.avatar)
                        .error (R.drawable.avatar)
                        .into (myViewHolderRIamge.message_icon);

                myViewHolderRIamge.message_time.setText (message.getDate ());
                break;


            case 2:
                myViewHolderS myViewHolderS = (myViewHolderS) holder;
                myViewHolderS.message_body.setText (message.getContent ());

                Picasso.with (mContext)
                        .load (user.getImage ())
                        .placeholder (R.drawable.avatar)
                        .error (R.drawable.avatar)
                        .into (myViewHolderS.message_icon);
                //   viewHolder0.message_icon.setImageBitmap (message.getImage ());
                myViewHolderS.message_time.setText (message.getDate ());
                break;
            case 3:

                myViewHolderSImage myViewHolderSImage = (myViewHolderSImage) holder;
                //viewHolder0.message_body.setText (message.getContent ());

                Picasso.with (mContext)
                        .load (message.getUrl ())
                        .placeholder (R.drawable.avater_image)
                        .error (R.drawable.avater_image)
                        .into (myViewHolderSImage.message_image);

                if (message.getUrl () == null) {
                    Picasso.with (mContext)
                            .load (user.getImage ())
                            .placeholder (R.drawable.avatar)
                            .error (R.drawable.avatar)
                            .into (myViewHolderSImage.message_icon);
                } else {
                    myViewHolderSImage.message_icon.setImageBitmap (message.getTempImage ());

                }

                //   viewHolder0.message_icon.setImageBitmap (message.getImage ());
                myViewHolderSImage.message_time.setText (message.getDate ());

                break;


        }
    }


    @Override
    public int getItemCount() {
        return messageArrayList.size ();
    }

    public void loadData(ArrayList<com.example.ibraheem.mirva.Model.Response.TicketsResonse.Message> messages) {
        messageArrayList.addAll (messages);
        notifyDataSetChanged ();

        recyclerView.scrollToPosition (getItemCount () - 1);


    }

    public void addMessage(com.example.ibraheem.mirva.Model.Response.TicketsResonse.Message messages) {
        messageArrayList.add (messages);


        notifyDataSetChanged ();

        recyclerView.scrollToPosition (getItemCount () - 1);


    }

    /**
     * ccreate sender layout messaage  (text)
     */
    class myViewHolderS extends RecyclerView.ViewHolder {

        public JFTextView message_body, message_time;
        public ImageView message_icon;

        public myViewHolderS(View itemView) {
            super (itemView);
            message_body = (JFTextView) itemView.findViewById (R.id.message_body);
            message_time = (JFTextView) itemView.findViewById (R.id.message_time);
            message_icon = (ImageView) itemView.findViewById (R.id.message_icon);


        }
    }
    /**
     * ccreate sender layout messaage  (image)
     */
    class myViewHolderSImage extends RecyclerView.ViewHolder {

        public JFTextView message_time;
        public ImageView message_icon, message_image;

        public myViewHolderSImage(View itemView) {
            super (itemView);
            message_image = (ImageView) itemView.findViewById (R.id.message_image);
            message_time = (JFTextView) itemView.findViewById (R.id.message_time);
            message_icon = (ImageView) itemView.findViewById (R.id.message_icon);


        }
    }

    class myViewHolderR extends RecyclerView.ViewHolder {

        public JFTextView message_body, message_time;
        public ImageView message_icon;

        public myViewHolderR(View itemView) {
            super (itemView);
            message_body = (JFTextView) itemView.findViewById (R.id.message_body);
            message_time = (JFTextView) itemView.findViewById (R.id.message_time);
            message_icon = (ImageView) itemView.findViewById (R.id.message_icon);


        }
    }

    class myViewHolderRIamge extends RecyclerView.ViewHolder {

        public JFTextView message_time;
        public ImageView message_icon, message_image;

        public myViewHolderRIamge(View itemView) {
            super (itemView);
            message_image = (ImageView) itemView.findViewById (R.id.message_image);
            message_time = (JFTextView) itemView.findViewById (R.id.message_time);
            message_icon = (ImageView) itemView.findViewById (R.id.message_icon);


        }
    }
}
