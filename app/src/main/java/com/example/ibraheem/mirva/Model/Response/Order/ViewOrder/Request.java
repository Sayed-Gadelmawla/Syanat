package com.example.ibraheem.mirva.Model.Response.Order.ViewOrder;

/**
 * Created by ibraheem on 02/07/2017.
 */

import com.example.ibraheem.mirva.Model.Response.Order.Technical.Requirement;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Request {

    @SerializedName("is_subscription")
    @Expose
    private Integer isSubscription;
    @SerializedName("order_status_type")
    @Expose
    private Integer order_status_type;
    @SerializedName("status_type")
    @Expose
    private String statusType;
    @SerializedName("service")
    @Expose
    private String service;
    @SerializedName("details")
    @Expose
    private String details;
    @SerializedName("price")
    @Expose
    private Object price;
    @SerializedName("subcategory")
    @Expose
    private String subcategory;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("status_id")
    @Expose
    private Integer statusId;
    @SerializedName("price_status")
    @Expose
    private Integer price_status;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("rejectNote")
    @Expose
    private String rejectNote;
    @SerializedName("start_time")
    @Expose
    private String startTime;
    @SerializedName("end_time")
    @Expose
    private String endTime;
    @SerializedName("images")
    @Expose
    private ArrayList<String> images = null;
    @SerializedName("videos")
    @Expose
    private ArrayList<String> videos = null;
    @SerializedName("requirement")
    @Expose
    private ArrayList<Requirement> requirement = null;
    @SerializedName("supervisor_notes")
    @Expose
    private String supervisor_notes;
    @SerializedName("created_date")
    @Expose
    private String created_date;
    @SerializedName("technical_notes")
    @Expose
    private String technical_notes;
    @SerializedName("technician_status")
    @Expose
    private String technician_status;
    @SerializedName("rated")
    @Expose
    private Integer rated;
    @SerializedName("isConfirmCompletion")
    @Expose
    private Integer isConfirmCompletion;
    @SerializedName("hasSupervisor")
    @Expose
    private Boolean hasSupervisor;
    @SerializedName("hasTechnician")
    @Expose
    private Boolean hasTechnician;

    public String getSupervisor_notes() {
        return supervisor_notes;
    }

    public void setSupervisor_notes(String supervisor_notes) {
        this.supervisor_notes = supervisor_notes;
    }

    public Integer getIsSubscription() {
        return isSubscription;
    }

    public void setIsSubscription(Integer isSubscription) {
        this.isSubscription = isSubscription;
    }

    public String getStatusType() {
        return statusType;
    }

    public void setStatusType(String statusType) {
        this.statusType = statusType;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Object getPrice() {
        return price;
    }

    public void setPrice(Object price) {
        this.price = price;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    public ArrayList<String> getVideos() {
        return videos;
    }

    public void setVideos(ArrayList<String> videos) {
        this.videos = videos;
    }

    public ArrayList<Requirement> getRequirement() {
        return requirement;
    }

    public void setRequirement(ArrayList<Requirement> requirement) {
        this.requirement = requirement;
    }

    public Integer getRated() {
        return rated;
    }

    public void setRated(Integer rated) {
        this.rated = rated;
    }

    public String getTechnical_notes() {
        return technical_notes;
    }

    public void setTechnical_notes(String technical_notes) {
        this.technical_notes = technical_notes;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getTechnician_status() {
        return technician_status;
    }

    public void setTechnician_status(String technician_status) {
        this.technician_status = technician_status;
    }

    public Integer getOrder_status_type() {
        return order_status_type;
    }

    public void setOrder_status_type(Integer order_status_type) {
        this.order_status_type = order_status_type;
    }

    public Integer getIsConfirmCompletion() {
        return isConfirmCompletion;
    }

    public void setIsConfirmCompletion(Integer isConfirmCompletion) {
        this.isConfirmCompletion = isConfirmCompletion;
    }

    public Integer getPrice_status() {
        return price_status;
    }

    public void setPrice_status(Integer price_status) {
        this.price_status = price_status;
    }

    public String getRejectNote() {
        return rejectNote;
    }

    public void setRejectNote(String rejectNote) {
        this.rejectNote = rejectNote;
    }


    public Boolean getHasSupervisor() {
        return hasSupervisor;
    }

    public void setHasSupervisor(Boolean hasSupervisor) {
        this.hasSupervisor = hasSupervisor;
    }

    public Boolean getHasTechnician() {
        return hasTechnician;
    }

    public void setHasTechnician(Boolean hasTechnician) {
        this.hasTechnician = hasTechnician;
    }

    @Override
    public String toString() {
        return "Request{" +
                "isSubscription=" + isSubscription +
                ", statusType='" + statusType + '\'' +
                ", service='" + service + '\'' +
                ", details='" + details + '\'' +
                ", price=" + price +
                ", subcategory='" + subcategory + '\'' +
                ", status='" + status + '\'' +
                ", statusId=" + statusId +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", images=" + images +
                ", videos=" + videos +
                ", requirement=" + requirement +
                ", supervisor_notes='" + supervisor_notes + '\'' +
                '}';
    }
}