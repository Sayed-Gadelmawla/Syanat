package com.example.ibraheem.mirva.Activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.example.ibraheem.mirva.CustomViews.JFButton;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.UitilsFun;
import com.example.ibraheem.mirva.util.helperFun;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class Splash extends AppCompatActivity implements View.OnClickListener {

    public final String TAG = Splash.class.getSimpleName();
    JFButton btn_english, btn_arabic;
    Intent intent;
    User user;
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        UitilsFun.setLocale (this, helperFun.getLanguage (this));

        setContentView (R.layout.activity_splash);
        bindView ();

        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            }
        }

        user = helperFun.getUser (getApplicationContext ());

        if (user != null && user.getApiToken() != null) {

            switch (user.getType ()) {
                case "Supervisor":
                    intent = new Intent (getApplicationContext (), MainPageManager.class);
                    break;
                case "Technician":
                    intent = new Intent (getApplicationContext (), MainPageTechnical.class);
                    break;
                case "Client":
                    intent = new Intent (getApplicationContext (), MainPageClient.class);
                    break;

            }
            startActivity (intent);
            finish ();
        }else{
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(getApplicationContext(),LoginActivity.class));
                    finish();
                }
            }, 3000);
        }
    }


    private void bindView() {
        btn_english = (JFButton) findViewById (R.id.btn_english);
        btn_arabic = (JFButton) findViewById (R.id.btn_arabic);
        btn_english.setOnClickListener (this);
        btn_arabic.setOnClickListener (this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId ()) {
            case R.id.btn_english:
                UitilsFun.setLocale (getApplicationContext (), "en");
                break;
            case R.id.btn_arabic:
                UitilsFun.setLocale (getApplicationContext (), "ar");
                break;
        }

        new Handler ().postDelayed (new Runnable () {
            @Override
            public void run() {
                startActivity (new Intent (getBaseContext (), LoginActivity.class));
                finish ();
            }
        }, 250);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                break;
        }
    }
}
