package com.example.ibraheem.mirva.Model.Response.TicketsResonse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by ibraheem on 01/07/2017.
 */

public class AllTickets {
    @SerializedName("total")
    @Expose
    private Object total;
    @SerializedName("per_page")
    @Expose
    private Object perPage;
    @SerializedName("current_page")
    @Expose
    private Object currentPage;
    @SerializedName("last_page")
    @Expose
    private Object lastPage;
    @SerializedName("next_page_url")
    @Expose
    private Object nextPageUrl;
    @SerializedName("prev_page_url")
    @Expose
    private Object prevPageUrl;
    @SerializedName("from")
    @Expose
    private Object from;
    @SerializedName("to")
    @Expose
    private Object to;
    @SerializedName("data")
    @Expose
    private ArrayList<Tikect> data = null;

    public Object getTotal() {
        return total;
    }

    public void setTotal(Object total) {
        this.total = total;
    }

    public Object getPerPage() {
        return perPage;
    }

    public void setPerPage(Object perPage) {
        this.perPage = perPage;
    }

    public Object getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Object currentPage) {
        this.currentPage = currentPage;
    }

    public Object getLastPage() {
        return lastPage;
    }

    public void setLastPage(Object lastPage) {
        this.lastPage = lastPage;
    }

    public Object getNextPageUrl() {
        return nextPageUrl;
    }

    public void setNextPageUrl(Object nextPageUrl) {
        this.nextPageUrl = nextPageUrl;
    }

    public Object getPrevPageUrl() {
        return prevPageUrl;
    }

    public void setPrevPageUrl(Object prevPageUrl) {
        this.prevPageUrl = prevPageUrl;
    }

    public Object getFrom() {
        return from;
    }

    public void setFrom(Object from) {
        this.from = from;
    }

    public Object getTo() {
        return to;
    }

    public void setTo(Object to) {
        this.to = to;
    }

    public ArrayList<Tikect> getData() {
        return data;
    }

    public void setData(ArrayList<Tikect> data) {
        this.data = data;
    }

}