package com.example.ibraheem.mirva.Fragments.common;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.ibraheem.mirva.Activities.MainPageClient;
import com.example.ibraheem.mirva.Activities.MainPageTechnical;
import com.example.ibraheem.mirva.Adapter.MessageAdpter;
import com.example.ibraheem.mirva.Const.FragConst;
import com.example.ibraheem.mirva.CustomViews.FWTextView;
import com.example.ibraheem.mirva.CustomViews.JFEditText;
import com.example.ibraheem.mirva.Interfaces.Communicator;
import com.example.ibraheem.mirva.Model.Response.TicketsResonse.Chat;
import com.example.ibraheem.mirva.Model.Response.TicketsResonse.Message;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.UitilsFun;
import com.example.ibraheem.mirva.util.D;
import com.example.ibraheem.mirva.util.L;
import com.example.ibraheem.mirva.util.V;
import com.example.ibraheem.mirva.util.helperFun;
import com.google.gson.JsonObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class Frag_Chat extends Fragment {

    final static int RESULT_LOAD_IMAGE = 1;
    public final String TAG = Frag_Chat.class.getSimpleName ();
    JFEditText text_chat_body;
    LinearLayout layout_icons;
    FWTextView text_message_icon, text_cam_icon, text_send_icon;
    ArrayList<com.example.ibraheem.mirva.Model.Response.TicketsResonse.Message> messageArrayList;
    MessageAdpter messageAdpter;
    RecyclerView recyclerView;
    Communicator comm;
    User user;
    int ticket_id;
    String content;
    ApiInterface apiInterface;
    V vd;
    ArrayList<Message> messages;
    D d;
    MultipartBody.Part requestImage;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        Bundle bundle = getArguments ();
        d = new D (getActivity ());

        if (bundle != null) {
            ticket_id = bundle.getInt (FragConst.TICKET_ID, -1);
            content = bundle.getString (FragConst.CONTENT, "");

            L.d (TAG, "bundle : contetnt ... " + content);
            L.d (TAG, "bundle :ticket_id  ... " + ticket_id);

        }
        if (ticket_id < 1) {
            d.ShowError ("Error");
        }

        user = helperFun.getUser (getContext ());
        apiInterface = ApiUtils.getAPIService (getActivity ());


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate (R.layout.fragment_fragment_chat, container, false);
        vd = new V (getActivity ());

        text_chat_body = (JFEditText) view.findViewById (R.id.text_chat_body);
        text_message_icon = (FWTextView) view.findViewById (R.id.text_message_icon);

        text_cam_icon = (FWTextView) view.findViewById (R.id.text_cam_icon);
        text_send_icon = (FWTextView) view.findViewById (R.id.text_send_icon);
        text_send_icon.setVisibility (View.GONE);
        layout_icons = (LinearLayout) view.findViewById (R.id.layout_icons);
        recyclerView = (RecyclerView) view.findViewById (R.id.message_recycler_view);
        messageArrayList = new ArrayList<> ();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager (getContext ());
        linearLayoutManager.setOrientation (LinearLayout.VERTICAL);
        recyclerView.setLayoutManager (linearLayoutManager);
        messageAdpter = new MessageAdpter (getActivity (), messageArrayList, recyclerView);
        recyclerView.setAdapter (messageAdpter);


        comm = (Communicator) getActivity ();
        comm.setTitleToolbar (getContext ().getResources ().getString (R.string.chat));

        if (getActivity ().getClass () == MainPageClient.class || getActivity ().getClass () == MainPageTechnical.class) {
            comm.selectVisibleToolbar (R.id.toolbar_clints);

        } else {
            comm.selectVisibleToolbar (R.id.manager_toolbar_fragments);


        }

        text_message_icon.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                Intent i = new Intent (Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult (i, RESULT_LOAD_IMAGE);

            }
        });

        text_chat_body.addTextChangedListener (new TextWatcher () {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!text_chat_body.getText ().toString ().trim ().isEmpty ()) {

                    text_message_icon.setVisibility (View.GONE);
                    text_cam_icon.setVisibility (View.GONE);

                    text_send_icon.setVisibility (View.VISIBLE);


                } else {

                    text_message_icon.setVisibility (View.VISIBLE);
                    text_cam_icon.setVisibility (View.VISIBLE);
                    text_send_icon.setVisibility (View.GONE);
                }

            }
        });


        vd.validateTextArea (R.id.text_chat_body);

        text_send_icon.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {


                if (vd.isValid () && UitilsFun.isNetworkAvailable (getActivity ())) {

                    Log.d (TAG, "onClick: ");
                    sendMessage (text_chat_body.getText ().toString ());
                }

            }
        });

        getData ();

        return view;

    }

    void getData() {

        d.ShowProgress (getString (R.string.loading));
        apiInterface = ApiUtils.getAPIService (getActivity ());
        apiInterface.getSpecificTicketData (user.getApiToken (), ticket_id).enqueue (new Callback<Chat> () {
            @Override
            public void onResponse(Call<Chat> call, Response<Chat> response) {
                if (response.isSuccessful ()) {
                    Chat body = response.body ();
                    messages = body.getData ();
                    messageAdpter.loadData (messages);


                }

                d.hide ();

            }

            @Override
            public void onFailure(Call<Chat> call, Throwable t) {
                d.hide ();
                d.ShowErrorInternetConnection ("");

            }
        });


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult (requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData ();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getActivity ().getContentResolver ().query (selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst ();
            int columnIndex = cursor.getColumnIndex (filePathColumn[0]);
            String picturePath = cursor.getString (columnIndex);
            sendImageMessage (picturePath);

        }
    }

    void sendMessage(final String content) {


        apiInterface = ApiUtils.getAPIService (getActivity ());
        apiInterface.sendMessage (user.getApiToken (), ticket_id, content).enqueue (new Callback<JsonObject> () {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                L.d (TAG, "ticket_id" + ticket_id);
                if (response.isSuccessful ()) {
                    com.example.ibraheem.mirva.Model.Response.TicketsResonse.Message message = new com.example.ibraheem.mirva.Model.Response.TicketsResonse.Message ();
                    message.setContent (content);
                    message.setDate (getString (R.string.now));
                    message.setIsUserSend ("1");
                    message.setImage (user.getImageToServer ());

                    messageAdpter.addMessage (message);
                    text_message_icon.setVisibility (View.VISIBLE);
                    text_cam_icon.setVisibility (View.VISIBLE);
                    text_send_icon.setVisibility (View.GONE);
                    text_chat_body.setText ("");
                    recyclerView.scrollToPosition (messageAdpter.getItemCount () - 1);
                    Log.d ("chat", "onResponse: body :" + response.body ());

                } else {

                    Log.d ("chat", "onResponse: errorBody :" + response.errorBody ().toString ());

                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d ("chat", "onResponse: isExecuted " + call.isExecuted ());

            }
        });
    }

    void sendImageMessage(final String imagePath) {

        d.ShowProgress (getString (R.string.sending));

        if (imagePath == null || imagePath.isEmpty ()) {
            return;
        }
        File file = new File (imagePath);
        if (file != null) {
            RequestBody requestFile =
                    RequestBody.create (MediaType.parse ("multipart/form-data"), file);
            requestImage = MultipartBody.Part.createFormData ("image", file.getName (), requestFile);
        }

        RequestBody ticket_idRequestBody = RequestBody.create (MediaType.parse ("multipart/form-data"), "" + ticket_id);

        apiInterface = ApiUtils.getAPIService (getActivity ());
        apiInterface.sendImageMessage (user.getApiToken (), ticket_idRequestBody, requestImage).enqueue (new Callback<JsonObject> () {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                L.d (TAG, "ticket_id" + ticket_id);
                if (response.isSuccessful ()) {

                    com.example.ibraheem.mirva.Model.Response.TicketsResonse.Message message = new com.example.ibraheem.mirva.Model.Response.TicketsResonse.Message ();
                    L.d (TAG, response.body ().get ("url").getAsString ());

                    message.setUrl (response.body ().get ("url").getAsString ());
                    message.setDate (new Date ().toString ());
                    message.setImage (user.getImageToServer ());
                    message.setIsUserSend ("1");


                    messageAdpter.addMessage (message);
                    text_message_icon.setVisibility (View.VISIBLE);
                    text_cam_icon.setVisibility (View.VISIBLE);
                    text_send_icon.setVisibility (View.GONE);
                    text_chat_body.setText ("");

                } else {

                    Log.d ("chat", "onResponse: errorBody :" + response.errorBody ());

                }
                d.hide ();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                d.hide ();
                d.ShowErrorInternetConnection ("");
            }
        });
    }

}




