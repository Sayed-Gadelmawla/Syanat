package com.example.ibraheem.mirva.Activities;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.example.ibraheem.mirva.Const.FragConst;
import com.example.ibraheem.mirva.CustomViews.FWTextView;
import com.example.ibraheem.mirva.CustomViews.JFButton;
import com.example.ibraheem.mirva.CustomViews.JFEditText;
import com.example.ibraheem.mirva.CustomViews.JFSpinnerAdapter;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.Fragments.Client.Frag_PaymentWay;
import com.example.ibraheem.mirva.Interfaces.Communicator;
import com.example.ibraheem.mirva.Model.Response.BankAccount;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.Model.Response.helperRes;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.util.helperFun;
import com.google.gson.JsonObject;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.ibraheem.mirva.R.string.subscriptions;

public class BankTransferActivity extends AppCompatActivity {
    private Context mContext;
    private JFTextView c_title_toolbar;
    private Toolbar toolbar_view_order;
    private JFTextView number_of_notifications;
    private FWTextView frag_back;
    private int REQUEST_ID;
    int SUBSCRIPTION_ID;
    String SUBSCRIPTION_PRICE, SUBSCRIPTION_NAME;
    public static final int PAYPAL_REQUEST_CODE = 123;
    private static final String PAYPAL_CLIENT_ID = "AYAq3lwOQxdHwmtHSaMrlF9m4BHdbbwQCaEszHuPHfb_xscGvEmVN_fO4xI2iEUjO5uRbTgsaah9wJkh";
    //Paypal Configuration Object
    private static final PayPalConfiguration config = new PayPalConfiguration()
            // Start with mock environment.  When ready, switch to sandbox (ENVIRONMENT_SANDBOX)
            // or live (ENVIRONMENT_PRODUCTION)
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
            .clientId(PAYPAL_CLIENT_ID);
    private String lang;
    private User user;
    LinearLayout bankLayout, visaLayout, paypalLayout, parentPaymentLayout;
    FWTextView pay_bank_type_icon, pay_visa_type_icon, pay_paypal_icon;
    JFTextView pay_bank_type_name, pay_visa_type_name, pay_paypal_type_name;
    ViewFlipper choose_payment_way;
    Communicator comm;
    InputMethodManager inputMethodManager;
    JFButton btn_pay_paypal, pay_visa;
    JFSpinnerAdapter adapterBankAccounts;
    ImageView bank_icon;
    JFTextView bank_name, bank_number, bank_ipn, bank_owner_name;
    JFEditText bank_transfer_name, bank_transfer_number;
    Spinner spinner_bankacounts;
    JFButton btn_bank_pay;
    int VISA_REQUEST_CODE = 101;
    ArrayList<BankAccount> bankAccounts;
    private String TAG="BankTransferActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_transfer);
        mContext = this;
        Intent arguments = getIntent();

        if (arguments != null) {
            SUBSCRIPTION_NAME = arguments.getStringExtra(FragConst.SUBSCRIPTION_NAME);
            SUBSCRIPTION_ID =arguments.getIntExtra(FragConst.SUBSCRIPTION_ID,-1);
            SUBSCRIPTION_PRICE = arguments.getStringExtra(FragConst.SUBSCRIPTION_PRICE);
        }


        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);

        findViewById(R.id.frag_notification).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), MyNotificationActivity.class));

            }
        });

        findViewById(R.id.view_order_notification_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getBaseContext(), MyNotificationActivity.class));

            }
        });

        toolbar_view_order = (Toolbar) findViewById(R.id.toolbar_view_order);
        c_title_toolbar = (JFTextView) toolbar_view_order.findViewById(R.id.c_title_toolbar);
        number_of_notifications = (JFTextView) toolbar_view_order.findViewById(R.id.number_of_notifications);

        c_title_toolbar.setText(getString(R.string.select_payment_way));

        frag_back = (FWTextView) toolbar_view_order.findViewById(R.id.frag_back);
        frag_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        lang = helperFun.getLanguage(this);
        user = helperFun.getUser(this);

        init_view();
        init_action();

        inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        adapterBankAccounts = new JFSpinnerAdapter(mContext, R.layout.item_spinner);
        spinner_bankacounts.setAdapter(adapterBankAccounts);

        getBankAccounts(new DataCallback() {
            @Override
            public void onSuccess(ArrayList<BankAccount> accounts) {
                bankAccounts = accounts;
                for (int i = 0; i < bankAccounts.size(); i++) {
                    adapterBankAccounts.add(bankAccounts.get(i).getBank());
                    Log.d(TAG, "onResponse: ");
                }

                adapterBankAccounts.notifyDataSetChanged();
            }
        });

    }

    private void init_action() {
        bankLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                bankLayout.setBackgroundColor(getResources().getColor(R.color.colorPrimarylight));
                pay_bank_type_name.setTextColor( getResources().getColor(R.color.colorWhite));
                pay_bank_type_icon.setTextColor( getResources().getColor(R.color.colorWhite));

                visaLayout.setBackground(ContextCompat.getDrawable(getBaseContext(),R.drawable.border_edit_text));
                pay_visa_type_icon.setTextColor( getResources().getColor(R.color.colorAccent));
                pay_visa_type_name.setTextColor( getResources().getColor(R.color.colorAccent));

                paypalLayout.setBackground(ContextCompat.getDrawable(getBaseContext(),R.drawable.border_edit_text));
                pay_paypal_type_name.setTextColor( getResources().getColor(R.color.colorAccent));
                pay_paypal_icon.setTextColor( getResources().getColor(R.color.colorAccent));
                choose_payment_way.setDisplayedChild(0);

            }
        });
        bankLayout.callOnClick();


        visaLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                visaLayout.setBackgroundColor( getResources().getColor(R.color.colorPrimarylight));
                pay_visa_type_icon.setTextColor( getResources().getColor(R.color.colorWhite));
                pay_visa_type_name.setTextColor( getResources().getColor(R.color.colorWhite));

                bankLayout.setBackground(ContextCompat.getDrawable(getBaseContext(),R.drawable.border_edit_text));
                pay_bank_type_icon.setTextColor( getResources().getColor(R.color.colorAccent));
                pay_bank_type_name.setTextColor( getResources().getColor(R.color.colorAccent));

                paypalLayout.setBackground(ContextCompat.getDrawable(getBaseContext(),R.drawable.border_edit_text));
                pay_paypal_type_name.setTextColor( getResources().getColor(R.color.colorAccent));
                pay_paypal_icon.setTextColor( getResources().getColor(R.color.colorAccent));
                choose_payment_way.setDisplayedChild(1);


            }
        });

        paypalLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                paypalLayout.setBackgroundColor( getResources().getColor(R.color.colorPrimarylight));
                pay_paypal_icon.setTextColor( getResources().getColor(R.color.colorWhite));
                pay_paypal_type_name.setTextColor( getResources().getColor(R.color.colorWhite));

                bankLayout.setBackground(ContextCompat.getDrawable(getBaseContext(),R.drawable.border_edit_text));
                pay_bank_type_icon.setTextColor( getResources().getColor(R.color.colorAccent));
                pay_bank_type_name.setTextColor( getResources().getColor(R.color.colorAccent));

                visaLayout.setBackground(ContextCompat.getDrawable(getBaseContext(),R.drawable.border_edit_text));
                pay_visa_type_icon.setTextColor( getResources().getColor(R.color.colorAccent));
                pay_visa_type_name.setTextColor( getResources().getColor(R.color.colorAccent));
                choose_payment_way.setDisplayedChild(2);

            }
        });
    }

    private void init_view() {
        choose_payment_way = (ViewFlipper)  findViewById(R.id.choose_payment_way);
        bankLayout = (LinearLayout)  findViewById(R.id.bankLayout);
        visaLayout = (LinearLayout)  findViewById(R.id.visaLayout);
        paypalLayout = (LinearLayout)  findViewById(R.id.paypalLayout);
        pay_bank_type_icon = (FWTextView)  findViewById(R.id.pay_bank_type_icon);
        pay_visa_type_icon = (FWTextView)  findViewById(R.id.pay_visa_type_icon);
        pay_paypal_icon = (FWTextView)  findViewById(R.id.pay_paypal_icon);
        pay_bank_type_name = (JFTextView)  findViewById(R.id.pay_bank_type_name);
        pay_visa_type_name = (JFTextView)  findViewById(R.id.pay_visa_type_name);

        pay_paypal_type_name = (JFTextView)  findViewById(R.id.pay_paypal_type_name);

        initViewBank();
        btn_pay_paypal = (JFButton)  findViewById(R.id.btn_pay_paypal);
        btn_pay_paypal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PayPayPal();
            }
        });
        pay_visa = (JFButton)  findViewById(R.id.pay_visa);
        pay_visa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //PayByVisa (SUBSCRIPTION_PRICE, SUBSCRIPTION_NAME, SUBSCRIPTION_ID);
            }
        });

    }

    private void initViewBank() {
        bank_owner_name = (JFTextView) findViewById(R.id.bank_owner_name);
        bank_ipn = (JFTextView) findViewById(R.id.bank_ipn);
        bank_number = (JFTextView) findViewById(R.id.bank_number);
        bank_name = (JFTextView) findViewById(R.id.bank_name);
        bank_icon = (ImageView) findViewById(R.id.bank_icon);
        btn_bank_pay = (JFButton) findViewById(R.id.btn_bank_pay);

        bank_transfer_name = (JFEditText) findViewById(R.id.bank_transfer_name);
        bank_transfer_number = (JFEditText) findViewById(R.id.bank_transfer_number);

        btn_bank_pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (spinner_bankacounts.getSelectedItemPosition() >= 1) {
                    int bank_Id = bankAccounts.get(spinner_bankacounts.getSelectedItemPosition() - 1).getId();
                    String transfer_name = bank_transfer_name.getText().toString();
                    String transfer_number = bank_transfer_number.getText().toString();

                    if (bank_Id > 0 & !TextUtils.isEmpty(transfer_name) && !TextUtils.isEmpty(transfer_number)) {
                        PayByBank(bank_Id, transfer_name, transfer_number, SUBSCRIPTION_ID);
                    }
                }else
                {
                    Toast.makeText(BankTransferActivity.this,getString(R.string.please_select_bank),Toast.LENGTH_LONG).show();
                }
            }
        });
        spinner_bankacounts = (Spinner) findViewById(R.id.spinner_bankacounts);
        spinner_bankacounts.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position != 0) {
                    BankAccount bankAccount = bankAccounts.get(position - 1);
                    bank_owner_name.setText(bankAccount.getOwner());
                    bank_ipn.setText(bankAccount.getIbanNumber());
                    bank_number.setText(bankAccount.getAccountNumber());
                    bank_name.setText(bankAccount.getBank());
                    Picasso.with(mContext)
                            .load(bankAccount.getImage())
                            .placeholder(R.drawable.avatar)
                            .error(R.drawable.avatar)
                            .into(bank_icon);

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinner_bankacounts.setSelection(0);
        spinner_bankacounts.setAdapter(adapterBankAccounts);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK && data != null) {
            //If the result is from paypal
            //If the result is OK i.e. user has not canceled the payment
            if (requestCode == PAYPAL_REQUEST_CODE) {
                //Getting the payment confirmation
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                //if confirmation is not null
                if (confirm != null) {
                    try {
                        //Getting the payment details
                        String paymentDetails = confirm.toJSONObject().toString(4);
                        Log.i("paymentExample", paymentDetails);

                        JSONObject jsonDetails = new JSONObject(paymentDetails);
                        String id = jsonDetails.getJSONObject("response").getString("id");
                        String state = jsonDetails.getJSONObject("response").getString("state");
                        ApiInterface apiInterface = ApiUtils.getAPIService(this);
                        apiInterface.payPayPal(user.getApiToken(), id, SUBSCRIPTION_ID).enqueue(new Callback<JsonObject>() {
                            @Override
                            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                if (response.isSuccessful()) {

                                    helperRes.showMessageSuccess(mContext, response.body(), null);
                                } else {
                                    helperRes.showMessageError(mContext, response.errorBody());


                                }
                            }

                            @Override
                            public void onFailure(Call<JsonObject> call, Throwable t) {

                            }
                        });
                    } catch (JSONException e) {
                        Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }

            } else if (requestCode == VISA_REQUEST_CODE) {

                SharedPreferences shared_prefs =  getSharedPreferences("myapp_shared", Context.MODE_PRIVATE);
                String pt_response_code = shared_prefs.getString("pt_response_code", "");
                String pt_transaction_id = shared_prefs.getString("pt_transaction_id", "");
         /*   Toast.makeText (this, "PayTabs Response Code : " + pt_response_code,
                        Toast.LENGTH_LONG).show ();
                Toast.makeText (this, "Paytabs transaction ID after payment : " +
                        pt_transaction_id, Toast.LENGTH_LONG).show ();*/

            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            Log.i("paymentExample", "The user canceled.");
        } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
            Log.i("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
        }


    }

    public interface DataCallback{
        public void onSuccess(ArrayList<BankAccount> bankAccounts);
    }

    void getBankAccounts(final DataCallback callback) {
        adapterBankAccounts.add( getString(R.string.select_bank));
        ApiInterface apiInterface = ApiUtils.getAPIService(this);
        apiInterface.getBankAccount(lang, user.getApiToken()).enqueue(new Callback<ArrayList<BankAccount>>() {
            private static final String TAG = "Frag_PaymentWay";

            @Override
            public void onResponse(Call<ArrayList<BankAccount>> call, Response<ArrayList<BankAccount>> response) {

                if (response.isSuccessful()) {
                    bankAccounts = response.body();
                    callback.onSuccess(bankAccounts);
                } else {

                    helperRes.showMessageError(mContext, response
                            .errorBody());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<BankAccount>> call, Throwable t) {

                Log.d(TAG, "onFailure: getMessage " + t.getMessage());

            }
        });


    }

    void PayByBank(int bank_transfer_id, String transfer_name, String transfer_number, int supscription_id) {
        ApiInterface apiInterface = ApiUtils.getAPIService(this);
        apiInterface.payByBankTransfer(user.getApiToken(), bank_transfer_id, transfer_name, transfer_number, supscription_id).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                if (response.isSuccessful()) {
                    helperRes.showMessageSuccess(mContext, response.body(), null);
                } else {
                    Log.d(TAG,response.errorBody().toString());
                    //helperRes.showMessageError(mContext, response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });

    }

    void PayPayPal() {


        PayPalPayment payment = new PayPalPayment(new BigDecimal(String.valueOf(SUBSCRIPTION_PRICE)), "USD", "Simplified Coding Fee",
                PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent = new Intent(this, PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);
        startActivityForResult(intent, PAYPAL_REQUEST_CODE);

    }
}
