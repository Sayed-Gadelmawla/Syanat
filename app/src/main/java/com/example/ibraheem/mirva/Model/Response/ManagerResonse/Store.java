package com.example.ibraheem.mirva.Model.Response.ManagerResonse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by ibraheem on 03/07/2017.
 */

public class Store {

    @SerializedName("status_types")
    @Expose
    private StoreItem_StatusTypes statusTypes;
    @SerializedName("data")
    @Expose
    private ArrayList<StoreItem> data = null;

    public StoreItem_StatusTypes getStatusTypes() {
        return statusTypes;
    }

    public void setStatusTypes(StoreItem_StatusTypes statusTypes) {
        this.statusTypes = statusTypes;
    }

    public ArrayList<StoreItem> getData() {
        return data;
    }

    public void setData(ArrayList<StoreItem> data) {
        this.data = data;
    }

}