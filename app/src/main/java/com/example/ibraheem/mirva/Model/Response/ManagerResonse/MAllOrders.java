package com.example.ibraheem.mirva.Model.Response.ManagerResonse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by ibraheem on 04/07/2017.
 */

///
public class MAllOrders {
    @SerializedName("status")
    @Expose
    private OrderStatus status;
    @SerializedName("data")
    @Expose
    private ArrayList<MOrder> data = null;

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public ArrayList<MOrder> getData() {
        return data;
    }

    public void setData(ArrayList<MOrder> data) {
        this.data = data;
    }
}