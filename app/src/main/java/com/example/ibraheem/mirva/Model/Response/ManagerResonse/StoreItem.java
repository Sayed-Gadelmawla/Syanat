package com.example.ibraheem.mirva.Model.Response.ManagerResonse;

import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ibraheem on 27/06/2017.
 */

public class StoreItem {

    @SerializedName("service")
    @Expose
    private String service;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("requirement")
    @Expose
    private String requirement;
    @SerializedName("quantity")
    @Expose
    private int quantity;
    @SerializedName("status")
    @Expose
    private Integer status;

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getImage() {
        return ApiUtils.BASE_URL + image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getRequirement() {
        return requirement;
    }

    public void setRequirement(String requirement) {
        this.requirement = requirement;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }


}