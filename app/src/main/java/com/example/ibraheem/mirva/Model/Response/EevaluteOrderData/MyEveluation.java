package com.example.ibraheem.mirva.Model.Response.EevaluteOrderData;

import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ibraheem on 14/05/2017.
 */

public class MyEveluation {

    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("evaluation")
    @Expose
    private String evaluation;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("request_id")
    @Expose
    private String request_id;

    public String getImage() {
        return ApiUtils.BASE_URL + image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(String evaluation) {
        this.evaluation = evaluation;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRequest_id() {
        return request_id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }
}
