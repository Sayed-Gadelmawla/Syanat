package com.example.ibraheem.mirva.Activities;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Chronometer;
import android.widget.EditText;

import com.example.ibraheem.mirva.Const.FragConst;
import com.example.ibraheem.mirva.CustomViews.FWTextView;
import com.example.ibraheem.mirva.CustomViews.JFButton;
import com.example.ibraheem.mirva.CustomViews.JFEditText;
import com.example.ibraheem.mirva.CustomViews.JFTextView;
import com.example.ibraheem.mirva.Fragments.Client.ActivityNewOrder;
import com.example.ibraheem.mirva.Model.Response.User;
import com.example.ibraheem.mirva.Model.Response.helperRes;
import com.example.ibraheem.mirva.R;
import com.example.ibraheem.mirva.Rest.ApiInterface;
import com.example.ibraheem.mirva.Rest.ApiUtils;
import com.example.ibraheem.mirva.UitilsFun;
import com.example.ibraheem.mirva.util.D;
import com.example.ibraheem.mirva.util.L;
import com.example.ibraheem.mirva.util.SharedPrefs;
import com.example.ibraheem.mirva.util.V;
import com.example.ibraheem.mirva.util.helperFun;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.Arrays;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivateAccountActivity extends AppCompatActivity {
    final static String TAG = "Activate_Account : ";
    JFEditText et_num_1, et_num_2, et_num_3, et_num_4, et_num_5;
    FWTextView tv_back;
    JFButton btn_active;
    JFTextView resend_activiation;
    String userMobileNumber;
    SweetAlertDialog sweetAlertDialog;
    D d;
    int Service_ID ;
    V vd;
    private ApiInterface mAPIService;
    private String lang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_activate_account);
        init_view ();
        userMobileNumber = getIntent ().getStringExtra ("mobile");
        Service_ID = getIntent().getIntExtra(FragConst.SERVICE_ID , 0);

    }

    /**
     *
     * get  reference  view   from xml
     */
    private void init_view() {
        d = new D (this);
        vd = new V (this);

//        et_num_1 = (JFEditText) findViewById (R.id.num_1);
        tv_back = (FWTextView) findViewById (R.id.tv_back);
        btn_active = (JFButton) findViewById (R.id.btn_bank_pay);
        resend_activiation = (JFTextView) findViewById (R.id.resend_activiation);

//        Log.d (TAG, "onCreate: hello calss");

        et_num_1 = (JFEditText) findViewById (R.id.num_1);
        et_num_2 = (JFEditText) findViewById (R.id.num_2);
        et_num_3 = (JFEditText) findViewById (R.id.num_3);
        et_num_4 = (JFEditText) findViewById (R.id.num_4);
        et_num_5 = (JFEditText) findViewById (R.id.num_5);

        (et_num_1).requestFocus ();
        (et_num_1).setCursorVisible (true);

        et_num_5.addTextChangedListener (new TextWatcher () {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {

                if (et_num_5.getText ().toString ().length () == 0) {

                    (et_num_5).clearFocus ();
                    (et_num_4).requestFocus ();
                    (et_num_4).setCursorVisible (true);

                }

            }


        });

        et_num_4.addTextChangedListener (new TextWatcher () {
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {


            }

            @Override
            public void afterTextChanged(Editable s) {
                if ((et_num_4).getText ().toString ().length () == 0) {
                    (et_num_4).clearFocus ();
                    (et_num_3).requestFocus ();
                    ((et_num_3)).setCursorVisible (true);

                } else {
                    (et_num_4).clearFocus ();
                    (et_num_5).requestFocus ();
                    ((et_num_5)).setCursorVisible (true);
                }
            }


        });

        (et_num_3).addTextChangedListener (new TextWatcher () {
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {


            }

            @Override
            public void afterTextChanged(Editable s) {
                (et_num_3).clearFocus ();
                if (((EditText) findViewById (R.id.num_3)).getText ().toString ().length () == 0) {

                    (et_num_2).requestFocus ();
                    (et_num_2).setCursorVisible (true);

                } else {
                    (et_num_4).requestFocus ();
                    (et_num_4).setCursorVisible (true);


                }
            }


        });

        (et_num_2).addTextChangedListener (new TextWatcher () {
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {


            }

            @Override
            public void afterTextChanged(Editable s) {
                if ((et_num_2).getText ().toString ().length () == 0) {
                    (et_num_2).clearFocus ();
                    (et_num_1).requestFocus ();
                    (et_num_1).setCursorVisible (true);

                } else {

                    (et_num_2).clearFocus ();
                    (et_num_3).requestFocus ();
                    (et_num_3).setCursorVisible (true);
                }
            }


        });

        (et_num_1).addTextChangedListener (new TextWatcher () {
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {


            }

            @Override
            public void afterTextChanged(Editable s) {
                if ((et_num_1).getText ().toString ().length () == 0) {


                } else {

                    (et_num_1).clearFocus ();
                    (et_num_2).requestFocus ();
                    (et_num_2).setCursorVisible (true);
                }
            }


        });


        et_num_2.setOnKeyListener (new View.OnKeyListener () {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_DEL || keyCode == KeyEvent.KEYCODE_BACK ) {
                    et_num_2.setText (null);
                    (et_num_2).clearFocus ();
                    (et_num_1).requestFocus ();
                    (et_num_1).setCursorVisible (true);

                }
                return false;
            }
        });
        et_num_3.setOnKeyListener (new View.OnKeyListener () {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_DEL|| keyCode == KeyEvent.KEYCODE_BACK ) {
                    et_num_3.setText (null);
                    (et_num_3).clearFocus ();
                    (et_num_2).requestFocus ();
                    (et_num_2).setCursorVisible (true);

                }
                return false;
            }
        });

        et_num_4.setOnKeyListener (new View.OnKeyListener () {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_DEL|| keyCode == KeyEvent.KEYCODE_BACK ) {
                    et_num_4.setText (null);
                    (et_num_4).clearFocus ();
                    (et_num_3).requestFocus ();
                    (et_num_3).setCursorVisible (true);
                }
                return false;
            }
        });

        et_num_5.setOnKeyListener (new View.OnKeyListener () {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_DEL|| keyCode == KeyEvent.KEYCODE_BACK ) {
                    et_num_5.setText (null);
                    (et_num_5).clearFocus ();
                    (et_num_4).requestFocus ();
                    (et_num_4).setCursorVisible (true);

                }
                return false;


            }
        });

        vd.validateBoxNumber (R.id.num_1);
        vd.validateBoxNumber (R.id.num_2);
        vd.validateBoxNumber (R.id.num_3);
        vd.validateBoxNumber (R.id.num_4);
        vd.validateBoxNumber (R.id.num_5);

        tv_back.setOnClickListener (new View.OnClickListener ()

        {
            @Override
            public void onClick(View v) {
                ActivateAccountActivity.super.onBackPressed();

//                startActivity (new Intent (getBaseContext (), LoginActivity.class));
                finish ();
            }
        });
        btn_active.setOnClickListener (new View.OnClickListener ()

        {
            @Override
            public void onClick(View v) {

                if (vd.isValid () & UitilsFun.isNetworkAvailable (ActivateAccountActivity.this))
                {
                    activateAccount ();

                }

            }
        });

        resend_activiation.setOnClickListener (new View.OnClickListener ()
        {
            @Override
            public void onClick(View v) {
                resendActivationCode ();
            }
        });
    }


    String getActivationCode() {
        String str = et_num_1.getText ().toString () +
                et_num_2.getText ().toString () +
                et_num_3.getText ().toString () +
                et_num_4.getText ().toString () +
                et_num_5.getText ().toString ();

        Log.d (TAG, "getActivationCode: " + str);
        return str;

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_FORWARD_DEL || keyCode == KeyEvent.KEYCODE_DEL) {
            Log.d (TAG, "onKeyDown: " + "hello ");
            if (et_num_2.hasWindowFocus ()) {
                (et_num_1).requestFocus ();
                (et_num_1).setCursorVisible (true);


            } else if (et_num_3.hasWindowFocus ()) {
                (et_num_2).requestFocus ();
                (et_num_2).setCursorVisible (true);

            } else if (et_num_4.hasWindowFocus ()) {
                (et_num_3).requestFocus ();
                (et_num_3).setCursorVisible (true);

            } else if (et_num_5.hasWindowFocus ()) {
                (et_num_4).requestFocus ();
                (et_num_4).setCursorVisible (true);

            }
            return true;
        }

        return super.onKeyDown (keyCode, event);
    }


    void resendActivationCode() {

        lang = helperFun.getLanguage (this);
        mAPIService = ApiUtils.getAPIService (getBaseContext ());
        //sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        d.ShowProgress(getResources().getString(R.string.resend_code));
        mAPIService.resendActiviation (lang, userMobileNumber).enqueue (new Callback<JsonObject> () {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                d.hide ();

                if (response.isSuccessful ()) {
                    //helperRes.showMessageSuccess (ActivateAccountActivity.this, , ActivateAccountActivity.class);
                    d.ShowSuccess(getResources().getString(R.string.activate_msg) , getResources().getString(R.string.resend_code_message));
                } else {

                    helperRes.showMessageError (ActivateAccountActivity.this, response.errorBody ());
                /*    /// Error error = helperRes.getErrorMessage (response.errorBody ());
                    String error = null;
                    try {
                        error = response.errorBody ().string ();
                    } catch (IOException e) {
                        Log.d (TAG, "onResponse: IOException ee " + e.getMessage ());
                    }

                    new SweetAlertDialog (ActivateAccountActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText (getString (R.string.error))
                            .setContentText (error)
                            .show ()
                    ;
*/
                }


            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                d.ShowErrorInternetConnection ("");

            }
        });
    }

    void activateAccount() {

        lang = helperFun.getLanguage (this);
        d.ShowProgress ("");
        mAPIService = ApiUtils.getAPIService (getBaseContext ());

        Log.d (TAG, "onResponse: getMobile " + userMobileNumber);

        mAPIService.activiation (lang,getActivationCode(), userMobileNumber).enqueue (new Callback<User> () {

            @Override
            public void onResponse(Call<User> call, Response<User> response) {

                d.hide ();
                if (response.isSuccessful ()) {
                    User user = response.body ();
                    Gson gson = new Gson ();
                    L.d (TAG, "getApiToken " + user.getApiToken ());
                    SharedPrefs.replace (getApplicationContext (), SharedPrefs.USER, gson.toJson (user));
                    Intent intent;
                    if (Service_ID == 0){

                        intent = new Intent(getApplicationContext(), AllSubscriptionsActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    } else {

                        intent = new Intent(getApplicationContext(), ActivityNewOrder.class);
                        intent.putExtra(FragConst.SERVICE_ID, Service_ID);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    }
                    getApplicationContext().startActivity(intent);
                    finish();
                    //d.showMessageSuccess (user.getMessage (), new Intent (ActivateAccountActivity.this, EditProfileActivity.class));

                    /// d.showMessageSuccess (success.getMessage (), new Intent (ActivateAccountActivity.this, LoginActivity.class));


                } else {

                    d.ShowError (getString (R.string.err_activation));
                    //   helperRes.showMessageError (getBaseContext (), response.errorBody ());

                    //   Log.d (TAG, "onResponse: " + response.toString ());


                }


            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                d.hide ();
                call.cancel ();
                d.ShowErrorInternetConnection ("");
            }
        });

    }
}
