package com.example.ibraheem.mirva.Model.Response.ErrorResponse;

import android.app.Activity;
import android.util.Log;
import android.widget.EditText;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by ibraheem on 08/07/2017.
 */

public class ErrorType {

    EditText editText;
    @SerializedName("password")
    @Expose
    private ArrayList<String> password = null;
    @SerializedName("email")
    @Expose
    private ArrayList<String> email = null;

    public ArrayList<String> getPassword() {
        return password;

    }

    public void setPassword(EditText editTextEmail) {

        if (password != null && password.get (0) != null) {
            (editTextEmail).setError (password.get (0));
            Log.d ("ErrorType", "setEmail: " + password.get (0));
        }

    }

    public ArrayList<String> getEmail() {
        return email;
    }

    public void setEmail(EditText editTextEmail) {
        if (email != null && email.get (0) != null) {
            Log.d ("ErrorType", "setEmail: ");
            (editTextEmail).setError (email.get (0));
        }

    }

    @Override
    public String toString() {
        return "ErrorType{" +
                "password=" + password +
                ", editText=" + editText +
                ", email=" + email +
                '}';
    }
}
